<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('seller_id');
            $table->text('shipping_address')->nullable();
            $table->string('delivery_status', 20)->default('pending');
            $table->string('payment_type', 20)->nullable();
            $table->string('payment_status', 20)->default('unpaid');
            $table->text('payment_details')->nullable();
            $table->double('grand_total', 20, 2)->nullable();
            $table->double('discount', 10, 2)->default('0');
            $table->string('currency_code', 10);
            $table->text('code')->nullable();
            $table->date('date');
            $table->tinyInteger('viewed')->default('0');
            $table->tinyInteger('delivery_viewed')->default('0');
            $table->tinyInteger('payment_status_viewed')->default('0');
            $table->tinyInteger('commision_calculated')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
