<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'card_id',
        'card_img',
        'bank_name',
        'bank_acc_name',
        'bank_acc_no',
        'admin_to_pay',
    ];
    public function Seller()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function seller_withdraw_request()
    {
        return $this->hasMany("App\Models\WithdrawRequest", "user_id", "user_id");
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}