@php

$user_id = Session::get('user')->id;
$user = Session::get('user');

@endphp

@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
        <div class="col-sm-9">
            @if ($user->user_type == 'sale')
            <div class="centent_card_1r3 tab-content">
                <div class="col-sm-4">
                    <div class="bg-grad-3 text-white rounded-lg mb-4 overflow-hidden">
                        <div class="px-3 pt-3">
                            <div class="h3 fw-700">
                                @php                   
                                    $product = App\Models\Product::where('user_id', $user_id)->get();
                                    echo count($product);
                                @endphp
                            </div>
                            <div class="opacity-50">
                                <h4><a href="{{ route('sale.product.index') }}" style="color:white;">Products</a></h4>
                            </div>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                            <path fill="rgba(255,255,255,0.3)" fill-opacity="1"
                                d="M0,192L26.7,192C53.3,192,107,192,160,202.7C213.3,213,267,235,320,218.7C373.3,203,427,149,480,117.3C533.3,85,587,75,640,90.7C693.3,107,747,149,800,149.3C853.3,149,907,107,960,112C1013.3,117,1067,171,1120,202.7C1173.3,235,1227,245,1280,213.3C1333.3,181,1387,107,1413,69.3L1440,32L1440,320L1413.3,320C1386.7,320,1333,320,1280,320C1226.7,320,1173,320,1120,320C1066.7,320,1013,320,960,320C906.7,320,853,320,800,320C746.7,320,693,320,640,320C586.7,320,533,320,480,320C426.7,320,373,320,320,320C266.7,320,213,320,160,320C106.7,320,53,320,27,320L0,320Z">
                            </path>
                        </svg>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="bg-grad-1 text-white rounded-lg mb-4 overflow-hidden">
                    <div class="px-3 pt-3">
                        <div class="h3 fw-700">
                            @php
                                $Sorder = App\Models\Order::where('seller_id', App\Models\Seller::where('user_id', $user_id)->first()->id)
                                    ->where('payment_status', 'paid')
                                    ->get();
                                echo count($Sorder);
                            @endphp

                        </div>
                        <div class="opacity-50">
                            <h4><a href="{{ route('order.all_order') }}" style="color:white;">Total sale</a></h4>
                        </div>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                        <path fill="rgba(255,255,255,0.3)" fill-opacity="1"
                            d="M0,192L30,208C60,224,120,256,180,245.3C240,235,300,181,360,144C420,107,480,85,540,96C600,107,660,149,720,154.7C780,160,840,128,900,117.3C960,107,1020,117,1080,112C1140,107,1200,85,1260,74.7C1320,64,1380,64,1410,64L1440,64L1440,320L1410,320C1380,320,1320,320,1260,320C1200,320,1140,320,1080,320C1020,320,960,320,900,320C840,320,780,320,720,320C660,320,600,320,540,320C480,320,420,320,360,320C300,320,240,320,180,320C120,320,60,320,30,320L0,320Z">
                        </path>
                    </svg>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bg-grad-3 text-white rounded-lg mb-4 overflow-hidden">
                    <div class="px-3 pt-3">
                        <div class="h3 fw-700">
                            @php
                                $Sorder = App\Models\Order::where('seller_id', App\Models\Seller::where('user_id', $user_id)->first()->id)
                                    ->where('delivery_status', 'delivered')
                                    ->get();
                                echo count($Sorder);
                            @endphp
                        </div>
                        <div class="opacity-50">
                            <h4><a href="{{ route('order.all_order') }}" style="color:white;"> Successful orders</a></h4>
                        </div>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                        <path fill="rgba(255,255,255,0.3)" fill-opacity="1"
                            d="M0,192L26.7,192C53.3,192,107,192,160,202.7C213.3,213,267,235,320,218.7C373.3,203,427,149,480,117.3C533.3,85,587,75,640,90.7C693.3,107,747,149,800,149.3C853.3,149,907,107,960,112C1013.3,117,1067,171,1120,202.7C1173.3,235,1227,245,1280,213.3C1333.3,181,1387,107,1413,69.3L1440,32L1440,320L1413.3,320C1386.7,320,1333,320,1280,320C1226.7,320,1173,320,1120,320C1066.7,320,1013,320,960,320C906.7,320,853,320,800,320C746.7,320,693,320,640,320C586.7,320,533,320,480,320C426.7,320,373,320,320,320C266.7,320,213,320,160,320C106.7,320,53,320,27,320L0,320Z">
                        </path>
                    </svg>
                </div>
            </div>
            <div class="col-sm-7">
                <p class="title_header"></p>
                <div class="centent_card_1r clearfix">
                    <div class="centent_card_1r3 tab-content">
                        <p style="font-size:19px;"><B>Orders </B> </p>
                        <hr>
                        <div class="card-content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <td>Total orders:</td>
                                        <td>
                                            @php
                                                $or = App\Models\order::where('seller_id', App\Models\Seller::where('user_id', $user_id)->first()->id)->get();
                                                
                                                echo count($or);
                                            @endphp</td>
                                    </tr>
                                    <tr>
                                        <td>Pending order:</td>
                                        <td>
                                         @php
                                            $Sorder = App\Models\Order::where('seller_id', App\Models\Seller::where('user_id', $user_id)->first()->id)
                                                ->where('delivery_status', 'pending')
                                                ->get();
                                            echo count($Sorder);
                                        @endphp
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Successful order:</td>
                                        <td> @php
                                            
                                            $Sorder = App\Models\Order::where('seller_id', App\Models\Seller::where('user_id', $user_id)->first()->id)
                                                ->where('delivery_status', 'delivered')
                                                ->get();
                                            echo count($Sorder);
                                        @endphp</td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            @endif

            <!--close order-->

            <div class="col-sm-5">
                <br><br>
                <p class="title_header"></p>
                <div class="centent_card_1r clearfix">
                    <div class="centent_card_1r3 tab-content">
                        <p></p>
                        <div class="card-content">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-2">
                                <img loading="lazy" src="https://webr.la/public/assets/img/verified.png" alt="" width="190">
                            </div>
                            <div class="col-sm-2"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <p class="title_header"></p>
                <div class="centent_card_1r clearfix">
                    <div class="centent_card_1r3 tab-content">
                        <p style="font-size:19px;"><B>Product </B> </p>
                        <hr>
                        <div class="card-content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Category</th>
                                        <th>Total Products</th>
                                    </tr>
                                    @foreach ($pd as $row)
                                        <tr>
                                            <td>{{ $row->Category->name }}</td>
                                            <td>{{ $row->amount }} items</td>
                                        </tr>
                                    @endforeach

                                </table>
                            </div>
                            <div class="center_product_1r4 clearfix">
                                <div class="col-sm-6">
                                    <div class="center_product_1r4l clearfix">
                                        {{ $pd->links() }}
                                    </div>
                                </div>
                            </div>
                            <div class="addproduct" style="text-align:center;">
                                <a href="{{ route('sale.product.create') }}" class="btn btn-success">+Add Products</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-5"> <br><br>
                <p class="title_header"></p>
                <div class="centent_card_1r clearfix">
                    <div class="centent_card_1r3 tab-content">
                        <h3 style="text-align:center;size:14px;"><B>Shop </B></h3>
                        <div class="card-content">
                            <p style="text-align:center;">Manage & organize your shop</p>
                            <br>
                            <div class="addproduct" style="text-align:center;">
                                <a href="{{ route('shop.setting') }}" class="btn btn-success"> Go to setting</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5"> <br><br>
                <p class="title_header"></p>
                <div class="centent_card_1r clearfix">
                    <div class="centent_card_1r3 tab-content">
                        <h3 style="text-align:center;size:14px;"><B>Payment </B></h3>
                        <div class="card-content">
                            <p style="text-align:center;">Configure your payment method</p>
                            <br>
                            <div class="addproduct" style="text-align:center;">
                                <a href="{{ route('payment.history') }}" class="btn btn-success"> Payment history</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
