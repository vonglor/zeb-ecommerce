@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
    <div class="content">
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-xl font-medium mr-auto">
            Order Details
        </h2>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Vertical Form -->
            <form action="{{route('product.store')}}">
            <div class="intro-y box">
                <div id="vertical-form" class="p-5">
                    <div class="preview">
                        <div>
                            <label for="vertical-form-1" class="form-label text-lg">Order Name: David</label>
                                <p class="text-base">
                                02077412886
                                khamhoung village saythany district, Vientiane Province, 01080
                                Lao People's Democratic Republic
                                </p>
                        </div>
                            <!-- BEGIN:  -->
                        <div>
                        <table class="table table-report -mt-2">
                        <thead>
                            <tr>
                                <th class="whitespace-nowrap text-base">Photo #</th>
                                <th class="whitespace-nowrap text-base">DESCRIPTION</th>
                            </tr>
                            
                        </thead>
                        <tbody>
                            <tr>
                                <td><img alt="Icewall Tailwind HTML Admin Template" class="tooltip" src="{{asset('backend/dist/images/preview-2.jpg')}}" title="Image Product"></td>
                                <td class="text-base">defined.  not all acts of that description are actually illegal  CHARACTER implies a group marked by distinctive likenesses peculiar to the type.  research on the subject so far has been of an elementary character </td>
                            </tr>
                        </tbody>
                        </table>
                        </div>
                        <!-- END:  -->
                    </div>
                </div>
            </div>
            </form>
        </div>

            <!-- BEGIN: Shipping Configuration -->

        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Vertical Form -->
            <form action="">
                <div class="intro-y box">
                <div id="vertical-form" class="p-5">
                    <div class="preview">
                            <div class="mt-3">
                                <label for="vertical-form-1" class="form-label text-base">Delivery Status</label>
                                <select class="form-select mt-2 sm:mr-2" aria-label="Default select example">
                                <option class="text-base">Pedding</option>
                                <option class="text-base">Confirmed</option>
                                <option class="text-base">Completed</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y box">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    
                <table class="table table-report -mt-2">
                        <thead>
                            <tr>
                                <th class="whitespace-nowrap text-base">Order #</th>
                                <th class="whitespace-nowrap text-base">20210910-16190927</th>
                            </tr>
                            <tr>
                                <th class="whitespace-nowrap text-base">Order Status</th>
                                <th class="whitespace-nowrap text-base">  <a class="btn btn-success" href="#">Pedding</a></th>
                            </tr>
                            <tr>
                                <th class="whitespace-nowrap text-base">Order Date</th>
                                <th class="whitespace-nowrap text-base">	10-09-2021 04:19 PM</th>
                            </tr>
                            <tr>
                                <th class="whitespace-nowrap text-base">Total amount</th>
                                <th class="whitespace-nowrap text-base">3,180,000₭</th>
                            </tr>
                            <tr>
                                <th class="whitespace-nowrap text-base">Payment method</th>
                                <th class="whitespace-nowrap text-base">VISA</th>
                            </tr>
                        </thead>
                </table>
                </div>
                <div id="vertical-form" class="p-5">
                    <div class="preview">
                            <div class="mt-3">
                                <table class="table table-report -mt-2">
                            <thead>
                                <tr>
                                    <th class="whitespace-nowrap text-base">Sub Total :</th>
                                    <th class="whitespace-nowrap text-base">3,180,000₭</th>
                                </tr>
                                <tr>
                                    <th class="whitespace-nowrap text-base">tax:</th>
                                    <th class="whitespace-nowrap text-base"> 0₭</th>
                                </tr>
                                <tr>
                                    <th class="whitespace-nowrap text-base">Shipping :</th>
                                    <th class="whitespace-nowrap text-base">0₭</th>
                                </tr>
                                <tr class="text-lg">
                                    <th class="whitespace-nowrap text-base">TOTAL :</th>
                                    <th class="whitespace-nowrap text-base">3,180,000₭</th>
                                </tr>
                            </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- END: Content -->
    
@endsection
