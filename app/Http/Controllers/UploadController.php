<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use move;
class UploadController extends Controller
{
    public function upload(Request $request)
    {
        $img = $request->img_name;
        $name_img = hexdec(uniqid());
        $img_ext = $request->img_extension;
        $img_new_name = $name_img.'.'.$img_ext;
        $upload_path = 'uploads/images/';
        $new_name = $upload_path.$img_new_name;

        //move_uploaded_file($img_new_name, $upload_path);
        $img->move($upload_path, $img_new_name);
        //$img->Storage::move($upload_path, $img_new_name);

        echo $new_name;
        // echo'<img src="'.$new_name.'" alt="" style="max-height: 80px; max-width: 80px;">';
    }
}
