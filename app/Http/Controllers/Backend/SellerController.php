<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Seller;
use App\Models\User;
use App\Models\Whitdraw_request;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seller = User::where('user_type', 'sale')->get();

        return view('backend.sellers.index', compact('seller'));
    }
    public function sellerCommission()
    {
        return view('backend.sellers.commission');
    }
    public function sellerPayout()
    {
        return view('backend.sellers.payout');
    }
    public function sellerWithdraw()
    {
        $sellerwithdraw = Whitdraw_request::all();
        return view('backend.sellers.payoutrequest', compact('sellerwithdraw'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seller = User::find($id);
        return view('backend.sellers.edit', compact('seller'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user              = User::find($id);
        $user->name        = $request->input('name');
        $user->surname     = $request->input('surname');
        $user->phone       = $request->input('phone');
        $user->email       = $request->input('email');
        $user->postal_code = $request->input('postal_code');

        $user->save();
        $seller              = Seller::find($request->sellerId);
        $seller->bank_name   = $request->input('bank_name');
        $seller->bank_acc_no = $request->input('bank_acc_no');
        $seller->save();

        if ($seller->save()) {
            $notification = array(
                'message'    => 'Update successfully.',
                'alert-type' => 'success',
            );
            return redirect()->route('seller')->with($notification);
        } else {
            $notification = array(
                'message'    => 'Update warning!!.',
                'alert-type' => 'warning',
            );
            return redirect()->route('seller')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}