<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shop;
use Str;

class ShopController extends Controller
{
    public function setting()
    {
        $user_id = session()->get('user')->id;
        $shop = Shop::where('user_id', $user_id)->first();
        return view('frontend.saller.shop.index', \compact('shop'));
    }

    public function store(Request $request)
    {
        for ($i = 'AAA'; $i < 'ZZZ'; $i++) {
            $data = Shop::where('short', $i)->get();
            if(count($data) <= 0){
                $short[] = $i;
            }
        }

        $user_id = session()->get('user')->id;
        $shop = new Shop();
        $shop->name = $request->name;
        $shop->user_id = $user_id;
        $upload_path = 'uploads/shop_img/';
        if($request->file('logo')){
            $logo = $request->file('logo');
            $name_logo = hexdec(uniqid());
            $logo_ext = strtolower($logo->extension());
            $logo_new_name = $name_logo.'.'.$logo_ext;
            $photo = $upload_path.$logo_new_name;
            $logo->move($upload_path, $logo_new_name);
            $shop->logo = $photo;
        }
        $shop->short = $short[0];
        $slide = $request->file('slider');
        if($slide > 0){  
            $slid = array();
                foreach($slide as $row){
                    $slider_img = hexdec(uniqid());
                    $slider_ext = strtolower($row->extension());
                    $slider_new_name = $slider_img.'.'.$slider_ext;
                    $slider = $upload_path.$slider_new_name;
                    $row->move($upload_path, $slider_new_name);
                    array_push($slid, $slider);
                }
                $shop->sliders = implode(',', $slid);
        }
        $shop->address = $request->address;
        $shop->facebook = $request->facebook;
        $shop->youtube = $request->youtube;
        $shop->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.Str::random(5);
        $shop->meta_title = $request->meta_title;
        $shop->meta_description = $request->meta_description;
        
        if($shop->save()){
            $notification = [
                'message' => 'Add your shop successfully.',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        }
    }

    public function update(Request $request)
    {

        $shop = Shop::find($request->shop_id);

        // $chars = $request->name;
        //     srand((double)microtime()*1000000); 
        //     $i = 0; 
        //     $pass = '' ; 

        //     while ($i <= 7) { 
        //         $num = rand() % 33; 
        //         $tmp = substr($chars, $num, 1); 
        //         $pass = $pass . $tmp; 
        //         $i++; 
        //     } 

        // dd($pass); 

        $shop->name = $request->name;
        // photos upload
        $upload_path = 'uploads/shop_img/';
        if($request->file('logo')){
            if($shop->logo){
                unlink($shop->logo);
            }

            $logo = $request->file('logo');
            $name_logo = hexdec(uniqid());
            $logo_ext = strtolower($logo->extension());
            $logo_new_name = $name_logo.'.'.$logo_ext;
            $photo = $upload_path.$logo_new_name;
            $logo->move($upload_path, $logo_new_name);

            $shop->logo = $photo;
        }

        $slide = $request->file('slider');
        if($slide > 0){
            if($shop->sliders != null){
                $slider = explode(',', $shop->sliders);
                foreach($slider as $row){
                        unlink($row);
                }
            }  
            $slid = array();
                foreach($slide as $row){
                    $slider_img = hexdec(uniqid());
                    $slider_ext = strtolower($row->extension());
                    $slider_new_name = $slider_img.'.'.$slider_ext;
                    $slider = $upload_path.$slider_new_name;
                    $row->move($upload_path, $slider_new_name);
                    array_push($slid, $slider);
                }
                $shop->sliders = implode(',', $slid);
        }

        $shop->address = $request->address;
        $shop->facebook = $request->facebook;
        $shop->youtube = $request->youtube;
        $shop->meta_title = $request->meta_title;
        $shop->meta_description = $request->meta_description;
        if($shop->save()){
            $notification = [
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        }
    }

    public function location($id)
    {
        $shop = Shop::where('user_id', $id)->first();
        return view('frontend.seller_location', \compact('shop'));
    }
}
