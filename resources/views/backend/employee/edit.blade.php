@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
                   <div class="content">
                    <div class="intro-y flex items-center mt-8">
                        <h2 class="text-lg font-medium mr-auto">
                            Edit Employee
                        </h2>
                    </div>
                    <div class="grid grid-cols-6 gap-6 mt-5">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <!-- BEGIN: Vertical Form -->
                            <form action="{{ route('employee.update', $employee->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                            <div class="intro-y box">
                                <div id="vertical-form" class="p-5">
                                    <div class="preview">
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">First Name</label>
                                            <input id="vertical-form-1" type="text" name="first_name" class="form-control border-gray-500 text-base" value="{{$employee->first_name}}" placeholder="First Name" required>
                                        </div>
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Last Name</label>
                                            <input id="vertical-form-1" type="text" name="last_name" class="form-control border-gray-500 text-base" value="{{$employee->last_name}}" placeholder="Last Name" required>
                                        </div>
                                         <div>
                                            <label for="vertical-form-1" class="form-label text-base">Phone</label>
                                            <input id="vertical-form-1" type="number" name="phone" class="form-control border-gray-500 text-base" value="{{$employee->phone}}" placeholder="+85620" required>
                                        </div>
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Address</label>
                                            <input id="vertical-form-1" type="text" name="address" class="form-control border-gray-500 text-base" value="{{$employee->address}}" placeholder="Address..." required>
                                        </div>
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Email</label>
                                            <input id="vertical-form-1" type="email" name="email" class="form-control border-gray-500 text-base" value="{{$employee->email}}" placeholder="Email" required>
                                        </div>
                                         <div>
                                            <label for="vertical-form-1" class="form-label text-base">Password</label>
                                            <input id="vertical-form-1" type="password" name="password" class="form-control border-gray-500 text-base" value="{{$employee->password}}" placeholder="Password" required>
                                        </div>
                                        <div class="mt-3">
                                            <label class="text-base">Role</label>
                                            <div class="mt-2">
                                                <select class="form-select mt-2 sm:mr-2 border-gray-500" aria-label="Default select example">
                                                    <option value="{{$employee->position}}">Admin</option>
                                                    <option>Manager</option>
                                                    <option>Accountant</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="mt-10">
                                            <button type="submit" class="btn btn-primary w-32 mr-2 mb-2"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Edit</button> 
                                        </div>

                                    </div>
                                </div>
                            </div>
                            </form>
                        </div> 
                    </div>
                </div>
s<!-- END: Content -->
    
@endsection