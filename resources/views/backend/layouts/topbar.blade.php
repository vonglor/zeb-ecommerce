 
 @php 
 $user = Session::get('user');
 @endphp

 <!-- BEGIN: Mobile Menu -->
        <div class="mobile-menu md:hidden">
            <div class="mobile-menu-bar">
                <a href="{{route('admin.dashboard')}}" class="flex mr-auto">
                    <img alt="Icewall Tailwind HTML Admin Template" class="w-6" src="{{ asset('backend/dist/images/logo.svg') }}">
                </a>
                <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
            </div>
            <ul class="border-t border-theme-2 py-5 hidden">
                <li>
                    <a href="{{route('admin.dashboard')}}" class="menu menu--active">
                        <div class="menu__icon"> <i data-feather="home"></i> </div>
                        <div class="menu__title"> Dashboard </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon"> <i data-feather="shopping-cart" class="block mx-auto"></i> </div>
                        <div class="menu__title"> Products <i data-feather="chevron-down" class="menu__sub-icon "></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{route('product')}}" class="menu menu--active">
                                <div class="menu__icon"> <i data-feather="plus-circle" class="block mx-auto"></i> </div>
                                <div class="menu__title"> Add New Product </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('product_all')}}" class="menu menu--active">
                                <div class="menu__icon"> <i data-feather="shopping-cart" class="block mx-auto"></i>  </div>
                                <div class="menu__title"> All Product </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('product_all_inhouse')}}" class="menu menu--active">
                                <div class="menu__icon"> <i data-feather="shopping-cart" class="block mx-auto"></i>  </div>
                                <div class="menu__title"> In House Product </div>
                            </a>
                        </li>
                         <li>
                            <a href="{{route('product_all_seller')}}" class="menu menu--active">
                                <div class="menu__icon"> <i data-feather="shopping-cart" class="block mx-auto"></i>  </div>
                                <div class="menu__title"> Seller Product </div>
                            </a>
                        </li>
                         <li>
                            <a href="{{route('get.category')}}" class="menu menu--active">
                                <div class="menu__icon"> <i data-feather="book" class="block mx-auto"></i>   </div>
                                <div class="menu__title"> Category </div>
                            </a>
                        </li>
                          <li>
                            <a href="{{route('brand.index')}}" class="menu menu--active">
                                <div class="menu__icon"> <i data-feather="book" class="block mx-auto"></i>   </div>
                                <div class="menu__title"> brand </div>
                            </a>
                        </li>
                          <li>
                            <a href="{{route('attribute.index')}}" class="menu menu--active">
                                <div class="menu__icon"> <i data-feather="book" class="block mx-auto"></i>   </div>
                                <div class="menu__title"> Attribute </div>
                            </a>
                        </li>
                    </ul>
                </li>
                 <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon">  <i data-feather="credit-card"></i> </div>
                        <div class="menu__title"> Sales <i data-feather="chevron-down" class="menu__sub-icon "></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{route('sale.index')}}" class="menu">
                                <div class="menu__icon">  <i data-feather="shopping-bag" class="block mx-auto"></i> </div>
                                <div class="menu__title"> All Orders </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('inhouse_order')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="shopping-bag" class="block mx-auto"></i></div>
                                <div class="menu__title"> In House Orders </div>
                            </a>
                        </li>
                         <li>
                            <a href="{{route('seller_order')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="shopping-bag" class="block mx-auto"></i></div>
                                <div class="menu__title"> Seller Orders </div>
                            </a>
                        </li>
                    </ul>
                </li>
                  <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon">  <i data-feather="users" class="block mx-auto"></i> </div>
                        <div class="menu__title"> Customers <i data-feather="chevron-down" class="menu__sub-icon "></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{route('customer')}}" class="menu">
                                <div class="menu__icon">  <i data-feather="user-plus" class="block mx-auto"></i>  </div>
                                <div class="menu__title"> All Customers </div>
                            </a>
                        </li>
                         <li>
                            <a href="{{route('customer_payout')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i> </div>
                                <div class="menu__title"> Customer payout </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('customer_withdraw')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i> </div>
                                <div class="menu__title"> Resquest Withdraw </div>
                            </a>
                        </li>
                    </ul>
                </li>

                  <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon">  <i data-feather="user" class="block mx-auto"></i> </div>
                        <div class="menu__title"> Sellers <i data-feather="chevron-down" class="menu__sub-icon "></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{route('seller.index')}}" class="menu">
                                <div class="menu__icon">  <i data-feather="user" class="block mx-auto"></i>  </div>
                                <div class="menu__title"> All Sellers </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('seller_commission')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="users" class="block mx-auto"></i> </div>
                                <div class="menu__title"> Sellers Commission </div>
                            </a>
                        </li>
                         <li>
                            <a href="{{route('seller_payput')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i> </div>
                                <div class="menu__title"> Sellers Payout </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('seller_withdraw')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i> </div>
                                <div class="menu__title"> Resquest Withdraw </div>
                            </a>
                        </li>
                    </ul>
                </li>
                  <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon">  <i data-feather="users" class="block mx-auto"></i></div>
                        <div class="menu__title"> Employee <i data-feather="chevron-down" class="menu__sub-icon "></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{route('employee')}}" class="menu">
                                <div class="menu__icon"><i data-feather="user-plus" class="block mx-auto"></i>  </div>
                                <div class="menu__title"> Add New Employee </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('all_employee')}}" class="menu">
                                <div class="menu__icon">  <i data-feather="users" class="block mx-auto"></i> </div>
                                <div class="menu__title"> All Employee </div>
                            </a>
                        </li>
                         <li>
                            <a href="{{route('employee_permission')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i>  </div>
                                <div class="menu__title"> Employee Permission </div>
                            </a>
                        </li>
                    </ul>
                </li>
               
               
                <li class="menu__devider my-6"></li>
                 <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon">  <i data-feather="inbox"></i></div>
                        <div class="menu__title"> Messages <i data-feather="chevron-down" class="menu__sub-icon "></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{route('show.message')}}" class="menu">
                                <div class="menu__icon"><i data-feather="message-square" class="block mx-auto"></i>  </div>
                                <div class="menu__title"> Message to Customers </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('seller.message')}}" class="menu">
                                <div class="menu__icon">  <i data-feather="message-square" class="block mx-auto"></i> </div>
                                <div class="menu__title"> Message to Sellers </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('all.message')}}" class="menu">
                                <div class="menu__icon">  <i data-feather="message-square" class="block mx-auto"></i> </div>
                                <div class="menu__title"> Message to All </div>
                            </a>
                        </li>
                    </ul>
                </li>
                 <li>
                    <a href="{{route('upload/file')}}" class="menu">
                        <div class="menu__icon"> <i data-feather="hard-drive"></i> </div>
                        <div class="menu__title"> Uploaded File </div>
                    </a>
                </li>
               
                <li>
                    <a href="javascript:;" class="menu">
                        <div class="menu__icon"> <i data-feather="settings" class="block mx-auto"></i> </div>
                        <div class="menu__title"> Settings <i data-feather="chevron-down" class="menu__sub-icon "></i> </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{route('get.currency')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="credit-card"></i> </div>
                                <div class="menu__title"> Currency </div>
                            </a>
                        </li>
                         <li>
                            <a href="{{route('tax')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="credit-card"></i> </div>
                                <div class="menu__title"> TAX </div>
                            </a>
                        </li>
                         <li>
                            <a href="{{route('rule')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="credit-card"></i> </div>
                                <div class="menu__title"> Rules </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('get.policy')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="credit-card"></i> </div>
                                <div class="menu__title"> Policy </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('about')}}" class="menu">
                                <div class="menu__icon"> <i data-feather="credit-card"></i> </div>
                                <div class="menu__title"> About Us </div>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- END: Mobile Menu -->
        <!-- BEGIN: Top Bar -->
        <div class="top-bar-boxed border-b border-theme-2 -mt-7 md:-mt-5 -mx-3 sm:-mx-8 px-3 sm:px-8 md:pt-0 mb-12">
            <div class="h-full flex items-center mt-5">
                <!-- BEGIN: Logo -->
                <a href="{{route('admin.dashboard')}}" class="-intro-x hidden md:flex">
                    <img alt="Icewall Tailwind HTML Admin Template" class="w-6" src="{{asset('backend/dist/images/logo.svg')}}">
                    <span class="text-white text-lg ml-3" style="color: orange"> HMOOB<span class="font-medium" style="color: orange">ONLINE</span> </span>
                </a>
                <!-- END: Logo -->
                <!-- BEGIN: Breadcrumb -->
                <div class="-intro-x breadcrumb mr-auto"> <a href="{{route('admin.dashboard')}}">HMOOBONLINE</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="{{route('admin.dashboard')}}" class="breadcrumb--active">Dashboard</a> </div>
                <!-- END: Breadcrumb -->
                <!-- BEGIN: Notifications 1 -->
                <div class="intro-x dropdown mr-4 sm:mr-6">
                    <div class="dropdown-toggle notification notification--bullet cursor-pointer" role="button" aria-expanded="false"> <i data-feather="bell" class="notification__icon dark:text-gray-300"></i> </div>
                    <div class="notification-content pt-2 dropdown-menu">
                        <div class="notification-content__box dropdown-menu__content box dark:bg-dark-6">
                            <div class="notification-content__title">Notifications</div>
                            <div class="cursor-pointer relative flex items-center ">
                                <div class="w-12 h-12 flex-none image-fit mr-1">
                                    <img alt="Icewall Tailwind HTML Admin Template" class="rounded-full" src="{{ asset('backend/dist/images/profile-4.jpg') }}">
                                    <div class="w-3 h-3 bg-theme-10 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                </div>
                                <div class="ml-2 overflow-hidden">
                                    <div class="flex items-center">
                                        <a href="javascript:;" class="font-medium truncate mr-5">David Moua</a> 
                                        <div class="text-xs text-gray-500 ml-auto whitespace-nowrap">06:05 AM</div>
                                    </div>
                                    <div class="w-full truncate text-gray-600 mt-0.5">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 20</div>
                                </div>
                            </div>
                            <div class="cursor-pointer relative flex items-center mt-5">
                                <div class="w-12 h-12 flex-none image-fit mr-1">
                                    <img alt="Icewall Tailwind HTML Admin Template" class="rounded-full" src="{{ asset('backend/dist/images/profile-8.jpg') }}">
                                    <div class="w-3 h-3 bg-theme-10 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                </div>
                                <div class="ml-2 overflow-hidden">
                                    <div class="flex items-center">
                                        <a href="javascript:;" class="font-medium truncate mr-5">Nicolas Cage</a> 
                                        <div class="text-xs text-gray-500 ml-auto whitespace-nowrap">01:10 PM</div>
                                    </div>
                                    <div class="w-full truncate text-gray-600 mt-0.5">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem </div>
                                </div>
                            </div>
                            <div class="cursor-pointer relative flex items-center mt-5">
                                <div class="w-12 h-12 flex-none image-fit mr-1">
                                    <img alt="Icewall Tailwind HTML Admin Template" class="rounded-full" src="{{ asset('backend/dist/images/profile-7.jpg') }}">
                                    <div class="w-3 h-3 bg-theme-10 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                </div>
                                <div class="ml-2 overflow-hidden">
                                    <div class="flex items-center">
                                        <a href="javascript:;" class="font-medium truncate mr-5">Al Pacino</a> 
                                        <div class="text-xs text-gray-500 ml-auto whitespace-nowrap">01:10 PM</div>
                                    </div>
                                    <div class="w-full truncate text-gray-600 mt-0.5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500</div>
                                </div>
                            </div>
                            <div class="cursor-pointer relative flex items-center mt-5">
                                <div class="w-12 h-12 flex-none image-fit mr-1">
                                    <img alt="Icewall Tailwind HTML Admin Template" class="rounded-full" src="{{ asset('backend/dist/images/profile-6.jpg') }}">
                                    <div class="w-3 h-3 bg-theme-10 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                </div>
                                <div class="ml-2 overflow-hidden">
                                    <div class="flex items-center">
                                        <a href="javascript:;" class="font-medium truncate mr-5">Morgan Freeman</a> 
                                        <div class="text-xs text-gray-500 ml-auto whitespace-nowrap">05:09 AM</div>
                                    </div>
                                    <div class="w-full truncate text-gray-600 mt-0.5">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 20</div>
                                </div>
                            </div>
                            <div class="cursor-pointer relative flex items-center mt-5">
                                <div class="w-12 h-12 flex-none image-fit mr-1">
                                    <img alt="Icewall Tailwind HTML Admin Template" class="rounded-full" src="{{ asset('backend/dist/images/profile-11.jpg') }}">
                                    <div class="w-3 h-3 bg-theme-10 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                </div>
                                <div class="ml-2 overflow-hidden">
                                    <div class="flex items-center">
                                        <a href="javascript:;" class="font-medium truncate mr-5">Xuemoua Yongcue</a> 
                                        <div class="text-xs text-gray-500 ml-auto whitespace-nowrap">01:10 PM</div>
                                    </div>
                                    <div class="w-full truncate text-gray-600 mt-0.5">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 20</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: Notifications 1 -->
                <!-- BEGIN: Account Menu -->
                <div class="intro-x dropdown w-8 h-8">
                    <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg image-fit zoom-in scale-110" role="button" aria-expanded="false">
                        @if(!empty($user->photo))
                            <img src="{{ asset('uploads/user_img/'.$user->photo) }}" alt="photo" class="rounded-full" width="50" height="50">
                            @else
                            <img src="{{ asset('uploads/user_img/default.png') }}" alt="photo" class="rounded-full" width="50" height="50">
                        @endif
                    </div>
                    <div class="dropdown-menu w-56">
                        <div class="dropdown-menu__content box bg-theme-11 dark:bg-dark-6 text-white">
                            <div class="p-4 border-b border-theme-12 dark:border-dark-3">
                                <div class="font-medium">{{ $user->name .' '. $user->surname }}</div>
                                {{-- <div class="text-xs text-theme-13 mt-0.5 dark:text-gray-600">Frontend Engineer</div> --}}
                            </div>
                            <div class="p-2">
                                <a href="{{ route('users.profile', $user->id) }}" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> 
                                    <i data-feather="user" class="w-4 h-4 mr-2"></i> {{(__('Profile'))}} </a>
                                <a href="{{ route('users.editPwd', ['user' => $user->id, 'param' => 'changePwd']) }}" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> 
                                    <i data-feather="lock" class="w-4 h-4 mr-2"></i> {{__('Change Password')}} </a>
                            </div>
                            <div class="p-2 border-t border-theme-12 dark:border-dark-3">
                                <a href="{{ route('users.signOut') }}" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md"> <i data-feather="toggle-right" class="w-4 h-4 mr-2"></i> Logout </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: Account Menu -->
            </div>
        </div>
        <!-- END: Top Bar -->