<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Percent extends Model
{
    use HasFactory;
    protected $table = 'percents';
    protected $fillable = [
        'start_amount',
        'end_amount', 
        'percent', 
        'name'
    ];

    /**
     * Get the user that owns the Percent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
