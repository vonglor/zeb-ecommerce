<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exchangerate extends Model
{
    use HasFactory;
    protected $table = 'exchangerate';
    protected $fillable = [
        'from',
        'to',
        'operator',
        'exchange'
    ];
}
