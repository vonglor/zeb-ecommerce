
       @include('backend.layouts.header')
       @include('backend.layouts.topbar')
        <div class="wrapper">
            <div class="wrapper-box">
                <!-- BEGIN: Side Menu -->
                <nav class="side-nav" >
                    <ul>
                        <li>
                            <a href="{{route('admin.dashboard')}}" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                                <div class="side-menu__title text-lg">
                                    Dashboard 
                                    <div class="side-menu__sub-icon transform rotate-180"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('pos-sale.index')}}" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="shopping-cart" class="block mx-auto"></i> </div>
                                <div class="side-menu__title text-lg">
                                    {{__('Pos')}}
                                    <div class="side-menu__sub-icon "></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="shopping-cart" class="block mx-auto"></i> </div>
                                <div class="side-menu__title text-lg">
                                    Products 
                                    <div class="side-menu__sub-icon "> <i data-feather="chevron-down"></i> </div>
                                </div>
                            </a>
                            <ul class="">
                                <li>
                                    <a href="{{route('product')}}" class="side-menu">
                                        <div class="side-menu__icon">  <i data-feather="plus-circle" class="block mx-auto"></i> </div>
                                        <div class="side-menu__title text-lg"> Add New Product </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('product_all')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="shopping-cart" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg"> All Product </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('product_all_inhouse')}}" class="side-menu">
                                        <div class="side-menu__icon"><i data-feather="shopping-cart" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg"> In House Product </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('product_all_seller')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="shopping-cart" class="block mx-auto"></i> </div>
                                        <div class="side-menu__title text-lg"> Seller Product </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('get.category')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="book" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg"> Category </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('brand.index')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="book" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg"> Brand </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('attribute.index')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="book" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg"> Attribute </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('color.index')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="book" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg"> Color </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="credit-card"></i></div>
                                <div class="side-menu__title text-lg">
                                    Orders  
                                    <div class="side-menu__sub-icon "> <i data-feather="chevron-down"></i> </div>
                                </div>
                            </a>
                            <ul class="">
                                <li>
                                    <a href="{{route('order.fetchOrderBySeller', ['status' => 'all'])}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="shopping-bag" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg">Orders Saller </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('order.fetch', ['status' => 'all']) }}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="shopping-bag" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg">{{__('Orders')}} </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="javascript:;" class="side-menu">
                                <div class="side-menu__icon">  <i data-feather="users" class="block mx-auto"></i>  </div>
                                <div class="side-menu__title text-lg">
                                    Buyer Info  
                                    <div class="side-menu__sub-icon "><i data-feather="chevron-down"></i> </div>
                                </div>
                            </a>
                            <ul class="">
                                <li>
                                    <a href="{{route('customer.index')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="user-plus" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg"> Buyer Info </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="user-plus" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg"> Sign Up Buy </div>
                                    </a>
                                </li>
                                 <li>
                                    <a href="{{ route('withdraw-request', ['status' => 'pending']) }}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i> </div>
                                        <div class="side-menu__title text-lg"> {{__('Withdraw')}} </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="javascript:;" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="user" class="block mx-auto"></i>  </div>
                                <div class="side-menu__title text-lg">
                                    Seller Info  
                                    <div class="side-menu__sub-icon "> <i data-feather="chevron-down"></i> </div>
                                </div>
                            </a>
                            <ul class="">
                                <li>
                                    <a href="{{route('seller.index')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="user" class="block mx-auto"></i> </div>
                                        <div class="side-menu__title text-lg"> Seller Info </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="user" class="block mx-auto"></i> </div>
                                        <div class="side-menu__title text-lg"> Sign Up Seller </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('commission.index')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="users" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg">{{__('Commission')}} </div>
                                    </a>
                                </li>
                                 <li>
                                    <a href="{{ route('seller-withdraw-request', ['status' => 'pending']) }}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i> </div>
                                        <div class="side-menu__title text-lg"> {{__('Withdraw')}} </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="users" class="block mx-auto"></i> </div>
                                <div class="side-menu__title text-lg">
                                    {{__('Users')}} 
                                    <div class="side-menu__sub-icon "> <i data-feather="chevron-down"></i> </div>
                                </div>
                            </a>
                            <ul class="">
                                <li>
                                    <a href="{{ route('users.create') }}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="user-plus" class="block mx-auto"></i> </div>
                                        <div class="side-menu__title text-lg"> {{__('Add User')}} </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('users.index') }}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="users" class="block mx-auto"></i> </div>
                                        <div class="side-menu__title text-lg"> {{__('User Lists')}} </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('roles.index') }}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i> </div>
                                        <div class="side-menu__title text-lg">{{__('Role & Permission')}} </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{route('membership.index')}}" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i>  </div>
                                <div class="side-menu__title text-lg"> Membership Fee </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i> </div>
                                <div class="side-menu__title text-lg">
                                    Text Customer 
                                    <div class="side-menu__sub-icon "> <i data-feather="chevron-down"></i> </div>
                                </div>
                            </a>
                            <ul class="">
                                <li>
                                    <a href="{{route('show.message')}}" class="side-menu">
                                        <div class="side-menu__icon"><i data-feather="message-square" class="block mx-auto"></i></div>
                                        <div class="side-menu__title text-lg"> To Buyer </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('seller.message')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="message-square" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg">To Seller </div>
                                    </a>
                                </li>
                                 <li>
                                    <a href="{{route('all.message')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="message-square" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg">To All </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="message-circle" class="block mx-auto"></i> </div>
                                <div class="side-menu__title text-lg">
                                    Text SMS 
                                    <div class="side-menu__sub-icon "> <i data-feather="chevron-down"></i> </div>
                                </div>
                            </a>
                            <ul class="">
                                <li>
                                    <a href="{{route('chat')}}" class="side-menu">
                                        <div class="side-menu__icon"><i data-feather="message-square" class="block mx-auto"></i></div>
                                        <div class="side-menu__title text-lg"> From Buyer </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('chat')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="message-square" class="block mx-auto"></i>  </div>
                                        <div class="side-menu__title text-lg">From Seller </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{route('upload/file')}}" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="folder-minus" class="block mx-auto"></i> </div>
                                <div class="side-menu__title text-lg"> Uploaded File </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="settings" class="block mx-auto"></i> </div>
                                <div class="side-menu__title text-lg">
                                    Settings 
                                    <div class="side-menu__sub-icon "> <i data-feather="chevron-down"></i> </div>
                                </div>
                            </a>
                            <ul class="">
                                <li>
                                    <a href="{{route('percentproduct')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="credit-card"></i> </div>
                                        <div class="side-menu__title text-lg"> Percent buy </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('percentsale')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="credit-card"></i> </div>
                                        <div class="side-menu__title text-lg"> Percent sale </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('get.currency')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="credit-card"></i> </div>
                                        <div class="side-menu__title text-lg"> Currency </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('exchangerate')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="credit-card"></i> </div>
                                        <div class="side-menu__title text-lg"> Exchange Rate </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('tax')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="credit-card"></i> </div>
                                        <div class="side-menu__title text-lg"> Tax </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('rule')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="credit-card"></i> </div>
                                        <div class="side-menu__title text-lg"> Rules </div>
                                    </a>
                                </li>
                                 <li>
                                    <a href="{{route('get.policy')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="credit-card"></i> </div>
                                        <div class="side-menu__title text-lg"> Policy Return </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('warning.policy')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="credit-card"></i> </div>
                                        <div class="side-menu__title text-lg"> Warning Policy </div>
                                    </a>
                                </li>
                                 <li>
                                    <a href="{{route('about')}}" class="side-menu">
                                        <div class="side-menu__icon"> <i data-feather="credit-card"></i> </div>
                                        <div class="side-menu__title text-lg"> About Us </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- END: Side Menu -->
               @yield('content')
            </div>
        </div>
        @yield('script');
      @include('backend.layouts.footer')