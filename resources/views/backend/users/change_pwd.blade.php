@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y box mt-5">
          <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
              <h2 class="text-lg font-medium text-base mr-auto">
                <i data-feather="edit"></i> {{__('Change Password')}}
              </h2>
              <div class="form-check w-full sm:w-auto sm:ml-auto mt-3 sm:mt-0">
                <a href="{{ route('users.index') }}">{{__('Back')}}</a>
              </div>
          </div>
          <div id="select-options" class="p-5">
            <form action="{{ route('users.changePwd', $user->id) }}" autocomplete="off" enctype="multipart/form-data" method="POST">
              @csrf
              @method('PUT')
              <div class="preview">
                    <div class="mt-3"> 
                      <label for="old_password" class="form-label">{{__('Old Password')}} <span class="txt-red">*</span></label> 
                      <input name="old_password" type="password" class="form-control">
                      @if ($errors->has('old_password'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('old_password') }}</strong>
                              </span>
                          @endif 
                    </div>

                    <div class="mt-3"> 
                      <label for="password" class="form-label">{{__('Password')}} <span class="txt-red">*</span></label> 
                      <input name="password" type="password" class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
                      @if ($errors->has('password'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif 
                    </div>

                    <div class="mt-3"> 
                      <label for="password_confirmation" class="form-label">{{__('Confrim Password')}} <span class="txt-red">*</span></label> 
                      <input name="password_confirmation" type="password" class="form-control">
                      @if ($errors->has('password_confirmation'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                              </span>
                          @endif 
                    </div>

                    <button type="submit" id="btnSubmit" class="btn btn-primary mt-5" style="width:100%">{{__('Save')}}</button>
                    <a href="{{ route('users.index') }}" class="btn btn-secondary" style="width:100%">{{__('Cancel')}}</a> 
                  </div>
              </div> 
            </form>
          </div>
      </div>
    </div>
    <!-- END: Content -->

    <script>
      $(document).ready(function() {
        $("#avatar").change(function(){
          let avatar = document.getElementById("avatar");
          if (typeof (avatar.files) !== "undefined") {
            let size = parseFloat(avatar.files[0].size / (1024 * 1024)).toFixed(2); 
            if (size > 2) {
                $("#nameFile").text("File size must be less than 2MB");
                document.getElementById("btnSubmit").disabled = true;
            }else{
                $("#nameFile").text("");
                document.getElementById("btnSubmit").disabled = false;
            }
          }else{
              $("#nameFile").text("browser not support");
          }
        });
      });
  </script>

@endsection