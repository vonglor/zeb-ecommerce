@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-xl font-medium mr-auto">
                Edit Exchange Rate
            </h2>
        </div>
        <div class="grid grid-cols-6 gap-12 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
                <form action="{{route('update.exchangerate', $exchangerate->id )}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="intro-y box">
                        <div id="vertical-form" class="p-5">
                            <div class="preview">
                                `
                                <div class="col-span-12 sm:col-span-6">
                                    <label for="modal-form-1" class="form-label text-base">From</label>
                                    <select class="form-select mt-2 sm:mr-2 border-gray-500" name="from" aria-label="Default select example">
                                        <option value="">Select Currency</option>
                                        @foreach (\App\Models\Currency::all() as $key => $currency)
                                        <option value="{{ $currency->name }}">{{ $currency->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-span-12 sm:col-span-6">
                                    <label for="modal-form-1" class="form-label text-base">To</label>
                                    <select class="form-select mt-2 sm:mr-2 border-gray-500" name="from" aria-label="Default select example">
                                        <option value="">Select Currency</option>
                                        @foreach (\App\Models\Currency::all() as $key => $currency)
                                        <option value="{{ $currency->name }}">{{ $currency->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-span-12 sm:col-span-6">
                                    <label for="modal-form-3" class="form-label text-base">Operator</label>
                                    <select class="form-select mt-2 sm:mr-2 border-gray-500" name="operator" aria-label="Default select example">
                                        <option value="">Select Operator</option>
                                        <option value="*">*</option>
                                        <option value="/">/</option>
                                    </select>
                                </div>
                                <div>
                                    <label for="vertical-form-1" class="form-label text-base">Exchange Rate</label>
                                    <input type="text" class="form-control border-gray-500 text-base" name="exchange" value="{{$exchangerate->exchange}}" placeholder="Exchange Rate">
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary mt-5 text-base">update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- END: Content -->
    
@endsection