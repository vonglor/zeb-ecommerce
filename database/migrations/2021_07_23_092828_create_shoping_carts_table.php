<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopingCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoping_carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_id');
            $table->integer('user_id');
            $table->integer('product_id');
            $table->text('variantion')->nullable();
            $table->double('price', 10, 2);
            $table->double('tax', 10, 2);
            $table->double('shipping_cost', 10, 2);
            $table->double('discount', 10, 2);
            $table->integer('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoping_carts');
    }
}
