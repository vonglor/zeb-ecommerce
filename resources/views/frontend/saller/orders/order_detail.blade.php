@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <div class="col-sm-9">
        <div class="centent_card_1r clearfix">
            <div class="centent_card_1r3 tab-content">
                <p>Order detail</p>
                <hr>
                <div class="card-content">
                        <div class="row">
                            <div class="shipping_address col-sm-6" style="margin-bottom: 20px">
                                @if ($order->shipping_address)
                                <p>Name: {{ json_decode($order->shipping_address)->name }}</p>
                                <p>Address: {{ json_decode($order->shipping_address)->address.', '.json_decode($order->shipping_address)->city.', '.json_decode($order->shipping_address)->country }}</p>
                                <p>Email: {{ json_decode($order->shipping_address)->email }}</p>
                                <p>Phone: {{ json_decode($order->shipping_address)->phone }}</p>
                                @else
                                <p>Name: {{ $order->user->name }}</p>
                                <p>Email: {{ $order->user->email }}</p>
                                <p>Phone: {{ $order->user->phone }}</p>
                                @endif
                                
                            </div>
                            <div class="shipping_address col-sm-4" style="margin-bottom: 20px; margin-right:15px; float: right;">
                                <p>Order Id: {{ $order->code }}</p>
                                <p>Order status: {{ $order->delivery_status }}</p>
                                <p>Order date: {{ $order->date }}</p>
                                <p>Payment method: {{ $order->payment_type }}</p>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                        Order Details
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>qty</th>
                                            <th>price</th>
                                            <th>Refund</th>
                                        </tr>
                                        @php
                                            $i = 1;
                                            $subtotal = 0;
                                        @endphp
                                        @foreach ($order_detail as $item)
                                        @php
                                            $subtotal += $item->price*$item->qty;
                                        @endphp
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ App\Models\Product::find($item->productStock->product_id)->name }}</td>
                                            <td>{{ $item->qty }}</td>
                                            <td>{{ number_format($item->price, 2) }}</td>
                                            <td></td>
                                        </tr>
                                        @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-3 float-right" style="float: right">
                            <table class="table table-hover">
                                    <tr>
                                        Order Ammount
                                    </tr>
                                    <tr>
                                        <td width="50%">Sub Total:</td>
                                        <td width="50%">{{ number_format($subtotal, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>tax:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Shipping:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>TOTAL:</td>
                                        <td>{{ number_format($subtotal, 2) }}</td>
                                    </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- @section('script')
    <script>
        $(document).ready(function() {
            $('.btn-veiw-detail').click(function(e) {
                e.preventDefault();
                var order_id = $(this).attr('id');
                $.ajax({
                    method: "get",
                    url: "/order/order_detail/"+order_id,
                    success: function (data) {
                        $('#my-modal').modal('show');
                        $('#cus_name').text(data.order.order_code);
                    }
                });
            });
        });
    </script>
@endsection --}}
