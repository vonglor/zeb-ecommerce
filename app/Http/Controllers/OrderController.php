<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Order;
use App\Models\Order_detail;
use App\Models\Percent;
use App\Models\PercentSale;
use App\Models\Product;
use App\Models\Product_stock;
use App\Models\Seller;
use App\Models\Tax;
use App\Models\User;
use App\Models\UserTax;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Session;

class OrderController extends Controller
{

    public function store(Request $request)
    {
        // dd($request->all());
        $total = payment_price($request->total, $request->currency_code);
        $discount = payment_price($request->discount, $request->currency_code);
        $order            = new Order;
        $order->user_id   = Session::get('user')->id;
        $order->seller_id = Seller::where('user_id', session()->get('owner_id'))->first()->id;

        $address                 = Address::find($request->address_id);
        $data['name']            = Session::get('user')->name;
        $data['email']           = Session::get('user')->email;
        $data['address']         = $address->address;
        $data['country']         = $address->country;
        $data['city']            = $address->city;
        $data['postal_code']     = $address->postal_code;
        $data['phone']           = $address->phone;
        $shipping_info           = $data;
        $order->shipping_address = json_encode($shipping_info);
        $order->payment_type = $request->payment_type;

        if($request->payment_type == 'paypal'){
            $order->payment_status = 'paid';
        }elseif($request->payment_type == 'wallet'){
            $order->payment_status = 'paid';
        }

        $order->payment_type = $request->payment_option;
        $order->code         = date('Ymd-His') . rand(10, 99);
        $order->date         = now();
        if ($order->save()) {
            $subtotal = 0;
            $tax      = 0;
            $shipping = 0;

            //calculate shipping is to get shipping costs of different types
            $admin_products  = array();
            $seller_products = array();

            //Order Details Storing
            foreach (Session::get('cart')->where('owner_id', Session::get('owner_id')) as $key => $cartItem) {
                $product = Product::find(Product_stock::find($cartItem['id'])->product_id);

                if ($product->added_by == 'admin') {
                    array_push($admin_products, $cartItem['id']);
                } else {
                    $product_ids = array();
                    if (array_key_exists($product->user_id, $seller_products)) {
                        $product_ids = $seller_products[$product->user_id];
                    }
                    array_push($product_ids, $cartItem['id']);
                    $seller_products[$product->user_id] = $product_ids;
                }

                $subtotal += $cartItem['price'] * $cartItem['qty'];
                // $tax += $cartItem['tax']*$cartItem['qty'];

                $product_variation = $cartItem['variant'];

                if ($product_variation != null) {
                    $product_stock = Product_stock::where(['id' => $cartItem['id'], 'variant' => $product_variation])->first();
                    if ($cartItem['qty'] > $product_stock->qty) {
                        //flash(translate('The requested quantity is not available for ').$product->getTranslation('name'))->warning();
                        Session::flash('success', 'Payment successful!');
                        $order->delete();
                        return redirect()->route('cart')->send();
                    } else {
                        $product_stock->qty -= $cartItem['qty'];
                        $product_stock->save();
                    }
                } else {
                    $product_stock = Product_stock::find($cartItem['id']);
                    if ($cartItem['qty'] > $product_stock->qty) {
                        // flash(translate('The requested quantity is not available for ') . $product->getTranslation('name'))->warning();
                        $notification = array(
                            'message' => 'This item is out of stock!.',
                            'alert-type' => 'warning'
                        );
                        $order->delete();
                        return back()->with($notification);
                    } else {
                        $product_stock->qty -= $cartItem['qty'];
                        $product_stock->save();
                    }
                }

                $order_detail             = new Order_detail;
                $order_detail->order_id   = $order->id;
                $order_detail->seller_id  = Seller::where('user_id', session()->get('owner_id'))->first()->id;
                $order_detail->product_id = $product_stock->id;
                $order_detail->variantion = $product_variation;
                $order_detail->color      = $cartItem['color'];
                $order_detail->size       = $cartItem['size'];
                $order_detail->fabric     = $cartItem['fabric'];
                $order_detail->poud       = $cartItem['poud'];
                
                $product = Product::find($cartItem['pro_id']);
                $order_detail->price      = payment_price($cartItem['price'] * $cartItem['qty'], $product->currency_code);

                $order_detail->qty = $cartItem['qty'];
                $order_detail->save();

                $product->num_of_sale++;
                $product->save();
            }

            $order->grand_total = $total;
            $order->discount = $discount;
            $order->save();

            $array['view'] = 'emails.invoice';
            $array['from']  = env('MAIL_USERNAME');
            $array['order'] = $order;

            $request->session()->put('order_id', $order->id);
        }
        $order = Order::find($order->id);
        if($order->payment_status = 'paid'){
            $user_id = array();
            $seller  = User::find(session()->get('owner_id'));
            if ($seller) {
                array_push($user_id, $seller->id);
            }
            $refer_seller = User::where('refer_code_others', $seller->refer_id)->first();
            if ($refer_seller) {
                array_push($user_id, $refer_seller->id);
            }
            $user = User::find(Session::get('user')->id);
            if ($user) {
                array_push($user_id, $user->id);
            }
            if ($user->level1) {
                $level1 = User::where('refer_code_others', $user->level1)->first();
                if ($level1) {
                    array_push($user_id, $level1->id);
                }
            }
            if ($user->level2) {
                $level2 = User::where('refer_code_others', $level1->level1)->first();
                if ($level2) {
                    array_push($user_id, $level2->id);
                }
            }
            if ($user->level3) {
                $level3 = User::where('refer_code_others', $level2->level1)->first();
                if ($level3) {
                    array_push($user_id, $level3->id);
                }
            }
            if ($user->level4) {
                $level4 = User::where('refer_code_others', $level3->level1)->first();
                if ($level4) {
                    array_push($user_id, $level4->id);
                }
            }
    
            //ຕັດຄ່າການຊື້ສິນຄ້າ
            $percent = Percent::where('name', 'balance_buy')->where('status', '1')->get();
            foreach ($percent as $pc) {
                //check percent
                if ($order->grand_total > $pc->start_amount && $order->grand_total <= $pc->end_amount) {
                    $cs = $order->grand_total * $pc->percent;
                    foreach ($user_id as $id) {
                        $wallet = Wallet::where('user_id', $id)->first();
                        if ($wallet) {
                            $wallet->amount = $wallet->amount + $cs;
                        } else {
                            $wallet          = new Wallet;
                            $wallet->user_id = $id;
                            $wallet->amount  = $cs;
                        }
                        $wallet->save();
                    }
                }
            }
    
            //ຕັດຄ່າການຂາຍສິນຄ້າ
            $percent_sale = Percent::where('name', 'percent_sale')->where('status', '1')->get();
            foreach ($percent_sale as $ps) {
                if ($order->grand_total > $ps->start_amount && $order->grand_total <= $ps->end_amount) {
                    $per_sale    = $order->grand_total * $ps->percent;
                    $percentSale = PercentSale::where('user_id', $seller->id)->first();
                    if ($percentSale) {
                        $percentSale->amount = $percentSale->amount + $per_sale;
                    } else {
                        $percentSale          = new PercentSale;
                        $percentSale->user_id = $seller->id;
                        $percentSale->amount  = $per_sale;
                    }
                    $percentSale->save();
                }
            }
    
            //ຕັດຄ່າພາສີການຊື້ສິນຄ້າ
            $tax      = Tax::where('name', 'tax_sale')->where('status', '1')->first();
            $tax_sale = $cs * $tax->tax;
            $user_tax = UserTax::where('user_id', Session::get('user')->id)->first();
            if ($user_tax) {
                $user_tax->amount = $user_tax->amount + $tax_sale;
            } else {
                $user_tax          = new UserTax;
                $user_tax->user_id = Session::get('user')->id;
                $user_tax->amount  = $tax_sale;
            }
            $user_tax->save();
        }else{
            dd('ssss');
        }
        
    }

    public function all_order()
    {
        $user_id   = Session::get('user')->id;
        $seller_id = Seller::where('user_id', Session::get('user')->id)->first()->id;
        $orders    = Order::where('seller_id', $seller_id)->orderBy('id', 'desc')->paginate(15);
        return view('frontend.saller.orders.index', \compact('orders'));
    }

    public function order_detail($order_id)
    {
        $order         = Order::find($order_id);
        $order->viewed = 1;
        $order->save();
        $order_details = Order_detail::where('order_id', $order_id)->get();
        return view('frontend.saller.orders.order_detail_seller', \compact('order', 'order_details'));
        // return response()->json(['order'=>$order, 'order_detail'=>$order_detail]);
    }

    public function update_payment_status($id)
    {
        $order                 = Order::find($id);
        $order->payment_status = "paid";
        foreach (Order_detail::where('order_id', $id)->get() as $order_detail) {
            $order_detail->payment_status = "paid";
            $order_detail->save();
        }
        $order->save();
        return \back();

    }

    public function update_delivery_status(Request $request)
    {
        $order                  = Order::find($request->order_id);
        $order->delivery_status = $request->status;
        foreach (Order_detail::where('order_id', $request->order_id)->get() as $order_detail) {
            $order_detail->delivery_status = $request->status;
            $order_detail->save();
        }
        $order->save();
    }

    public function customer_order()
    {

        $user   = session()->get('user');
        $orders = Order::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(15);
        return view('frontend.customer.customer_order', \compact('orders'));

    }

    public function customer_order_detail($order_id)
    {
        $order         = Order::find($order_id);
        $order_details = Order_detail::where('order_id', $order_id)->get();
        return view('frontend.customer.order_detail_customer', \compact('order', 'order_details'));
    }

    public function search(Request $request)
    {
        $user_id   = Session::get('user')->id;
        $seller_id = Seller::where('user_id', $user_id)->first()->id;
        $orders    = Order::where('code', 'LIKE', '%' . $request->search . '%')
            ->where('seller_id', $seller_id)
            ->paginate(15);
        return view('frontend.saller.orders.index', \compact('orders'));
    }

    public function customerSearch(Request $request)
    {
        $user_id = Session::get('user')->id;
        $orders  = Order::where('code', 'LIKE', '%' . $request->search . '%')
            ->where('user_id', $user_id)
            ->paginate(15);
        return view('frontend.customer.customer_order', \compact('orders'));
    }

    public function date(Request $request)
    {
        $orders = Order::where('date', $request->date)->get();
        return view('frontend.saller.orders.order_date', \compact('orders'));
    }

    public function saleAll()
    {
        
        $user_id = Session::get('user')->id;
        $seller_id = Seller::where('user_id', Session::get('user')->id)->first()->id;
        $sales = Order::where('seller_id', $seller_id)
                ->where('payment_status', 'paid')
                ->where('delivery_status', 'delivered')
                ->orderBy('id', 'desc')->paginate(15);
        return view('frontend.saller.sales.index', \compact('sales'));

    }

    public function sale_detail($order_id)
    {
        $sales = Order::find($order_id);
        $sale_details = Order_detail::where('order_id', $order_id)->get();
        return view('frontend.saller.sales.sale_detail', \compact('sales', 'sale_details'));
    }

    public function saleDate(Request $request)
    {
        $user_id = Session::get('user')->id;
        $seller_id = Seller::where('user_id', Session::get('user')->id)->first()->id;
        $sales = Order::where('seller_id', $seller_id)
                ->where('date', $request->date)
                ->where('payment_status', 'paid')
                ->where('delivery_status', 'delivered')
                ->get();
        return view('frontend.saller.sales.sale_date', \compact('sales'));
    }
}
