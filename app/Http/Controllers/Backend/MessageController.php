<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Message;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $message = Message::latest()->paginate(10);
        return view('backend.messages.customer',compact('message'));
    }
    public function sellerMessage()
    { 
        $message = Message::all();
        return view('backend.messages.seller' ,compact('message'));
    }
    public function allMessage()
    {
        $message = Message::all();
        return view('backend.messages.all',compact('message'));
    }
    public function UpdateStatusNoti(Request $request){ 

        $NotiUpdate = Noticias::findOrFail($request->id)->update(['status' => $request->status]); 
    
        if($request->status == 1)  {
            $newStatus = '<br> <button type="button" class="btn btn-sm btn-danger">Inactiva</button>';
        }else{
            $newStatus ='<br> <button type="button" class="btn btn-sm btn-success">Activa</button>';
        }
    
        return response()->json(['var'=>''.$newStatus.'']);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate(
            [
                'title' => 'required',
                'message' => 'required'
            ]
        );

        $message = $request->all();

        Message::create($message);

        if ($message) {
            $notification = array(
                'message' => 'Insert successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('show.message')->with($notification);
        } else {
            $notification = array(
                'message' => 'Insert warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('show.message')->with($notification);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = Message::find($id);
        return view('backend.messages.edit',compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {

        $request->validate([
            'title' => 'required',
            'message' => 'required'
        ]);
        $input->update($request->all());

        // $message->update($input);

        if ($message) {
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('show.message')->with($notification);
        } else {
            $notification = array(
                'message' => 'Update warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('show.message')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $message = Message::find($id);
        $message->delete();

        if ($message) {
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('show.message')->with($notification);
        } else {
            $notification = array(
                'message' => 'Delete warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('show.message')->with($notification);
        }
    }
}
