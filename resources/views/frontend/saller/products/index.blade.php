@extends('frontend.saller.content_left')
@section('css')
<link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
<div class="col-sm-9">
    <div class="centent_card_1r clearfix">
        <div class="centent_card_1r3 tab-content">
            <p></p>
            <div class="row">
                <div class="addproduct col-sm-4">
                    <a href="{{ route('sale.product.create') }}" class="btn btn-success">+Add Products</a>
                </div>
                    {!! Form::open(['method'=>'get', 'route'=>'saller.product.search']) !!}
                        <div class="col-sm-6">
                            
                            <div class="input-group">
                                {!! Form::text('search', '', ['class'=>'form-control', 'placeholder'=>'Search product name...']) !!}
                                <span class="input-group-btn">
                                    {!! Form::submit('Search', ['class'=>'btn btn-search']) !!}
                                </span>
                            </div><!-- /input-group -->
                        </div>
                    {!! Form::close() !!}
            </div>
            <div class="col-sm-3">
                <div class="bg-grad-3 text-white">
                    <div class="px-3 text-center">
                        <div class="h4">
                            <h4><a href="{{ route('sale.product.index') }}" >All Products</a></h4>
                        </div>
                        <div class="h4">
                            {{ countProduct() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="bg-grad-3 text-white">
                    <div class="px-3 text-center">
                        <div class="h4">
                            <h4><a href="{{ route('sale.product.index') }}" >All Sold out</a></h4>
                        </div>
                        <div class="h4">
                            45
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="bg-grad-3 text-white">
                    <div class="px-3 text-center">
                        <div class="h4">
                            <h4><a href="{{ route('sale.product.index') }}" >Total sale</a></h4>
                        </div>
                        <div class="h4">
                            {{ single_price(totalSale()) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="bg-grad-3 text-white">
                    <div class="px-3 text-center">
                        <div class="h4">
                            <h4><a href="{{ route('sale.product.index') }}" >Sold out today</a></h4>
                        </div>
                        <div class="h4">
                            {{ single_price(saleToday()) }}
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="card-content">
                <div class="table-responsive">
                    <table class="table table-hover">
                      <tr>
                          <th>#</th>
                          <th>Image</th>
                          <th width="">Name</th>
                          <th>Category</th>
                          <th>Quantity</th>
                          <th>Price</th>
                          <th>Location</th>
                          <th class="text-right">Action</th>
                      </tr>
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($product as $row)
                        <tr>
                            <td scope="row">{{ $i }}</td>
                            <td>
                                <a data-lightbox="{{$row->Upload->file_name}}" data-title="{{$row->Upload->file_name}}" href="{{URL::to($row->Upload->file_name)}}" target="_blank">
                                <img src="{{ url($row->Upload->file_name) }}" alt="Snow" style="max-width: 50px; max-height: 50px">
                                </a>
                            </td>
                            <td>
                                <?php
                                $array = explode( "\n", wordwrap( $row->name, 30));
                                echo implode("<br>",$array);
                                ?>
                            </td>
                            {{-- <td>{{ substr($row->name, 0, 35) }}</td> --}}
                            <td>{{ $row->Category->name }}</td>
                            <td>
                                @php
                                    $pro_stock = App\Models\Product_stock::where('product_id', $row->id)->get();
                                    $qty = 0;
                                    foreach($pro_stock as $pt){
                                        $qty += $pt->qty;
                                    }
                                    echo $qty;
                                @endphp
                            </td>
                            <td>{{ format_product_price($row->unit_price, $row->currency_code) }}</td>
                            <td>{{ $row->product_location }}</td>
                            <td class="text-right">
                                <form action="{{ route('sale.product.destroy', $row->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <a href="{{ route('sale.product.edit', $row->id) }}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
                                    <button type="submit" id="btn_delete" class="btn btn-danger btn-xs btn_delete float-left"><i class="fa fa-trash-o"></i></button>
                                </form>
                            </td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                        @endforeach
                    </table>
                  </div>
            </div>
        </div>
        <div class="center_product_1r4 clearfix">
            <div class="col-sm-6">
                <div class="center_product_1r4l clearfix">
                    {{ $product->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script>
         $(document).ready(function () {
            $(".btn_delete").click(function (e) { 
            var form = $(this).closest('form');
            e.preventDefault();
            swal("Are you sure you want to do this?", {
                buttons: ["Cancel", true],
            }).then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
            });
        });
    </script>
    
@endsection