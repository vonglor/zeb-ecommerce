@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <h2 class="intro-y text-lg font-medium mt-10 text-xl">
            All Category
        </h2>
        <div class="grid grid-cols-12 gap-6 mt-5"> 
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                <a href="" class="btn btn-primary shadow-md mr-2 text-base">Add New Category</a>
                <div class="hidden md:block mx-auto text-gray-600"></div>
                
            </div>
            <!-- BEGIN: Data List -->
            <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
                <table class="table table-report -mt-2" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base">No.</th>
                            <th class="whitespace-nowrap text-base">NAME</th>
                            <th class="whitespace-nowrap text-base">BANNER</th>
                            <th class="whitespace-nowrap text-base">META TITLE</th>
                            <th class="text-center whitespace-nowrap text-base">ACTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 0;
                        @endphp
                        @foreach ($category as $item)
                        <tr class="intro-x">
                            <td class="text-base">{{ ++$i }}</td>
                            <td class="text-base">{{$item->name}}</td>
                            <td> <img src="/uploads/images/{{ $item->banner }}" width="100px"></td>
                            <td class="text-base">{{$item->meta_title}}</td>
                            <td class="table-report__action w-56 p-5">
                                <div class="flex justify-center items-center">
                                    <a class="btn btn-primary" href=""> <i data-feather="check-square" class="w-4 h-3 mr-1"></i></a>
                                    <form action="" method="POST" class="px-2">
                                            @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button>
                                    </form>
                                    
                                </div>
                            </td>
                        </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
        </div>
    </div>
    <!-- END: Content -->

    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();    
        });
    </script>
    
@endsection