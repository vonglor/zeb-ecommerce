<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Commission;
use Illuminate\Http\Request;

class CommissionController extends Controller
{
    public function index()
    {
        $c = Commission::find(1);
        if (!$c) {
            toastr()->error(__("Commission Not Found"), __("Errors"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        return view('backend.commissions.index', compact('c'));
    }

    public function update(Request $request, Commission $c)
    {
        $request->validate([
            "amount" => ['required', 'numeric', 'min:10', 'max:100'],
        ]);

        $c->amount = $request->amount;
        if ($c->save()) {
            toastr()->success('Save', 'Successfully', ['timeOut' => 2000]);
            return redirect()->back();
        } else {
            toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
            return redirect()->back();
        }
    }
}