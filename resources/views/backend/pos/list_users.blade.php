
@php
$users = App\Models\User::all();
@endphp

<select class="form-control selectpicker list_users" name="user_id" data-live-search="true" required>
    <option data-tokens="" value="">{{__('Customers')}}</option>
    @if($users->count() > 0)
    @foreach($users as $row)
    <option data-tokens="" value="{{ $row->id }}">{{ $row->name }}</option>
    @endforeach
    @endif
</select>
  