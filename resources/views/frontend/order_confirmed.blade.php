@php
$user = Session::get('user');
@endphp

@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/checkout.css') }}" rel="stylesheet">
    <style>
        .card {
            margin-bottom: 30px;
        }

        .detail span {
            font-size: 14px;
        }

        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            min-height: 1px;
            padding: 1.25rem;
        }

        *,
        ::after,
        ::before {
            box-sizing: border-box;
        }

        div {
            display: block;
        }

        .card {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0, 0, 0, .125);
            border-radius: .25rem;
        }

        .text-center {
            text-align: center !important;
        }

        .pb-4,
        .py-4 {
            padding-bottom: 1.5rem !important;
        }

        .pt-4,
        .py-4 {
            padding-top: 1.5rem !important;
        }

        .mb-4,
        .my-4 {
            margin-bottom: 1.5rem !important;
        }

        .opacity-70 {
            opacity: 0.7 !important;
        }

        .font-italic {
            font-style: italic !important;
            font-size: 13px;
        }

        p {
            margin-top: 0;
            margin-bottom: 1rem;
        }

        .text-primary {
            color: #e62e04 !important;
        }

        .h5 {
            font-size: 20px;
        }

        .mb-3 {
            font-size: 27px;
        }

        i.fa.fa-check-circle-o.la-3x.text-success.mb-3::before {
            font-size: 39px;
        }

        .produt_qty {
            display: flex;
            flex-direction: row;
        }

        .color {
            margin-left: 10px;
            width: 25px;
            height: 25px;
            border: 1px solid #000;
            display: inline-block;
            margin-right: 10px;
            cursor: pointer;
            text-align: center;
            line-height: 35px;
            font-size: 12px;
        }

        .address {
            border: 1px solid #ddd;
            margin-left: 25px;
            padding-left: 15px;
            width: 50%;
        }

        table {
            font-size: 14px;
        }

    </style>
@endsection
@section('content')
    <section id="center" class="clearfix checkout">
        <div class="container">
            <div class="row">
                <section class="py-4">
                    <div class="container text-left">
                        <div class="row">
                            <div class="col-sm-12 mx-auto">
                                <div class="card shadow-sm border-0 rounded">
                                    <div class="card-body">
                                        <div class="text-center py-4 mb-4">
                                            <i class="fa fa-check-circle-o la-3x text-success mb-3"></i>
                                            <h1 class="h3 mb-3 fw-600">Thank You for Your Order!</h1>
                                            <h2 class="h5">Order Number: <span
                                                    class="fw-700 text-primary">{{ $order->code }}</span></h2>
                                            <h2 class="h5">Date: {{ date("M d Y", strtotime($order->date)) }}</h2>
                                        </div>
                                        <div class="mb-4">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="address">
                                                        <p>Name: {{ json_decode($order->shipping_address)->name }}</p>
                                                        <p>Address: {{ json_decode($order->shipping_address)->address }}
                                                        </p>
                                                        <p>City: {{ json_decode($order->shipping_address)->city }}</p>
                                                        <p>Country: {{ json_decode($order->shipping_address)->country }}
                                                        </p>
                                                        <p>Email: {{ json_decode($order->shipping_address)->email }}</p>
                                                        <p>Phone: {{ json_decode($order->shipping_address)->phone }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover">
                                                            Order Details
                                                            <tr>
                                                                <th width="5%">Item</th>
                                                                <th width="10%" class="text-center">Image</th>
                                                                <th>Name</th>
                                                                <th width="10%">qty</th>
                                                                <th width="15%">price</th>
                                                            </tr>
                                                            @php
                                                                $subtotal = 0;
                                                            @endphp
                                                            @foreach (App\Models\Order_detail::where('order_id', $order->id)->get() as $item)
                                                                @php
                                                                    // $subtotal += $item->price * $item->qty;
                                                                    $subtotal += total_price($item->price * $item->qty, $order->currency_code);
                                                                @endphp
                                                                <tr>
                                                                    <td>{{ $item->product_id }}</td>
                                                                    <td>
                                                                        @php
                                                                            $stock = App\Models\Product_stock::find($item->product_id);
                                                                        @endphp
                                                                        @if ($stock != null)
                                                                            @if ($stock->image != null)
                                                                                <img src="{{ url(App\Models\Upload::find($stock->image)->file_name) }}"
                                                                                    alt="" class="img-fluid mb-2"
                                                                                    style="max-height: 80px; max-width:80px; margin-top: 10px;">
                                                                            @else
                                                                                <img src="{{ url(App\Models\Upload::find(App\Models\Product::find($stock->product_id)->photos)->file_name) }}"
                                                                                    class="img-fluid mb-2" alt=""
                                                                                    style="max-height: 80px; max-width:80px; margin-top: 10px;">
                                                                            @endif
                                                                        @else
                                                                            <img src="{{ url(App\Models\Upload::find(App\Models\Product::find($stock->product_id)->photos)->file_name) }}"
                                                                                class="img-fluid mb-2" alt=""
                                                                                style="max-height: 80px; max-width:80px; margin-top: 10px;">
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        <span><?php
                                                                        $array = explode("\n", wordwrap(App\Models\Product::find($item->productStock->product_id)->name, 80));
                                                                        echo implode('<br>', $array);
                                                                        ?></span><br>
                                                                        @if ($item->color)
                                                                            <span>{{ $item->color }}</span><br>
                                                                        @endif
                                                                        @if ($item->size)
                                                                            <span>{{ $item->size }}</span><br>
                                                                        @endif
                                                                        @if ($item->fabric)
                                                                            <span>{{ $item->fabric }}</span><br>
                                                                        @endif
                                                                        @if ($item->poud)
                                                                            <span>{{ $item->poud }}</span>
                                                                        @endif
                                                                    </td>
                                                                    <td>{{ $item->qty }}</td>
                                                                    <td>{{ convert_price($item->price, $order->currency_code) }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3" style="float: right">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            Order Ammount
                                                        </tr>
                                                        <tr>
                                                            <td width="50%">Sub Total:</td>
                                                            <td width="50%">{{ format_price($subtotal) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>tax:</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Shipping:</td>
                                                            <td>{{ convert_price($order->discount, $order->currency_code) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>TOTAL:</td>
                                                            <td>{{ convert_price($order->grand_total, $order->currency_code) }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="back text-center" style="margin: 10px 0;">
                                        <a href="{{ route('product') }}">Go to shopping</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection

<script src=""></script>
