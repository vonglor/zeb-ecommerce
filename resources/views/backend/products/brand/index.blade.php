@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <h2 class="intro-y text-lg font-medium mt-10 text-xl">
            All Brand
        </h2>
        <div class="grid grid-cols-12 gap-6 mt-5"> 
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                <a href="{{route('brand.create')}}" class="btn btn-primary shadow-md mr-2 text-base">Add New Brand</a>
                <div class="hidden md:block mx-auto text-gray-600"></div>
               
            </div>
            <!-- BEGIN: Data List -->
            <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
                <table class="table table-report -mt-2" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base">#</th>
                            <th class="whitespace-nowrap text-base">NAME</th>
                            <th class="whitespace-nowrap text-base">LOGO</th>
                            <th class="whitespace-nowrap text-base">META TITLE</th>
                            <th class="text-center whitespace-nowrap text-base">ACTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 0;
                        @endphp
                        @foreach ($brands as $brand)
                        <tr class="intro-x">
                            <td class="text-base">{{++$i}}</td>
                            <td class="text-base">{{$brand->name}}</td>
                            <td><img src="uploads/images/{{$brand->logo}}" style="max-width: 60px;max-height:60px" alt=""></td>
                            <td class="text-base">{{$brand->meta_title ?? ''}}</td>
                            <td class="table-report__action w-56 p-5">
                                <div class="flex justify-center items-center">
                                    <a class="btn btn-primary" href="{{route('brand.edit', $brand->id)}}"> <i data-feather="check-square" class="w-4 h-3 mr-1"></i></a>
                                    <form action="{{route('brand.destroy', $brand->id)}}" method="POST" class="px-2">
                                            @csrf
                                        @method('DELETE')
                                        {{-- <button class="flex items-center text-theme-24" data-toggle="modal" data-target="#delete-confirmation-modal"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i>Delete</button> --}}
                                        <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button>
                                    </form>
                                    
                                </div>
                            </td>
                        </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
        </div>
    </div>
    <!-- END: Content -->

    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();    
        });
    </script>
    
@endsection