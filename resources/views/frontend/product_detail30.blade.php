@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/detail.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/product-list.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend/css/img-zoom.css') }}">
    <style>
        * {
            box-sizing: border-box;
            }

            /* Hide the images by default */
            .mySlides {
            display: none;
            }

            /* Add a pointer when hovering over the thumbnail images */
            .cursor {
            cursor: pointer;
            }

            /* Next & previous buttons */
            .prev,
            .next {
            cursor: pointer;
            position: absolute;
            top: 40%;
            width: auto;
            padding: 12px;
            margin-top: -50px;
            color: #3665f3;
            font-weight: bold;
            font-size: 20px;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
            }

            /* Position the "next button" to the right */
            .prev {
            left: 0;
            border-radius: 3px 0 0 3px;
            }
            .next {
            right: 0;
            border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover,
            .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
            }

            /* Number text (1/3 etc) */
            .numbertext {
            color: #3665f3;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
            }

            /* Container for image text */
            /* .caption-container {
            text-align: center;
            background-color: #222;
            padding: 2px 16px;
            color: white;
            } */

            .row:after {
            content: "";
            display: table;
            clear: both;
            }

            /* Six columns side by side */
            .column {
            float: left;
            border: 1px solid #ddd;
            border-radius: 4px;
            width: 80px;
            height: 80px;
            }

            /* Add a transparency effect for thumnbail images */
            .demo {
            /* opacity: 0.6; */
            max-width: 70px;
            max-height: 70px;
            padding: 5px;
            align-items: center;
            }

            .active,
            .demo:hover {
            opacity: 1;
            }

            .gallery .active,
            .column:hover {
            border: 2px solid #3665f3;
            }

            .gallery{
                padding-left: 20px;
                padding-right: 20px;
            }


            /* img-zoom css */
            .mySlides {
            position: relative;
            }

            .lens{
                width: 100px;
                height: 100px;
                position: absolute;
                top: 0;
                left: 0;
                border: 2px solid #3665f3;
                background-color: rgba(255, 255, 255, 0.6)
            }
            .result{
                position: absolute;
                left: calc(100% + 50px);
                top: 30px;
                width: 300px;
                height: 300px;
                border: 2px solid #3665f3;
            }
    </style>
    <script>
        function imageZoom(imgID, resultID) {
          var img, lens, result, cx, cy;
          img = document.getElementById(imgID);
          result = document.getElementById(resultID);
          /*create lens:*/
          lens = document.createElement("DIV");
          lens.setAttribute("class", "img-zoom-lens");
          /*insert lens:*/
          img.parentElement.insertBefore(lens, img);
          /*calculate the ratio between result DIV and lens:*/
          cx = result.offsetWidth / lens.offsetWidth;
          cy = result.offsetHeight / lens.offsetHeight;
          /*set background properties for the result DIV:*/
          result.style.backgroundImage = "url('" + img.src + "')";
          result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
          /*execute a function when someone moves the cursor over the image, or the lens:*/
          lens.addEventListener("mousemove", moveLens);
          img.addEventListener("mousemove", moveLens);
          /*and also for touch screens:*/
          lens.addEventListener("touchmove", moveLens);
          img.addEventListener("touchmove", moveLens);
          function moveLens(e) {
            var pos, x, y;
            /*prevent any other actions that may occur when moving over the image:*/
            e.preventDefault();
            /*get the cursor's x and y positions:*/
            pos = getCursorPos(e);
            /*calculate the position of the lens:*/
            x = pos.x - (lens.offsetWidth / 2);
            y = pos.y - (lens.offsetHeight / 2);
            /*prevent the lens from being positioned outside the image:*/
            if (x > img.width - lens.offsetWidth) {x = img.width - lens.offsetWidth;}
            if (x < 0) {x = 0;}
            if (y > img.height - lens.offsetHeight) {y = img.height - lens.offsetHeight;}
            if (y < 0) {y = 0;}
            /*set the position of the lens:*/
            lens.style.left = x + "px";
            lens.style.top = y + "px";
            /*display what the lens "sees":*/
            result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
          }
          function getCursorPos(e) {
            var a, x = 0, y = 0;
            e = e || window.event;
            /*get the x and y positions of the image:*/
            a = img.getBoundingClientRect();
            /*calculate the cursor's x and y coordinates, relative to the image:*/
            x = e.pageX - a.left;
            y = e.pageY - a.top;
            /*consider any page scrolling:*/
            x = x - window.pageXOffset;
            y = y - window.pageYOffset;
            return {x : x, y : y};
          }
        }
        </script>
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Product Detail</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="product" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="product_1 clearfix">
                    <div class="col-sm-12">
                        <div class="center_detail_2 clearfix">
                            <div class="col-sm-6">
                                <span class="war-title">Warning:</span> <span class="war-content"> Make sure you
                                    understand the policy below befor your purchase</span><br>
                                <div class="center_detail_2_left clearfix">
                                    <div class="carousel slide article-slide" id="article-photo-carousel">
                                        <!-- Container for the image gallery -->

                                            <!-- Full-width images with number text -->
                                            <div class="mySlides" style="border: 1px solid#3665f3;">
                                                <div class="numbertext">1</div>
                                                <img src="{{ url($detail->Upload->file_name) }}" class="img-responsive image" style="width:100%">
                                                <div class="lens"></div>
                                                <div class="result"></div>
                                            </div>
                                            @if ($detail->thumbnail_img != null)
                                                @php
                                                    $i = 2;
                                                    $thumb = explode(',', $detail->thumbnail_img);
                                                @endphp
                                                @foreach ($thumb as $key => $img)
                                                    @php
                                                        $thumbnail = App\Models\Upload::where('id', $img)->first();
                                                    @endphp
                                                    <div class="mySlides">
                                                        <div class="numbertext">{{ $i }}</div>
                                                        <img src="{{ url($thumbnail->file_name) }}" class="img-responsive" style="width:100%">
                                                    </div>
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            @endif
                                        
                                            <!-- Next and previous buttons -->
                                            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                                            <a class="next" onclick="plusSlides(1)">&#10095;</a>
                                        
                                            <!-- Image text -->
                                            <div class="caption-container">
                                            <p id="caption"></p>
                                            </div>
                                        
                                            <!-- Thumbnail images -->
                                            <div class="row gallery">
                                                <div class="column cursor" onclick="currentSlide(1)">
                                                    <img class="demo img-responsive" src="{{ url($detail->Upload->file_name) }}" style="width:100%">
                                                </div>
                                                @if ($detail->thumbnail_img != null)
                                                    @php
                                                        $i = 2;
                                                        $thumb = explode(',', $detail->thumbnail_img);
                                                    @endphp
                                                    @foreach ($thumb as $key => $img)
                                                        @php
                                                            $thumbnail = App\Models\Upload::where('id', $img)->first();
                                                        @endphp
                                                        <div class="column cursor" onclick="currentSlide({{ $i }})">
                                                            <img class="demo img-responsive" src="{{ url($thumbnail->file_name) }}" style="max-width: 70px; max-height:70px;">
                                                        </div>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @endif
                                            </div>
                                    </div>
                                </div>
                                <span class="war-video"></span> <span class="war-content">
                                    @if ($detail->videos != null)
                                        <a href="{{ $detail->videos }}" target="_blank">{{ $detail->videos }}</a>
                                    @else
                                        No video link
                                    @endif
                                </span>
                            </div>
                            <div class="col-sm-6">
                                <div class="center_detail_2_right clearfix">
                                    <form action="{{ route('add_to_cart') }}" id="form-add-cart" method="POST">
                                        @csrf
                                        <div class="center_detail_2_right_inner clearfix">
                                            <h4>
                                                <?php
                                                $array = explode("\n", wordwrap($detail->name, 50));
                                                echo implode('<br>', $array);
                                                ?>
                                            </h4>
                                            <h4 class="reviewed">
                                                <a href="#">3 reviewed</a>
                                            </h4>
                                            <h5><span>Condition:</span>{{ $detail->condition }}</h5>

                                            @if (count(json_decode($detail->colors)) > 0)
                                                <div class="pd_n clearfix color-choose">
                                                    <h5 class="bold">Color:
                                                    </h5>
                                                    <ul>
                                                        @php
                                                            $i = 1;
                                                        @endphp
                                                        @foreach (json_decode($detail->colors) as $key => $color)
                                                            @php
                                                                $colors = App\Models\Color::where('name', $color)->first();
                                                            @endphp
                                                            <input type="radio" name="color" id="{{ $color }}"
                                                                class="radio_{{ $i }}"
                                                                value="{{ $color }}" style="display: none">
                                                            <label for="{{ $color }}">
                                                                <li style="background: {{ $colors->code }}"
                                                                    id="color_{{ $i }}"
                                                                    name="{{ $color }}"></li>
                                                            </label>
                                                            @php
                                                                $i++;
                                                            @endphp
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            @if ($detail->choice_options != null && count(json_decode($detail->choice_options)) > 0)
                                                @foreach (json_decode($detail->choice_options) as $key => $choice)
                                                    <div class="pd_n_1 clearfix choice_options">
                                                        @php
                                                            $option = App\Models\Attributes::find($choice->attribute_id);
                                                        @endphp
                                                        <h5 class="bold">{{ $option->name }}:
                                                        </h5>
                                                        <ul>
                                                            @php
                                                                $i = 1;
                                                            @endphp
                                                            @foreach ($choice->values as $key => $value)
                                                                <label>
                                                                    <input type="radio" name="{{ $option->name }}"
                                                                        id="{{ $option->name }}"
                                                                        class="radio_{{ $i }}"
                                                                        value="{{ $value }}" style="display: none">
                                                                    <li id="{{ $option->name . $value }}"
                                                                        name="{{ $value }}"
                                                                        class="{{ $option->name }}_{{ $i }}"
                                                                        data="{{ $option->name }}">{{ $value }}
                                                                    </li>
                                                                </label>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endforeach
                                            @else
                                                <input type="hidden" id="Size" name="Size" value="">
                                                <input type="hidden" id="Fabric" name="Fabric" value="">
                                                <input type="hidden" id="Poud" name="Poud" value="">
                                            @endif
                                            <h5>
                                                <span>Price:</span>
                                                @if ($detail->discount != null)
                                                    <span
                                                        class="col_2">${{ number_format($detail->unit_price, 2) }}</span>
                                                    $<span
                                                        id="show_price">{{ number_format($detail->unit_price - $detail->discount, 2) }}</span>
                                            </h5>
                                            @else
                                            $<span id="show_price">{{ number_format($detail->unit_price, 2) }}</span>
                                            </h5>
                                            @endif
                                            <div class="row">
                                                <h5 class="col-sm-2">
                                                    Quatity:
                                                </h5>
                                                <div class="input-group number-spinner col-sm-4">
                                                    <span class="input-group-btn">
                                                        <a class="btn btn-default" data-dir="dwn"><span
                                                                class="glyphicon glyphicon-minus"></span></a>
                                                    </span>
                                                    <input type="text" name="qty" class="form-control text-center" value="1">
                                                    <span class="input-group-btn">
                                                        <a class="btn btn-default" data-dir="up"><span
                                                                class="glyphicon glyphicon-plus"></span></a>
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <h5>ItemID: {{ $detail->id }}</h5>
                                                </div>
                                                <div class="col-sm-4">
                                                    <h5>Total:
                                                        @php
                                                            $pro_stock = App\Models\Product_stock::where('product_id', $detail->id)->get();
                                                            $qty = 0;
                                                            foreach ($pro_stock as $pt) {
                                                                $qty += $pt->qty;
                                                            }
                                                            echo $qty;
                                                        @endphp
                                                    </h5>
                                                </div>
                                            </div>
                                            <h5>Shipping: @php
                                                if($detail->shipping != null){
                                                    echo $detail->shipping;
                                                }else{
                                                    echo 'free';
                                                }
                                            @endphp</h5>
                                            <input type="hidden" name="product_id" id="product_id"
                                                value="{{ $detail->id }}">
                                        </div>
                                        <div class="center_detail_2_right_inner_1 clearfix">
                                            <ul>
                                                <li><button type="submit" id="add_to_cart" class="btn-addcart">
                                                        <i class="fa fa-shopping-bag"></i> ADD TO CART</button>
                                                </li>
                                                <li><a href="#" id="c" class="btn-buy"> Order</a></li>
                                            </ul>
                                        </div>
                                    </form>
                                    <div class="center_detail_2_right_inner_2 clearfix">
                                        @if ($detail->delivery)
                                            <p class="mgt">
                                                <span class="col-sm-2 delivery-title">Delivery:</span> <span
                                                    class="col-sm-10 delivery">{{ $detail->delivery }}</span>
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="center_detail_2 clearfix">
                            <div class="center_detail_2_right_inner_2 clearfix">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home_description">Seller
                                            Description</a>
                                    </li>
                                    <li class=""><a data-toggle="tab" href="#menu_information">Policy
                                            return</a></li>
                                </ul>

                                <div class="tab-content clearfix">
                                    <div id="home_description" class="tab-pane fade clearfix active in">
                                        <div class="click clearfix">
                                            <p>{{ $detail->description }}</p>
                                        </div>
                                    </div>
                                    <div id="menu_information" class="tab-pane fade clearfix ">
                                        <div class="click clearfix">
                                            <p>Praesent libero Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at
                                                nibh elementum imperdiet. Duis sagittis ipsum.Praesent mauris. Fusce nec
                                                tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu
                                                eget nulla.Class aptent taciti sociosqu ad litora torquent per conubia
                                                nostra, per inceptos himenaeos. Curabitursodales ligula in libero.Sed
                                                dignissim lacinia nunc.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="product_list">
        <div class="container" style="background: #fff; padding: 15px 0 30px 0; border: 1px solid #f3f3f3;">
            <div class="row">
                <div class="product_list text-center clearfix">
                    <div class="col-sm-12">
                        <h3 class="mgt">Product Items</h3>
                        <hr>
                    </div>
                </div>
                <div class="gallery_1 clearfix">
                    <div class="col-sm-12">
                        <div class="workout_page_1_left clearfix">

                            <div class="tab-content clearfix">
                                <div id="home" class="tab-pane fade  clearfix active in">
                                    <div class="click clearfix">
                                        <div class="home_inner clearfix">
                                            @php
                                                $product = App\Models\Product::paginate(8);
                                            @endphp
                                            @foreach ($product as $products)
                                                <div class="col-sm-3 offset-md-0 offset-sm-1 pt-md-0 pt-4">
                                                    <div class="card"> <a
                                                            href="{{ route('product_detail', $products->id) }}"><img
                                                                class="card-img-top"
                                                                src="{{ url($products->Upload->file_name) }}"></a>
                                                        <div class="card-body">
                                                            <h5 class="font-weight-bold pt-1"><a
                                                                    href="{{ route('product_detail', $products->id) }}">{{ Str::limit($products->name, 27) }}</a>
                                                            </h5>
                                                            <div
                                                                class="d-flex align-items-center justify-content-between pt-3">
                                                                <div class="d-flex flex-column">
                                                                    <div class="h5 font-weight-bold">
                                                                        @if ($products->discount)
                                                                            <span
                                                                                class="col_2">${{ number_format($products->unit_price, 2) }}</span>
                                                                        @endif
                                                                        ${{ number_format($products->unit_price - $products->discount, 2) }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_1_last text-center clearfix">
                    <div class="col-sm-12">
                        <ul>
                            {!! $product->links() !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
<script src=""></script>
@section('script')
    <script type="text/javascript">
        ! function($) {

            "use strict"; // jshint ;_;


            /* MAGNIFY PUBLIC CLASS DEFINITION
             * =============================== */

            var Magnify = function(element, options) {
                this.init('magnify', element, options);
            }

            Magnify.prototype = {

                constructor: Magnify

                    ,
                init: function(type, element, options) {
                    var event = 'mousemove',
                        eventOut = 'mouseleave';

                    this.type = type;
                    this.$element = $(element);
                    this.options = this.getOptions(options);
                    this.nativeWidth = 0;
                    this.nativeHeight = 0;

                    this.$element.wrap('<div class="magnify" \>');
                    this.$element.parent('.magnify').append('<div class="magnify-large" \>');
                    this.$element.siblings(".magnify-large").css("background", "url('" + this.$element.attr("src") +
                        "') no-repeat");

                    this.$element.parent('.magnify').on(event + '.' + this.type, $.proxy(this.check, this));
                    this.$element.parent('.magnify').on(eventOut + '.' + this.type, $.proxy(this.check, this));
                },
                getOptions: function(options) {
                    options = $.extend({}, $.fn[this.type].defaults, options, this.$element.data())

                    if (options.delay && typeof options.delay == 'number') {
                        options.delay = {
                            show: options.delay,
                            hide: options.delay
                        }
                    }

                    return options;
                },
                check: function(e) {
                    var container = $(e.currentTarget);
                    var self = container.children('img');
                    var mag = container.children(".magnify-large");

                    // Get the native dimensions of the image
                    if (!this.nativeWidth && !this.nativeHeight) {
                        var image = new Image();
                        image.src = self.attr("src");

                        this.nativeWidth = image.width;
                        this.nativeHeight = image.height;

                    } else {

                        var magnifyOffset = container.offset();
                        var mx = e.pageX - magnifyOffset.left;
                        var my = e.pageY - magnifyOffset.top;

                        if (mx < container.width() && my < container.height() && mx > 0 && my > 0) {
                            mag.fadeIn(100);
                        } else {
                            mag.fadeOut(100);
                        }

                        if (mag.is(":visible")) {
                            var rx = Math.round(mx / container.width() * this.nativeWidth - mag.width() / 2) * -1;
                            var ry = Math.round(my / container.height() * this.nativeHeight - mag.height() / 2) * -
                                1;
                            var bgp = rx + "px " + ry + "px";

                            var px = mx - mag.width() / 2;
                            var py = my - mag.height() / 2;

                            mag.css({
                                left: px,
                                top: py,
                                backgroundPosition: bgp
                            });
                        }
                    }

                }
            }

            /* MAGNIFY PLUGIN DEFINITION
             * ========================= */

            $.fn.magnify = function(option) {
                return this.each(function() {
                    var $this = $(this),
                        data = $this.data('magnify'),
                        options = typeof option == 'object' && option
                    if (!data) $this.data('tooltip', (data = new Magnify(this, options)))
                    if (typeof option == 'string') data[option]()
                })
            }

            $.fn.magnify.Constructor = Magnify

            $.fn.magnify.defaults = {
                delay: 0
            }


            /* MAGNIFY DATA-API
             * ================ */

            $(window).on('load', function() {
                $('[data-toggle="magnify"]').each(function() {
                    var $mag = $(this);
                    $mag.magnify()
                })
            })

        }(window.jQuery);


        $(document).ready(function() {
            $('.color-choose label #color_1').addClass('act');
            $('.color-choose .radio_1').attr('checked', true);
            $('.color-choose label li').on('click', function() {
                $('.act').removeClass('act');
                $(this).addClass('act');
                var colors = $(this).attr('name');
                var Size = $('#Size').val();
                var Fabric = $('#Fabric').val();
                var Poud = $('#Poud').val();
                var product_id = $('#product_id').val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "POST",
                    url: "{{ route('select') }}",
                    data: {
                        Size: Size,
                        Fabric: Fabric,
                        Poud: Poud,
                        colors: colors,
                        product_id: product_id,
                        _token: _token,
                    },
                    dataType: 'JSON',
                    success: function(data) {
                        $('#show_price').html(data.price);
                    }
                });
            });

            $('.choice_options label .Size_1').addClass('act_1');
            $('.choice_options label .Fabric_1').addClass('act_1');
            $('.choice_options label .Poud_1').addClass('act_1');
            $('.choice_options .radio_1').attr('checked', true);
            $('.choice_options label li').on('click', function() {
                var att = $(this).attr('id');
                var data = $(this).attr('data');
                $('.choice_options .act_1').removeClass('act_1');
                $('.choice_options #' + att).addClass('act_1');

                var colors = $('#color').val();
                var Size = $('#Size').val();
                var Poud = $(this).attr('name');
                var Fabric = $('#Fabric').val();
                var product_id = $('#product_id').val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "POST",
                    url: "{{ route('select') }}",
                    data: {
                        Size: Size,
                        Fabric: Fabric,
                        Poud: Poud,
                        colors: colors,
                        product_id: product_id,
                        _token: _token,
                    },
                    dataType: 'JSON',
                    success: function(data) {
                        $('#show_price').html(data.price);
                    }
                });
            });

            $('#btn-buy').click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: '{{ route('buy') }}',
                    data: $('#form-add-cart').serialize(),
                    success: function(data) {
                        window.location.href = "/cart";
                    }
                });
            });
        });
    </script>

    <script type="text/javascript">
        var price = $('#show_price').text();
        $(document).on('click', '.number-spinner .btn', function() {
            var btn = $(this),
                oldValue = btn.closest('.number-spinner').find('input').val().trim();
            newVal = 0;
            if (btn.attr('data-dir') == 'up') {
                newVal = parseInt(oldValue) + 1;
            } else {
                if (oldValue > 1) {
                    newVal = parseInt(oldValue) - 1;
                } else {
                    newVal = 1;
                }
            }
            var subtotal = price * newVal;
            subtotal = subtotal.toFixed(2);
            btn.closest('.number-spinner').find('input').val(newVal);
            $('#subtotal').html(subtotal);
        });
    </script>

    <script>
        var slideIndex = 1;
        showSlides(slideIndex);
        
        function plusSlides(n) {
        showSlides(slideIndex += n);
        }
        
        function currentSlide(n) {
        showSlides(slideIndex = n);
        }
        
        function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("column");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        captionText.innerHTML = dots[slideIndex-1].alt;
        }
    </script>

    {{-- img-zoom --}}
    <script>
       
            const mySlides = document.querySelector(".mySlides");
            const image = document.querySelector(".image");
            const lens = document.querySelector(".lens");
            const result = document.querySelector(".result");

            const mySlidesRect = mySlides.getBoundingClientRect()
            const imageRect = image.getBoundingClientRect()
            const lensRect = lens.getBoundingClientRect()
            const resultRect = result.getBoundingClientRect()

            mySlides.addEventListener("mousemove", zoomImage);
            function zoomImage(e){
                console.log("zoom image", e.clientX, e.clientY);
                let x = e.clientX - mySlidesRect.left - lensRect.width / 2
                let y = e.clientY - mySlidesRect.top - lensRect.height / 2

                let minX = 0
                let minY = 0
                let maxX = mySlidesRect.width - lensRect.width
                let maxY = mySlidesRect.height - lensRect.height

                if(x <= minX){
                    x = minX
                }else if(x >= maxX){
                    x = maxX
                }

                if(y <= minY){
                    y = minY
                }else if(y >= maxY){
                    y = maxY
                }
                
                lens.style.left = x + "px"
                lens.style.top = y + "px"
            }

    </script>
@endsection
