@extends('frontend.saller.content_left')
@section('css')
<link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
<div class="col-sm-9">
    <div class="centent_card_1r clearfix">
        <div class="centent_card_1r3 tab-content">
            <p>Shop</p>
            <div class="card-content">
                <div class="table-responsive">
                    <table class="table table-hover">
                      <tr>
                          <th>#</th>
                          <th width="">Logo</th>
                          <th width="">Shop name</th>
                          <th width="">Address</th>
                          <th width="">Facebook</th>
                          <th width="">Youtube</th>
                      </tr>
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($member as $row)
                        <tr>
                            <td scope="row">{{ $i }}</td>
                            <td>
                                @if ($row->logo)
                                <a data-lightbox="{{$row->logo}}" data-title="{{$row->logo}}" href="{{URL::to($row->logo)}}" target="_blank">
                                    <img id="myImg" src="{{ url($row->logo) }}" alt="" style="max-width: 50px; max-height: 50px">
                                </a>
                                @endif
                                
                            </td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->address }}</td>
                            <td>{{ $row->facebook }}</td>
                            <td>{{ $row->youtube }}</td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                        @endforeach
                    </table>
                  </div>
            </div>
        </div>
        <div class="center_product_1r4 clearfix">
            <div class="col-sm-6">
                <div class="center_product_1r4l clearfix">
                    {{ $member->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
