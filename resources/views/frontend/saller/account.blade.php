@php
    $user_id = Session::get('user')->id;
    $user = App\Models\User::find($user_id);
    if($user->user_type == 'sale'){
        $seller = App\Models\Seller::where('user_id', $user->id)->first();
    }elseif ($user->user_type == 'buy') {
        $seller = App\Models\Customer::where('user_id', $user->id)->first();
    }
    
@endphp
@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <div class="col-sm-9">
        <p class="title_header">Manage Profile</p>
        <div class="centent_card_1r clearfix">
            <div class="centent_card_1r3 tab-content">
                    <div class="card-content">
                        <h4>Basic Info</h4>
                        <hr>
                        <p class="profile">
                            @if ($user->photo == null)
                            <img src="{{ asset('frontend/img/1.jpg') }}" class="rounded img-rounded" id="profile_img" style="max-width: 100px; max-height: 100px;">
                            @else
                            <img src="{{ asset(App\Models\Upload::find($user->photo)->file_name) }}" class="rounded img-rounded" style="max-width: 100px; max-height: 100px;">
                            @endif
                        </p>
                        <p><b>Your ID: </b> <span>{{ $user->id }}</span></p>
                        <p><b>Name: </b> <span id="edit_name" style="cursor: pointer;">{{ $user->name.' '.$user->surname }}</span></p>
                        <p><b>Email: </b> <span id="edit_email" style="cursor: pointer;">{{ $user->email }}</span></p>
                        <p><b>Phone: </b> <span id="edit_phone" style="cursor: pointer;">{{ $user->phone }}</span></p>
                        <p><b>Gender: </b> <span>{{ $user->gender }}</span></p>
                        <p><b>Data of birth: </b> <span>{{ date('d-m-Y', strtotime($user->date_of_birth)) }}</span></p>
                        <p><b>Refer ID: </b> <span>{{ $user->refer_id }}</span></p>
                        <p><b>Refer code others: </b> <span>{{ $user->refer_code_others }}</span></p>
                        <p><b>Card ID: </b> <span>{{ $seller->card_id }}</span></p>
                        <p><b>SS ID: </b> <span>{{ $seller->card_id }}</span></p>
                    </div>
                    <div class="card-content">
                        <hr>
                        <h4>Payment Info</h4>
                        <hr> 
                        <div class="payment" style="padding-bottom: 15px;">
                            <p><b>Bank Name: </b><span>{{ $seller->bank_name }}</span> <a style="float: right; margin-right: 15px; cursor: pointer;">Edit</a></p>
                            <p><b>Bank Account Name: </b> <span>{{ $seller->bank_acc_name }}</span></p>
                            <p><b>Bank Account Number: </b> <span>{{ $seller->bank_acc_no }}</span></p>
                        </div>
                    </div>
                    <div class="card-content">
                        <hr>
                        <h4>Address info</h4>
                        <hr>
                        <div class="row">
                            @php
                                $address = App\Models\Address::where('user_id', $user->id)->get();
                            @endphp
                            @foreach ($address as $item)
                            <div class="col-sm-5 address">
                                <div class="float-right">
                                    <form action="{{ route('address.destroy', $item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" id="btn_delete" class="btn btn-danger btn-xs btn_delete float-right" style="float: right; margin-right: 10px; margin-top:15px; cursor: pointer;"><i class="fa fa-trash-o"></i></button>
                                        <a id="{{ $item->id }}" class="btn btn-info btn-xs edit_address" style="float: right; margin-right: 10px; margin-top:15px; cursor: pointer;"><i class="fa fa-edit"></i></a>
                                    </form>
                                </div>
                                <p><b>Address: </b> {{ $item->address }} 
                                    {{-- <a id="" href="{{ route('address.delete', $item->id) }}" style="float: right; margin-right: 10px; cursor: pointer;">Delete</a>
                                    <a id="{{ $item->id }}" class="edit_address" style="float: right; margin-right: 10px; cursor: pointer;">Edit</a> --}}
                                </p>
                                <p><b>Postal Code: </b> {{ $item->postal_code }}</p>
                                <p><b>State: </b> {{ $item->province }}</p>
                                <p><b>City: </b> {{ $item->city }}</p>
                                <p><b>Country: </b> {{ $item->country }}</p>
                                <p><b>Phone: </b> {{ $item->phone }}</p>
                            </div>
                            @endforeach
                            <div class="col-sm-5 address">
                                <div class="add_address">
                                    <i class="fa fa-plus la-2x mb-3"></i>
                                    <div class="alpha-7">Add New Address</div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    {{-- modal add new address --}}
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Address</h4>
                </div>
                <form class="form-horizontal" action="{{ route('address.store') }}" id="form_address"
                    method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="address" id="address"
                                    placeholder="Your address" required></textarea>
                                <span id="msg_address" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Country</label>
                            <div class="col-sm-10">
                                <select name="country" class="form-control" id="" required>
                                    <option value="">Select Country</option>
                                    @foreach (\App\Models\Country::all() as $country)
                                        <option value="{{ $country->name }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                                <span id="msg_country" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">City</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="city" id="city" placeholder="City"
                                    required>
                                <span id="msg_city" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Postal Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="postal_code" id="postal_code"
                                    placeholder="Postal Code" required>
                                <span id="msg_postal_code" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="phone" id="phone"
                                    placeholder="Phone" required>
                                <span id="msg_phone" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                                <input type="hidden" class="form-control" name="user_id" id="user_id"
                                    value="{{ $user->id }}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{-- modal edit address --}}
    <div class="modal fade" id="myModalEditAddress" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Address</h4>
                </div>
                <form class="form-horizontal" action="{{ route('address.address_update') }}" id="form_address"
                    method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="address_id" id="address_id">
                                <textarea class="form-control" name="address" id="address"
                                    placeholder="Your address" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Country</label>
                            <div class="col-sm-10">
                                <select name="country" class="form-control" id="country" required>
                                    <option value="">Select Country</option>
                                    @foreach (\App\Models\Country::all() as $country)
                                        <option value="{{ $country->name }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">State</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="state" id="state" placeholder="State"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">City</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="city" id="city" placeholder="City"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Postal Code</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="postal_code" id="postal_code"
                                    placeholder="Postal Code" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="phone" id="phone"
                                    placeholder="Phone" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="myModalProfile" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update image profile</h4>
                </div>
                <form class="form-horizontal" action="{{ route('user.update_profile') }}" id="form_address"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Image</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <input type="file" class="form-control" name="photo" accept="image/*" id="photo"
                                    placeholder="Postal Code" required>
                                <span id="photo" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="myModalName" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update name</h4>
                </div>
                <form class="form-horizontal" action="{{ route('user.update_name') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <input type="text" class="form-control" name="name" id="name"
                                    placeholder="Name" value="{{ $user->name }}" required>
                                <span id="name" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Surname</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="surname" id="surname"
                                    placeholder="Surname" value="{{ $user->surname }}" required>
                                <span id="surname" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div class="modal fade" id="myModalEmail" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Email</h4>
                </div>
                <form class="form-horizontal" action="{{ route('user.update_email') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <input type="email" class="form-control" name="email" id="email"
                                    placeholder="Email" value="{{ $user->email }}" required>
                                <span id="email" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="myModalPhone" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Phone</h4>
                </div>
                <form class="form-horizontal" action="{{ route('user.update_phone') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <input type="number" class="form-control" name="phone" id="phone"
                                    placeholder="Email" value="{{ $user->phone }}" required>
                                <span id="phone" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{-- modal payment --}}
    <div class="modal fade" id="myModalPayment" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Payment Setting</h4>
                </div>
                <form class="form-horizontal" action="{{ route('user.update_payment') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Bank Name</label>
                            <div class="col-sm-7">
                                <input type="hidden" name="seller_id" value="{{ $seller->id }}">
                                <input type="hidden" name="user_type" value="{{ $user->user_type }}">
                                <input type="text" class="form-control" name="bank_name" id="bank_name"
                                    placeholder="Bank Name" value="{{ $seller->bank_name }}" required>
                                <span id="bank_name" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Bank Account Name</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="bank_acc_name" id="bank_acc_name"
                                    placeholder="Bank Name" value="{{ $seller->bank_acc_name }}" required>
                                <span id="bank_acc_name" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Bank Acount Number</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="bank_acc_no" id="bank_acc_no"
                                    placeholder="Bank Name" value="{{ $seller->bank_acc_no }}" required>
                                <span id="bank_acc_no" class="alert-message"
                                    style="color: red; font-size: 12px;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.add_address').click(function() {
                $('#myModal').modal('show');
            });

            $('.profile img').click(function(){
                $('#myModalProfile').modal('show');
            });

            $('#edit_name').click(function(){
                $('#myModalName').modal('show');
            });
            $('#edit_email').click(function(){
                $('#myModalEmail').modal('show');
            });
            $('#edit_phone').click(function(){
                $('#myModalPhone').modal('show');
            });

            $('.payment a').click(function(){
                $('#myModalPayment').modal('show');
            });

            $('.address .edit_address').click(function(){
                var id = $(this).attr('id');
                $.ajax({
                    type: "get",
                    url: 'address/edit/'+id,
                    success: function (data) {
                        $('#myModalEditAddress').modal('show');
                        $('#myModalEditAddress #address_id').val(data.id);
                        $('#myModalEditAddress #address').val(data.address);
                        $('#myModalEditAddress #country').val(data.country);
                        $('#myModalEditAddress #state').val(data.province);
                        $('#myModalEditAddress #city').val(data.city);
                        $('#myModalEditAddress #postal_code').val(data.postal_code);
                        $('#myModalEditAddress #phone').val(data.phone);
                    }
                });
            });

            $(".btn_delete").click(function (e) { 
            var form = $(this).closest('form');
            e.preventDefault();
            swal("Are you sure you want to do this?", {
                buttons: ["Cancel", true],
            }).then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
            });
        });
    </script>
@endsection
