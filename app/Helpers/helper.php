<?php 

use App\Models\Product;
use App\Models\Currency;
use App\Models\Exchangerate;
use App\Models\BusinessSetting;

//converts currency to home default currency
if (! function_exists('convert_price')) {
    function convert_price($price, $cur_code)
    {
        $code = Currency::findOrFail(BusinessSetting::where('type', 'system_default_currency')->first()->value)->code;
        if(Session::has('currency_code')){
            $currency_to = Currency::where('code', Session::get('currency_code', $code))->first();
        }
        else{
            $currency_to = Currency::where('code', $code)->first();
        }
        $exchange = Exchangerate::where('from', $cur_code)
                    ->where('to', $currency_to->code)
                    ->get();

        foreach ($exchange as $key => $value) {
            switch ($value->operator) {
                case '*': 
                    $price = $price * $value->exchange;
                    break;
                case '/': 
                    $price= $price / $value->exchange;
                    break;
                default: 
                    $price;
                    break;
            }
        }
        
        return format_price($price);
    }
}

if (! function_exists('total_price')) {
    function total_price($price, $cur_code)
    {
        $code = Currency::findOrFail(BusinessSetting::where('type', 'system_default_currency')->first()->value)->code;
        if(Session::has('currency_code')){
            $currency_to = Currency::where('code', Session::get('currency_code', $code))->first();
        }
        else{
            $currency_to = Currency::where('code', $code)->first();
        }
        $exchange = Exchangerate::where('from', $cur_code)
                    ->where('to', $currency_to->code)
                    ->get();

        foreach ($exchange as $key => $value) {
            switch ($value->operator) {
                case '*': 
                    $price = $price * $value->exchange;
                    break;
                case '/': 
                    $price= $price / $value->exchange;
                    break;
                default: 
                    $price;
                    break;
            }
        }
        
        return $price;
    }
}

//payment price
if(! function_exists('payment_price')) {
    function payment_price($price, $cur_code){
        $exchange = Exchangerate::where('from', $cur_code)
                    ->where('to', 'USD')
                    ->get();

        foreach ($exchange as $key => $value) {
            switch ($value->operator) {
                case '*': 
                    $price = $price * $value->exchange;
                    break;
                case '/': 
                    $price= $price / $value->exchange;
                    break;
                default: 
                    $price;
                    break;
            }
        }
        
        return $price;
    }
}

//formats currency
if (! function_exists('format_price')) {
    function format_price($price)
    {
        $fomated_price = number_format($price, 2);
        return currency_symbol().$fomated_price;
    }
}

// currency symbol
if (! function_exists('currency_symbol')) {
    function currency_symbol()
    {
        $code = Currency::findOrFail(BusinessSetting::where('type', 'system_default_currency')->first()->value)->code;
        if(Session::has('currency_code')){
            $currency = Currency::where('code', Session::get('currency_code', $code))->first();
        }
        else{
            $currency = Currency::where('code', $code)->first();
        }
        return $currency->symbol;
    }
}

// currency code
if (! function_exists('currency_code')) {
    function currency_code()
    {
        $code = Currency::findOrFail(BusinessSetting::where('type', 'system_default_currency')->first()->value)->code;
        if(Session::has('currency_code')){
            $currency = Currency::where('code', Session::get('currency_code', $code))->first();
        }
        else{
            $currency = Currency::where('code', $code)->first();
        }
        return $currency->code;
    }
}

// currency symbol account
if (! function_exists('currency_symbol_account')) {
    function currency_symbol_account()
    {
        $user = Session::get('user');
        $currency = Currency::where('code', $user->currency)->first();
        return $currency->symbol;
    }
}

// currency code account
if (! function_exists('currency_code_account')) {
    function currency_code_account()
    {
        $user = Session::get('user');
        $currency = Currency::where('code', $user->currency)->first();
        return $currency->code;
    }
}

//formats price to home default price with convertion
if (! function_exists('single_price')) {
    function single_price($price)
    {
        // return format_price(convert_price($price));
        $user = Session::get('user');
        $exchange = Exchangerate::where('from', 'USD')
                    ->where('to', $user->currency)
                    ->get();

        foreach($exchange as $key => $value){
            switch ($value->operator) {
                case '*':
                    $price = $price * $value->exchange;
                    break;

                case '/':
                    $price = $price / $value->exchange;
                    break;
                
                default:
                    $price;
                    break;
            }
        }

        return currency_symbol_account().number_format($price, 2);
    }
}

//convert balance

if(! function_exists('convert_balance')){
    function convert_balance($balance){
        $user = Session::get('user');
        $currency = Currency::where('code', $user->currency)->first();
        $exchange = Exchangerate::where('from', 'USD')
                    ->where('to', $user->currency)
                    ->get();

        foreach($exchange as $key => $value){
            switch ($value->operator) {
                case '*':
                    $balance = $balance * $value->exchange;
                    break;

                case '/':
                    $balance = $balance / $value->exchange;
                    break;
                
                default:
                    $balance;
                    break;
            }
        }

        return $currency->symbol.number_format($balance, 2);
    }
}

// format product price
if(! function_exists('format_product_price')){
    function format_product_price($price, $curr_code){
        $currency = Currency::where('code', $curr_code)->first();
        return $currency->symbol.number_format($price, 2);
    }
}

// usd symbol
if(! function_exists('usd_symbol')){
    function usd_symbol(){
        $currency = Currency::where('code', 'USD')->first();
        return $currency->symbol;
    }
}
//pos cart price
if(! function_exists('pos_price')) {
    function pos_price($price, $cur_code){
        $user = Session::get('user');
        $exchange = Exchangerate::where('from', $cur_code)
                    ->where('to', $user->currency)
                    ->get();

        foreach ($exchange as $key => $value) {
            switch ($value->operator) {
                case '*': 
                    $price = $price * $value->exchange;
                    break;
                case '/': 
                    $price= $price / $value->exchange;
                    break;
                default: 
                    $price;
                    break;
            }
        }
        // return $price;
        return format_number($price);
    }
}


if(! function_exists('format_number')){
    function format_number($num){
        return number_format($num, 2);
    }
}