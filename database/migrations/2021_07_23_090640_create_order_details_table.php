<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('seller_id')->nullable();
            $table->integer('product_id');
            $table->string('variantion')->nullable();
            $table->double('price', 20, 2);
            $table->double('tax', 10, 2);
            $table->double('shipping_cost', 20, 2);
            $table->integer('qty');
            $table->string('payment_status', 10)->default('unpaid');
            $table->string('delivery_status', 20)->default('pending');
            $table->string('shipping_type', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
