@php
$user = Session::get('user');
@endphp

@extends('frontend.layouts.app')

@section('css')
    <link href="{{ asset('frontend/css/cart.css') }}" rel="stylesheet">
    <style>
        .cart-emtry{
            margin-top: 20px;
        }

    </style>
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Cart</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cart" class="clearfix">
        <div class="container">
            <div class="row">

                @if (Session::get('cart') != null && count(Session::get('cart')) > 0)
                    <div class="cart_1 clearfix">
                        <div class="col-sm-2">
                            <div class="cart_1i clearfix">
                                <h4 class="mgt col">PRODUCT</h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="cart_1i clearfix">
                                <h4 class="mgt col">NAME</h4>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="cart_1i clearfix">
                                <h4 class="mgt col">UNIT PRICE</h4>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="cart_1i clearfix">
                                <h4 class="mgt col">QUANTITY</h4>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="cart_1i clearfix">
                                <h4 class="mgt col">TOTAL</h4>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="cart_1i clearfix text-center">
                                <h4 class="mgt col"><a href="{{ route('cart_destroy') }}" class="btn btn-danger"><i
                                            class=""></i> Clear</a></h4>
                            </div>
                        </div>
                    </div>
                @endif

                @php $total = 0; @endphp
                @if (Session::get('cart') != null && count(Session::get('cart')) > 0)

                @php
                    $admin_products = array();
                    $seller_products = array();
                    $cart = Session::get('cart');
                    foreach ($cart as $key => $cartItem) {
                        if(App\Models\Product::find(App\Models\Product_stock::find($cartItem['id'])->product_id)->added_by == 'admin'){
                                array_push($admin_products, $cartItem['id']);
                            }
                            else{
                                $product_ids = array();
                                if(array_key_exists(\App\Models\Product::find(App\Models\Product_stock::find($cartItem['id'])->product_id)->user_id, $seller_products)){
                                    $product_ids = $seller_products[\App\Models\Product::find(App\Models\Product_stock::find($cartItem['id'])->product_id)->user_id];
                                }
                                array_push($product_ids, $cartItem['id']);
                                $seller_products[\App\Models\Product::find(App\Models\Product_stock::find($cartItem['id'])->product_id)->user_id] = $product_ids;
                            }
                    }
                @endphp
                @foreach ($seller_products as $key => $seller_product)
                <div class="cart_2 clearfix">
                    <h5 class="fs-16 fw-600 mb-0 shop_name">{{ \App\Models\Shop::where('user_id', $key)->first()->name }} Products</h5>
                </div>
                 
                    @php
                        $total = 0;
                    @endphp
                    @foreach (Session::get('cart')->where('owner_id', $key) as $item => $row)
                    @php
                        $total += $row['qty'] * $row['price'];
                    @endphp
                        <div class="cart_2 clearfix">
                            <div class="col-sm-2">
                                <div class="cart_2i clearfix">
                                    <a href="{{ route('product_detail', $row['pro_id']) }}">
                                        <img src="{{ url(App\Models\Upload::find($row['image'])->file_name) }}"
                                            class="iw" alt="abc"
                                            style="max-width:60px; max-height:80px; float:left; margin-right:10px;">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="cart_2i clearfix">
                                    <h4 class="mgt"><a href="{{ route('product_detail', $row['pro_id']) }}">{{ $row['name'] }}</a></h4>
                                    @if ($row['color'])
                                    <p>Color: {{ $row['color'] }}</p>
                                    @endif
                                    @if ($row['size'])
                                    <p>Size: {{ $row['size'] }}</p>
                                    @endif
                                    @if ($row['fabric'])
                                    <p>Fabric: {{ $row['fabric'] }}</p>
                                    @endif
                                    @if ($row['poud'])
                                    <p>Poud: {{ $row['poud'] }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="cart_2i clearfix">
                                    <p class="mgt">${{ number_format($row['price'], 2) }}</p>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="cart_2i clearfix">
                                    <div class="input-group number-spinner">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" id="{{ $item }}" data-dir="dwn"><span
                                                    class="glyphicon glyphicon-minus"></span></button>
                                        </span>
                                        <input type="hidden" name="key" value="{{ $item }}">
                                        <input type="text" name="qty" class="form-control text-center"
                                            value="{{ $row['qty'] }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" id="{{ $item }}" data-dir="up"><span
                                                    class="glyphicon glyphicon-plus"></span></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="cart_2i clearfix">
                                    <p class="mgt">${{ number_format($row['price'] * $row['qty'], 2) }}</p>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="cart_2i clearfix text-center">
                                    <p class="mgt"><a href="{{ route('remove', $item) }}" class="btn btn-danger"><i
                                                class="fa fa-trash"></i></a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="cart_2 clearfix">
                        <div class="col-sm-2">
                            <div class="cart_2i clearfix text-center">
                                
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="cart_2i clearfix text-center">
                                
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="cart_2i clearfix text-center">
                               
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="cart_2i clearfix">
                                <p>Total</p>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="cart_2i clearfix">
                                <p>${{ number_format($total, 2) }}</p>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="cart_2i clearfix text-center">
                                @if ($user)
                                <a href="{{ route('checkout.store_delivery_info', $key) }}" class="button_2">Go Order</a>
                                @else
                                <a href="" id="black_login" class="button_2">Go Order</a>
                                @endif
                                
                            </div>
                        </div>
                    </div>
                @endforeach
            
                @else
                    <div class="cart_2 clearfix">
                        <div class="row">
                            <div class="col-sm-4 text-center">
                                <object type="image/svg+xml"
                                    data="https://m.media-amazon.com/images/G/01/cart/empty/animated/rolling-cart-desaturated._CB405694243_.svg"
                                    width="160" height="160">
                                    <img alt=""
                                        src="https://m.media-amazon.com/images/G/01/cart/empty/animated/cart-fallback-desaturated._CB405682035_.svg"
                                        height="160" width="160">
                                </object>
                            </div>
                            <h4 class="">
                                Your Cart is empty
                            </h4>
                            <div class="cart-emtry">
                                @if ($user != null)
                                <a href="{{ route('product') }}" class="btn btn-primary"> Continue to shopping</a>
                                @else
                                <a href="{{ route('sign_in') }}" class="btn btn-primary"> Sign in to your account</a>
                                <a href="{{ route('registration', 'buy') }}" class="btn btn-info"> Sign up now</a>
                                @endif
                                
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        {{-- moal detail --}}
        <div class="modal fade" id="login_modal" tabindex="-1" role="dialog" style="z-index: 10000;">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Login</h4>
                    </div>
                    <form class="form-horizontal" action="{{ route('user.buy.login') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required><br>
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required><br>
                            <button type="submit" class="btn btn-primary" id="btn_save">LOGIN</button>

                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </section>
@endsection
<script src=""></script>
@section('script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {
            $(document).on('click', '.number-spinner .btn', function() {
                var btn = $(this),
                    oldValue = btn.closest('.number-spinner').find('input[name="qty"]').val().trim();
                newVal = 0;
                var key = $(this).attr('id');
                if (btn.attr('data-dir') == 'up') {
                    newVal = parseInt(oldValue) + 1;
                } else {
                    if (oldValue > 1) {
                        newVal = parseInt(oldValue) - 1;
                    } else {
                        newVal = 1;
                    }
                }
                btn.closest('.number-spinner').find('input[name="qty"]').val(newVal);
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "post",
                    url: '{{ route('update_cart') }}',
                    data: {
                        qty: newVal,
                        key: key,
                        _token: _token
                    },
                    success: function(data) {
                        window.location.reload();
                    }
                });
            });

            $('#black_login').click(function (e) { 
                e.preventDefault();
                $('#login_modal').modal('show');
            });
        });
    </script>
@endsection
