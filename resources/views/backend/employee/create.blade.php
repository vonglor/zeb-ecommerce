@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
                   <div class="content">
                    <div class="intro-y flex items-center mt-8">
                        <h2 class="text-xl font-medium mr-auto">
                            Add New Employee
                        </h2>
                    </div>
                    <div class="grid grid-cols-6 gap-6 mt-5">
                        <div class="intro-y col-span-12 lg:col-span-6">
                            <!-- BEGIN: Vertical Form -->
                            <form action="{{route('employee.store')}}" method="POST">
                                @csrf
                            <div class="intro-y box">
                                <div id="vertical-form" class="p-5">
                                    <div class="preview">
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">First Name</label>
                                            <input id="vertical-form-1" type="text" name="first_name" class="form-control border-gray-500 text-base" placeholder="First Name" required>
                                        </div>
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Last Name</label>
                                            <input id="vertical-form-1" type="text" name="last_name" class="form-control border-gray-500 text-base" placeholder="Last Name" required>
                                        </div>
                                         <div>
                                            <label for="vertical-form-1" class="form-label text-base">Phone</label>
                                            <input id="vertical-form-1" type="number" name="phone" class="form-control border-gray-500 text-base" placeholder="+85620" required>
                                        </div>
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Address</label>
                                            <input id="vertical-form-1" type="text" name="address" class="form-control border-gray-500 text-base" placeholder="Address..." required>
                                        </div>
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Email</label>
                                            <input id="vertical-form-1" type="email" name="email" class="form-control border-gray-500 text-base" placeholder="Email" required >
                                             @error('email')
                                            <span style="color: red;">{{ $message }}</span>
                                            @enderror
                                        </div>
                                         <div>
                                            <label for="vertical-form-1" class="form-label text-base">Password</label>
                                            <input id="vertical-form-1" type="password" name="password" class="form-control border-gray-500 text-base" placeholder="Password" required>
                                        </div>
                                        <div class="mt-3">
                                            <label>Role</label>
                                            <div class="mt-2">
                                                <select class="form-select mt-2 sm:mr-2 border-gray-500" name="position" aria-label="Default select example">
                                                    <option value="Admin">Admin</option>
                                                    <option value="Manager">Manager</option>
                                                    <option class="Accountant">Accountant</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="mt-10">
                                            <button type="submit" class="btn btn-primary w-32 mr-2 mb-2"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Save </button> 
                                        </div>

                                    </div>
                                </div>
                            </div>
                            </form>
                        </div> 
                    </div>
                </div>
s<!-- END: Content -->
    
@endsection