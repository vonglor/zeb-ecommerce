<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Membership;
use Illuminate\Http\Request;

class MemberShipController extends Controller
{
    public function index()
    {
        $m = Membership::find(1);
        if (!$m) {
            toastr()->error(__("404 Not Found"), __("Errors"), ['timeOut' => 2000]);
            return redirect()->back();
        }
        return view('backend.memberships.index', compact('m'));
    }

    public function update(Request $request, MemberShip $m)
    {
        $request->validate([
            "name" => ['required', 'numeric', 'min:10', 'max:100'],
        ]);

        $m->name = $request->name;
        if ($m->save()) {
            toastr()->success('Save', 'Successfully', ['timeOut' => 2000]);
            return redirect()->back();
        } else {
            toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
            return redirect()->back();
        }
    }
}
