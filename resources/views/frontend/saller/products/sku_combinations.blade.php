@if (count($combinations[0]) > 0)

    <table class="table table-bordered" id="example">
        <thead>
            <tr>
                <th style="width: 30%">Variant</th>
                <th>Variant Price</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($combinations as $key => $combination)

                @php
                    $sku = '';
                    foreach (explode(' ', $product_name) as $key => $value) {
                        $sku .= substr($value, 0, 1);
                    }
                    
                    $str = '';
                    foreach ($combination as $key => $item) {
                        if ($key > 0) {
                            $str .= '-' . str_replace(' ', '', $item);
                            $sku .= '-' . str_replace(' ', '', $item);
                        } else {
                            $str .= str_replace(' ', '', $item);
                            $sku .= '-' . str_replace(' ', '', $item);
                        }
                    }
                @endphp
                @if (strlen($str) > 0)
                    <tr class="variant_{{ $str }}">
                        <td><label for="" class="control-label"><span class="{{ $str }}"><i
                                        class="fa fa-plus-circle text-primary" id="icon_{{ $str }}"
                                        aria-hidden="true"></i></span> {{ $str }}</label></td>

                        <td><input type="number" name="price_{{ $str }}" value="{{ $unit_price }}"
                                class="form-control" placeholder="0"></td>
                        <td>
                            <button type="button" class="btn btn-sm btn-danger" id="{{ $str }}"><i
                                    class="fa fa-trash"></i></button>
                        </td>
                    </tr>
        <tbody class="table_middle_{{ $str }}  child_row" id="table_middle">
            <tr>
                <td style="text-align: right"><label for="" class="control-label">Total</label></td>
                <td colspan="2"><input type="number" name="qty_{{ $str }}" value="10" class="form-control"
                        placeholder="0"></td>
            </tr>
            <tr>
                <td style="text-align: right"><label for="" class="control-label">Photo</label></td>
                <td colspan="2">
                    <input type="file" name="img_{{ $str }}" accept="image/*" id="img_{{ $str }}"
                        class="form-control">
                    <div class="image-preview" id="show_img_{{ $str }}">
                    </div>
                </td>
            </tr>
        </tbody>
        @endif
        @endforeach
    </tbody>
</table>
<script>
    $(document).ready(function() {
        $('#example #table_middle').hide();

        $('#example tbody').on('click', 'tr td span', function() {
            var attr = $(this).attr('class');
            var oldClass = $('#icon_' + attr).attr('class');

            if (oldClass == 'fa fa-plus-circle text-primary') {
                $('.table_middle_' + attr).show();
                $('#icon_' + attr).removeClass("fa fa-plus-circle text-primary").addClass(
                    "fa fa-minus-circle text-danger");
            } else {
                $('.table_middle_' + attr).hide();
                $('#icon_' + attr).removeClass("fa fa-minus-circle text-danger").addClass(
                    "fa fa-plus-circle text-primary");
            }
        });

        $('#example tbody').on('click', 'tr td button', function() {
            var attr = $(this).attr('id');
            $('.variant_' + attr).remove();
            $('.table_middle_' + attr).hide();
        });



        $('#example tbody').on('change', 'tr td input[type="file"]', function() {
            var attr = $(this).attr('id');
            var property = document.getElementById(attr).files[0];
            var img_name = property.name;
            var img_extension = img_name.split('.').pop().toLowerCase();
            var img_preview = $('#show_' + attr);
            if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg', 'gif']) == -1) {
                img_preview.html('<span style="color: red;">This not support file reader</span>');
                return false;
            } else {
                var img_size = property.size;
                if (img_size > 2000000) {
                    img_preview.html('<span style="color: red;">This file is to big</span>');
                    return false;
                } else {
                    if (typeof(FileReader) != 'undefined') {
                        img_preview.empty();
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('<img/>', {
                                'src': e.target.result,
                                'class': 'img-fluid',
                                'style': 'max-height: 80px; max-width: 80px;'
                            }).appendTo(img_preview);
                        }
                        img_preview.show();
                        reader.readAsDataURL($(this)[0].files[0]);
                    } else {
                        img_preview.html(
                            '<span style="color:red;">This not support file reader</span>');
                    }
                }
            }
        })


    });
</script>
@endif
