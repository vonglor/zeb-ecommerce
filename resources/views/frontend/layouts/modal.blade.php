 
 {{-- moal login detail --}}
 <div class="modal fade" id="login_modal" tabindex="-1" role="dialog" style="z-index: 10000;">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Login</h4>
            </div>
            <form class="form-horizontal" action="{{ route('user.buy.login') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required><br>
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required><br>
                    <button type="submit" class="btn btn-primary" id="btn_save">LOGIN</button>

                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal login -->