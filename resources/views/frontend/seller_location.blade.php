@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/login.css') }}" rel="stylesheet">
    <style>
      table{
        font-size: 14px;
      }
    </style>
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Seller location</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="login" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="login_1 clearfix">
                    <div class="col-sm-8">
                        <table class="table">
                            <tbody>
                              <tr>
                                <td>Shop name:</td>
                                <td>{{ $shop->name }}</td>
                              </tr>
                              <tr>
                                <td>Address:</td>
                                <td>{{ $shop->address }}</td>
                              </tr>
                              <tr>
                                <td>Email:</td>
                                <td>{{ $shop->user->email }}</td>
                              </tr>
                              <tr>
                                <td>Phone:</td>
                                <td>{{ $shop->user->phone }}</td>
                              </tr>
                              <tr>
                                <td>Facebook:</td>
                                <td>{{ $shop->facebook }}</td>
                              </tr>
                            </tbody>
                          </table>
                    </div>
                   
                </div>
            </div>
        </div>
    </section>
@endsection

<script src=""></script>