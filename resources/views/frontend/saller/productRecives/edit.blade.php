@extends('frontend.saller.content_left')
@section('css')
<link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
<div class="col-sm-9">
    <div class="centent_card_1r clearfix">
        <div class="centent_card_1r3 tab-content">
            <p>Update Import products</p>
            <hr>
            <div class="card-content">
                <form action="{{ route('product_recive.update', $pro_recive->id) }}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="row col-sm-12 field">
                        <div class="col-sm-2">
                            <label for="Brands" class="col-form-label">Product name</label>
                        </div>
                        <div class="col-sm-8">
                            <select name="product_id" id="product_id" class="form-control selectpicker" data-live-search="true" required>
                                @foreach (App\Models\Product::where('user_id', user()->id)->get() as $item)
                                    <option value="{{ $item->id }}" @if ($item->id == $pro_recive->product_id)
                                        selected
                                    @endif>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row col-sm-12 field" id="variantion">
                        @if (count($pro_stock) > 1)
                        <div class="col-sm-2">
                            <label for="Brands" class="col-form-label">Variantion</label>
                        </div>
                        <div class="col-sm-8">
                           
                            <select name="variantion" class="form-control selectpicker" data-live-search="true">
                                @foreach ($pro_stock as $pro)
                                <option value="{{ $pro->id }}" @if ($pro->id == $pro_recive->pro_stock_id)
                                    selected
                                @endif>{{ $pro->variant }}</option>
                                @endforeach
                            </select>                          
                        </div>
                        @else
                        <div class="col-sm-8">
                            <input type="hidden" name="variantion" value="{{ $pro_recive->pro_stock_id }}">                          
                        </div>
                        @endif
                    </div>
                    
                    <div class="row col-sm-12 field">
                        <div class="col-sm-2">
                            <label for="Brands" class="col-form-label">Quantity</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" name="qty" value="{{ $pro_recive->qty }}">
                        </div>
                    </div>

                    <div class="row col-sm-12 field">
                        <div class="col-sm-2">
                            <label for="Brands" class="col-form-label">Price</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" name="price" value="{{ $pro_recive->price }}">
                        </div>
                        <div class="col-sm-2">
                            <select name="currency" class="form-control" id="">
                                @foreach (App\Models\Currency::where('status', 1)->get() as $item)
                                <option value="{{ $item->code }}" @if ($item->code == $pro_recive->currency_code)
                                    selected
                                @endif>{{ $item->code }} ({{ $item->symbol }})</option>
                                @endforeach
                            </select>
                        </div>

                        <input type="hidden" name="producted_id" value="{{ $pro_recive->product_id }}">
                        <input type="hidden" name="pro_stocked_id" value="{{ $pro_recive->pro_stock_id }}">
                        <input type="hidden" name="qtyed" value="{{ $pro_recive->qty }}">
                    </div>
                    
                    <div class="row col-sm-12 field">
                        <div class="col-sm-12 text-center mb-5" style="margin-bottom: 10px;">
                            <button class="button" id="btn_save"> Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    $(document).ready(function () {
    
        $('#product_id').change(function (){
            var product_id = $(this).val();
            $.ajax({
                type: "get",
                url: "{{ route("variantion") }}",
                data: {product_id: product_id},
                success: function (response) {
                    $('#variantion').html(response);
                    $('.my-select').selectpicker();
                }
            });
        });



    });

    

</script>
@endsection