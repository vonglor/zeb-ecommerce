@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <h2 class="intro-y text-lg font-medium mt-10 text-xl">
            All Seller Products
        </h2>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                
            <div class="w-100">
                <div class="mt-3">
                    <select class="form-select mt-2 sm:mr-2" aria-label="Default select example">
                        <option class="text-base">Baby Store</option>
                        <option class="text-base">Nike Store</option>
                        <option class="text-base">Adidas Shop</option>
                        <option class="text-base">David Shop</option>
                    </select>
                </div>
            </div>
            <div class="hidden md:block mx-auto text-gray-600"></div>
            </div>
            <!-- BEGIN: Data List -->
            <div class="intro-y col-span-12 overflow-auto lg:overflow">
                <table class="table table-report -mt-2" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base">#</th>
                            <th class="whitespace-nowrap text-base">IMAGES</th>
                            <th class="whitespace-nowrap text-base">PRODUCT NAME</th>
                            <th class="text-center whitespace-nowrap text-base">PRICE</th>
                            <th class="text-center whitespace-nowrap text-base">STOCK</th>
                            <th class="text-center whitespace-nowrap text-base">ACTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 0;
                        @endphp
                        @foreach ($sellerproduct as $item)
                        <tr class="intro-x">
                            <td class="w-40text-base">{{++$i}}</td>
                            <td class="w-40">
                                <div class="flex">
                                    <div class="w-10 h-10 image-fit zoom-in">
                                        
                                       @php
                                           $file = App\Models\Upload::find($item->photos);   
                                       @endphp
                                       @if ($file)
                                       {{-- <img src="{{ url($item->Upload->file_name)}}" alt="" style="max-width: 100px; max-height: 100px"> --}}
                                       {{-- <img alt="IMAGE" class="tooltip" src="{{url($item->Upload->file_name)}}" title="Product Image"> --}}
                                       <a data-lightbox="{{$item->Upload->file_name}}" data-title="{{$item->Upload->file_name}}" href="{{URL::to($item->Upload->file_name)}}" target="_blank">
                                        <img src="{{ url($item->Upload->file_name) }}" alt="" style="max-width: 50px; max-height: 50px">
                                        </a>
                                       @endif
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="" class="font-medium whitespace-nowrap text-base">{{ substr($item->name,0,20) }}</a> 
                                <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">Added by:{{$item->added_by}}</div>
                            </td>
                            <td class="text-center text-base">{{$item->purchase_price}}</td>
                            <td class="w-40">
                                <div class="flex items-center justify-center text-theme-10 text-base">{{$item->num_of_sale}} </div>
                            </td>
                            <td class="table-report__action w-56 p-5">
                                <div class="flex justify-center items-center">
                                    <a class="btn btn-primary" href="#"> <i data-feather="check-square" class="w-4 h-3 mr-1"></i></a>
                                    <form action="#" method="POST" class="px-2">
                                        <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button>
                                    </form>
                                    
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
        </div>
    </div>
    <!-- END: Content -->

    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();    
        });
    </script>
    
@endsection