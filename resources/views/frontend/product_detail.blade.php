@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/detail.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/product-list.css') }}" rel="stylesheet">
    {{-- <link rel="stylesheet" href="{{ asset('frontend/css/img-zoom.css') }}"> --}}
    <style>
            /* img-zoom css */
            .xzoom-thumbs a {
                text-decoration: none;
            }

            .xzoom-thumbs img {
                display: inline-block;
                height: 80px;
                width: 80px;
                margin-top: 15px;
                border: 1px solid #ccc;
            }
            .xzoom-preview {
                border: 1px solid #e0e0e0;
                box-shadow: 0 4px 20px 2px rgba(0, 0, 0, 0.2);
            }
            .xzoom-thumbs .active {
                border: 2px solid #3665f3;
                box-shadow: 0 0 4px 0 #3665f3;
            }
            .center_detail_2_right h5{
                font-size: 15px;
            }
    </style>
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Product Detail</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="product" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="product_1 clearfix">
                    <div class="col-sm-12">
                        <div class="center_detail_2 clearfix">
                            <div class="col-sm-6">
                                <span class="war-title">Warning:</span> <span class="war-content"> Make sure you
                                    understand the policy below befor your purchase</span><br>
                                <div class="center_detail_2_left clearfix">
                                    <div class="xzoom-container">
                                        <img src="{{ url($detail->Upload->file_name) }}" class="img-responsive xzoom" xoriginal="{{ url($detail->Upload->file_name) }}">
                                        
                                        <div class="xzoom-thumbs">

                                            <a href="{{ url($detail->Upload->file_name) }}">
                                                <img src="{{ url($detail->Upload->file_name) }}" class="img-responsive xzoom-gallery" xpreview="{{ url($detail->Upload->file_name) }}">
                                            </a>
                                            @if ($detail->thumbnail_img != null)
                                                @php
                                                    $thumb = explode(',', $detail->thumbnail_img);
                                                @endphp
                                                @foreach ($thumb as $key => $img)
                                                    @php
                                                        $thumbnail = App\Models\Upload::where('id', $img)->first();
                                                    @endphp
                                                <a href="{{ url($thumbnail->file_name) }}">
                                                    <img src="{{ url($thumbnail->file_name) }}" class="img-responsive xzoom-gallery" xpreview="{{ url($thumbnail->file_name) }}">
                                                </a>
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <span class="war-video"></span> <span class="war-content">
                                    @if ($detail->videos != null)
                                        <a href="{{ $detail->videos }}" target="_blank">{{ $detail->videos }}</a>
                                    @else
                                        No video link
                                    @endif
                                </span>
                            </div>
                            <div class="col-sm-6">
                                <div class="center_detail_2_right clearfix">
                                    <form action="{{ route('add_to_cart') }}" id="form-add-cart" method="POST">
                                        @csrf
                                        <div class="center_detail_2_right_inner clearfix">
                                            <h4>
                                                <?php
                                                $array = explode("\n", wordwrap($detail->name, 50));
                                                echo implode('<br>', $array);
                                                ?>
                                            </h4>
                                            <h5 class="reviewed">
                                                <a href="#">{{ $detail->views }} views</a>
                                            </h5>
                                            <h5><span>Condition:</span>{{ $detail->condition }}</h5>

                                            @if (count(json_decode($detail->colors)) > 0)
                                                <div class="pd_n clearfix color-choose">
                                                    <h5 class="bold">Color:
                                                    </h5>
                                                    <ul>
                                                        @php
                                                            $i = 1;
                                                        @endphp
                                                        @foreach (json_decode($detail->colors) as $key => $color)
                                                            @php
                                                                $colors = App\Models\Color::where('name', $color)->first();
                                                            @endphp
                                                            <input type="radio" name="color" id="{{ $color }}"
                                                                class="radio_{{ $i }}"
                                                                value="{{ $color }}" style="display: none">
                                                            <label for="{{ $color }}">
                                                                <li style="background: {{ $colors->code }}"
                                                                    id="color_{{ $i }}"
                                                                    name="{{ $color }}"></li>
                                                            </label>
                                                            @php
                                                                $i++;
                                                            @endphp
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            @if ($detail->choice_options != null && count(json_decode($detail->choice_options)) > 0)
                                                @foreach (json_decode($detail->choice_options) as $key => $choice)
                                                    <div class="pd_n_1 clearfix choice_options">
                                                        @php
                                                            $option = App\Models\Attributes::find($choice->attribute_id);
                                                        @endphp
                                                        <h5 class="bold">{{ $option->name }}:
                                                        </h5>
                                                        <ul>
                                                            @php
                                                                $i = 1;
                                                            @endphp
                                                            @foreach ($choice->values as $key => $value)
                                                                <label>
                                                                    <input type="radio" name="{{ $option->name }}"
                                                                        id="{{ $option->name }}"
                                                                        class="radio_{{ $i }}"
                                                                        value="{{ $value }}" style="display: none">
                                                                    <li id="{{ $option->name . $value }}"
                                                                        name="{{ $value }}"
                                                                        class="{{ $option->name }}_{{ $i }}"
                                                                        data="{{ $option->name }}">{{ $value }}
                                                                    </li>
                                                                </label>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endforeach
                                            @else
                                                <input type="hidden" id="Size" name="Size" value="">
                                                <input type="hidden" id="Fabric" name="Fabric" value="">
                                                <input type="hidden" id="Poud" name="Poud" value="">
                                            @endif
                                            <h5>
                                                <span>Price:</span>
                                                @if ($detail->discount != null)
                                                    <span
                                                        class="col_2" style="font-size: 18px;">{{ convert_price($detail->unit_price, $detail->currency_code) }}</span>
                                                    <span
                                                        id="show_price" style="font-size: 18px;">{{ convert_price($detail->unit_price - $detail->discount, $detail->currency_code) }}</span>
                                            </h5>
                                            @else
                                            <span id="show_price" style="font-size: 18px;">{{ convert_price($detail->unit_price, $detail->currency_code) }}</span>
                                            </h5>
                                            @endif
                                            <div class="row">
                                                <h5 class="col-sm-2">
                                                    Quatity:
                                                </h5>
                                                <div class="input-group number-spinner col-sm-4" style="z-index: 0;">
                                                    <span class="input-group-btn">
                                                        <a class="btn btn-default" data-dir="dwn"><span
                                                                class="glyphicon glyphicon-minus"></span></a>
                                                    </span>
                                                    <input type="text" name="qty" class="form-control text-center" value="1" >
                                                    <span class="input-group-btn">
                                                        <a class="btn btn-default" data-dir="up"><span
                                                                class="glyphicon glyphicon-plus"></span></a>
                                                    </span>
                                                </div>
                                            </div>
                                            <div>
                                                <h5>Item number: {{ $detail->id }}</h5>
                                            </div>
                                            <div>
                                                <h5>Total:
                                                    @php
                                                        $pro_stock = App\Models\Product_stock::where('product_id', $detail->id)->get();
                                                        $qty = 0;
                                                        foreach ($pro_stock as $pt) {
                                                            $qty += $pt->qty;
                                                        }
                                                        echo $qty;
                                                    @endphp
                                                </h5>
                                            </div>
                                            <h5>Shipping: @php
                                                if($detail->shipping != null){
                                                    echo $detail->shipping;
                                                }else{
                                                    echo 'free';
                                                }
                                            @endphp</h5>
                                            <input type="hidden" name="product_id" id="product_id"
                                                value="{{ $detail->id }}">
                                        </div>
                                        <div class="center_detail_2_right_inner_1 clearfix">
                                            <ul>
                                                <li><button type="submit" id="add_to_cart" class="btn-addcart">
                                                        <i class="fa fa-shopping-bag"></i> Add to cart</button>
                                                </li>

                                                @if (user())
                                                    <li><a href="#" id="btn-buy" class="btn-buy"> Order</a></li>
                                                @else
                                                    <li><a href="#" id="black_login" class="btn-buy"> Order</a></li>
                                                @endif
                                                
                                            </ul>
                                        </div>
                                    </form>
                                    <a href="{{ route('seller.location', $detail->user_id) }}">Seller location</a>
                                    <div class="center_detail_2_right_inner_2 clearfix">
                                        
                                        @if ($detail->delivery)
                                            <p class="mgt">
                                                <span class="col-sm-2 delivery-title">Delivery:</span> <span
                                                    class="col-sm-10 delivery">{{ $detail->delivery }}</span>
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="center_detail_2 clearfix">
                            <div class="center_detail_2_right_inner_2 clearfix">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home_description">Seller
                                            Description</a>
                                    </li>
                                    <li class=""><a data-toggle="tab" href="#menu_information">Policy
                                            return</a></li>
                                </ul>

                                <div class="tab-content clearfix">
                                    <div id="home_description" class="tab-pane fade clearfix active in">
                                        <div class="click clearfix">
                                            <p>{{ $detail->description }}</p>
                                        </div>
                                    </div>
                                    <div id="menu_information" class="tab-pane fade clearfix ">
                                        <div class="click clearfix">
                                            @php
                                                $policy = App\Models\Policy::where('policy_type', 'policy_return')->first();
                                            @endphp
                                            @if ($policy)
                                            <p>
                                                {{ $policy->content }}
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="product_list">
        <div class="container" style="background: #fff; padding: 15px 0 30px 0; border: 1px solid #f3f3f3;">
            <div class="row">
                <div class="product_list text-center clearfix">
                    <div class="col-sm-12">
                        <h3 class="mgt">Product Items</h3>
                        <hr>
                    </div>
                </div>
                <div class="gallery_1 clearfix">
                    <div class="col-sm-12">
                        <div class="workout_page_1_left clearfix">

                            <div class="tab-content clearfix">
                                <div id="home" class="tab-pane fade  clearfix active in">
                                    <div class="click clearfix">
                                        <div class="home_inner clearfix">
                                            @php
                                                $product = App\Models\Product::paginate(8);
                                            @endphp
                                            @foreach ($product as $products)
                                                <div class="col-sm-3 offset-md-0 offset-sm-1 pt-md-0 pt-4">
                                                    <div class="card"> <a
                                                            href="{{ route('product_detail', $products->id) }}"><img
                                                                class="card-img-top"
                                                                src="{{ url($products->Upload->file_name) }}"></a>
                                                        <div class="card-body">
                                                            <h5 class="font-weight-bold pt-1"><a
                                                                    href="{{ route('product_detail', $products->id) }}">{{ Str::limit($products->name, 27) }}</a>
                                                            </h5>
                                                            <div
                                                                class="d-flex align-items-center justify-content-between pt-3">
                                                                <div class="d-flex flex-column">
                                                                    <div class="h5 font-weight-bold">
                                                                        @if ($products->discount)
                                                                            <span
                                                                                class="col_2">${{ number_format($products->unit_price, 2) }}</span>
                                                                        @endif
                                                                        ${{ number_format($products->unit_price - $products->discount, 2) }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_1_last text-center clearfix">
                    <div class="col-sm-12">
                        <ul>
                            {!! $product->links() !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
<script src=""></script>
@section('script')
<script src="{{ asset('js/zoom.js') }}"></script>
    <script type="text/javascript">
        ! function($) {

            "use strict"; // jshint ;_;


            /* MAGNIFY PUBLIC CLASS DEFINITION
             * =============================== */

            var Magnify = function(element, options) {
                this.init('magnify', element, options);
            }

            Magnify.prototype = {

                constructor: Magnify

                    ,
                init: function(type, element, options) {
                    var event = 'mousemove',
                        eventOut = 'mouseleave';

                    this.type = type;
                    this.$element = $(element);
                    this.options = this.getOptions(options);
                    this.nativeWidth = 0;
                    this.nativeHeight = 0;

                    this.$element.wrap('<div class="magnify" \>');
                    this.$element.parent('.magnify').append('<div class="magnify-large" \>');
                    this.$element.siblings(".magnify-large").css("background", "url('" + this.$element.attr("src") +
                        "') no-repeat");

                    this.$element.parent('.magnify').on(event + '.' + this.type, $.proxy(this.check, this));
                    this.$element.parent('.magnify').on(eventOut + '.' + this.type, $.proxy(this.check, this));
                },
                getOptions: function(options) {
                    options = $.extend({}, $.fn[this.type].defaults, options, this.$element.data())

                    if (options.delay && typeof options.delay == 'number') {
                        options.delay = {
                            show: options.delay,
                            hide: options.delay
                        }
                    }

                    return options;
                },
                check: function(e) {
                    var container = $(e.currentTarget);
                    var self = container.children('img');
                    var mag = container.children(".magnify-large");

                    // Get the native dimensions of the image
                    if (!this.nativeWidth && !this.nativeHeight) {
                        var image = new Image();
                        image.src = self.attr("src");

                        this.nativeWidth = image.width;
                        this.nativeHeight = image.height;

                    } else {

                        var magnifyOffset = container.offset();
                        var mx = e.pageX - magnifyOffset.left;
                        var my = e.pageY - magnifyOffset.top;

                        if (mx < container.width() && my < container.height() && mx > 0 && my > 0) {
                            mag.fadeIn(100);
                        } else {
                            mag.fadeOut(100);
                        }

                        if (mag.is(":visible")) {
                            var rx = Math.round(mx / container.width() * this.nativeWidth - mag.width() / 2) * -1;
                            var ry = Math.round(my / container.height() * this.nativeHeight - mag.height() / 2) * -
                                1;
                            var bgp = rx + "px " + ry + "px";

                            var px = mx - mag.width() / 2;
                            var py = my - mag.height() / 2;

                            mag.css({
                                left: px,
                                top: py,
                                backgroundPosition: bgp
                            });
                        }
                    }

                }
            }

            /* MAGNIFY PLUGIN DEFINITION
             * ========================= */

            $.fn.magnify = function(option) {
                return this.each(function() {
                    var $this = $(this),
                        data = $this.data('magnify'),
                        options = typeof option == 'object' && option
                    if (!data) $this.data('tooltip', (data = new Magnify(this, options)))
                    if (typeof option == 'string') data[option]()
                })
            }

            $.fn.magnify.Constructor = Magnify

            $.fn.magnify.defaults = {
                delay: 0
            }


            /* MAGNIFY DATA-API
             * ================ */

            $(window).on('load', function() {
                $('[data-toggle="magnify"]').each(function() {
                    var $mag = $(this);
                    $mag.magnify()
                })
            })

        }(window.jQuery);


        $(document).ready(function() {
            $('.color-choose label #color_1').addClass('act');
            $('.color-choose .radio_1').attr('checked', true);
            $('.color-choose label li').on('click', function() {
                $('.act').removeClass('act');
                $(this).addClass('act');
                var colors = $(this).attr('name');
                var Size = $('#Size').val();
                var Fabric = $('#Fabric').val();
                var Poud = $('#Poud').val();
                var product_id = $('#product_id').val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "POST",
                    url: "{{ route('select') }}",
                    data: {
                        Size: Size,
                        Fabric: Fabric,
                        Poud: Poud,
                        colors: colors,
                        product_id: product_id,
                        _token: _token,
                    },
                    dataType: 'JSON',
                    success: function(data) {
                        $('#show_price').html(data.price);
                    }
                });
            });

            $('.choice_options label .Size_1').addClass('act_1');
            $('.choice_options label .Fabric_1').addClass('act_1');
            $('.choice_options label .Poud_1').addClass('act_1');
            $('.choice_options .radio_1').attr('checked', true);
            $('.choice_options label li').on('click', function() {
                var att = $(this).attr('id');
                var data = $(this).attr('data');
                $('.choice_options .act_1').removeClass('act_1');
                $('.choice_options #' + att).addClass('act_1');

                var colors = $('#color').val();
                var Size = $('#Size').val();
                var Poud = $(this).attr('name');
                var Fabric = $('#Fabric').val();
                var product_id = $('#product_id').val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "POST",
                    url: "{{ route('select') }}",
                    data: {
                        Size: Size,
                        Fabric: Fabric,
                        Poud: Poud,
                        colors: colors,
                        product_id: product_id,
                        _token: _token,
                    },
                    dataType: 'JSON',
                    success: function(data) {
                        $('#show_price').html(data.price);
                    }
                });
            });

            $('#btn-buy').click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: '{{ route('buy') }}',
                    data: $('#form-add-cart').serialize(),
                    success: function(data) {
                        window.location.href = "/checkout/payment_select/"+data;
                    }
                });
            });
        });
    </script>

    <script type="text/javascript">
        var price = $('#show_price').text();
        $(document).on('click', '.number-spinner .btn', function() {
            var btn = $(this),
                oldValue = btn.closest('.number-spinner').find('input').val().trim();
            newVal = 0;
            if (btn.attr('data-dir') == 'up') {
                newVal = parseInt(oldValue) + 1;
            } else {
                if (oldValue > 1) {
                    newVal = parseInt(oldValue) - 1;
                } else {
                    newVal = 1;
                }
            }
            var subtotal = price * newVal;
            subtotal = subtotal.toFixed(2);
            btn.closest('.number-spinner').find('input').val(newVal);
            $('#subtotal').html(subtotal);
        });
    </script>

    {{-- img-zoom --}}
    <script>
       
           $(function(){
               $(".xzoom, .xzoom-gallery").xzoom({
                   zoomWidth: 600,
                   tint: "#333",
                   Xoffset: 15,
                   vertical:0,
               })
           })

    </script>
@endsection
