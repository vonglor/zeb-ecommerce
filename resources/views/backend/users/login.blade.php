<!DOCTYPE html>
<html lang="en" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <link href="{{asset('backend/dist/images/logo.svg')}}" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Icewall admin is super flexible, powerful, clean & modern responsive tailwind admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, Icewall Admin Template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="LEFT4CODE">
        
        @yield('css')

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <title>Login</title>
        <!-- BEGIN: CSS Assets-->
        <link rel="stylesheet" href="{{ asset('backend/dist/css/app.css') }}" />
        <!-- END: CSS Assets-->
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" type="text/javascript"></script>

        @stack('css')
        @toastr_css
        
    </head>
    <body class="login">
        <div class="container sm:px-10">
            <div class="block xl:grid grid-cols-2 gap-4">
                <!-- BEGIN: Login Info -->
                <div class="hidden xl:flex flex-col min-h-screen">
                    <a href="" class="-intro-x flex items-center pt-5">
                        <img alt="Icewall Tailwind HTML Admin Template" class="w-6" src="{{asset('backend/dist/images/logo.svg')}}">
                        <span class="text-white text-lg ml-3"> TECH<span class="font-medium"> SOLE</span> </span>
                    </a>
                    <div class="my-auto">
                        <img alt="Icewall Tailwind HTML Admin Template" class="-intro-x w-1/2 -mt-16" src="{{asset('backend/dist/images/illustration.svg')}}">
                        <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">
                            A few more clicks to 
                            <br>
                            sign in to your account.
                        </div>
                        <div class="-intro-x mt-5 text-lg text-white text-opacity-70 dark:text-gray-500">Manage all your e-commerce accounts in one place</div>
                    </div>
                </div>
                <!-- END: Login Info -->
                <!-- BEGIN: Login Form -->
                <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
                    <div class="my-auto mx-auto xl:ml-20 bg-white dark:bg-dark-1 xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                        <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
                            {{__('Login')}}
                        </h2>
                        <div class="intro-x mt-2 text-gray-500 xl:hidden text-center">A few more clicks to sign in to your account. Manage all your e-commerce accounts in one place</div>
                        <form action="{{ route('users.signInPost') }}" method="post">
                            @csrf
                            <div class="intro-x mt-8">
                                <input type="email" name="email" id="email" 
                                class="intro-x login__input form-control py-3 px-4 border-gray-500 block text-lg" 
                                placeholder="Email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                            </div>

                            <div class="intro-x mt-8">
                              <input type="password" name="password" id="password" 
                              class="intro-x login__input form-control py-3 px-4 border-gray-500 block mt-4 text-lg" 
                              placeholder="Password">
                              @if ($errors->has('password'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                          </div>

                            {{-- <div class="intro-x flex text-gray-700 dark:text-gray-600 text-xs sm:text-sm mt-4">
                                <div class="flex items-center mr-auto">
                                    <input id="remember-me" type="checkbox" class="form-check-input border mr-2">
                                    <label class="cursor-pointer select-none text-lg" for="remember-me">Remember me</label>
                                </div>
                                <a href="" class="text-lg">Forgot Password?</a> 
                            </div> --}}

                            <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                                <button type="submit" class="btn btn-primary py-3 px-4 w-full xl:w-32 xl:mr-3 align-top text-lg" style="width: 100%">{{__('Login')}}</button>
                                {{-- <a href="{{route('register')}}" class="btn btn-outline-secondary py-3 px-4 w-full xl:w-32 mt-3 xl:mt-0 align-top text-lg">Sign Up</a> --}}
                            </div>
                        </form>
                        
                        {{-- <div class="intro-x mt-10 xl:mt-24 text-gray-700 dark:text-gray-600 text-center xl:text-left">
                            <a class="text-theme-17 dark:text-gray-300 text-lg" href="">Terms and Conditions</a> & <a class="text-theme-17 dark:text-gray-300" href="">Privacy Policy</a> 
                        </div> --}}
                    </div>
                </div>
                <!-- END: Login Form -->
            </div>
        </div>
         <!-- BEGIN: JS Assets-->
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
        {{-- <script src="https://maps.googleapis.com/maps/api/js?key=["AIzaSyA_Re-gp4pnQCBUvBDFV1aU6_zyJHZAXiI"]&libraries=places"></script> --}}
        <script src="{{asset('backend/dist/js/app.js')}}"></script>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script>
            $(document).ready(function () {
            $(".btn_delete").click(function (e) { 
            var form = $(this).closest('form');
            e.preventDefault();
            swal("Are you sure you want to do this?", {
                buttons: ["Cancel", true],
            }).then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
            });
        });
        
        @if (Session::has('message'))
         var type = "{{ Session::get('alert-type') }}";
         toastr.options = {
         "closeButton": true,
         "debug": false,
         "newestOnTop": false,
         "progressBar": true,
         "positionClass": "toast-bottom-right",
         "preventDuplicates": false,
         "onclick": null,
         "showDuration": "300",
         "hideDuration": "1000",
         "timeOut": "5000",
         "extendedTimeOut": "1000",
         "showEasing": "swing",
         "hideEasing": "linear",
         "showMethod": "fadeIn",
         "hideMethod": "fadeOut"
         }
     
         switch (type) {
         case 'info':
         toastr.info("{{ Session::get('message') }}");
         break;
     
         case 'success':
         toastr.success("{{ Session::get('message') }}");
         break;
     
         case 'warning':
         toastr.warning("{{ Session::get('message') }}");
         break;
     
         case 'error':
         toastr.error("{{ Session::get('message') }}");
         break;
         }
             
     @endif
     
        </script>

@toastr_js
@toastr_render
    </body>
</html>