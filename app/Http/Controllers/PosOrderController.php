<?php

namespace App\Http\Controllers;

use Session;
use App\Models\Product;
use App\Models\Product_stock;
use App\Models\User;
use App\Models\Seller;
use App\Models\Order;
use App\Models\Order_detail;
use App\Models\Percent;
use App\Models\Wallet;
use App\Models\PercentSale;
use App\Models\Tax;
use App\Models\UserTax;

use Illuminate\Http\Request;

class PosOrderController extends Controller
{
    public function order(Request $request)
    {
        $order = new Order;
        // dd($request->all());
        if($request->member_type == 'member'){
            $user = User::find($request->user_id);
            if($user){
                $order->user_id = $request->user_id;
            }else{
                $notification = array(
                    'message' => 'Not have user ID.',
                    'alert-type' => 'warning',
                );
                return back()->with($notification);
            }
        }else{
            $order->user_id = null;
        }
        
        $order->seller_id = Seller::where('user_id', Session::get('user')->id)->first()->id;
        $order->payment_type = $request->payment_type;
        $order->payment_status = 'paid';
        $order->delivery_status = 'delivered';
        $order->code = date('Ymd-His').rand(10, 99);
        $order->date = now();
        if($order->save()){
            $subtotal = 0;
            $tax = 0;
            //calculate shipping is to get shipping costs of different types
            $admin_products = array();
            $seller_products = array();
            //Order Details Storing
            foreach (Session::get('pos_cart') as $key => $cartItem){
                $product = Product::find(Product_stock::find($cartItem['id'])->product_id);
                if($product->added_by == 'admin') {
                    array_push($admin_products, $cartItem['id']);
                }
                else {
                    $product_ids = array();
                    if(array_key_exists($product->user_id, $seller_products)){
                        $product_ids = $seller_products[$product->user_id];
                    }
                    array_push($product_ids, $cartItem['id']);
                    $seller_products[$product->user_id] = $product_ids;
                }
                $subtotal += $cartItem['price'] * $cartItem['qty'];
                $product_variation = $cartItem['variant'];
                if($product_variation != null) {
                    $product_stock = Product_stock::where(['id' => $cartItem['id'],'variant' => $product_variation])->first();
                    if($cartItem['qty'] > $product_stock->qty) {
                        //flash(translate('The requested quantity is not available for ').$product->getTranslation('name'))->warning();
                        Session::flash('success', 'Payment successful!');
                        $order->delete();
                        return redirect()->route('pos_cart')->send();
                    } else {
                        $product_stock->qty -= $cartItem['qty'];
                        $product_stock->save();
                    }
                } else {
                    $product_stock = Product_stock::find($cartItem['id']);
                    if ($cartItem['qty'] > $product_stock->qty) {
                        flash(translate('The requested quantity is not available for ').$product->getTranslation('name'))->warning();
                        $order->delete();
                        return redirect()->route('pos_cart')->send();
                    } else {
                        $product_stock->qty -= $cartItem['qty'];
                        $product_stock->save();
                    }
                }

                $order_detail = new Order_detail;
                $order_detail->order_id  = $order->id;
                $order_detail->seller_id = Seller::where('user_id', Session::get('user')->id)->first()->id;;
                $order_detail->product_id = $product_stock->id;
                $order_detail->variantion = $product_variation;
                $order_detail->payment_status = 'paid';
                $order_detail->price = payment_price($cartItem['price'] * $cartItem['qty'], $product->currency_code);
                $order_detail->qty = $cartItem['qty'];
                $order_detail->save();
                $product->num_of_sale++;
                $product->save();
            }
            $order->grand_total = payment_price($request->total, $request->currency_code);
            $order->save();
            $request->session()->put('order_id', $order->id);
        }

        $user_id = array();
        $seller = User::find(Session::get('user')->id);
        if($seller){
            array_push($user_id, $seller->id);
        }
        $refer_seller = User::where('refer_code_others', $seller->refer_id)->first();
        if($refer_seller){
            array_push($user_id, $refer_seller->id);
        }
        if($request->member_type == 'member'){
            $user = User::find($request->user_id);
            if($user){
                array_push($user_id, $user->id);
            }
            if($user->level1){
                $level1 = User::where('refer_code_others', $user->level1)->first();
                if($level1){
                    array_push($user_id, $level1->id);
                }
            }
            if($user->level2){
                $level2 = User::where('refer_code_others', $user->level2)->first();
                if($level2){
                    array_push($user_id, $level2->id);
                }
            }
            if($user->level3){
                $level3 = User::where('refer_code_others', $user->level3)->first();
                if($level3){
                    array_push($user_id, $level3->id);
                }
            }
            if($user->level4){
                $level4 = User::where('refer_code_others', $user->level4)->first();
                if($level3){
                    array_push($user_id, $level4->id);
                }
            }
        }

        //ຕັດຄ່າການຊື້ສິນຄ້າ
        $percent = Percent::where('name', 'balance_buy')->where('status', '1')->get();
        foreach ($percent as $pc) {
            //check percent
            if($order->grand_total > $pc->start_amount && $order->grand_total <= $pc->end_amount){
                $cs = $order->grand_total * $pc->percent;
                foreach ($user_id as $id) {
                    $wallet = Wallet::where('user_id', $id)->first();
                    if($wallet){
                        $wallet->amount = $wallet->amount + $cs;
                    }else{
                        $wallet = new Wallet;
                        $wallet->user_id = $id;
                        $wallet->amount = $cs;
                    }
                    $wallet->save();
                }
            }
        }
        //ຕັດຄ່າການຂາຍສິນຄ້າ
        $percent_sale = Percent::where('name', 'percent_sale')->where('status', '1')->get();
        foreach ($percent_sale as $ps) {
            if($order->grand_total > $ps->start_amount && $order->grand_total <= $ps->end_amount){
                $per_sale = $order->grand_total * $ps->percent;
                $percentSale = PercentSale::where('user_id', $seller->id)->first();
                if($percentSale){
                    $percentSale->amount = $percentSale->amount+$per_sale;
                }else{
                    $percentSale = new PercentSale;
                    $percentSale->user_id = $seller->id;
                    $percentSale->amount = $per_sale;
                }
                $percentSale->save();
            }
        }
        if($request->member_type == 'member'){
            //ຕັດຄ່າພາສີການຊື້ສິນຄ້າ
            $tax = Tax::where('name', 'tax_sale')->where('status', '1')->first();
            $tax_sale = $cs * $tax->tax;
            $user_tax = UserTax::where('user_id', $request->user_id)->first();
            if($user_tax){
                $user_tax->amount = $user_tax->amount + $tax_sale;
            }else{
                $user_tax = new UserTax;
                $user_tax->user_id = $request->user_id;
                $user_tax->amount = $tax_sale;
            }
            $user_tax->save();
        }
        
    }
}
