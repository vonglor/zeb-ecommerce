@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
    <div class="content">
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">
            Edit Product
        </h2>
    </div>
    <form action="" method="POST" enctype="multipart/form-data" id="choice_form">
        @csrf
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Vertical Form -->
            <div class="intro-y box">
                <div id="vertical-form" class="p-5">
                    <div class="preview">
                        <div>
                            <label for="vertical-form-1" class="form-label text-base">Product Name</label>
                            <input id="vertical-form-1" type="text" name="name" class="form-control border-gray-500 text-base" placeholder="Product Name" required>
                        </div>
                            <!-- BEGIN: category -->
                        <div>
                            <label for="Category" class="text-base">Category</label>
                            {{-- <div class="mt-2">
                                <select class="form-select mt-2 sm:mr-2 border-gray-500" name="category_id" aria-label="Default select example">
                                    <option value="" class="text-lg">Select Category</option>
                                    @foreach ($category as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                        </div>
                        <!-- END: category -->
                            <!-- BEGIN: brand -->
                        {{-- <div class="w-full">
                            <label for="Brands" class="text-base">Brand</label>
                            <div class="mt-2">
                                <select class="form-select mt-2 sm:mr-2 border-gray-500" name="brand_id" aria-label="Default select example">
                                    <option value="" class="text-lg">Select Brand</option>
                                        @foreach ($brands as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    
                                </select>
                            </div>
                        </div> --}}
                        <!-- END: brand -->
                            <div>
                            <label for="unit" class="form-label text-base">Unit</label>
                            <input id="vertical-form-2" type="text" id="unit" class="form-control border-gray-500 text-base" placeholder="Unit (e.g.KG, Pc etc)" required>
                        </div>
                            <div>
                            <label for="min_qty" class="form-label text-base">Minimum Qty</label>
                            <input type="text" id="min_qty" name="min_qty" class="form-control border-gray-500 text-base" placeholder="1" required>
                        </div>
                    </div>
                </div>
            </div>
                <div class="intro-y box">
                <div id="vertical-form" class="p-5">
                    <div class="preview">
                            <div>
                            <label for="image" class="form-label text-base"> Images</label>
                            <input type="file" id="image" name="image" accept="image/*" class="form-control cursor-pointer border-gray-500 text-base" placeholder="choose file" required>
                            <div class="image-preview" id="image-preview"></div>
                        </div>
                            <div>
                            <label for="Thumbnail" class="form-label text-base"> Thumbnail Image</label>
                            <input type="file" id="thumbnail" name="thumbnail[]" accept="image/*" class="form-control cursor-pointer border-gray-500 text-base" placeholder="choose file" multiple required>
                            <div class="image-preview" id="thumbnail-preview">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y box">
                <div id="vertical-form" class="p-5">
                    <div class="preview">
                            <div>
                            <label for="video_link" class="form-label text-base">Video Link</label>
                            <input type="text" id="video_link" name="video_link" class="form-control border-gray-500 text-base" placeholder="Video Link">
                        </div>
                    </div>
                </div>
                    <div id="vertical-form" class="p-5">
                    <div class="preview">
                            <div>
                            <label for="PDF" class="form-label text-base">PDF Specification</label>
                            <input type="file" id="pdf" name="pdf" class="form-control border-gray-500 text-base" placeholder="PDF Specification">
                            <div class="image-preview" id="pdf-preview">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="intro-y box">
                <div id="vertical-form" class="p-5">
                    <div class="preview">
                    <div id="multiple-select" class="p-t-5">
                        <label for="Colors" class="form-label text-base">Colors:</label>
                        <div class="preview">
                        {{-- <select name="colors[]" id="colors" data-placeholder="Select your favorite colors" class="tom-select w-full border-gray-500" data-live-search="true" multiple data-selected-text-format="count">
                                @foreach (\App\Models\Color::all() as $key => $color)
                            <option value="{{ $color->name }}">
                                {{ $color->name }}
                            </option>
                            @endforeach
                        </select> --}}
                        {{-- <select class="form-select mt-2 sm:mr-2 border-gray-500" name="colors[]" id="colors" aria-label="Default select example">
                            <option value="">red</option>
                            <option value="">white</option> --}}
                            {{-- @foreach (\App\Models\Color::all() as $key => $color)
                                <option value="{{ $color->name }}">
                                    {{ $color->name }}
                                </option>
                                @endforeach --}}
                        
                        {{-- </select> --}}
                        </div>
                    </div>
                    {{-- <div id="multiple-select" class="p-t-5">
                        <label for="Attributes" class="form-label text-base">Attributes</label>
                        <div class="preview">
                        <select name="attributes[]" id="attributes" data-placeholder="Select your favorite actors" class="tom-select w-full border-gray-500" multiple>
                            @foreach (\App\Models\Attributes::all() as $key => $attribute)
                            <option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div> --}}
                        
                    <div class="intro-y box">
                        <div id="vertical-form">
                            <div class="preview">
                                <div>
                                    <label for="unit_price" class="form-label text-base">Unit price</label>
                                    <input type="number" id="unit_price" name="unit_price" class="form-control border-gray-500 text-base" placeholder="0" required>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    </div>
                </div>
            </div>
            
        </div>

            <!-- BEGIN: Shipping Configuration -->

        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Vertical Form -->
            
                <div class="intro-y box px-5">
                <div id="vertical-form">
                    <div class="preview">
                            <div>
                            <label for="purchase_price" class="form-label text-base">Purchase price</label>
                            <input type="number" id="purchase_price" name="purchase_price" class="form-control border-gray-500 text-base" placeholder="0" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y box px-5">
                <div id="vertical-form">
                    <div class="preview">
                            <div>
                            <label for="Discount" class="form-label text-base">Discount</label>
                            <input type="number" id="discount" name="discount" class="form-control border-gray-500 text-base" placeholder="0">
                        </div>
                    </div>
                </div>
            </div>
                <div class="intro-y box px-5">
                <div id="vertical-form">
                    <div class="preview">
                            <div>
                            <label for="Quantity" class="form-label text-base">Quantity</label>
                            <input type="number" name="quantity" class="form-control border-gray-500 text-base" placeholder="0" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y box px-5">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto text-base">
                        Product Description
                    </h2>
                </div>
                <div id="vertical-form">
                    <div class="preview">
                        <div>
                            <label for="title" class="form-label text-base">Title</label>
                            <input type="text" name="title" class="form-control border-gray-500 text-base" placeholder="Title" >
                        </div>
                    </div>
                </div>
            </div>
                <div class="intro-y box px-5">
                <div id="vertical-form">
                    <div class="preview">
                            <div>
                            <label for="description" class="form-label text-base"> Description</label>
                            <textarea name="description" id="description" cols="5" rows="10" class="form-control text-base"></textarea>
                        </div>
                    </div>
                </div>
            </div>
                <div class="intro-y box px-5">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto text-base">
                        Estimate Shipping Time
                    </h2>
                </div>
                <div id="vertical-form">
                    <div class="preview">
                        <div>
                            <label for="vertical-form-1" class="form-label text-base">Shipping Days</label>
                            <input type="text" name="shipping_day" class="form-control border-gray-500 text-base" placeholder="Shipping Days" required>
                        </div>
                    </div>
                </div>
            </div>
                <div class="intro-y box p-5">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium  text-lg">
                        TAX
                    </h2>
                </div>
                <div id="vertical-form">
                    <div class="preview">
                        <div>
                            <label for="vertical-form-1" class="form-label text-base">Tax</label>
                            <input type="text" name="tax" class="form-control border-gray-500 text-base" placeholder="0" >
                        </div>
                    </div>
                </div>
            </div>
                <button type="submit" class="btn btn-primary mt-5 text-base" id="btn_save">Upload Product</button>
        </div>
        
    </div>
    </form>
</div>
<!-- END: Content -->
    
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            $('.my-select').selectpicker();
            $('#colors').change(function(e) {
                update_sku();
            });

            function add_more_choice_option(i, name) {
                $('#choice_options').append(
                    '<div class="row col-sm-12 field"><div class="col-sm-3"><label for="" class="col-form-label">' +
                    name +
                    '</label><input type="hidden" name="choice_no[]" value="' + i +
                    '"><input type="hidden" name="choice[]" value="' + name +
                    '"></div><div class="col-sm-8"><input name="choice_options_' + i +
                    '[]" id="idInput' + i + '" class="tags" placeholder="Enter ' + name +
                    ' values" autofocus></div></div>');

                $('[class=tags]').tagify();
                var input = document.querySelector('input[class=tags]'),
                    tagify = new Tagify(input);
                $('#idInput' + i).on('change', function(e) {
                    update_sku();
                });
            }

            $(document).on('change', '#image', function() {
                var property = document.getElementById('image').files[0];
                var img_name = property.name;
                var img_extension = img_name.split('.').pop().toLowerCase();
                var img_preview = $('#image-preview');
                if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg', 'gif']) == -1) {
                    img_preview.html('<span style="color: red;">This not support file reader</span>');
                    return false;
                } else {
                    var img_size = property.size;
                    if (img_size > 2000000) {
                        img_preview.html('<span style="color: red;">This file is to big</span>');
                        return false;
                    } else {
                        if (typeof(FileReader) != 'undefined') {
                            img_preview.empty();
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('<img/>', {
                                    'src': e.target.result,
                                    'class': 'img-fluid',
                                    'style': 'max-height: 80px; max-width: 80px;'
                                }).appendTo(img_preview);
                            }
                            img_preview.show();
                            reader.readAsDataURL($(this)[0].files[0]);
                        } else {
                            img_preview.html(
                            '<span style="color:red;">This not support file reader</span>');
                        }
                    }
                }

            });

            $(document).on('change', '#thumbnail', function() {
                var totalfiles = document.getElementById('thumbnail').files.length;
                for (var index = 0; index < totalfiles; index++) {
                    var property = document.getElementById('thumbnail').files[index];
                    var img_name = property.name;
                    var img_extension = img_name.split('.').pop().toLowerCase();
                    var img_preview = $('#thumbnail-preview');
                    if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg']) == -1) {
                        img_preview.html('<span style="color: red;">This not support file reader</span>');
                        return false;
                    } else {
                        var img_size = property.size;
                        if (img_size > 2000000) {
                            img_preview.html('<span style="color: red;">This file is to big</span>');
                            return false;
                        } else {
                            if (typeof(FileReader) != 'undefined') {
                                img_preview.empty();
                                var reader = new FileReader();
                                reader.onload = function(e) {
                                    $('<img/>', {
                                        'src': e.target.result,
                                        'style': 'max-height: 80px; max-width: 80px;'
                                    }).appendTo(img_preview);
                                }
                                img_preview.show();
                                reader.readAsDataURL($(this)[0].files[index]);
                            } else {
                                img_preview.html(
                                    '<span style="color:red;">This not support file reader</span>');
                            }
                        }
                    }
                }

            });

            function update_sku() {
                $.ajax({
                    type: "POST",
                    url: '{{ route('products.sku_combination') }}',
                    data: $('#choice_form').serialize(),
                    success: function(data) {

                        $('#sku_combination').html(data);

                        if (data.length > 1) {
                            $('#quantity').hide();
                        } else {
                            $('#quantity').show();
                        }
                    }
                });
            }


            $('input[name="unit_price"]').on('input', function() {
                update_sku();
            });

            $('#attributes').change(function(e) {
                $('#choice_options').html(null);
                $.each($("#attributes option:selected"), function() {
                    add_more_choice_option($(this).val(), $(this).text());

                });
                update_sku();
            });

            $('#btn_delete').click(function(e) {
                e.preventDefault();
                alert('vong');
            });
        });
    </script>
@endsection