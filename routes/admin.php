<?php

use App\Http\Controllers\Backend\AboutController;
use App\Http\Controllers\Backend\AddressController;
use App\Http\Controllers\Backend\AttributeController;
use App\Http\Controllers\Backend\AuthController;
use App\Http\Controllers\Backend\BrandController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\ChatController;
use App\Http\Controllers\Backend\ColorController;
use App\Http\Controllers\Backend\CommissionController;
use App\Http\Controllers\Backend\CurrencyController;
use App\Http\Controllers\Backend\CustomerController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\ErrorController;
use App\Http\Controllers\Backend\MemberShipController;
use App\Http\Controllers\Backend\MessageController;
use App\Http\Controllers\Backend\OrderController;
use App\Http\Controllers\Backend\PercentProductController;
use App\Http\Controllers\Backend\PolicyController;
use App\Http\Controllers\Backend\PosController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\ProfileController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\RuleController;
use App\Http\Controllers\Backend\SaleController;
use App\Http\Controllers\Backend\SellerController;
use App\Http\Controllers\Backend\StaffController;
use App\Http\Controllers\Backend\TaxController;
use App\Http\Controllers\Backend\UploadController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\WithdrawRequestController;
use Illuminate\Support\Facades\Route;

use Illuminate\Events\Message;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('admin/', function () {
    return view('backend.dashboard');
});

Route::post('/send-message', function(Request $request){
    event(
        new Message(
            $request->input('username'),
            $request->input('message'))
    );
    return ["Succes"=>true];
});
    

// Route::get('auth/login', [AuthController::class, 'index'])->name('login');
// Route::post('/custom-signin', [AuthController::class, 'createSignin'])->name('signin.custom');

// Route::get('auth/register', [AuthController::class, 'register'])->name('register');
// Route::post('/create-user', [AuthController::class, 'customSignup'])->name('user.registration');

// Route::get('/dashboard', [AuthController::class, 'dashboardView'])->name('dashboard');
// Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

// route product
Route::get('product', [ProductController::class, 'index'])->name('product');
Route::get('product/store', [ProductController::class, 'store'])->name('product.store');
Route::post('add/product', [ProductController::class, 'store'])->name('add/product');
Route::post('product/update/', [ProductController::class, 'update_product'])->name('product.update_product');
Route::get('all_product', [ProductController::class, 'allProuct'])->name('product_all');
Route::get('all_product_inhouse', [ProductController::class, 'allProuctInhouse'])->name('product_all_inhouse');
Route::get('all_product_seller', [ProductController::class, 'allProuctSeller'])->name('product_all_seller');
Route::post('product/sku_combination', [ProductController::class, 'sku_combination'])->name('products.sku_combination');
Route::post('product/sku_combination_edit', [ProductController::class, 'sku_combination_edit'])->name('products.sku_combination_edit');

// route category
Route::get('category', [CategoryController::class, 'index'])->name('get.category');
Route::get('add/category', [CategoryController::class, 'create'])->name('add.category');
Route::get('edit/category', [CategoryController::class, 'edit'])->name('edit.category');
Route::get('update/category', [CategoryController::class, 'update'])->name('update.category');
Route::get('delete/category', [CategoryController::class, 'destroy'])->name('delete.category');

// route brand
Route::resource('brand', BrandController::class);

Route::get('request_withdraw', [CustomerController::class, 'payoutRequest'])->name('customer_withdraw');

// route attribute
Route::resource('attribute', AttributeController::class);

// route color
Route::resource('color', ColorController::class);
Route::post('add/color', [ColorController::class, 'add_color'])->name('add.color');

Route::get('customer', [CustomerController::class, 'index'])->name('customer');
Route::get('payout', [CustomerController::class, 'payout'])->name('customer_payout');

Route::get('customer/index', [CustomerController::class, 'index'])->name('customer.index');
Route::get('payout', [CustomerController::class, 'payout'])->name('customer_payout');
Route::get('request_withdraw', [CustomerController::class, 'payoutRequest'])->name('customer_withdraw');
Route::get('status/{id}', [CustomerController::class, 'UpdateStatusUser'])->name('UpdateStatus');
Route::get('delete/customer/{id}', [CustomerController::class, 'destroy'])->name('delete/customer');
Route::put('update/customer/{id}', [CustomerController::class, 'update'])->name('update/customer');
Route::get('customer/memberfee', [CustomerController::class, 'membership'])->name('customer/memberfee');
Route::get('edit/customer/{id}', [CustomerController::class, 'edit'])->name('edit/customer');

Route::get('customer/refferid', [CustomerController::class, 'genkeyreffer'])->name('customer/refferid');
Route::post('add/customer/memberfee', [CustomerController::class, 'store'])->name('add/customer/memberfee');
Route::get('edit/customer/memberfee/{id}', [CustomerController::class, 'editMembership'])->name('edit/customer/memberfee');
Route::post('upate/customer/memberfee/{id}', [CustomerController::class, 'updateMembership'])->name('upate/customer/memberfee');

// route seller
Route::get('seller/index', [SellerController::class, 'index'])->name('seller.index');
Route::get('seller_commission', [SellerController::class, 'sellerCommission'])->name('seller_commission');
Route::get('seller_payout', [SellerController::class, 'sellerPayout'])->name('seller_payput');
Route::get('seller_withdraw', [SellerController::class, 'sellerWithdraw'])->name('seller_withdraw');
Route::put('update/seller/{id}', [SellerController::class, 'update'])->name('update/seller');
Route::get('edit/seller/{id}', [SellerController::class, 'edit'])->name('edit/seller');

// route employee
Route::resource('employee', StaffController::class);
Route::get('employee', [StaffController::class, 'index'])->name('employee');
Route::get('allemployee', [StaffController::class, 'allEmployee'])->name('all_employee');
Route::get('permission', [StaffController::class, 'permission'])->name('employee_permission');

Route::get('save/employee', [StaffController::class, 'store'])->name('manage.employee');

// rote order
Route::resource('sale', SaleController::class);
Route::get('sellerorder', [SaleController::class, 'sellerOrder'])->name('seller_order');
Route::get('inhouseorder', [SaleController::class, 'inHouseOrder'])->name('inhouse_order');
Route::get('detail/order', [SaleController::class, 'detailOrder'])->name('detail.order');

// route currency
Route::get('get/currency', [CurrencyController::class, 'index'])->name('get.currency');
Route::post('add/currency', [CurrencyController::class, 'store'])->name('add.currency');
Route::get('edit/{id}', [CurrencyController::class, 'edit'])->name('edit.currency');
Route::put('update/{id}', [CurrencyController::class, 'update'])->name('update.currency');
Route::delete('delete/{id}', [CurrencyController::class, 'destroy'])->name('delete.currency');
Route::get('status', [CurrencyController::class, 'submitDefault'])->name('defaultSubmit');

// route exchange
Route::get('exchange', [CurrencyController::class, 'exchangerate'])->name('exchangerate');
Route::post('add/exchange', [CurrencyController::class, 'insertexchangerate'])->name('add.exchangerate');
Route::get('edit/exchange/{id}', [CurrencyController::class, 'editexchangerate'])->name('edit.exchangerate');
Route::put('exchange/update/{id}', [CurrencyController::class, 'updateExchangerate'])->name('update.exchangerate');
Route::delete('delete/exchange/{id}', [CurrencyController::class, 'delete'])->name('delete.exchangerate');

// route rule
Route::get('rule', [RuleController::class, 'index'])->name('rule');

// route tax
Route::resource('tax', TaxController::class);
Route::get('tax', [TaxController::class, 'index'])->name('tax');
Route::get('status/tax', [TaxController::class, 'UpdateStatusNoti'])->name('UpdateStatusNoti');

// route message
Route::get('show/message', [MessageController::class, 'index'])->name('show.message');
Route::post('add/message', [MessageController::class, 'store'])->name('add.message');
Route::get('edit/message/{id}', [MessageController::class, 'edit'])->name('edit.message');
Route::put('update/message/{id}', [MessageController::class, 'update'])->name('update.message');
Route::delete('delete/message/{id}', [MessageController::class, 'destroy'])->name('delete.message');
Route::get('all_message', [MessageController::class, 'allMessage'])->name('all.message');
Route::get('seller_message', [MessageController::class, 'sellerMessage'])->name('seller.message');

// update message status
Route::get('update/status', [MessageController::class, 'UpdateStatusNoti'])->name('UpdateStatusNoti');

// route chat
Route::get('chat', [ChatController::class, 'index'])->name('chat');




//route upload file
Route::get('upload/file', [UploadController::class, 'index'])->name('upload/file');
Route::delete('delete/file/{id}', [UploadController::class, 'destroy'])->name('delete/file');

// route about
Route::resource('about', AboutController::class);
Route::get('about', [AboutController::class, 'index'])->name('about');

// Route::get('about-us', [AboutController::class,'about_us'])->name('about-us');
Route::patch('update/about-us/{about_us}', [AboutController::class, 'update'])->name('update/about');

// rote policy
Route::get('policy', [PolicyController::class, 'index'])->name('get.policy');
Route::get('add/policy', [PolicyController::class, 'store'])->name('add.policy');
Route::get('edit', [PolicyController::class, 'edit'])->name('edit.policy');
Route::get('update', [PolicyController::class, 'update'])->name('update.policy');
Route::get('delete', [PolicyController::class, 'destroy'])->name('delete.policy');

// route warning policy
Route::get('warningpolicy', [PolicyController::class, 'warnigPolicy'])->name('warning.policy');
// rote percent product
Route::get('percentproduct', [PercentProductController::class, 'index'])->name('percentproduct');
Route::post('add/percentproduct', [PercentProductController::class, 'store'])->name('add/percentproduct');
Route::delete('delete/percentproduct/{id}', [PercentProductController::class, 'destroy'])->name('delete/percentproduct');
Route::get('edit/percentproduct/{id}', [PercentProductController::class, 'edit'])->name('edit/percentproduct');
Route::post('update/percentproduct/{id}', [PercentProductController::class, 'update'])->name('update/percentproduct');

// rote percent sale
Route::get('percentsale', [PercentProductController::class, 'percentsale'])->name('percentsale');
Route::post('add/percentsale', [PercentProductController::class, 'addpercentsale'])->name('add/percentsale');
Route::delete('delete/percentsale/{id}', [PercentProductController::class, 'Delete'])->name('delete/percentsale');
Route::get('edit/percentsale/{id}', [PercentProductController::class, 'editsale'])->name('edit/percentsale');
Route::post('update/percentsale/{id}', [PercentProductController::class, 'updatepercent'])->name('update/percentsale');

// route pos
Route::get('pos', [PosController::class, 'index'])->name('point-of-sale');

// route profile
Route::resource('profile', ProfileController::class);

// route address
Route::resource('address', AddressController::class);
Route::get('address/edit/{id}', [AddressController::class, 'address_edit'])->name('address.address_edit');
Route::post('address/update', [AddressController::class, 'address_update'])->name('address.address_update');
Route::get('address/delete/{id}', [AddressController::class, 'delete'])->name('address.delete');

// route error 404
Route::get('error/', [ErrorController::class, 'Error404']);

// ===========================================================================================================

//route dashboard
Route::get('admin/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

//route membership & commission
Route::get('membership/index', [MemberShipController::class, 'index'])->name('membership.index');
Route::patch('membership/{m}', [MemberShipController::class, 'update'])->name('membership.update');
Route::get('commission/index', [CommissionController::class, 'index'])->name('commission.index');
Route::patch('commission/{c}', [CommissionController::class, 'update'])->name('commission.update');

//route withdraw request
Route::get('withdraw-request/{status}', [WithdrawRequestController::class, 'index'])->name('withdraw-request');
Route::get('withdraw-request/edit/{w}', [WithdrawRequestController::class, 'edit'])->name('withdraw-request.edit');
Route::patch('withdraw-request/update{w}', [WithdrawRequestController::class, 'update'])->name('withdraw-request.update');
Route::post('withdraw-request/pay', [WithdrawRequestController::class, 'pay'])->name('withdraw.pay');
Route::delete('withdraw-request/destroy/{w}', [WithdrawRequestController::class, 'destroy'])->name('withdraw.destroy');
Route::get('seller-withdraw-request/{status}', [WithdrawRequestController::class, 'seller'])->name('seller-withdraw-request');

//route order
Route::get('order/fetch/{status}', [OrderController::class, 'index'])->name('order.fetch');
Route::get('order/show/{order}/{param}', [OrderController::class, 'show'])->name('order.show');
Route::patch('order/change-status/{order}', [OrderController::class, 'change_status'])->name('order.change_status');
Route::delete('order/destroy/{order}', [OrderController::class, 'destroy'])->name('order.destroy');
Route::get('order/fetch/order-seller/{status}', [OrderController::class, 'fetchOrderBySeller'])->name('order.fetchOrderBySeller');

//user routes
Route::get('users/index', [UserController::class, 'index'])->name('users.index');
Route::get('users/create', [UserController::class, 'create'])->name('users.create');
Route::post('users/store', [UserController::class, 'store'])->name('users.store');
Route::get('users/edit/{staff}', [UserController::class, 'edit'])->name('users.edit');
Route::put('users/update/{staff}', [UserController::class, 'update'])->name('users.update');
Route::get('users/edit/pwd/{user}/{param}', [UserController::class, 'editPwd'])->name('users.editPwd');
Route::put('users/reset-pwd/{user}', [UserController::class, 'resetPwd'])->name('users.resetPwd');
Route::post('users/changeStatus', [UserController::class, 'changeStatus'])->name('users.changeStatus');
Route::delete('users/destroy/{user}', [UserController::class, 'destroy'])->name('users.destroy');
Route::get('users/sign-in', [UserController::class, 'signIn'])->name('users.signIn');
Route::post('users/signInPost', [UserController::class, 'signInPost'])->name('users.signInPost');
Route::get('users/sign-out', [UserController::class, 'signOut'])->name('users.signOut');
Route::get('users/profile/{user}', [UserController::class, 'profile'])->name('users.profile');
Route::put('users/update-profile/{user}', [UserController::class, 'updateProfile'])->name('users.updateProfile');
Route::put('users/change-pwd/{user}', [UserController::class, 'changePwd'])->name('users.changePwd');

//role routes
Route::get('roles/index', [RoleController::class, 'index'])->name('roles.index');
Route::get('roles/create', [RoleController::class, 'create'])->name('roles.create');
Route::post('roles/store', [RoleController::class, 'store'])->name('roles.store');
Route::get('roles/{role}', [RoleController::class, 'edit'])->name('roles.edit');
Route::put('roles/update/{role}', [RoleController::class, 'update'])->name('roles.update');
Route::delete('roles/destroy/{role}', [RoleController::class, 'destroy'])->name('roles.destroy');

//route pos
Route::get('pos-sale/index', [PosController::class, 'index'])->name('pos-sale.index');
Route::get('pos-sale/add_to_cart/{id}', [PosController::class, 'add_to_cart'])->name('pos-sale.add_to_cart');
Route::post('pos-sale/update_cart', [POSController::class, 'update_cart'])->name('pos-sale.update_cart');
Route::get('pos-sale/remove_cart/{key}', [POSController::class, 'remove_cart'])->name('pos-sale.remove_cart');
Route::get('pos-sale/clear_cart_items/{key}', [POSController::class, 'clear_cart_items'])->name('pos-sale.clear_cart_items');
Route::post('pos-sale/filter_category', [POSController::class, 'filter_category'])->name('pos-sale.filter_category');
Route::get('pos-sale/search/product', [POSController::class, 'search_product'])->name('pos-sale.search_product');
Route::get('pos-sale/get_users', [POSController::class, 'get_users'])->name('pos-sale.get_users');



