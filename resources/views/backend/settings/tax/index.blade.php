@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
                
    <div class="content">
    <div class="intro-y flex items-center mt-8">
        <h1 class="text-xl font-medium mr-auto">
        All Taxes
        </h1>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 overflow-auto">
            <div class="float-right">
                <a type="submit" class="btn btn-primary text-base" data-toggle="modal" data-target="#modal-add">Add New Tax</a>
            </div>
        <!-- insert -->
        <div class="intro-y box mt-5">
                <div id="header-footer-modal">
                    <div class="preview">
                        <div id="modal-add" class="modal" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h2 class="font-medium text-base mr-auto text-lg">
                                        Add New Tax
                                        </h2>
                                        <button type="button" data-dismiss="modal" class="col-span-6 sm:col-span-3 lg:col-span-2 xl:col-span-1"><i data-feather="x" class="block mx-auto"></i> </button>
                                        
                                    </div>
                                    <form action="{{route('tax.store')}}" method="POST">
                                        @csrf
                                        <div class="modal-body grid grid-cols-6 gap-4 gap-y-3">
                                            <div class="col-span-12 sm:col-span-6">
                                                <label for="modal-form-1" class="form-label text-base">Country Name</label>
                                                <input id="modal-form-1" type="text" name="name" class="form-control border-gray-500 text-base" placeholder="Country Name" required>
                                            </div>
                                            <div class="col-span-12 sm:col-span-6">
                                                <label for="modal-form-1" class="form-label text-base">Tax Price</label>
                                                <input id="modal-form-1" type="text" name="tax" class="form-control border-gray-500 text-base" placeholder="Tax price" required>
                                            </div>
                                        </div>
                                        <div class="modal-footer text-right">
                                            <button type="button" data-dismiss="modal" class="btn btn-danger w-20 mr-1 text-base">Cancel</button>
                                            <button type="submit" class="btn btn-primary w-20 text-base">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <!-- end insert -->
            <table class="table table-report -mt-2" id="dataTable">
                <thead>
                    <tr>
                        <th class="whitespace-nowrap text-base">#</th>
                        <th class="whitespace-nowrap text-base">Country Name</th>
                        <th class="whitespace-nowrap text-base">Tax Price</th>
                        <th class="whitespace-nowrap text-base">Status</th>
                        <th class="whitespace-nowrap text-base">Switch</th>
                        <th class="text-center whitespace-nowrap text-base">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 0;
                    @endphp
                    @foreach ($tax as $item)
                    <tr class="intro-x">
                        <td class="w-40">{{++$i}}</td>
                        <td>{{ substr($item->name,0,80) }} ....</td>
                        <td>{{ $item->tax }} ....</td>
                        <td id="resp{{ $item->id }}">
                            <br>
                                @if($item->status == 1)
                                <button type="button" class="btn btn-sm btn-success text-base">Active</button>
                                    @else
                                <button type="button" class="btn btn-sm btn-danger text-base">Inactive</button>
                                @endif
                            </td>
                        <td>
                        <input data-id="{{ $item->id }}" class="form-check-switch" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $item->status ? 'checked' : '' }}>
                        <span class="slider round"></span>
                        </td>
                        <td class="table-report__action w-56 p-5">
                            <div class="flex justify-center items-center">
                                <a class="btn btn-primary" href="">  <i class="w-4 h-3 mr-2" data-feather="eye"></i></a>
                                <form action="{{route('tax.destroy', $item->id)}}" method="POST" class="px-2">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>

    </div>
</div>
<!-- END: Content -->
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();    
    });
</script>

        <script src="{{ asset('js/jquery.min.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $(window).load(function() {
                    $(".cargando").fadeOut(1000);
                });


        $('.form-check-switch').change(function() {
            //Verifico el estado del checkbox, si esta seleccionado sera igual a 1 de lo contrario sera igual a 0
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var id = $(this).data('id'); 
                console.log(status);

            $.ajax({
                type: "GET",
                dataType: "json",
                //url: '/StatusNoticia',
                url: '{{ route('UpdateStatusNoti') }}',
                data: {'status': status, 'id': id},
                success: function(data){
                    $('#resp' + id).html(data.var); 
                    console.log(data.var)
                
                }
            });
        })
            
        });
        </script>
    
@endsection