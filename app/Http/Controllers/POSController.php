<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PosOrderController;
use App\Models\Order;
use App\Models\Product;
use App\Models\Product_stock;
use Illuminate\Http\Request;
use Session;

class POSController extends Controller
{
    public function index()
    {
        $user = Session::get('user');
        // $products = Product::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(12);
        $products = Product::join('product_stocks', 'product_stocks.product_id', 'products.id')
            ->select('product_stocks.*', 'products.name', 'products.photos', 'products.discount', 'products.currency_code')
            ->where('products.user_id', $user->id)->orderBy('product_stocks.id', 'desc')->paginate(12);
        // dd(count($products));
        return view('frontend.saller.pos.index', \compact('products'));
    }

    public function detail($id)
    {
        $product = Product::find($id);
        return view('frontend.saller.pos.detail', \compact('product'));
    }

    public function add_to_cart($id)
    {

        // $product_id = $request->product_id;
        $pro_stock = Product_stock::find($id);
        $product   = Product::find($pro_stock->product_id);

        $data             = array();
        $data['pro_id']   = $product->id;
        $data['id']       = $pro_stock->id;
        $data['owner_id'] = $product->user_id;
        $data['name']     = $product->name;
        $data['variant']  = $pro_stock->variant;
        $data['qty']      = 1;
        $data['price']    = $pro_stock->price - $product->discount;

        if ($pro_stock->image) {
            $data['image'] = $pro_stock->image;
        } else {
            $data['image'] = $product->photos;
        }

        if (Session::has('pos_cart')) {
            $foundInCart = false;
            $pos_cart    = collect();

            foreach (Session::get('pos_cart') as $key => $cartItem) {
                if ($cartItem['id'] == $pro_stock->id) {
                    if ($pro_stock->qty < $cartItem['qty']) {
                        $notification = array(
                            'message'    => 'This item is out of stock!.',
                            'alert-type' => 'warning',
                        );
                        return back()->with($notification);
                    } else {
                        $foundInCart     = true;
                        $cartItem['qty'] = $cartItem['qty'] + 1;
                    }
                }
                $pos_cart->push($cartItem);
            }
            if (!$foundInCart) {
                $pos_cart->push($data);
            }
            Session::put('pos_cart', $pos_cart);
        } else {
            $pos_cart = collect([$data]);
            Session::put('pos_cart', $pos_cart);
        }
        $notification = array(
            'message'    => 'Item has been added to cart.',
            'alert-type' => 'success',
        );
        return back()->with($notification);
    }

    public function update_cart(Request $request)
    {

        $pos_cart = $request->session()->get('pos_cart', collect([]));

        $pos_cart = $pos_cart->map(function ($object, $key) use ($request) {
            if ($key == $request->key) {
                $product  = Product_stock::find($object['id']);
                $quantity = $product->qty;
                if ($quantity >= $request->qty) {
                    if ($request->qty >= $product->min_qty) {
                        $object['qty'] = $request->qty;
                    }
                }
            }
            return $object;
        });
        $request->session()->put('pos_cart', $pos_cart);

    }

    public function remove_cart($key)
    {
        if (session()->has('pos_cart')) {
            $pos_cart = session()->get('pos_cart', collect([]));
            $pos_cart->forget($key);
            session()->put('pos_cart', $pos_cart);
        }
        $notification = array(
            'message'    => 'Item has been removed from cart.',
            'alert-type' => 'success',
        );
        return back()->with($notification);
    }

    public function search_id($value)
    {
        $user_id  = Session::get('user')->id;
        $products = Product::join('product_stocks', 'product_stocks.product_id', 'products.id')
            ->where('products.product_id', 'LIKE', '%'.$value.'%')
            ->where('products.user_id', $user_id)
            ->paginate(12);

        // $products = Product::where('id', 'LIKE', '%'.$value.'%')->where('user_id', $user_id)->paginate(12);
        return view('frontend.saller.pos.product_list', \compact('products'));
    }

    public function search()
    {
        $user_id  = Session::get('user')->id;
        $search   = request('search');
        $products = Product::join('product_stocks', 'product_stocks.product_id', 'products.id')
            ->where('products.name', 'LIKE', '%' . $search . '%')->where('products.user_id', $user_id)->paginate(12);
        return view('frontend.saller.pos.index', \compact('products'));
    }

    public function category($id)
    {
        $user_id = Session::get('user')->id;
        if ($id == 'all') {
            $products = Product::join('product_stocks', 'product_stocks.product_id', 'products.id')
                ->where('products.user_id', $user_id)->paginate(12);
        } else {
            $products = Product::join('product_stocks', 'product_stocks.product_id', 'products.id')
                ->where('products.category_id', '=', $id)->where('products.user_id', $user_id)->paginate(12);
        }

        return view('frontend.saller.pos.product_list', \compact('products'));
    }

    public function checkout(Request $request)
    {
        if ($request->payment_type != null) {
            // $pos = new POSController;
            // return $pos->order($request);
            $request->session()->put('payment_type', 'cart_payment');
            if ($request->payment_type == 'cash') {
                $pos = new PosOrderController;
                $pos->order($request);
                $request->session()->forget('pos_cart');
                return redirect()->route('pos_order_confirmed');
            } elseif ($request->payment_type == 'wallet') {
                $user  = Session::get('user');
                $order = Order::findOrFail($request->session()->get('order_id'));
                if ($user->balance >= $order->grand_total) {
                    $user->balance -= $order->grand_total;
                    $user->save();
                    return $this->checkout_done($request->session()->get('order_id'), null);
                }
            } else {
                $order                 = Order::findOrFail($request->session()->get('order_id'));
                $order->manual_payment = 1;
                $order->save();

                $request->session()->put('cart', Session::get('cart')->where('owner_id', '!=', Session::get('owner_id')));
                $request->session()->forget('owner_id');

                flash(translate('Your order has been placed successfully. Please submit payment information from purchase history'))->success();
                return redirect()->route('order_confirmed');
            }
        } else {
            // flash(translate('Select Payment Option.'))->warning();
            // return back();
        }
    }

    public function select_user_id()
    {

        // $u = User::all();
        // $out = '<label for="inputHelpBlock">Customer ID</label><select name="user_id" class="form-control" data-live-search="true">';
        // $out .= '<option value="">Customer ID</option>';
        // foreach ($u as $user){
        //     $out .= '<option value="'.$user->id.'">'.$user->id.'</option>';
        // }
        // $out .= '</select>';
        // echo $out;
        return view('frontend.saller.pos.user_id');
    }

    public function credit_card(Request $request)
    {

        $pos = new PosOrderController;
        $pos->order($request);
        $request->session()->forget('pos_cart');
        return redirect()->route('pos_order_confirmed');
    }

    public function pos_order_confirmed()
    {
        $order = Order::findOrFail(Session::get('order_id'));
        return view('frontend.saller.pos.order_confirmed', \compact('order'));
    }

}
