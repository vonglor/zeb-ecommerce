@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h1 class="text-xl font-medium mr-auto">
                Policy return
            </h1>
        </div>
        <div class="grid grid-cols-6 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
            <form action="" method="post" id="tnc-form">
                @csrf
                <div class="form-group">
                    <textarea class="ckeditor form-control text-base" name="policy"></textarea>
                </div>

                <button type="submit" class="btn btn-primary text-base">Submit</button>
            </form>
            </div>

        </div>
    </div>
<!-- END: Content -->
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });
    </script>
    
@endsection

