@php
$user = Session::get('user');
@endphp

@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/checkout.css') }}" rel="stylesheet">
    <style>
    .card {
        margin-bottom: 30px;
    }
    .detail span {
        font-size: 14px;
    }
    .card-body {
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        min-height: 1px;
        padding: 1.25rem;
    }
    
    *, ::after, ::before {
        box-sizing: border-box;
    }

    div {
        display: block;
    }
    .card {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0,0,0,.125);
        border-radius: .25rem;
    }

    .text-center {
        text-align: center!important;
    }
    .pb-4, .py-4 {
        padding-bottom: 1.5rem!important;
    }
    
    .pt-4, .py-4 {
        padding-top: 1.5rem!important;
    }
    .mb-4, .my-4 {
        margin-bottom: 1.5rem!important;
    }
    .opacity-70 {
        opacity: 0.7 !important;
    }
    .font-italic {
        font-style: italic!important;
        font-size: 13px;
    }
    
    p {
        margin-top: 0;
        margin-bottom: 1rem;
    }
    .text-primary {
        color: #e62e04 !important;
    }
    .h5 {
        font-size: 20px;
    }
    .mb-3{
        font-size: 27px;
    }
    i.fa.fa-check-circle-o.la-3x.text-success.mb-3::before{
        font-size: 39px;
    }
    .produt_qty{
            display: flex;
            flex-direction: row;
        }
        .color{
            margin-left: 10px;
            width:25px;
            height:25px;
            border:1px solid #000;
            display:inline-block;
            margin-right:10px;
            cursor:pointer;
            text-align:center;
            line-height:35px;
            font-size:12px;
        }
        .address{
            border: 1px solid #ddd;
            margin-left: 25px;
            padding-left: 15px;
            width: 50%;
        }
    </style>
@endsection
@section('content')
    <section id="center" class="clearfix checkout">
        <div class="container">
            <div class="row">
                    <section class="py-4">
                        <div class="container text-left">
                            <div class="row">
                                <div class="col-sm-12 mx-auto">
                                    <div class="card shadow-sm border-0 rounded">
                                        <div class="card-body">
                                            <div class="text-center py-4 mb-4">
                                                <i class="fa fa-check-circle-o la-3x text-success mb-3"></i>
                                                <h1 class="h3 mb-3 fw-600">Thank You for Your Order!</h1>
                                                <h2 class="h5">Order Number: <span class="fw-700 text-primary">{{ $order->code }}</span></h2>
                                            </div>
                                            <div class="mb-4">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        @if ($order->user_id)
                                                        <div class="address">
                                                            <p>Name: {{ $order->user->name }}</p>
                                                            <p>Email: {{ $order->user->email }}</p>
                                                            <p>Phone: {{ $order->user->phone }}</p>
                                                        </div>
                                                        @else
                                                        Have not member
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered">
                                                                    Order Detailsbordered
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Image</th>
                                                                        <th>Name</th>
                                                                        <th>qty</th>
                                                                        <th>price</th>
                                                                    </tr>
                                                                    @php
                                                                        $order_details = App\Models\Order_detail::where('order_id', $order->id)->paginate(5);
                                                                        $i = 1;
                                                                        $subtotal = 0;
                                                                    @endphp
                                                                    @foreach ($order_details as $key => $order_detail)
                                                                    @php
                                                                        $subtotal += $order_detail->price*$order_detail->qty;
                                                                    @endphp
                                                                    <tr>
                                                                        <td>{{ $i }}</td>
                                                                        <td>
                                                                            @php
                                                                            $stock = App\Models\Product_stock::find($order_detail->product_id);
                                                                            @endphp
                                                                            @if ($stock != null)
                                                                                @if ($stock->image != null)
                                                                                <img src="{{ url(App\Models\Upload::find($stock->image)->file_name) }}" alt="" class="img-responsive" style="height: 80px; margin-top: 10px;">
                                                                                @else
                                                                                <img src="{{ url(App\Models\Upload::find(App\Models\Product::find($stock->product_id)->photos)->file_name) }}" class="img-responsive" alt="" style="height: 80px; margin-top: 10px;">
                                                                                @endif
                                                                            @else
                                                                            <img src="{{ url(App\Models\Upload::find(App\Models\Product::find($stock->product_id)->photos)->file_name) }}" class="img-responsive" alt="" style="height: 80px; margin-top: 10px;">
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            <span>{{ App\Models\Product::find($order_detail->productStock->product_id)->name }}</span><br>
                                                                            @if ($order_detail->color)
                                                                            <span>Color: {{ $order_detail->color }}</span><br>
                                                                            @endif
                                                                            @if ($order_detail->size)
                                                                            <span>Size: {{ $order_detail->size }}</span><br>
                                                                            @endif
                                                                            @if ($order_detail->fabric)
                                                                            <span>Fabric: {{ $order_detail->fabric }}</span><br>
                                                                            @endif
                                                                            @if ($order_detail->poud)
                                                                            <span>Poud: {{ $order_detail->poud }}</span>
                                                                            @endif
                                                                        </td>
                                                                        <td>{{ $order_detail->qty }}</td>
                                                                        <td>{{ usd_symbol().number_format($order_detail->price, 2) }}</td>
                                                                    </tr>
                                                                    @php
                                                                        $i++;
                                                                    @endphp
                                                                    @endforeach
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3" style="float: right">
                                                        <table class="table table-bordered">
                                                                <tr>
                                                                    Order Ammount
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%">Sub Total:</td>
                                                                    <td width="50%">{{ usd_symbol().number_format($subtotal, 2) }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>tax:</td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Shipping:</td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>TOTAL:</td>
                                                                    <td>{{ usd_symbol().number_format($order->grand_total, 2) }}</td>
                                                                </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="center_product_1r4 clearfix">
                                            <div class="col-sm-6">
                                                <div class="center_product_1r4l clearfix">
                                                    {{ $order_details->links() }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="back text-center" style="margin: 10px 0;">
                                            
                                            <a href="{{ route('pos.index') }}">Go to shopping</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
            </div>
        </div>
    </section>
@endsection

