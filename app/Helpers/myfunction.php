<?php

use App\Models\Order;
use App\Models\Seller;
use App\Models\Product;
use Illuminate\Support\Facades\Session;

function countProduct(){
    $user = Session::get('user');
    $product = Product::all();
    $count = count($product);
    return $count;
}


function totalSale(){
    $user_id = Session::get('user')->id;
    $seller_id = Seller::where('user_id', Session::get('user')->id)->first()->id;
    $sales = Order::where('seller_id', $seller_id)
                ->where('payment_status', 'paid')
                ->where('delivery_status', 'delivered')
                ->get();
    $total_sale = 0;
    foreach ($sales as $value) {
        $total_sale += $value->grand_total;
    }

    return $total_sale;
}

function saleToday(){
    $user_id = Session::get('user')->id;
    $seller_id = Seller::where('user_id', Session::get('user')->id)->first()->id;
    $date = now();
    $sales = Order::where('seller_id', $seller_id)
                ->where('payment_status', 'paid')
                ->where('delivery_status', 'delivered')
                ->where('date', $date)
                ->get();
    $total_sale = 0;
    foreach ($sales as $value) {
        $total_sale += $value->grand_total;
    }
    return $total_sale;
}

function countProductBestsale(){
    $user = Session::get('user');
    $product = Product::where('user_id', $user->id)
                ->where('num_of_sale', '>', 0)
                ->orderBy('num_of_sale', 'desc')
                ->get();
    $count = count($product);
    return $count;
}

function user(){
    $user = Session::get('user');
    return $user;
}

if(! function_exists('newOrder')){
    function newOrder(){
        $user_id   = Session::get('user')->id;
        $seller_id = Seller::where('user_id', Session::get('user')->id)->first()->id;
        $orders    = Order::where('seller_id', $seller_id)
                    ->where('viewed', 0)
                    ->orderBy('id', 'desc')
                    ->get();
        return count($orders);
    }
}


