<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PercentProduct extends Model
{
    use HasFactory;
    protected $table = 'percents';
    protected $fillable = [
        'start_amount',
        'end_amount', 
        'percent', 
        'name'
    ];
}

