<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product_stock extends Model
{
    use HasFactory; 
    
    protected $fallable = [
        'product_id',
        'variant',
        'price',
        'qty',
        'image'
    ];

    /**
     * Get the Product that owns the Product_stock
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
