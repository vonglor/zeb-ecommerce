@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="grid grid-cols-12 gap-6">
            <div class="col-span-12 xxl:col-span-9">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: General Report -->
                    <div class="col-span-12 mt-8">
                        <div class="intro-y flex items-center h-10">
                            <h2 class="text-xl font-medium truncate mr-5">
                                Owner Report
                            </h2>
                            <a href="" class="ml-auto flex items-center text-theme-26 text-base"> <i data-feather="refresh-ccw" class="w-4 h-4 mr-3"></i> Reload Data </a>
                        </div>
                        <div class="grid grid-cols-12 gap-6 mt-5">
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Instore</div>
                                        <div class=" relative text-2xl font-medium leading-5 mt-3.5">47,578 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Sold out</div>
                                        <div class=" relative text-2xl font-medium leading-5 pl-4 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5">$</span> 47,578.77 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Total sold</div>
                                        <div class=" relative text-2xl font-medium leading-5 pl-4 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5">$</span> 20,578.77 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Sold out today</div>
                                        <div class=" relative text-2xl font-medium leading-5 pl-4 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5">$</span> 17,578.77 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Pedding order</div>
                                        <div class=" relative text-2xl font-medium leading-5 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5"></span> 270.00</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Total order</div>
                                        <div class=" relative text-2xl font-medium leading-5 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5"></span> 470.00 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Cstm%</div>
                                        <div class=" relative text-2xl font-medium leading-5 pl-4 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5">$</span> 10,578.73 </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: General Report -->
                    <!-- BEGIN: Seller Report -->
                    <div class="col-span-12">
                        <div class="intro-y flex items-center h-10">
                            <h2 class="text-xl font-medium truncate mr-5">
                                Seller Report
                            </h2>
                            <a href="" class="ml-auto flex items-center text-theme-26 dark:text-theme-33 text-base"> <i data-feather="refresh-ccw" class="w-4 h-4 mr-3"></i> Reload Data </a>
                        </div>
                        <div class="grid grid-cols-12 gap-6 mt-5">
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> All Buyer</div>
                                        <div class=" relative text-2xl font-medium leading-5 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5"></span> 10,578.00 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> All Seller</div>
                                        <div class=" relative text-2xl font-medium leading-5 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5"></span> 12,000.00 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> All Product</div>
                                        <div class=" relative text-2xl font-medium leading-5 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5"></span> 57,900.00 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Sold out</div>
                                        <div class=" relative text-2xl font-medium leading-5 pl-4 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5">$</span> 90,238.77 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Total sale</div>
                                        <div class=" relative text-2xl font-medium leading-5 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5"></span> 20,178.00 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Sold out today</div>
                                        <div class=" relative text-2xl font-medium leading-5  mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5"></span> 10,007.00 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Sale today</div>
                                        <div class=" relative text-2xl font-medium leading-5  mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5"></span> 47,000.00 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Total order</div>
                                        <div class=" relative text-2xl font-medium leading-5  mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5"></span> 1,578.00 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Cstm%</div>
                                        <div class=" relative text-2xl font-medium leading-5 pl-4 mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5">$</span> 90,578.00 </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-2 sm:col-span-6 xl:col-span-2 p-5 text-white rounded-lg" style="background:green">
                                <div class="flex flex-wrap gap-3">
                                    <div class="mr-auto">
                                        <div class=" text-opacity-70 text-xl flex items-center leading-3"> Pedding order</div>
                                        <div class=" relative text-2xl font-medium leading-5  mt-3.5"> <span class="absolute text-xl top-0 left-0 -mt-1.5"></span> 47,000.00 </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Seller Report -->
                    <!-- BEGIN: Sales Report -->
                    <div class="col-span-12 lg:col-span-6 mt-8">
                        <div class="intro-y block sm:flex items-center h-10">
                            <h2 class="text-lg font-medium truncate mr-5">
                                Sales Report
                            </h2>
                            <div class="sm:ml-auto mt-3 sm:mt-0 relative text-gray-700 dark:text-gray-300">
                                <i data-feather="calendar" class="w-4 h-4 z-10 absolute my-auto inset-y-0 ml-3 left-0"></i> 
                                <input type="text" class="datepicker form-control sm:w-56 box pl-10">
                            </div>
                        </div>
                        <div class="intro-y box p-5 mt-12 sm:mt-5">
                            <div class="flex flex-col xl:flex-row xl:items-center">
                                <div class="flex">
                                    <div>
                                        <div class="text-theme-26 dark:text-gray-300 text-lg xl:text-xl font-medium">$15,000</div>
                                        <div class="mt-0.5  dark:text-gray-600">This Month</div>
                                    </div>
                                    <div class="w-px h-12 border border-r border-dashed border-gray-300 dark:border-dark-5 mx-4 xl:mx-5"></div>
                                    <div>
                                        <div class="text-gray-600 dark:text-gray-600 text-lg xl:text-xl font-medium">$10,000</div>
                                        <div class="mt-0.5 text-gray-600 dark:text-gray-600">Last Month</div>
                                    </div>
                                </div>
                            </div>
                            <div class="report-chart">
                                <canvas id="report-line-chart" height="169" class="mt-6"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- END: Sales Report -->
                    <!-- BEGIN: Weekly Top Seller -->
                    <div class="col-span-12 sm:col-span-6 lg:col-span-3 mt-8">
                        <div class="intro-y flex items-center h-10">
                            <h2 class="text-lg font-medium truncate mr-5">
                                Weekly Top Seller
                            </h2>
                        </div>
                        <div class="intro-y box p-5 mt-5">
                            <canvas class="mt-3" id="report-pie-chart" height="300"></canvas>
                            <div class="mt-8">
                                <div class="flex items-center">
                                    <div class="w-2 h-2 bg-theme-17 rounded-full mr-3"></div>
                                    <span class="truncate">17 - 30 Years old</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">62%</span> 
                                </div>
                                <div class="flex items-center mt-4">
                                    <div class="w-2 h-2 bg-theme-35 rounded-full mr-3"></div>
                                    <span class="truncate">31 - 50 Years old</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">33%</span> 
                                </div>
                                <div class="flex items-center mt-4">
                                    <div class="w-2 h-2 bg-theme-23 rounded-full mr-3"></div>
                                    <span class="truncate">>= 50 Years old</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">10%</span> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Weekly Top Seller -->
                    <!-- BEGIN: Sales Report -->
                    <div class="col-span-12 sm:col-span-6 lg:col-span-3 mt-8">
                        <div class="intro-y flex items-center h-10">
                            <h2 class="text-lg font-medium truncate mr-5">
                                Sales Report
                            </h2>
                        </div>
                        <div class="intro-y box p-5 mt-5">
                            <canvas class="mt-3" id="report-donut-chart" height="300"></canvas>
                            <div class="mt-8">
                                <div class="flex items-center">
                                    <div class="w-2 h-2 bg-theme-17 rounded-full mr-3"></div>
                                    <span class="truncate">17 - 30 Years old</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">62%</span> 
                                </div>
                                <div class="flex items-center mt-4">
                                    <div class="w-2 h-2 bg-theme-35 rounded-full mr-3"></div>
                                    <span class="truncate">31 - 50 Years old</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">33%</span> 
                                </div>
                                <div class="flex items-center mt-4">
                                    <div class="w-2 h-2 bg-theme-23 rounded-full mr-3"></div>
                                    <span class="truncate">>= 50 Years old</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">10%</span> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Sales Report -->
                    <!-- BEGIN: Weekly Top Products -->
                    {{-- <div class="col-span-12 mt-6">
                        <div class="intro-y block sm:flex items-center h-10">
                            <h2 class="text-lg font-medium truncate mr-5">
                                Weekly Top Products
                            </h2>
                        </div>
                            <!-- BEGIN: Data List -->
                        <div class="intro-y col-span-12 overflow-auto lg:overflow">
                            <table class="table table-report -mt-2">
                                <thead>
                                    <tr>
                                        <th class="whitespace-nowrap">IMAGES</th>
                                        <th class="whitespace-nowrap">PRODUCT NAME</th>
                                        <th class="text-center whitespace-nowrap">STOCK</th>
                                        <th class="text-center whitespace-nowrap">STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="intro-x">
                                        <td class="w-40">
                                            <div class="flex">
                                                <div class="w-10 h-10 image-fit zoom-in">
                                                    <img alt="Icewall Tailwind HTML Admin Template" class="tooltip" src="{{asset('backend/dist/images/preview-10.jpg')}}" title="Uploaded at 5 September 2022">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="" class="font-medium whitespace-nowrap">Sony A7 III</a> 
                                            <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">Photography</div>
                                        </td>
                                        <td class="text-center">50</td>
                                        <td class="w-40">
                                            <div class="flex items-center justify-center text-theme-10"> <i data-feather="check-square" class="w-4 h-4 mr-2"></i> Inactive </div>
                                        </td>
                                    </tr>
                                    <tr class="intro-x">
                                        <td class="w-40">
                                            <div class="flex">
                                                <div class="w-10 h-10 image-fit zoom-in">
                                                    <img alt="Icewall Tailwind HTML Admin Template" class="tooltip" src="{{asset('backend/dist/images/preview-2.jpg')}}" title="Uploaded at 16 January 2022">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="" class="font-medium whitespace-nowrap">Sony Master Series A9G</a> 
                                            <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">Electronic</div>
                                        </td>
                                        <td class="text-center">50</td>
                                        <td class="w-40">
                                            <div class="flex items-center justify-center text-theme-10"> <i data-feather="check-square" class="w-4 h-4 mr-2"></i> Inactive </div>
                                        </td>
                                    </tr>
                                    <tr class="intro-x">
                                        <td class="w-40">
                                            <div class="flex">
                                                <div class="w-10 h-10 image-fit zoom-in">
                                                    <img alt="Icewall Tailwind HTML Admin Template" class="tooltip" src="{{asset('backend/dist/images/preview-13.jpg')}}" title="Uploaded at 5 October 2020">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="" class="font-medium whitespace-nowrap">Nikon Z6</a> 
                                            <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">Photography</div>
                                        </td>
                                        <td class="text-center">50</td>
                                        <td class="w-40">
                                            <div class="flex items-center justify-center text-theme-24"> <i data-feather="check-square" class="w-4 h-4 mr-2"></i> Active </div>
                                        </td>
                                    </tr>
                                    <tr class="intro-x">
                                        <td class="w-40">
                                            <div class="flex">
                                                <div class="w-10 h-10 image-fit zoom-in">
                                                    <img alt="Icewall Tailwind HTML Admin Template" class="tooltip" src="{{asset('backend/dist/images/preview-4.jpg')}}" title="Uploaded at 27 May 2022">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="" class="font-medium whitespace-nowrap">Samsung Q90 QLED TV</a> 
                                            <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">Electronic</div>
                                        </td>
                                        <td class="text-center">72</td>
                                        <td class="w-40">
                                            <div class="flex items-center justify-center text-theme-24"> <i data-feather="check-square" class="w-4 h-4 mr-2"></i> Active </div>
                                        </td>
                                    </tr>
                                    <tr class="intro-x">
                                        <td class="w-40">
                                            <div class="flex">
                                                <div class="w-10 h-10 image-fit zoom-in">
                                                    <img alt="Icewall Tailwind HTML Admin Template" class="tooltip" src="{{asset('backend/dist/images/preview-9.jpg')}}" title="Uploaded at 12 May 2021">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="" class="font-medium whitespace-nowrap">Apple MacBook Pro 13</a> 
                                            <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">PC &amp; Laptop</div>
                                        </td>
                                        <td class="text-center">50</td>
                                        <td class="w-40">
                                            <div class="flex items-center justify-center text-theme-24"> <i data-feather="check-square" class="w-4 h-4 mr-2"></i> Active </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END: Data List -->
                    </div> --}}
                    <!-- END: Weekly Top Products -->
                </div>
            </div>
        </div>
    </div>
<!-- END: Content -->
    
@endsection