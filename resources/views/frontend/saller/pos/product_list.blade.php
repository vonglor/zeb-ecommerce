<div class="gallery_1 clearfix">
    <div class="col-sm-12">
        <div class="workout_page_1_left clearfix">
            <div class="tab-content clearfix">
                <div id="home" class="tab-pane fade  clearfix active in">
                    <div class="click clearfix">
                        <div class="home_inner clearfix">
                            @foreach ($products as $product)
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 offset-md-0 offset-sm-1 pt-md-0 pt-4">
                                                                <div class="card_product card-product">
                                                                    @if ($product->image)
                                                                    <img src="{{ url(App\Models\Upload::find($product->image)->file_name) }}" class="img-responsive" alt="Responsive image">
                                                                    @else
                                                                    <img src="{{ url($product->Upload->file_name) }}" class="img-responsive" alt="Responsive image">
                                                                    @endif
                                                                    <div class="card_product-body">
                                                                        <h5 class="font-weight-bold pt-1">{{ Str::limit($product->name, 12) }}</h5>
                                                                        <div class="d-flex align-items-center justify-content-between pt-3">
                                                                            <div class="d-flex flex-column">
                                                                                <div class="h5 font-weight-bold">
                                                                                    ${{ number_format($product->price - $product->discount, 2) }} 
                                                                                </div>
                                                                                <a href="{{ route('pos.add_to_cart', $product->id) }}" class="add_cart btn btn-primary btn-xs"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product_1_last text-center clearfix">
    <div class="col-sm-12">
        <ul>
            {!! $products->links() !!}
        </ul>
    </div>
</div>