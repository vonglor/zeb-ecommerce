@extends('frontend.saller.content_left')
@section('css')
<link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
<div class="col-sm-9">
    <div class="centent_card_1r clearfix">
        <div class="centent_card_1r3 tab-content">
            <p>Member level 2</p>
            <div class="card-content">
                <div class="table-responsive">
                    <table class="table table-hover">
                      <tr>
                          <th>#</th>
                          <th width="">Name</th>
                          <th width="">Last name</th>
                          <th width="">Type</th>
                          <th></th>
                      </tr>
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($member as $row)
                        <tr>
                            <td scope="row">{{ $i }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->surname }}</td>
                            <td>{{ $row->user_type }}</td>
                            <td><a href="{{ route('member.view2', $row->refer_code_others) }}" class="btn btn-info btn-xs"> view</a></td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                        @endforeach
                    </table>
                  </div>
            </div>
        </div>
        <div class="center_product_1r4 clearfix">
            <div class="col-sm-6">
                <div class="center_product_1r4l clearfix">
                    {{ $member->links() }}
                </div>
            </-div>
        </div>
    </div>
</div>
@endsection