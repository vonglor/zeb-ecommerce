<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Product_stock;
use App\Models\ProductRecive;
use App\Http\Controllers\Controller;

class ProductReciveController extends Controller
{
    public function index()
    {
        $pro_recive = ProductRecive::all();
        return view('frontend/saller/productRecives/product_recive', \compact('pro_recive'));
    }

    public function create()
    {
        return view('frontend/saller/productRecives/create');
    }

    public function variantion(Request $request)
    {
        $variantion = Product_stock::where('product_id', $request->product_id)->get();
        return view('frontend.saller.productRecives.variantion', \compact('variantion'));
    }

    public function store(Request $request)
    {
        $recive = ProductRecive::create([
            'qty' => $request->qty,
            'product_id' => $request->product_id,
            'pro_stock_id' => $request->variantion,
            'price' => $request->price,
            'currency_code' => $request->currency,
            'date' => now(),
        ]);

        $product_stock = Product_stock::find($request->variantion);
        $product_stock->qty = $product_stock->qty + $request->qty;
        $product_stock->save();

        if($recive){
            $notification = array(
                'message' => 'Import product successfully.',
                'alert-type' => 'success'
            );
        }else{
            $notification = array(
                'message' => 'Import product warning...',
                'alert-type' => 'warning'
            );
        }

        return \redirect()->route('sale.product_recive')->with($notification);
    }

    public function edit($id)
    {
        $pro_recive = ProductRecive::find($id);
        $pro_stock = Product_stock::where('product_id', $pro_recive->product_id)->get();
        return view('frontend.saller.productRecives.edit', \compact('pro_recive', 'pro_stock'));
    }

    public function update(Request $request, $id)
    {
        $pro_recive = ProductRecive::find($id);

        $pro_stock = Product_stock::find($request->pro_stocked_id);
        $qty_old = $pro_stock->qty - $request->qtyed;
        $qty_update = $qty_old + $request->qty;
        $pro_stock->qty = $qty_update;
        $pro_stock->save();

        $pro_recive->qty = $request->qty;
        $pro_recive->product_id = $request->product_id;
        $pro_recive->pro_stock_id = $request->variantion;
        $pro_recive->price = $request->price;
        $pro_recive->currency_code = $request->currency;
        $pro_recive->save();

        if($pro_recive){
            $notification = array(
                'message' => 'Update products successfully.',
                'alert-type' => 'success'
            );
        }else{
            $notification = array(
                'message' => 'Update products warning...',
                'alert-type' => 'warning'
            );
        }

        return \redirect()->route('sale.product_recive')->with($notification);
    }
}
