<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Product_stock;
use Illuminate\Http\Request;
use Session;

class PosController extends Controller
{

    public function index()
    {
        $user     = Session::get('user');
        $products = Product::join('product_stocks', 'product_stocks.product_id', 'products.id')
            ->select('product_stocks.*', 'products.name', 'products.photos', 'products.discount')
        // ->where('products.user_id', $user->id)
            ->orderBy('product_stocks.id', 'desc')
            ->paginate(6);

        return view('backend.pos.index', compact('products'));
    }

    public function detail($id)
    {
        $product = Product::find($id);
        return view('backend.pos.detail', compact('product'));
    }

    public function add_to_cart($id = null)
    {
        if ($id) {
            $product_id = $id;
        }

        $pro_stock = Product_stock::find($product_id);
        $product   = Product::find($pro_stock->product_id);
        if (!$product) {
            toastr()->error(__("Product Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $data             = array();
        $data['pro_id']   = $product->id;
        $data['id']       = $pro_stock->id;
        $data['owner_id'] = $product->user_id;
        $data['name']     = $product->name;
        $data['variant']  = $pro_stock->variant;
        $data['qty']      = 1;
        $data['price']    = ($pro_stock->price - $product->discount);
        if ($pro_stock->image) {
            $data['image'] = $pro_stock->image;
        } else {
            $data['image'] = $product->photos;
        }

        if (Session::has('cart')) {
            $foundInCart = false;
            $cart        = collect();
            foreach (Session::get('cart') as $key => $cartItem) {
                if ($cartItem['id'] == $pro_stock->id) {
                    if ($pro_stock->qty < $cartItem['qty']) {
                        toastr()->error('Error', 'This item is out of stock!.', ['timeOut' => 2000]);
                        return redirect()->back();
                    } else {
                        $foundInCart     = true;
                        $cartItem['qty'] = $cartItem['qty'] + 1;
                    }
                }
                $cart->push($cartItem);
            }

            if (!$foundInCart) {
                $cart->push($data);
            }
            Session::put('cart', $cart);
        } else {
            $cart = collect([$data]);
            Session::put('cart', $cart);
        }

        toastr()->success('Success', 'Item has been added to cart.', ['timeOut' => 2000]);
        return redirect()->back();
    }

    public function update_cart(Request $request)
    {
        $cart = $request->session()->get('cart', collect([]));
        $cart = $cart->map(function ($item, $key) use ($request) {
            if ($key == $request->key) {
                $product = Product_stock::find($item['id']);
                $qty     = $product->qty;
                if ($qty >= $request->qty && $request->qty >= $product->min_qty) {
                    $item['qty'] = $request->qty;
                }
            }
            return $item;
        });

        $request->session()->put('cart', $cart);
    }

    public function remove_cart($key)
    {
        if (Session::has('cart')) {
            $cart = Session::get('cart', collect([]));
            $cart->forget($key);
            Session::put('cart', $cart);
        }

        toastr()->success('Success', 'Item has been removed from cart.', ['timeOut' => 2000]);
        return redirect()->back();
    }

    public function clear_cart_items($key)
    {
        if (Session::has('cart')) {
            $cart = collect();
            foreach (Session::get('cart') as $key => $cartItem) {
                if ($key !== $key) {
                    $cart->push($cartItem);
                }
            }
            Session::put('cart', $cart);
        }

        toastr()->success('Success', 'Item has been clear from cart.', ['timeOut' => 2000]);
        return redirect()->back();
    }

    public function filter_category(Request $request)
    {
        $filterBy = $request->filter_by;
        $user_id  = Session::get('user')->id;
        if ($filterBy === 'all') {
            $products = Product::join('product_stocks', 'product_stocks.product_id', 'products.id')
                ->select('product_stocks.*', 'products.name', 'products.photos', 'products.discount')
            // ->where('products.user_id', $user_id)
                ->orderBy('product_stocks.id', 'desc')
                ->paginate(6);
        } else {
            $products = Product::join('product_stocks', 'product_stocks.product_id', 'products.id')
                ->select('product_stocks.*', 'products.name', 'products.photos', 'products.discount')
                ->where('products.category_id', $filterBy)
            // ->where('products.user_id', $user_id)
                ->orderBy('product_stocks.id', 'desc')
                ->paginate(6);
        }

        return view('backend.pos.list_products', compact('products'));
    }

    public function search_product(Request $request)
    {
        $search   = $request->search;
        $user_id  = Session::get('user')->id;
        $products = Product::join('product_stocks', 'product_stocks.product_id', 'products.id')
            ->select('product_stocks.*', 'products.name', 'products.photos', 'products.discount')
            ->where('products.name', 'like', '%' . $search . '%')
        // ->where('products.user_id', $user_id)
            ->orderBy('product_stocks.id', 'desc')
            ->paginate(6);

        return view('backend.pos.index', compact('products'));
    }

    public function get_users()
    {
        return view('backend.pos.list_users');
    }

}