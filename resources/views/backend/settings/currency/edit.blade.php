@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-xl font-medium mr-auto">
                Edit currency
            </h2>
        </div>
        <div class="grid grid-cols-6 gap-12 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
                <form action="{{route('update.currency', $currency->id )}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="intro-y box">
                        <div id="vertical-form" class="p-5">
                            <div class="preview">
                                <div>
                                    <label for="vertical-form-1" class="form-label text-base">Currency Code</label>
                                    <input id="vertical-form-1" type="text" class="form-control border-gray-500 text-base" name="code" value="{{$currency->code}}" placeholder=" Currency Code" required>
                                </div>
                                <div>
                                    <label for="vertical-form-1" class="form-label text-base">Currency Name</label>
                                    <input id="vertical-form-1" type="text" class="form-control border-gray-500 text-base" name="name" value="{{$currency->name}}" placeholder="Currency Name">
                                </div>
                                <div>
                                    <label for="vertical-form-1" class="form-label text-base">Currency Symbol</label>
                                    <input type="text" class="form-control border-gray-500 text-base" name="symbol" value="{{$currency->symbol}}" placeholder="Currency Symbol">
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary mt-5 text-lg">Update Currency</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- END: Content -->
    
@endsection