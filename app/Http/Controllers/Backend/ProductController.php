<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Upload;
use App\Models\Shop;
use App\Models\Product_stock;
use Illuminate\Support\Facades\Extension;
use Str;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        $brand = Brand::all();
        return view('backend.products.product.create',compact('category', 'brand'));
    }
    public function allProuct(){

        $allproduct = Product::paginate(10);
        return view('backend.products.product.index',compact('allproduct'));
    }
    public function allProuctInhouse()
    {
        $inhouseproduct = Product::paginate(10);
        return view('backend.products.inhouseproduct.index',compact('inhouseproduct'));
    }
    public function allProuctSeller()
    {
        $sellerproduct = Product::paginate(10);
        return view('backend.products.sellerproduct.index',compact('sellerproduct'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());
        $user = Session::get('user');
        $shop = Shop::where('user_id', $user->id)->first();
        $prod = Product::where('user_id', $user->id)->get();
        $count = count($prod);
        if($count > 0){
            // $exportId = str_pad($count, 7, '0', STR_PAD_LEFT);
            $count = $count + 1;
            $p_id = $shop->short.$count;
        }else{
            // $exportId = str_pad(1, 7, '0', STR_PAD_LEFT);
            $count = $count + 1;
            $p_id = $shop->short.$count;
        }
        
        $request->validate([
                'name' => 'required|max:200',
            ],
            [
            ]
        );
        $product = new Product;
        $product->product_id = $p_id;
        $product->name = $request->name;
        $product->added_by = $user->name;
        $product->user_id = $user->id;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
    
        // photos upload
        $upload_path = 'uploads/images/';
        if($request->file('image')){
            $img = $request->file('image');
            $name_img = hexdec(uniqid());
            $img_ext = strtolower($img->extension());
            $img_new_name = $name_img.'.'.$img_ext;
            $photo = $upload_path.$img_new_name;
            $img->move($upload_path, $img_new_name);
            $upload = new Upload;
            // $upload->user_id = $user->id;
            $upload->file_name = $photo;
            $upload->type = 'image';
            $upload->extension = $img_ext;
            $upload->save();
            $product->photos = $upload->id;
        }
        
        // thumbnail upload
        $thumbnail = $request->file('thumbnail');
        if($thumbnail > 0){
            $thum = array();
            foreach($thumbnail as $row){
                $thum_img = hexdec(uniqid());
                $thum_ext = strtolower($row->extension());
                $thum_new_name = $thum_img.'.'.$thum_ext;
                $photo = $upload_path.$thum_new_name;
                $row->move($upload_path, $thum_new_name);

                $uploads = new Upload;
                // $uploads->user_id = $user->id;
                $uploads->file_name = $photo;
                $uploads->type = 'image';
                $uploads->extension = $thum_ext;
                $uploads->save();
                array_push($thum, $uploads->id);
            }
            $product->thumbnail_img = implode(',', $thum);
        }
        
        //   pdf upload
        if($request->file('pdf')){
            $pdf = $request->file('pdf');
            $name_pdf = hexdec(uniqid());
            $pdf_ext = strtolower($pdf->extension());
            $pdf_new_name = $name_pdf.'.'.$pdf_ext;
            $upload_pdf_path = 'uploads/pdf/';
            $pdfs = $upload_pdf_path.$pdf_new_name;
            $pdf->move($upload_pdf_path, $pdf_new_name);
            $upload_pdf = new Upload;
            // $upload_pdf->user_id = $user->id;
            $upload_pdf->file_name = $pdfs;
            $upload_pdf->type = 'document';
            $upload_pdf->extension = $pdf_ext;
            $upload_pdf->save();
            $product->pdf = $upload_pdf->id;
        }
        
        $product->videos = $request->video_link;
        $product->description = $request->description;
        $product->unit_price = $request->unit_price;
        $product->purchase_price = $request->purchase_price;
        $product->currency_code = $request->currency_code;
       
        if($request->has('colors') && count($request->colors) > 0){
            $product->colors = json_encode($request->colors);
        }
        else {
            $colors = array();
            $product->colors = json_encode($colors);
        }
       
        $choice_options = array();
        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $str = 'choice_options_'.$no;

                $item['attribute_id'] = $no;

                $data = array();
                foreach (json_decode($request[$str][0]) as $key => $eachValue) {
                    array_push($data, $eachValue->value);
                }

                $item['values'] = $data;
                array_push($choice_options, $item);
            }
        }

        if (!empty($request->choice_no)) {
            $product->attributes = json_encode($request->choice_no);
        }
        else {
            $product->attributes = json_encode(array());
        }
        $product->choice_options = json_encode($choice_options, JSON_UNESCAPED_UNICODE);

        $product->unit = $request->unit;
        $product->min_qty = $request->min_qty;
        $product->discount = $request->discount;
        $product->slug = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->name)).'-'.Str::random(5);

        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;
        $product->product_location = $request->product_location;

        if($product->meta_title == null){
            $product->meta_title = $product->name;
        }
        if($product->meta_description == null){
            $product->meta_description = \strip_tags($product->description);
        }
        $product->delivery = $request->delivery;
        $product->shipping_day = $request->shipping_day;
        $product->shipping = $request->shipping;
        $product->save();
        $product_id = $product->id;

       //combinations start
       $options = array();
       if($request->has('colors') && count($request->colors) > 0) {
           array_push($options, $request->colors);
       }

       if($request->has('choice_no')){
           foreach ($request->choice_no as $key => $no) {
               $name = 'choice_options_'.$no;
               $data = array();
               foreach (json_decode($request[$name][0]) as $key => $item) {
                   array_push($data, $item->value);
               }
               array_push($options, $data);
           }
       }
       $combinations = array(array());

        foreach ($options as $key => $values) {
            $append = array();
            foreach($combinations as $product) {
                foreach($values as $item) {
                    $product[$key] = $item;
                    $append[] = $product;
                }
            }

            $combinations = $append;
        }

        if(count($combinations[0]) > 0){
            foreach ($combinations as $key => $combination){
                $str = '';
                foreach ($combination as $key => $item){
                    if($key > 0 ){
                        $str .= '-'.str_replace(' ', '', $item);
                    }
                    else{
                        $str .= str_replace(' ', '', $item);
                    }
                }
                $product_stock = Product_stock::where('product_id', $product_id)->where('variant', $str)->first();
                if($product_stock == null){
                    $product_stock = new Product_stock;
                    $product_stock->product_id = $product_id;
                }

                $product_stock->variant = $str;
                $product_stock->price = $request['price_'.str_replace('.', '_', $str)];
                $product_stock->qty = $request['qty_'.str_replace('.', '_', $str)];
                
                if($request['img_'.str_replace('.', '_', $str)]){
                    $img_stock = $request['img_'.str_replace('.', '_', $str)];
                    $name_stock = hexdec(uniqid());
                    $stock_ext = strtolower($img_stock->extension());
                    $stock_new_name = $name_stock.'.'.$stock_ext;
                    $stocks = $upload_path.$stock_new_name;
                    $img_stock->move($upload_path, $stock_new_name);
                    $upload_stock = new Upload;
                    // $upload_stock->user_id = $user->id;
                    $upload_stock->file_name = $stocks;
                    $upload_stock->type = 'image';
                    $upload_stock->extension = $stock_ext;
                    $upload_stock->save();
                    $product_stock->image = $upload_stock->id;
                }
                
                $product_stock->save();
            }
        }
        else{
            $product_stock = new Product_stock;
            $product_stock->product_id = $product_id;
            $product_stock->price = $request->unit_price;
            $product_stock->qty = $request->quantity;
            $product_stock->save();
        }

        $notification = array(
            'message' => 'Add product successfully.',
            'alert-type' => 'success'
        );

        if ($user->user_type == 'sale'){
            return redirect()->route('sale.product.index')->with($notification);
        }else{
            dd('admin Add product..');
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
        
    }

    public function update_product(Request $request)
    {
        $request->validate([
                'name' => 'required|max:200',
            ],
            [
                
            ]
        );
        $user = Session::get('user');
         $product = Product::findOrFail($request->product_id);
         $product->name = $request->name;
         $product->category_id = $request->category_id;
         $product->brand_id = $request->brand_id;
 
         // photos upload
         $upload_path = 'uploads/images/';
         if($request->file('image')){
             $upload = Upload::find($product->photos);
             if($upload->file_name){
                unlink($upload->file_name);
             }
             $img = $request->file('image');
             $name_img = hexdec(uniqid());
             $img_ext = strtolower($img->extension());
             $img_new_name = $name_img.'.'.$img_ext;
             $photo = $upload_path.$img_new_name;
             $img->move($upload_path, $img_new_name);
             // $upload->user_id = $user->id;
             
             $upload->file_name = $photo;
             $upload->type = 'image';
             $upload->extension = $img_ext;
             $upload->save();
            //  $product->photos = $upload->id;
         }
         
         // thumbnail upload
         $thumbnail = $request->file('thumbnail');
         if($thumbnail > 0){
                if($product->thumbnail_img != null){
                    $thumb = explode(',', $product->thumbnail_img);
                    foreach($thumb as $row){
                        $upload = Upload::find($row);
                        if($upload->file_name){
                            unlink($upload->file_name);
                        }
                        $upload->delete();
                    }

                    $thums = array();
                    foreach($thumbnail as $row){
                        $thum_img = hexdec(uniqid());
                        $thum_ext = strtolower($row->extension());
                        $thum_new_name = $thum_img.'.'.$thum_ext;
                        $thum = $upload_path.$thum_new_name;
                        $row->move($upload_path, $thum_new_name);

                        $uploads = new Upload;
                        // $uploads->user_id = $user->id;
                        $uploads->file_name = $thum;
                        $uploads->type = 'image';
                        $uploads->extension = $thum_ext;
                        $uploads->save();
                        array_push($thums, $uploads->id);
                    }
                    $product->thumbnail_img = implode(',', $thums);
                }  
            
         }
         
         //   pdf upload
         if($request->file('pdf')){
             $pdf = $request->file('pdf');
             $name_pdf = hexdec(uniqid());
             $pdf_ext = strtolower($pdf->extension());
             $pdf_new_name = $name_pdf.'.'.$pdf_ext;
             $upload_pdf_path = 'uploads/pdf/';
             $pdfs = $upload_pdf_path.$pdf_new_name;
             $pdf->move($upload_pdf_path, $pdf_new_name);
             $upload_pdf = new Upload;
             // $upload_pdf->user_id = $user->id;
             $upload_pdf->file_name = $pdfs;
             $upload_pdf->type = 'document';
             $upload_pdf->extension = $pdf_ext;
             $upload_pdf->save();
             $product->pdf = $upload_pdf->id;
         }
 
         $product->videos = $request->video_link;
         $product->description = $request->description;
         $product->unit_price = $request->unit_price;
         $product->purchase_price = $request->purchase_price;
         $product->currency_code = $request->currency_code;
        
         if($request->has('colors') && count($request->colors) > 0){
             $product->colors = json_encode($request->colors);
         }
         else {
             $colors = array();
             $product->colors = json_encode($colors);
         }

         $choice_options = array();
         if($request->has('choice_no')){
             foreach ($request->choice_no as $key => $no) {
                 $str = 'choice_options_'.$no;
 
                 $item['attribute_id'] = $no;
 
                 $data = array();
                 foreach (json_decode($request[$str][0]) as $key => $eachValue) {
                     array_push($data, $eachValue->value);
                 }
 
                 $item['values'] = $data;
                 array_push($choice_options, $item);
             }
         }
 
         if (!empty($request->choice_no)) {
             $product->attributes = json_encode($request->choice_no);
         }
         else {
             $product->attributes = json_encode(array());
         }
         $product->choice_options = json_encode($choice_options, JSON_UNESCAPED_UNICODE);
 
         
         $product->unit = $request->unit;
         $product->min_qty = $request->min_qty;
         $product->discount = $request->discount;
         $product->meta_title = $request->meta_title;
         $product->meta_description = $request->meta_description;
         $product->product_location = $request->product_location;
         $product->condition = $request->condition;
         $product->delivery = $request->delivery;
         $product->shipping_day = $request->shipping_day;
         $product->shipping = $request->shipping;
         $product->save();
 
         if($request->product_stock){
            if($request->product_stock == 'old'){
                $product_stocks = Product_stock::where('product_id', $request->product_id)->get();
                foreach($product_stocks as $product_stock){
                   $pro_stock = Product_stock::where('product_id', $request->product_id)->where('variant', $product_stock->variant)->first();
                    // if($pro_stock == null){
                    //     $pro_stock = new Product_stock;
                    //     $pro_stock->product_id = $product->id;
                    // }
                    $pro_stock->price = $request['price_'.$product_stock->variant];
                    $pro_stock->qty = $request['qty_'.$product_stock->variant];
                    
                    if($request['img_'.$product_stock->variant]){
                        if($pro_stock->image){
                            $upload1 = Upload::find($pro_stock->image);
                            unlink($upload1->file_name);
                            $img1 = $request['img_'.$product_stock->variant];
                            $name_img1 = hexdec(uniqid());
                            $img_ext1 = strtolower($img1->extension());
                            $img_new_name1 = $name_img1.'.'.$img_ext1;
                            $photo1 = $upload_path.$img_new_name1;
                            $img1->move($upload_path, $img_new_name1);
                            // $upload->user_id = $user->id;
                            $upload1->file_name = $photo1;
                            $upload1->type = 'image';
                            $upload1->extension = $img_ext1;
                            $upload1->save();
                        }else{
                            $img1 = $request['img_'.$product_stock->variant];
                            $name_img1 = hexdec(uniqid());
                            $img_ext1 = strtolower($img1->extension());
                            $img_new_name1 = $name_img1.'.'.$img_ext1;
                            $photo1 = $upload_path.$img_new_name1;
                            $img1->move($upload_path, $img_new_name1);
                            // $upload->user_id = $user->id;
                            $upload1 = new Upload;
                            $upload1->file_name = $photo1;
                            $upload1->type = 'image';
                            $upload1->extension = $img_ext1;
                            $upload1->save();
                            $pro_stock->image = $upload1->id;
                        }
                        
                    }
    
                    $pro_stock->save();
                }
            }else{
               //combinations start
                $options = array();
                if($request->has('colors') && count($request->colors) > 0) {
                    array_push($options, $request->colors);
                }

                if($request->has('choice_no')){
                    foreach ($request->choice_no as $key => $no) {
                        $name = 'choice_options_'.$no;
                        $data = array();
                        foreach (json_decode($request[$name][0]) as $key => $item) {
                            array_push($data, $item->value);
                        }
                        array_push($options, $data);
                    }
                }
                $combinations = array(array());

                    foreach ($options as $key => $values) {
                        $append = array();
                        foreach($combinations as $product) {
                            foreach($values as $item) {
                                $product[$key] = $item;
                                $append[] = $product;
                            }
                        }

                        $combinations = $append;
                    }
                    if(count($combinations[0]) > 0){
                        foreach ($combinations as $key => $combination){
                            $str = '';
                            foreach ($combination as $key => $item){
                                if($key > 0 ){
                                    $str .= '-'.str_replace(' ', '', $item);
                                }
                                else{
                                    $str .= str_replace(' ', '', $item);
                                }
                            }
                            $product_stock = Product_stock::where('product_id', $request->product_id)->where('variant', $str)->first();
                            if($product_stock == null){
                                $product_stock = new Product_stock;
                                $product_stock->product_id = $request->product_id;
                            }
            
                            $product_stock->variant = $str;
                            $product_stock->price = $request['price_'.str_replace('.', '_', $str)];
                            $product_stock->qty = $request['qty_'.str_replace('.', '_', $str)];
                            
                            if($request['img_'.str_replace('.', '_', $str)]){

                                if($product_stock->image){
                                    $upload1 = Upload::find($product_stock->image);
                                    unlink($upload1->file_name);
                                    $img1 = $request['img_'.str_replace('.', '_', $str)];
                                    $name_img1 = hexdec(uniqid());
                                    $img_ext1 = strtolower($img1->extension());
                                    $img_new_name1 = $name_img1.'.'.$img_ext1;
                                    $photo1 = $upload_path.$img_new_name1;
                                    $img1->move($upload_path, $img_new_name1);
                                    // $upload->user_id = $user->id;
                                    $upload1->file_name = $photo1;
                                    $upload1->type = 'image';
                                    $upload1->extension = $img_ext1;
                                    $upload1->save();
                                }else{
                                    $img_stock = $request['img_'.str_replace('.', '_', $str)];
                                    $name_stock = hexdec(uniqid());
                                    $stock_ext = strtolower($img_stock->extension());
                                    $stock_new_name = $name_stock.'.'.$stock_ext;
                                    $stocks = $upload_path.$stock_new_name;
                                    $img_stock->move($upload_path, $stock_new_name);
                                    $upload_stock = new Upload;
                                    // $upload_stock->user_id = $user->id;
                                    $upload_stock->file_name = $stocks;
                                    $upload_stock->type = 'image';
                                    $upload_stock->extension = $stock_ext;
                                    $upload_stock->save();
                                    $product_stock->image = $upload_stock->id;
                                }

                                
                            }
                            
                            $product_stock->save();
                        }
                    }
                    else{
                        $product_stock = new Product_stock;
                        $product_stock->product_id = $request->product_id;
                        $product_stock->price = $request->unit_price;
                        $product_stock->qty = $request->quantity;
                        $product_stock->save();
                    }
            }
         }else{
            $pro_stock = Product_stock::where('product_id', $product->id)->first();
            $pro_stock->price = $request->unit_price;
            $pro_stock->qty = $request->quantity;
            $pro_stock->save();
         }

         $notification = array(
            'message' => 'Update product successfully.',
            'alert-type' => 'success'
        );

        if($user->user_type == 'sale'){
            return redirect()->route('sale.product.index')->with($notification);
        }else{
            return redirect()->route('backken.products.product.index')->with($notification);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product_stock = Product_stock::where('product_id', $id)->get();
        foreach ($product_stock as $key => $pro_st) {
            if($pro_st->image){
                $upload = Upload::find($pro_st->image);
                \unlink($upload->file_name);
                $upload->delete();
            }
            $prost = Product_stock::find($pro_st->id);
            $prost->delete();
        }
        if($product->photos){
            $upload1 = Upload::find($product->photos);
            unlink($upload1->file_name);
            $upload1->delete();
        }
        if($product->thumbnail_img){
            $thumb = explode(',', $product->thumbnail_img);
            foreach($thumb as $row){
                $upload2 = Upload::find($row);
                unlink($upload2->file_name);
                $upload2->delete();
            }
        }
        $product->delete();

        $notification = array(
            'message' => 'Delete item successfully.',
            'alert-type' => 'success'
        );

        return redirect()->route('sale.product.index')->with($notification);

    }

    public function sku_combination(Request $request)
    {
        $options = array();
        if($request->has('colors') && count($request->colors) > 0){
            array_push($options, $request->colors);
        }

        $unit_price = $request->unit_price;
        $product_name = $request->name;
        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;             
                $data = array();
                foreach (json_decode($request[$name][0]) as $key => $item) {

                    array_push($data, $item->value);
                }
                array_push($options, $data);
            }
        }
        
        $combinations = array(array());

        foreach ($options as $key => $values) {
            $append = array();
            foreach($combinations as $product) {
                foreach($values as $item) {
                    $product[$key] = $item;
                    $append[] = $product;
                }
            }

            $combinations = $append;
        }
        return view('frontend.saller.products.sku_combinations', compact('combinations', 'unit_price', 'product_name'));
    }

    public function sku_combination_edit(Request $request)
    {
       // dd($request->all());
        $products = Product::find($request->product_id);
        $options = array();
        if($request->has('colors') && count($request->colors) > 0){
            array_push($options, $request->colors);
        }

        $unit_price = $request->unit_price;
        $product_name = $request->name;
        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;             
                $data = array();
                foreach (json_decode($request[$name][0]) as $key => $item) {

                    array_push($data, $item->value);
                }
                array_push($options, $data);
            }
        }
        
        $combinations = array(array());

        foreach ($options as $key => $values) {
            $append = array();
            foreach($combinations as $product) {
                foreach($values as $item) {
                    $product[$key] = $item;
                    $append[] = $product;
                }
            }

            $combinations = $append;
        }
        return view('frontend.saller.products.sku_combinations_edit', compact('combinations', 'unit_price', 'product_name', 'products'));
        
    }
}
