
@php
 $user_id = Session::get('user')->id;
 $user_type = Session::get('user')->user_type;
 $user = App\Models\User::find($user_id);
 $shop = App\Models\Shop::where('user_id', $user_id)->get();
@endphp
@extends('frontend.layouts.app')
@section('content')
<section id="center" class="center_shop clearfix">
    <div class="container">
        <div class="row">
            <div class="center_shop_1 clearfix">
                <div class="col-sm-12">
                    <h5 class="mgt">
                    </h5>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="center" class="clearfix center_product" style="background: #f4f4f4;" >
    <div class="container_body">
        <div class="row">
            <div class="centent_card clearfix">
                <div class="col-sm-3">
                    <div class="centent_card_1l clearfix">
                        <div class="centent_card_1li clearfix">
                            <div class="text-center profile">
                                @if($user->photo == null)
                                <img src="{{ asset('frontend/img/1.jpg') }}" class="rounded img-rounded" id="profile_img" style="max-width: 80px; max-height: 80px;">
                                <span>{{ $user->name }}</span>
                                @else
                                <img src="{{ asset(App\Models\Upload::find($user->photo)->file_name) }}" class="rounded img-rounded" style="max-width: 80px; max-height: 80px;">
                                <span>{{ $user->name }}</span>
                                @endif
                                <div class="profile_1">
                                    <p>Your balance: <span>
                                        @php
                                            $amount = App\Models\Wallet::where('user_id', Session::get('user')->id)->first();
                                            if ($amount) {
                                                echo convert_balance($amount->amount);
                                            }else {
                                                echo convert_balance(0);
                                            }
                                        @endphp    
                                    </span></p>
                                    @if ($user_type == 'sale')
                                        <p>Percent sale: <span>
                                            @php
                                                $percent_sale = App\Models\PercentSale::where('user_id', Session::get('user')->id)->first();
                                                if($percent_sale){
                                                    echo convert_balance($percent_sale->amount);
                                                }else{
                                                    echo convert_balance(0);
                                                }
                                            @endphp    
                                        </span></p>
                                    @endif
                                    <p>F: <span>
                                        @php
                                            $f = App\Models\User::where('level1', Session::get('user')->refer_code_others)->get();
                                        @endphp
                                        <a href="{{ route('member.index') }}">{{ count($f) }}</a>
                                    </span></p>
                                    <p>F1: <span>
                                        @php
                                            $f1 = App\Models\User::where('level1', Session::get('user')->refer_code_others)->get();
                                            $count_f1 = count($f1);
                                            $f2 = App\Models\User::where('level2', Session::get('user')->refer_code_others)->get();
                                            $count_f2 = count($f2);
                                            $f3 = App\Models\User::where('level3', Session::get('user')->refer_code_others)->get();
                                            $count_f3 = count($f3);
                                            $f4 = App\Models\User::where('level4', Session::get('user')->refer_code_others)->get();
                                            $count_f4 = count($f4);

                                        @endphp
                                        <a href="{{ route('member.all') }}">{{ $count_f1+$count_f2+$count_f3+$count_f4 }}</a>
                                    </span></p>
                                    <p>Membership: <span>1254</span></p>
                                    <p>Tax: <span>
                                        @php
                                            $user_tax = App\Models\UserTax::where('user_id', Session::get('user')->id)->first();
                                            if($user_tax){
                                                echo convert_balance($user_tax->amount);
                                            }else{
                                                echo '0.00';
                                            }
                                        @endphp   
                                        </span></p>
                                        <p>Shops: <span>
                                            @php
                                                $s1 = App\Models\User::where('level1', Session::get('user')->refer_code_others)->where('user_type', 'sale')->get();
                                                $count_s1 = count($s1);
                                                $s2 = App\Models\User::where('level2', Session::get('user')->refer_code_others)->where('user_type', 'sale')->get();
                                                $count_s2 = count($s2);
                                                $s3 = App\Models\User::where('level3', Session::get('user')->refer_code_others)->where('user_type', 'sale')->get();
                                                $count_s3 = count($s3);
                                                $s4 = App\Models\User::where('level4', Session::get('user')->refer_code_others)->where('user_type', 'sale')->get();
                                                $count_s4 = count($s4);
                                            @endphp
                                            <a href="{{ route('member.shop') }}">{{ $count_s1 + $count_s2 + $count_s3 + $count_s4 }}</a>
                                        </span></p>
                                </div>
                            </div>
                            <hr>
                            <ul>
                                
                                @if ($user_type == 'sale')
                                    @if (count($shop))
                                        <div class="text-center">
                                            <a href="{{ route('pos.index') }}" class="btn btn-success"> Go To Buy</a>
                                        </div>
                                        <li><a href="{{ route('sale.product.index') }}" class="menu"><i class="fa fa-diamond"></i> Products</a></li>
                                        <li><a href="{{ route('sale.product_recive') }}" class="menu"><i class="fa fa-diamond"></i> Product Recived</a></li>
                                        <li><a href="{{ route('sale.bestsell') }}" class="menu"><i class="fa fa-diamond"></i>Best sale</a></li>
                                        <li><a href="{{ route('order.all_order') }}" class="menu"><i class="fa fa-money"></i> Buyers order history 
                                            @if (newOrder())
                                                <span class="label label-danger">{{ newOrder() }}</span>
                                            @endif
                                        </a></li>
                                        <li><a href="{{ route('sale.all') }}" class="menu"><i class="fa fa-money"></i> Sales history</a></li>
                                        <li><a href="{{ route('shop.setting') }}"><i class="fa fa-cog" class="menu"></i> Shop Setting</a></li>
                                    @else
                                        <div class="text-center">
                                            <a href="{{ route('shop.setting') }}" class="btn btn-success"> Go to shops</a>
                                        </div>
                                    @endif
                                @endif
                                <li><a href="{{ route('customer.order') }}" class="menu"><i class="fa fa-file-o"> </i> Your order history</a></li>
                                <li><a href="{{route('payment.history')}}"><i class="fa fa-history" class="menu"></i> Payment history</a></li>
                                <li><a href="{{route('my.wallet') }}" class="menu"><i class="fa fa-usd"></i> My wallet</a></li>
                                <li><a href="{{ route('moneywithdraw') }}" class="menu"><i class="fa fa-money"></i> Money withdraw</a></li>
                                <li><a href="{{ route('message') }}" class="menu"><i class="fa fa-commenting-o"></i> Conversations</a></li>
                                <li><a href="{{ route('account') }}" class="menu"><i class="fa fa-user" aria-hidden="true"></i> Manage profile</a></li>
                                <li><a href="{{ route('account') }}" class="menu"><i class="fa fa-user" aria-hidden="true"></i> DDDDD</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                @yield('content_right')

            </div>
        </div>
    </div>
</section>
@endsection

