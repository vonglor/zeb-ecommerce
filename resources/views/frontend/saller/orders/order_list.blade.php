<div class="centent_card_1r3 tab-content">
    <p>Orders</p>
    <div class="col-sm-6">
        <input type="text" name="search" id="search" class="form-control search" placeholder="Search Order Code">
        <br>
    </div>
    <div class="card-content">
        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th>Order Code</th>
                    <th>Date</th>
                    <th>Num of Products</th>
                    <th>Customer</th>
                    <th>Amount</th>
                    <th>Delivery Status</th>
                    <th>Payment Status</th>
                    <th class="text-right">Options</th>
                </tr>
                    <tr>
                        <td>{{ $order->code }}</td>
                        <td>{{ $order->date }}</td>
                        <td>{{ count(App\Models\Order_detail::where('order_id', $order->id)->get()) }}</td>
                        <td>
                            {{ $order->user->name }}
                        </td>
                        <td>${{ number_format($order->grand_total, 2) }}</td>
                        <td>{{ $order->delivery_status }}</td>
                        <td>
                            @if ($order->payment_status == 'paid')
                                <span class="label label-success">Paid</span>
                            @else
                                <span class="label label-danger">Unpaid</span>
                            @endif
                        </td>
                        <td class="text-right">
                            <a href="" id="{{ $order->id }}" class="btn btn-info btn-veiw-detail btn-xs"><i
                                    class="fa fa-eye"></i></a>
                        </td>
                    </tr>

            </table>
        </div>
    </div>
</div>