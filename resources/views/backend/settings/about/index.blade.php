@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h1 class="text-xl font-medium mr-auto">
                About us
            </h1>
        </div>
        <div class="grid grid-cols-6 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
                @if(!empty($about_us))
                @foreach ($about_us as $row)
            <form action="{{route('update/about', $row->id)}}" method="post">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <textarea class="ckeditor form-control text-base" value="{{ old('about_us', $row->about_us) }}" name="about_us"></textarea>
                    @if ($errors->has('about_us'))
                    <span class="invalid-feedback" role="alert" style="display: block; color:red">
                        <strong>{{ $errors->first('about_us') }}</strong>
                    </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary text-base  ">{{ __('Save')}}</button>
            </form>
            @endforeach
            @endif
            </div>
        </div>
    </div>
    <!-- END: Content -->
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });
    </script>
    
@endsection

