

	{{-- <section id="footer">
		<div class="container">
			<div class="row">
				<div class="footer_1 clearfix">
					<div class="col-sm-3">
						<div class="footer_1i clearfix">
							<h3 class="mgt"><a href="#">Shop On</a></h3>
							<p> Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus,
								metus.</p>
							<p>Got Question? Call us 24/7</p>
							<h4 class="mgt"><a class="col_1" href="#">+0123 456 789</a></h4>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="footer_1i1 clearfix">
							<h4 class="mgt">Useful links</h4>
							<ul class="normal">
								<li><a href="#">How it works</a></li>
								<li><a href="#">About us</a></li>
								<li><a href="#">Babysitters</a></li>
								<li><a href="#">Contact us</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="footer_1i1 clearfix">
							<h4 class="mgt">My Account</h4>
							<ul class="normal">
								<li><a href="#">Track my order</a></li>
								<li><a href="#">Terms of use</a></li>
								<li><a href="#">Wishlist</a></li>
								<li><a href="#">Submit Your feedback</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="footer_1i2 clearfix">
							<h4 class="mgt">Get In Tuch</h4>
							<p>NO. 172 - Kingdom Oxford Street.<br>
								000 United Kingdom.<br>
								info@gmail.com<br>
								+012 3456 7890</p>
							<ul class="social-network social-circle">
								<li><a href="#" class="icoRss" title="Rss"><i class="fa fa-rss"></i></a></li>
								<li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a>
								</li>
								<li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a>
								</li>
								<li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a>
								</li>
								<li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> --}}
<section id="footer_last">
    <div class="container">
        <div class="row">
            <div class="footer_last text-center clearfix">
                <div class="col-sm-12">
                    <p class="mgt">© 2013 Your Website Name. All Rights Reserved | Design by <a class="col_1"
                            href="http://www.templateonweb.com">TemplateOnWeb</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- toastr js --}}

@include('frontend.layouts.modal')


<script src="{{ asset('js/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('js/tagify.min.js') }}"></script>
<!-- Start datatable js -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
<!-- others plugins -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<script type="text/javascript" src="{{ asset('js/DateTimePicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/DateTimePicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/DateTimePicker-i18n.js') }}"></script>
<script src="{{ asset('frontend/js/myjs.js') }}"></script>
<script>
    $(document).ready(function () {
        /*****Fixed Menu******/
        var secondaryNav = $('.cd-secondary-nav'),
            secondaryNavTopPosition = secondaryNav.offset().top;
        $(window).on('scroll', function () {
            if ($(window).scrollTop() > secondaryNavTopPosition) {
                secondaryNav.addClass('is-fixed');
            } else {
                secondaryNav.removeClass('is-fixed');
            }
        });

        if ($('#currency-change').length > 0) {
                $('#currency-change .dropdown-menu a').each(function() {
                    $(this).on('click', function(e){
                        e.preventDefault();
                        var $this = $(this);
                        var currency_code = $this.data('currency');
                        var _token = $('input[name="_token"]').val();
                        $.post('{{ route('currency.change') }}',{_token: _token, currency_code:currency_code}, function(data){
                            location.reload();
                        });

                    });
                });
            }

    });

    @if (Session::has('message'))
        var type = "{{ Session::get('alert-type') }}";
        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-left",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
        }
    
        switch (type) {
        case 'info':
        toastr.info("{{ Session::get('message') }}");
        break;
    
        case 'success':
        toastr.success("{{ Session::get('message') }}");
        break;
    
        case 'warning':
        toastr.warning("{{ Session::get('message') }}");
        break;
    
        case 'error':
        toastr.error("{{ Session::get('message') }}");
        break;
        }
    
    @endif
</script>
<script>
    $(document).ready(function () {
		$('.centent_card_1li ul li a').on('click', function() {
            $('.centent_card_1li ul li a').removeClass('active');
            $(this).addClass('active');
        });

        $('#print').click(function (e) { 
                e.preventDefault();
                $('#printModal').modal('show');
            });

        $('#percent').click(function(e){
            e.preventDefault();
            $('#percentModal').modal('show');
        })
    });
</script>
@yield('script')
</body>
</html>