<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Staff;
use App\Models\User;
use Hash;
use Illuminate\Http\Request;
use Session;
use Uuid;

class UserController extends Controller
{
    public function index()
    {
        $staff = Staff::orderBy('id', 'desc')->get();

        return view('backend.users.index', compact('staff'));
    }

    public function create()
    {
        $roles = Role::orderBy('id', 'desc')->get();

        return view('backend.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'            => ['required', 'string', 'max:255'],
            'last_name'             => ['required', 'string', 'max:255'],
            "gender"                => ['required', 'string', 'in:male,female'],
            "phone"                 => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'unique:users'],
            'email'                 => ['required', 'email', 'string', 'unique:users'],
            'password'              => ['required', 'string', 'min:6'],
            'password_confirmation' => ['required', 'same:password'],
            'role_id'               => ['required', 'exists:roles,id', 'integer'],
            'user_type'             => ['required', 'string', 'in:admin,staff'],
        ]);

        $fileName = "default.png";
        if ($request->hasFile('avatar')) {
            $request->validate([
                "avatar" => 'required|mimes:jpeg,png,jpg|max:2048',
            ]);

            $filenameWithExt = $request->file('avatar')->getClientOriginalName();
            $extension       = $request->file('avatar')->getClientOriginalExtension();
            $fileName        = Uuid::generate(4) . '.' . $extension;
            $path            = $request->file('avatar')->move(public_path('uploads/user_img/'), $fileName);
        }

        $user            = new User();
        $user->name      = $request->first_name;
        $user->surname   = $request->last_name;
        $user->gender    = $request->gender;
        $user->phone     = $request->phone;
        $user->email     = $request->email;
        $user->user_type = $request->user_type;
        $user->password  = bcrypt($request->password);
        $user->photo     = $fileName;
        if ($user->save()) {
            $staff          = new Staff();
            $staff->user_id = $user->id;
            $staff->role_id = $request->role_id;
            if ($staff->save()) {
                toastr()->success('Save', 'User successfully.', ['timeOut' => 2000]);
                return redirect(route('users'));
            }
        } else {
            toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

    public function changeStatus(Request $request)
    {
        $user = User::find($request->id);

        if (!$user) {
            return response()->json(['status' => 'errors', 'message' => 'User not found']);
        }

        $user->status_active = $request->status;
        if ($user->save()) {
            return response()->json(['status' => 'success', 'message' => 'User status changed successfully']);
        } else {
            return response()->json(['status' => 'errors', 'message' => 'User status not changed']);
        }
    }

    public function edit(Staff $staff)
    {
        if (!$staff) {
            toastr()->error(__("Staff Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $roles = Role::orderBy('id', 'desc')->get();

        return view('backend.users.edit', compact('staff', 'roles'));
    }

    public function update(Request $request, Staff $staff)
    {
        if (!$staff) {
            toastr()->error(__("Staff Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $user = User::find($staff->user_id);

        if (!$user) {
            toastr()->error(__("User Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name'  => ['required', 'string', 'max:255'],
            "gender"     => ['required', 'string', 'in:male,female'],
            "phone"      => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'unique:users,phone,' . $user->id],
            'email'      => ['required', 'email', 'string', 'unique:users,email,' . $user->id],
            'role_id'    => ['required', 'exists:roles,id', 'integer'],
            'user_type'  => ['required', 'string', 'in:admin,staff'],
        ]);

        if ($request->hasFile('avatar')) {
            $request->validate([
                "avatar" => 'required|mimes:jpeg,png,jpg|max:2048',
            ]);

            if ($user->photo !== 'default.png') {
                unlink(public_path('uploads/user_img/' . $user->photo));
            }

            $filenameWithExt = $request->file('avatar')->getClientOriginalName();
            $extension       = $request->file('avatar')->getClientOriginalExtension();
            $user->photo     = Uuid::generate(4) . '.' . $extension;
            $path            = $request->file('avatar')->move(public_path('uploads/user_img/'), $user->photo);
        }

        $user->name      = $request->first_name;
        $user->surname   = $request->last_name;
        $user->gender    = $request->gender;
        $user->phone     = $request->phone;
        $user->email     = $request->email;
        $user->user_type = $request->user_type;
        if ($user->save()) {
            $staff->role_id = $request->role_id;
            if ($staff->save()) {
                toastr()->success('Save', 'User successfully.', ['timeOut' => 2000]);
                return redirect()->back();
            }
        } else {
            toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

    public function editPwd(User $user, $param)
    {
        if ($param !== 'editPwd' && $param !== 'changePwd') {
            toastr()->error(__("Invalid Parameter"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if (!$user) {
            toastr()->error(__("User Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if ($param === 'changePwd') {
            return view('backend.users.change_pwd', compact('user'));
        } else {
            return view('backend.users.edit_pwd', compact('user'));
        }
    }

    public function resetPwd(Request $request, User $user)
    {
        if (!$user) {
            toastr()->error(__("User Not Found"), __("Error"), ['timeOut' => 2000]);
        }

        $this->validate($request, [
            'password'              => ['required', 'string', 'min:6'],
            'password_confirmation' => ['required', 'same:password'],
        ]);

        $user->password = bcrypt($request->password);
        if ($user->save()) {
            toastr()->success('Save', 'User password successfully.', ['timeOut' => 2000]);
            return redirect(route('users'));
        } else {
            toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

    public function destroy(User $user)
    {
        if (!$user) {
            toastr()->error(__("User Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if ($user->photo !== 'default.png') {
            unlink(public_path('uploads/user_img/' . $user->photo));
        }

        if (User::destroy($user->id)) {
            toastr()->success(__("User Deleted"), __("Success"), ['timeOut' => 2000]);
            return redirect(route('users'));
        } else {
            toastr()->error(__("User Not Deleted"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

    public function signIn()
    {
        return view('backend.users.login');
    }

    public function signInPost(Request $request)
    {
        $this->validate($request, [
            'email'    => ['required', 'email', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:6'],
        ]);

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            toastr()->error(__("Invalid Email or Password"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if ($user->status_active === 'inactive') {
            toastr()->error(__("Your account is not active"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if ($user->email === $request->email && Hash::check($request->password, $user->password)) {
            $request->session()->put('user', $user);
            switch ($user->user_type) {
                case 'admin':
                    toastr()->success(__("Login Successfully"), __("Success"), ['timeOut' => 2000]);
                    return redirect()->route('admin.dashboard');
                    break;
                case 'staff':
                    toastr()->success(__("Login Successfully"), __("Success"), ['timeOut' => 2000]);
                    return redirect()->route('staff.dashboard');
                    break;
                default:
                    return redirect()->route('home');
                    break;
            }
        } else {
            toastr()->error(__("Invalid Email or Password"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

    public function signOut(Request $request)
    {
        $request->session()->forget('user');

        return redirect()->route('users.signIn');
    }

    public function profile(User $user)
    {
        if (!$user) {
            toastr()->error(__("User Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        return view('backend.users.profile', compact('user'));
    }

    public function updateProfile(Request $request, User $user)
    {
        if (!$user) {
            toastr()->error(__("User Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name'  => ['required', 'string', 'max:255'],
            "gender"     => ['required', 'string', 'in:male,female'],
            "phone"      => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'unique:users,phone,' . $user->id],
            'email'      => ['required', 'email', 'string', 'unique:users,email,' . $user->id],
        ]);

        if ($request->hasFile('avatar')) {
            $request->validate([
                "avatar" => 'required|mimes:jpeg,png,jpg|max:2048',
            ]);

            if ($user->photo !== 'default.png') {
                unlink(public_path('uploads/user_img/' . $user->photo));
            }

            $filenameWithExt = $request->file('avatar')->getClientOriginalName();
            $extension       = $request->file('avatar')->getClientOriginalExtension();
            $user->photo     = Uuid::generate(4) . '.' . $extension;
            $path            = $request->file('avatar')->move(public_path('uploads/user_img/'), $user->photo);
        }

        $user->name    = $request->first_name;
        $user->surname = $request->last_name;
        $user->gender  = $request->gender;
        $user->phone   = $request->phone;
        $user->email   = $request->email;
        if ($user->save()) {
            toastr()->success('Save', 'Profile successfully.', ['timeOut' => 2000]);
            return redirect()->back();
        } else {
            toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

    public function changePwd(Request $request, User $user)
    {
        if (!$user) {
            toastr()->error(__("User Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $this->validate($request, [
            'old_password'          => ['required', 'string', 'min:6'],
            'password'              => ['required', 'string', 'min:6'],
            'password_confirmation' => ['required', 'same:password'],
        ]);

        if (!Hash::check($request->old_password, $user->password)) {
            toastr()->error(__("Old Password is not correct"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if ($request->password === $request->old_password) {
            toastr()->error(__("New Password must be different from old password"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $user->password = bcrypt($request->password);
        if ($user->save()) {
            toastr()->success('Save', 'Change password successfully.', ['timeOut' => 2000]);
            return redirect()->route('users.profile', $user->id);
        } else {
            toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

}
