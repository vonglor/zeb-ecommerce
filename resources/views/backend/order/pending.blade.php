@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">
               {{__('Pending Orders')}}
            </h2>
            <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
                <div class="px-2">
                <a href="{{ route('order.fetch', ['status' => 'all']) }}"   class="btn btn-dark btn-sm">{{__('All Orders')}}</a>
                </div>
                {{-- <div class="px-2"> 
                    <a href="{{ route('order.fetch', ['status' => 'pending']) }}"   class="btn btn-danger btn-sm">{{__('Pending')}}</a> 
                    </div> --}}
                <div class="px-2">
                    <a href="{{ route('order.fetch', ['status' => 'confirmed']) }}"   class="btn btn-primary btn-sm">{{__('Confirmed')}}</a> 
                </div>
                    <div class="px-2">
                    <a href="{{ route('order.fetch', ['status' => 'completed']) }}"   class="btn btn-success btn-sm">{{__('Completed')}}</a> 
                    </div>
                {{-- <div class="pos-dropdown dropdown ml-auto sm:ml-0">
                    <button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
                        <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4" data-feather="chevron-down"></i> </span>
                    </button>
                    <div class="pos-dropdown__dropdown-menu dropdown-menu">
                        <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                            <a href="{{ route('order.fetch', ['status' => 'all']) }}" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> <span class="truncate">{{__('All')}}</span> </a>
                            <a href="{{ route('order.fetch', ['status' => 'pending']) }}" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> <span class="truncate">{{__('Pending')}}</span> </a>
                            <a href="{{ route('order.fetch', ['status' => 'confirmed']) }}" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> <span class="truncate">{{__('Confirmed')}}</span> </a>
                            <a href="{{ route('order.fetch', ['status' => 'completed']) }}" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> <span class="truncate">{{__('Completed')}}</span> </a>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 overflow-auto lg:overflow">
                <table class="table table-report -mt-2 display" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base">{{__('No.')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Date')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Customer')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Amount')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Delivery Status')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Payment Type')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Payment Status')}}</th>
                            <th class="whitespace-nowrap text-base text-center">{{__('Actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                            @php
                            $i = 1;
                        @endphp
                        @if(!empty($orders))
                        @foreach ($orders as $row)
                        <tr class="intro-x">
                            <td class="text-base">
                                {{ $i++ }}.
                            </td>
                            <td class="text-center text-base">
                                <h1 class="font-medium whitespace-nowrap text-base">{{ date("d-m-Y", strtotime($row->date)) }}</h1> 
                            </td>
                            <td class="text-base">{{ $row->user->name .' '. $row->user->surname }}</td>
                            <td class="text-center text-base">{{ '$'. number_format($row->grand_total, 2)}}</td>
                            <td class="text-center text-base">
                                @if($row->delivery_status === 'pending')
                                    <span class="text-theme-23 block">{{ $row->delivery_status }}</span>
                                @endif
                            </td>
                            <td class="text-center text-base">{{ $row->payment_type }}</td>
                            <td class="text-center text-base">
                                @if($row->payment_status === 'paid')
                                    <span class="text-theme-10 block">{{ $row->payment_status }}</span>
                                    @elseif($row->payment_status === 'unpaid')
                                    <span class="text-theme-23 block">{{ $row->payment_status }}</span>
                                @endif
                            </td>
                            <td class="text-center text-base">
                                <div class="dropdown"> 
                                    <a class="dropdown-toggle" aria-expanded="false"><i data-feather="more-vertical" class="w-4 h-4 mr-2"></i></a> 
                                    <div class="dropdown-menu w-48"> 
                                        <div class="dropdown-menu__content box dark:bg-dark-1 p-2"> 
                                                <a href="{{ route('order.show', ['order' => $row->id, 'param' => 'admin']) }}" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> 
                                                    <i data-feather="edit-2" class="w-4 h-4 mr-2"></i> {{__('View')}} 
                                                </a>
                                                
                                                <form action="{{ route('order.destroy', $row->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE') 
                                                    <button type="submit" class="btn_delete flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> 
                                                        <i data-feather="trash" class="w-4 h-4 mr-2"></i> {{__('Delete')}} 
                                                    </button>
                                                </form>      
                                        </div> 
                                    </div> 
                                </div> 
                            </td>
                        </tr>
                            @endforeach
                            @endif
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
        </div>
    </div>
    <!-- END: Content -->

    <script>
      $(document).ready(function() {
          $('#dataTable').DataTable({
            "lengthMenu": [
                [50, 75, 100, 150, 200, 1000],
                [50, 75, 100, 150, 200, 1000]
            ],
          });    
      });
  </script>

@endsection