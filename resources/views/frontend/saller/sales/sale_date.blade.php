@if (count($sales) > 0)
    @php
    $i = 1;
    @endphp
    @foreach ($sales as $sale)
    <tr>
        <td scope="row">{{ $i }}</td>
        <td>{{ $sale->code }}</td>
        <td>{{ $sale->date }}</td>
        <td>{{ count(App\Models\Order_detail::where('order_id', $sale->id)->get()) }}</td>
        <td>
            @if ($sale->user_id)
            {{ $sale->user->name }}
            @else
            Genneral
            @endif
            
        </td>
        <td>${{ number_format($sale->grand_total, 2) }}</td>
        <td>{{ $sale->delivery_status }}</td>
        <td>
            @if ($sale->payment_status == 'paid')
                <span class="label label-success">Paid</span>
            @else
                <span class="label label-danger">Unpaid</span>
            @endif
        </td>
        <td class="text-right">
            <a href="" id="{{ $sale->id }}" class="btn btn-info btn-veiw-detail btn-xs"><i class="fa fa-eye"></i></a>
        </td>
    </tr>
    @php
        $i++;
    @endphp
    @endforeach
@else
    <tr>
        <td colspan="9" class="text-center">Data not found...</td>
    </tr>
@endif
