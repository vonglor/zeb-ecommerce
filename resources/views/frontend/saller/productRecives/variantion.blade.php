@if (count($variantion) > 1)
    <div class="col-sm-2">
        <label for="Brands" class="col-form-label">Variantion</label>
    </div>
    <div class="col-sm-8">
        <select name="variantion" id="variant" class="form-control my-select selectpicker">
            @foreach ($variantion as $item)
                <option value="{{ $item->id }}">{{ $item->variant }}</option>
            @endforeach
            
        </select>
    </div>
@else
    @foreach ($variantion as $item)
    <input type="hidden" name="variantion" value="{{ $item->id }}">
    @endforeach
@endif



