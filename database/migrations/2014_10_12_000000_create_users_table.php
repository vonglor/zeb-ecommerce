<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_type', 20);
            $table->string('name', 50);
            $table->string('surname', 50);
            $table->string('email', 100)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->text('verification_code')->nullabel();
            $table->text('new_email_verification_code')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('country', 50);
            $table->string('state', 50);
            $table->string('city', 50);
            $table->string('postal_code', 20);
            $table->string('phone', 20);
            $table->string('gender', 10);
            $table->date('date_of_birth');
            $table->string('refer_id', 20);
            $table->string('refer_code_others', 20);
            $table->string('currency', 10)->nullable();
            $table->tinyInteger('banned')->default('0');
            $table->string('level1', 20)->nullable();
            $table->string('level2', 20)->nullable();
            $table->string('level3', 20)->nullable();
            $table->string('level4', 20)->nullable();
            $table->integer('status', 3)->default('0');
            $table->integer('delete', 3)->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
