@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
                   <div class="content">
                    <div class="intro-y flex items-center mt-8">
                        <h2 class="text-lg font-medium mr-auto text-xl">
                            Add New Category
                        </h2>
                    </div>
                    <div class="grid grid-cols-6 gap-6 mt-5">
                        <div class="intro-y col-span-12 lg:col-span-6">

                            <form action="{{route('category.store')}}"method="POST" enctype="multipart/form-data">
                                @csrf
                            <div class="intro-y box bg-gray">
                                <div id="vertical-form" class="p-5">
                                    <div class="preview">
                                        <div>
                                            <label for="vertical-form-1"  class="form-label text-base">Name</label>
                                            <input id="vertical-form-1" type="text" name="name" class="form-control border-gray-500 text-base" placeholder="Category Name" required>
                                            @error('name')
                                            <span style="color: red;">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Banner</label>
                                            <input id="vertical-form-1" type="file" name="banner" class="form-control border-gray-500" placeholder="banner">
                                        </div>
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Slug</label>
                                            <input id="vertical-form-1" type="text" name="slug" class="form-control border-gray-500" placeholder="slug">  
                                        </div>
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Meta Title</label>
                                            <input id="vertical-form-1" type="text" name="meta_title" class="form-control border-gray-500 text-base" placeholder="meta_title ">    
                                        </div>
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Meta Description</label>
                                            <textarea id="vertical-form-1" type="text" name="meta_description" class="form-control border-gray-500 text-base" placeholder="meta_description" rows="4" cols="50"> </textarea>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-primary mt-5 text-base">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
<!-- END: Content -->
    
@endsection