<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function index()
    {
        $user = \session()->get('user');
        $level = "level1";
        $member = User::where('level1', $user->refer_code_others)
                ->paginate(3);
        return view('frontend.saller.members.index', \compact('member', 'level'));
    }

    public function all() {
        $user = \session()->get('user');
        $level = "all";
        $member = User::where('level1', $user->refer_code_others)
                ->orWhere('level2', $user->refer_code_others)
                ->orWhere('level3', $user->refer_code_others)
                ->orWhere('level4', $user->refer_code_others)
                ->paginate(50);
        return view('frontend.saller.members.index', \compact('member', 'level'));
    }

    public function view($refer_id)
    {
        $member = User::where('refer_id', $refer_id)
                ->paginate(50);
        return view('frontend.saller.members.level2', \compact('member'));
    }

    public function level2($refer_id)
    {
        $member = User::where('refer_id', $refer_id)
                ->paginate(50);
        return view('frontend.saller.members.level3', \compact('member'));
    }

    public function level3($refer_id)
    {
        $member = User::where('refer_id', $refer_id)
                ->paginate(50);
        return view('frontend.saller.members.level4', \compact('member'));
    }

    public function shop()
    {
        $user = \session()->get('user');
        $member = User::join('shops', 'users.id', 'shops.user_id')
                ->select('shops.*', 'users.email')
                ->where('users.level1', $user->refer_code_others)
                ->orWhere('users.level2', $user->refer_code_others)
                ->orWhere('users.level3', $user->refer_code_others)
                ->orWhere('users.level4', $user->refer_code_others)
                ->paginate(50);
        return view('frontend.saller.members.shop', \compact('member'));
    }
}
