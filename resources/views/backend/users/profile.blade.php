@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y box mt-5">
          <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
              <h2 class="text-lg font-medium text-base mr-auto">
                <i data-feather="edit"></i> {{__('Profile')}}
              </h2>
              <div class="form-check w-full sm:w-auto sm:ml-auto mt-3 sm:mt-0">
                <a href="{{ route('users.index') }}">{{__('Back')}}</a>
              </div>
          </div>
          <div id="select-options" class="p-5">
            <form action="{{ route('users.updateProfile', $user->id) }}" autocomplete="off" enctype="multipart/form-data" method="POST">
              @csrf
              @method('PUT')
              <div class="preview">
                    <div class="mt-3">
                      <label for="gender">{{__('Gender')}} <span class="txt-red">*</span></label>
                      <div class="flex flex-col sm:flex-row mt-2">
                          <div class="form-check mr-2">
                              <input id="radio-switch-4" class="form-check-input" type="radio" name="gender" 
                              value="male" {{ old('gender', $user->gender) === "male" ? "checked": "" }}>
                              <label class="form-check-label" for="radio-switch-4">{{__('Male')}}</label>
                          </div>
                          <div class="form-check mr-2 mt-2 sm:mt-0">
                              <input id="radio-switch-5" class="form-check-input" type="radio" name="gender" 
                              value="female" {{ old('gender', $user->gender) === "female" ? "checked": "" }}>
                              <label class="form-check-label" for="radio-switch-5">{{__('FeMale')}}</label>
                          </div>
                      </div>
                      @if ($errors->has('gender'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('gender') }}</strong>
                              </span>
                          @endif
                  </div>
                  <br>

                  <div class="intro-y col-span-12 overflow-auto lg:overflow">
                    <div> 
                      <label for="first_name" class="form-label">{{__('Frist Name')}} <span class="txt-red">*</span></label> 
                      <input name="first_name" value="{{ old('first_name', $user->name) }}" type="text" class="form-control">
                      @if ($errors->has('first_name'))
                          <span class="invalid-feedback" role="alert" style="display: block; color:red">
                            <strong>{{ $errors->first('first_name') }}</strong>
                          </span>
                          @endif 
                    </div> 

                    <div class="mt-3"> 
                      <label for="last_name" class="form-label">{{__('Last Name')}} <span class="txt-red">*</span></label> 
                      <input name="last_name" value="{{ old('last_name',  $user->surname) }}" type="text" class="form-control">
                      @if ($errors->has('last_name'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('last_name') }}</strong>
                              </span>
                          @endif 
                    </div> 

                    <div class="mt-3"> 
                      <label for="phone" class="form-label">{{__('Phone')}} <span class="txt-red">*</span></label> 
                      <input name="phone" value="{{ old('phone',  $user->phone) }}" type="text" class="form-control">
                      @if ($errors->has('phone'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('phone') }}</strong>
                              </span>
                          @endif 
                    </div> 

                    <div class="mt-3"> 
                      <label for="email" class="form-label">{{__('Email')}} <span class="txt-red">*</span></label> 
                      <input name="email" value="{{ old('email',  $user->email) }}" type="email" class="form-control">
                      @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif 
                    </div>

                    {{-- <div class="mt-3"> 
                      <label for="password" class="form-label">{{__('Password')}} <span class="txt-red">*</span></label> 
                      <input name="password" type="password" class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
                      @if ($errors->has('password'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif 
                    </div>

                    <div class="mt-3"> 
                      <label for="password_confirmation" class="form-label">{{__('Confrim Password')}} <span class="txt-red">*</span></label> 
                      <input name="password_confirmation" type="password" class="form-control">
                      @if ($errors->has('password_confirmation'))
                              <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                              </span>
                          @endif 
                    </div> --}}

                    <div class="mt-3"> 
                      <label for="avatar" class="form-label">{{__('Avatar')}}</label>
                      @if(!empty($user->photo))
                          <img src="{{ asset('uploads/user_img/'.$user->photo) }}" alt="photo" class="rounded-full" width="60" height="60">
                          @else
                        <img src="{{ asset('uploads/user_img/default.png') }}" alt="photo" class="rounded-full" width="60" height="60">
                      @endif
                      <br>
                      <input name="avatar" id="avatar" type="file" class="form-control"> 
                      <span class="txt-red" id="nameFile"></span> 
                    </div> 

                    <button type="submit" id="btnSubmit" class="btn btn-primary mt-5" style="width:100%">{{__('Save')}}</button>
                    <a href="{{ route('admin.dashboard') }}" class="btn btn-secondary" style="width:100%">{{__('Cancel')}}</a> 
                  </div>
              </div> 
            </form>
          </div>
      </div>
    </div>
    <!-- END: Content -->

    <script>
      $(document).ready(function() {
        $("#avatar").change(function(){
          let avatar = document.getElementById("avatar");
          if (typeof (avatar.files) !== "undefined") {
            let size = parseFloat(avatar.files[0].size / (1024 * 1024)).toFixed(2); 
            if (size > 2) {
                $("#nameFile").text("File size must be less than 2MB");
                document.getElementById("btnSubmit").disabled = true;
            }else{
                $("#nameFile").text("");
                document.getElementById("btnSubmit").disabled = false;
            }
          }else{
              $("#nameFile").text("browser not support");
          }
        });
      });
  </script>

@endsection