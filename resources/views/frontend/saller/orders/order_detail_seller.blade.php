<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h5 class="modal-title">Order Number: {{ $order->code }}</h5>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-3 delivery">
            <span class="@if ($order->delivery_status == 'pending') active @else done @endif"><i class="fa fa-file"></i> Order completed</span> 
        </div>
        <div class="col-sm-3 delivery">
            <span class="@if ($order->delivery_status == 'confirmed') active @elseif($order->delivery_status == 'on_delivery' || $order->delivery_status == 'delivered') done  @endif"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Confirmed</span>
        </div>
        <div class="col-sm-3 delivery">
            <span class="@if ($order->delivery_status == 'on_delivery') active @elseif($order->delivery_status == 'delivered') done @endif"><i class="fa fa-truck" aria-hidden="true"></i> On delivery</span>
        </div>
        <div class="col-sm-3 delivery">
            <span class="@if ($order->delivery_status == 'delivered') done @endif"><i class="fa fa-check" aria-hidden="true"></i> Delivered</span>
        </div>
    </div>

    <div class="row">
        <div class="shipping_address col-sm-6" style="margin-bottom: 20px;">
            @if ($order->shipping_address)
            <span>Name: {{ json_decode($order->shipping_address)->name }}</span>
            <span>Address: {{ json_decode($order->shipping_address)->address.', '.json_decode($order->shipping_address)->city.', '.json_decode($order->shipping_address)->country }}</span>
            <span>Email: {{ json_decode($order->shipping_address)->email }}</span>
            <span>Phone: {{ json_decode($order->shipping_address)->phone }}</span>
            @else
                @if ($order->user_id)
                <span>Name: {{ $order->user->name }}</span>
                <span>Email: {{ $order->user->email }}</span>
                <span>Phone: {{ $order->user->phone }}</span>
                @else
                    General Customer
                @endif
            @endif
            <span>Order status: {{ $order->delivery_status }}</span>
            <span>Order date: {{ $order->date }}</span>
            <span>Payment method: {{ $order->payment_type }}</span>
        </div>
        <div class=" col-sm-4" style="margin-bottom: 20px; float: right;">
            <div class="form-group">
                <label>Delivery Status</label>
                <select class="form-control aiz-selectpicker form-control-sm" id="update_delivery_status">
                    <option value="pending" @if($order->delivery_status == 'pending') selected @endif>Pending</option>
                    <option value="confirmed" @if($order->delivery_status == 'confirmed') selected @endif>Confirmed</option>
                    <option value="on_delivery" @if($order->delivery_status == 'on_delivery') selected @endif>On delivery</option>
                    <option value="delivered" @if($order->delivery_status == 'delivered') selected @endif>Delivered</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-9">
            <div class="table-responsive">
                <table class="table table-bordered">
                        Order Detailsbordered
                        <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>qty</th>
                            <th>price</th>
                        </tr>
                        @php
                            $i = 1;
                            $subtotal = 0;
                        @endphp
                        @foreach ($order_details as $item)
                        @php
                            $subtotal += $item->price*$item->qty;
                        @endphp
                        <tr>
                            <td>{{ $i }}</td>
                            <td>
                                @php
                                $stock = App\Models\Product_stock::find($item->product_id);
                                @endphp
                                @if ($stock != null)
                                    @if ($stock->image != null)
                                    <img src="{{ url(App\Models\Upload::find($stock->image)->file_name) }}" alt="" class="img-fluid mb-2" style="height: 80px; margin-top: 10px;">
                                    @else
                                    <img src="{{ url(App\Models\Upload::find(App\Models\Product::find($stock->product_id)->photos)->file_name) }}" class="img-fluid mb-2" alt="" style="height: 80px; margin-top: 10px;">
                                    @endif
                                @else
                                <img src="{{ url(App\Models\Upload::find(App\Models\Product::find($stock->product_id)->photos)->file_name) }}" class="img-fluid mb-2" alt="" style="height: 80px; margin-top: 10px;">
                                @endif
                            </td>
                            <td>
                                <span>{{ App\Models\Product::find($item->productStock->product_id)->name }}</span><br>
                                @if ($item->color)
                                <span>{{ $item->color }}</span><br>
                                @endif
                                @if ($item->size)
                                <span>{{ $item->size }}</span><br>
                                @endif
                                @if ($item->fabric)
                                <span>{{ $item->fabric }}</span><br>
                                @endif
                                @if ($item->poud)
                                <span>{{ $item->poud }}</span>
                                @endif
                            </td>
                            <td>{{ $item->qty }}</td>
                            <td>{{ single_price($item->price) }}</td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
        <div class="col-sm-3" style="float: right">
            <table class="table table-bordered">
                    <tr>
                        Order Ammount
                    </tr>
                    <tr>
                        <td width="50%">Sub Total:</td>
                        <td width="50%">{{ single_price($subtotal) }}</td>
                    </tr>
                    <tr>
                        <td>tax:</td>
                        <td>{{ single_price(0) }}</td>
                    </tr>
                    <tr>
                        <td>Shipping:</td>
                        <td>{{ single_price($order->discount) }}</td>
                    </tr>
                    <tr>
                        <td>TOTAL:</td>
                        <td>{{ single_price($order->grand_total) }}</td>
                    </tr>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#update_delivery_status').on('change', function(){
        var order_id = {{ $order->id }};
        var status = $('#update_delivery_status').val();
        $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status}, function(data){
            $('#order_detail_modal').modal('hide');
            //console.log(data);
            location.reload().setTimeOut(500);
        });
    });
</script>