@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
 
    <div class="content">
        <form action="{{ route('withdraw.pay') }}" method="post">
            @csrf
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                <h1 class="text-xl mr-2 text-lg">{{ __('Request withdrawal')}}</h1>
                <div class="px-2">
                    <a href="{{ route('seller-withdraw-request', ['status' => 'pending']) }}" class="btn btn-outline-warning btn-sm">{{__('Pending')}}</a>
                </div>
                <div class="px-2">
                    <a href="{{ route('seller-withdraw-request', ['status' => 'completed']) }}" class="btn btn-outline-success btn-sm">{{__('Completed')}}</a>
                </div>
                <div class="hidden md:block mx-auto text-gray-600"></div>
                <button class="btn btn-dark btn-sm" type="submit" name='btn_update'>{{__('Withdraw')}}</button>
            </div>

            @if ($errors->has('withdraw_req_id.0'))
                <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                    <div class="alert alert-warning alert-dismissible show flex items-center mb-2" role="alert"> 
                        <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> 
                            <span class="invalid-feedback" style="display: block; color:red" role="alert">
                                <strong>{{ $errors->first('withdraw_req_id.0') }}</strong>
                            </span>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> 
                            <i data-feather="x" class="w-4 h-4"></i> 
                        </button> 
                    </div>
                </div>
            @endif 

            @if ($errors->has('withdraw_buy'))
                <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                    <div class="alert alert-warning alert-dismissible show flex items-center mb-2" role="alert"> 
                        <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> 
                            <span class="invalid-feedback" style="display: block; color:red" role="alert">
                                <strong>{{ $errors->first('withdraw_buy') }}</strong>
                            </span>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> 
                            <i data-feather="x" class="w-4 h-4"></i> 
                        </button> 
                    </div>
                </div>
            @endif 
            
            <div class="intro-y col-span-12 overflow-auto">
                <table class="table table-report -mt-2 display" id="dataTable2">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base"><input type='checkbox' id='checkAll' class="form-check-input"></th>
                            <th class="whitespace-nowrap text-base">{{__('Date time')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Customer Name')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Amount')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Membership')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Taxes 12%')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Total')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Status')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php 
                            $i = 1;
                        @endphp
                        @if(!empty($w))
                            @foreach ($w as $row)
                            <tr class="">
                                <td class="w-10">
                                    <input type="checkbox" name="withdraw_req_id[]" value="{{ $row->id }}" class="form-check-input">
                                    <input type="hidden" name="withdraw_buy" value="sale">
                                </td>
                                <td>
                                    <h1 class="font-medium whitespace-nowrap text-base">{{ date("d-m-Y H:i:s", strtotime($row->updated_at)) }}</h1> 
                                </td>
                                <td class="w-40">
                                    {{ $row->name.' '.$row->surname }}
                                </td>
                                <td class="text-center text-base">
                                    {{ number_format($row->amount, 2)}}
                                </td>
                                <td class="text-center text-base">
                                    {{ $row->membership }}
                                </td>
                                <td class="text-center">
                                    {{ $row->user_tax }}
                                </td>
                                <td class="text-center">{{ '$'. number_format($row->amount - ($row->membership + $row->user_tax), 2)}}</td>
                                <td class="text-center">
                                    <span class="text-theme-23 block">{{ $row->status }}</span> 
                                </td>

                            </form>

                                <td class="table-report__action w-56 text-center">
                                    <div class="dropdown"> 
                                        <a class="dropdown-toggle" aria-expanded="false"><i data-feather="more-vertical" class="w-4 h-4 mr-2"></i></a> 
                                        <div class="dropdown-menu w-48"> 
                                            <div class="dropdown-menu__content box dark:bg-dark-1 p-2"> 
                                                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> 
                                                        <i data-feather="edit-2" class="w-4 h-4 mr-2"></i> {{__('Edit')}}
                                                    </a>
                                                    
                                                    <form action="{{ route('withdraw.destroy', $row->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE') 
                                                        <button type="submit" class="btn_delete flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> 
                                                            <i data-feather="trash" class="w-4 h-4 mr-2"></i> {{__('Delete')}} 
                                                        </button>
                                                    </form>      
                                            </div> 
                                        </div> 
                                    </div> 
                                </td>
                            </tr> 
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
        </div>
        
    </div>


    <!-- END: Content -->

    <script>
        $(document).ready(function() {
            $('#dataTable2').DataTable({
                "order": [[ 0, "desc" ]],
                "columnDefs": [
                    { "orderable": false, "targets": [0,7] }
                ],
                "pageLength": 10,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "language": {
                    "info": "{{__('Showing')}} _START_ {{__('to')}} _END_ {{__('of')}} _TOTAL_ {{__('entries')}}",
                    "search": "{{__('Search')}}",
                    "lengthMenu": "{{__('Show')}} _MENU_ {{__('entries')}}",
                    "paginate": {
                        "next": "{{__('Next')}}",
                        "previous": "{{__('Previous')}}",
                        "first": "{{__('First')}}",
                        "last": "{{__('Last')}}"
                    },
                    "infoEmpty": "{{__('Showing 0 to 0 of 0 entries')}}",
                    "infoFiltered": "({{__('filtered from')}} _MAX_ {{__('total entries')}})",
                    "infoPostFix": "",
                    "thousands": ",",
                    "loadingRecords": "{{__('Loading')}}...",
                    "processing": "{{__('Processing')}}...",
                    "emptyTable": "{{__('No data available in table')}}",
                    "zeroRecords": "{{__('No matching records found')}}",
                    "paginate": {
                        "first": "{{__('First')}}",
                        "last": "{{__('Last')}}",
                    }
                }
            });
            
            // check/uncheck aLl
            $('#checkAll').change(function(){
                if($(this).is(':checked')){
                    $('input[name="withdraw_req_id[]"]').prop('checked',true);
                }else{
                    $('input[name="withdraw_req_id[]"]').each(function(){
                        $(this).prop('checked',false);
                    }); 
                }
            });

            // checkbox click
            $('input[name="withdraw_req_id[]"]').click(function(){
                var total_checkboxes = $('input[name="withdraw_req_id[]"]').length;
                var total_checkboxes_checked = $('input[name="withdraw_req_id[]"]:checked').length;
                if(total_checkboxes_checked === total_checkboxes){
                    $('#checkAll').prop('checked',true);
                }else{
                    $('#checkAll').prop('checked',false);
                }
            });
        });
    </script>
    
@endsection

