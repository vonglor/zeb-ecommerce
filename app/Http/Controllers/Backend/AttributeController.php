<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Attributes;
use Exception;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attribute = Attributes::latest()->paginate(10);
        return view('backend.products.attribute.index',compact('attribute'))
        ->with('i', (request()->input('page', 1) - 1) * 10);
    }
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('attribute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required|unique:attributes,name'
            ],
            [
                'name.unique' => 'The Attribute Name is Already taken'
            ]
        );

        $attribute = $request->all();

        Attributes::create($attribute);

        if ($attribute) {
            $notification = array(
                'message' => 'Insert successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('attribute.index')->with($notification);
        } else {
            $notification = array(
                'message' => 'Insert warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('attribute.index')->with($notification);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Attributes $attribute)
    {
        return view('backend.products.attribute.show', compact('attribute'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attribute = Attributes::find($id);
        return view('backend.products.attribute.edit', compact('attribute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attributes $attribute)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $attribute->update($request->all());


        if ($attribute) {
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('attribute.index')->with($notification);
        } else {
            $notification = array(
                'message' => 'Update warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('attribute.index')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attribute = Attributes::find($id);
        $attribute->delete();

        if ($attribute) {
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('attribute.index')->with($notification);
        } else {
            $notification = array(
                'message' => 'Delete warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('attribute.index')->with($notification);
        }
    }
}
