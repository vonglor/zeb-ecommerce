
@extends('frontend.layouts.app')
@section('css')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"> --}}
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css"> --}}
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
<link href="{{ asset('frontend/css/index2.css') }}" rel="stylesheet">
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script> --}}
@endsection
@section('content')
    
<div class="container mt-5">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="image-container">
                    <div class="first">
                        <div class="d-flex justify-content-between align-items-center"> <span class="discount">-25%</span> <span class="wishlist"><i class="fa fa-heart-o"></i></span> </div>
                    </div> <img src="{{ url('frontend/img/1.jpg') }}" class="img-fluid rounded thumbnail-image">
                </div>
                <div class="product-detail-container p-2">
                    <div class="d-flex justify-content-between align-items-center">
                        <h5 class="dress-name">White traditional long dress</h5>
                        <div class="d-flex flex-column mb-2"> <span class="new-price">$3.99</span> <small class="old-price text-right">$5.99</small> </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center pt-1">
                        <div class="color-select d-flex "> <input type="button" name="grey" class="btn creme"> <input type="button" name="red" class="btn red ml-2"> <input type="button" name="blue" class="btn blue ml-2"> </div>
                        <div class="d-flex "> <span class="item-size mr-2 btn" type="button">S</span> <span class="item-size mr-2 btn" type="button">M</span> <span class="item-size btn" type="button">L</span> </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center pt-1">
                        <div> <i class="fa fa-star-o rating-star"></i> <span class="rating-number">4.8</span> </div> <span class="buy">BUY +</span>
                    </div>
                </div>
            </div>
            <div class="mt-3">
                <div class="card voutchers">
                    <div class="voutcher-divider">
                        <div class="voutcher-left text-center"> <span class="voutcher-name">Monday Happy</span>
                            <h5 class="voutcher-code">#MONHPY</h5>
                        </div>
                        <div class="voutcher-right text-center border-left">
                            <h5 class="discount-percent">20%</h5> <span class="off">Off</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="image-container">
                    <div class="first">
                        <div class="d-flex justify-content-between align-items-center"> <span class="discount">-25%</span> <span class="wishlist"><i class="fa fa-heart-o"></i></span> </div>
                    </div> <img src="https://i.imgur.com/PtepOpe.jpg" class="img-fluid rounded thumbnail-image">
                </div>
                <div class="product-detail-container p-2">
                    <div class="d-flex justify-content-between align-items-center">
                        <h5 class="dress-name">Long sleeve denim jacket</h5>
                        <div class="d-flex flex-column mb-2"> <span class="new-price">$3.99</span> <small class="old-price text-right">$5.99</small> </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center pt-1">
                        <div class="color-select d-flex "> <input type="button" name="grey" class="btn creme"> <input type="button" name="darkblue" class="btn darkblue ml-2"> </div>
                        <div class="d-flex "> <span class="item-size mr-2 btn" type="button">S</span> <span class="item-size mr-2 btn" type="button">M</span> <span class="item-size btn" type="button">L</span> </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center pt-1">
                        <div> <i class="fa fa-star-o rating-star"></i> <span class="rating-number">4.8</span> </div> <span class="buy">BUY +</span>
                    </div>
                </div>
            </div>
            <div class="mt-3">
                <div class="card voutchers">
                    <div class="voutcher-divider">
                        <div class="voutcher-left text-center"> <span class="voutcher-name">Payday Surprise</span>
                            <h5 class="voutcher-code">#SRPSPYDY</h5>
                        </div>
                        <div class="voutcher-right text-center border-left">
                            <h5 class="discount-percent">20%</h5> <span class="off">Off</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="image-container">
                    <div class="first">
                        <div class="d-flex justify-content-between align-items-center"> <span class="discount">-25%</span> <span class="wishlist"><i class="fa fa-heart-o"></i></span> </div>
                    </div> <img src="https://i.imgur.com/ePJKs8Q.jpg" class="img-fluid rounded thumbnail-image">
                </div>
                <div class="product-detail-container p-2">
                    <div class="d-flex justify-content-between ">
                        <h5 class="dress-name">Hush Puppies</h5>
                        <div class="d-flex flex-column mb-2"> <span class="new-price">$3.99</span> <small class="old-price text-right">$5.99</small> </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center pt-1">
                        <div class="color-select d-flex "> <input type="button" name="grey" class="btn creme"> <input type="button" name="yellow" class="btn yellow ml-2"> <input type="button" name="blue" class="btn blue ml-2"> </div>
                        <div class="d-flex "> <span class="item-size mr-2 btn" type="button">S</span> <span class="item-size mr-2 btn" type="button">M</span> <span class="item-size btn" type="button">L</span> </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center pt-1">
                        <div> <i class="fa fa-star-o rating-star"></i> <span class="rating-number">4.2</span> </div> <span class="buy">BUY +</span>
                    </div>
                </div>
            </div>
            <div class="mt-3">
                <div class="card voutchers">
                    <div class="voutcher-divider">
                        <div class="voutcher-left text-center"> <span class="voutcher-name">First order</span>
                            <h5 class="voutcher-code">#HPYFRST</h5>
                        </div>
                        <div class="voutcher-right text-center border-left">
                            <h5 class="discount-percent">20%</h5> <span class="off">Off</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="image-container">
                    <div class="first">
                        <div class="d-flex justify-content-between align-items-center"> <span class="discount">-25%</span> <span class="wishlist"><i class="fa fa-heart-o"></i></span> </div>
                    </div> <img src="https://i.imgur.com/snffLH3.jpg" class="img-fluid rounded thumbnail-image">
                </div>
                <div class="product-detail-container p-2">
                    <div class="d-flex justify-content-between ">
                        <h5 class="dress-name">Athens skirt </h5>
                        <div class="d-flex flex-column mb-2"> <span class="new-price">$3.99</span> <small class="old-price text-right">$5.99</small> </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center pt-1">
                        <div class="color-select d-flex "> <input type="button" name="grey" class="btn creme"> <input type="button" name="red" class="btn red ml-2"> <input type="button" name="blue" class="btn blue ml-2"> </div>
                        <div class="d-flex "> <span class="item-size mr-2 btn" type="button">S</span> <span class="item-size mr-2 btn" type="button">M</span> <span class="item-size btn" type="button">L</span> </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center pt-1">
                        <div> <i class="fa fa-star-o rating-star"></i> <span class="rating-number">3.8</span> </div> <span class="buy">BUY +</span>
                    </div>
                </div>
            </div>
            <div class="mt-3">
                <div class="card voutchers">
                    <div class="voutcher-divider">
                        <div class="voutcher-left text-center"> <span class="voutcher-name">Vegetarian Food</span>
                            <h5 class="voutcher-code">#VEGANLOVE</h5>
                        </div>
                        <div class="voutcher-right text-center border-left">
                            <h5 class="discount-percent">20%</h5> <span class="off">Off</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function()
            {


            if($('.bbb_viewed_slider').length)
            {
            var viewedSlider = $('.bbb_viewed_slider');

            viewedSlider.owlCarousel(
            {
            loop:true,
            margin:30,
            autoplay:true,
            autoplayTimeout:6000,
            nav:false,
            dots:false,
            responsive:
            {
            0:{items:1},
            575:{items:2},
            768:{items:3},
            991:{items:4},
            1199:{items:6}
            }
            });

            if($('.bbb_viewed_prev').length)
            {
            var prev = $('.bbb_viewed_prev');
            prev.on('click', function()
            {
            viewedSlider.trigger('prev.owl.carousel');
            });
            }

            if($('.bbb_viewed_next').length)
            {
            var next = $('.bbb_viewed_next');
            next.on('click', function()
            {
            viewedSlider.trigger('next.owl.carousel');
            });
            }
            }


            });
    </script>
@endsection