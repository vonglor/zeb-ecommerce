<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Staff;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.auth.login');
    }

    public function createSignin(Request $request)
    {
        // dd($request->input());
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');
        // dd($credentials);
        if (Auth::attempt($credentials)) {
            return redirect()->intended('dashboard')
            // $notification = array(
            //     'message' => 'Register success!',
            //     'alert-type' => 'success'
            // );
            // return redirect()->route('dashboard')->with($notification)
            ->withSuccess('Logged-in');
        }
        // $notification = array(
        //     'message' => 'Register success!',
        //     'alert-type' => 'success'
        // );
        // return redirect()->route('login')->with($notification);

        return redirect("login")->withSuccess('Credentials are wrong.');
        
    }

    public function register()
    {
        return view('backend.auth.register');

    }

    public function customSignup(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'email' => 'required|email|unique:staff',
            'password' => 'required|min:6',
        ]);
        $data = $request->all();
        $check = $this->createUser($data);
        // return redirect("dashboard")->withSuccess('Successfully logged-in!');
        $notification = array(
            'message' => 'Register success!',
            'alert-type' => 'success'
        );
        return redirect()->route('login')->with($notification);
    }

    public function createUser(array $data)
    {
        return Staff::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'address' => $data['address'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }


    public function dashboardView()
    {
        if (Auth::check()) {
            return view('backend.index');
        }
        return redirect("login")->withSuccess('Access is not permitted');
    }


    public function logout()
    {
        Session::flush();
        Auth::logout();
        return Redirect('login');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate(
        //     [
        //         'first_name' => 'required',
        //         'last_name' => 'required',
        //         'phone' => 'required',
        //         'email' => 'required|unique:staff',
        //         'position' => 'required',
        //         'password' => 'required',

        //     ],
        //     [
        //         'email.unique' => 'This email alread exist',
        //     ]
        // );


        // $user = Staff::create([
        //     'first_name' => $request->first_name,
        //     'last_name' => $request->last_name,
        //     'phone' => $request->phone,
        //     'email' => $request->email,
        //     'position' => $request->position,
        //     'password' => bcrypt($request->password)
        // ]);

        // if ($user) {
        //     $notification = array(
        //         'message' => 'Add user success!',
        //         'alert-type' => 'success'
        //     );
        //     return redirect()->route('auth.login')->with($notification);
        // } else {
        //     $notification = array(
        //         'message' => 'Add user success!',
        //         'alert-type' => 'warning'
        //     );
        //     return redirect()->route('auth.login')->with($notification);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
