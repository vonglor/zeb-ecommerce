<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Product_stock;
use App\Models\Upload;
use Cart;
use Session;

class CartController extends Controller
{
    
    public function select_color(Request $request)
    {
        $option = array();
        if($request->color){
            $color = $request->color;
            array_push($option, $color);
        }
        if($request->Size){
            $size = $request->Size;
            array_push($option, $size);
        }
        if($request->Fabric){
            $fabric = $request->Fabric;
            array_push($option, $fabric);
        }

        $variant = implode('-', $option);
        $product = Product_stock::where(['product_id' => $request->product_id, 'variant' => $variant])->first();
        $image = Upload::find($product->image)->file_name;
            echo '<img alt="" title="" src="'.url($image).'" class="iw">';
            // $photos = Product::where('id', $request->product_id)->first();
            // $img = Upload::find($photos->photos)->file_name;
            // echo '<img alt="" title="" src="'.url($img).'" class="iw">';
    }

    public function add_to_cart(Request $request)
    {
        $product_id = $request->product_id;
        $product = Product::find($product_id);

        $option = array();
        $data = array();
        $data['pro_id'] = $product_id;
        if($request->color){
            $color = $request->color;
            $data['color'] = $color;
            array_push($option, $color);
        }else{
            $data['color'] = null;
        }
        if($request->Size){
            $size = $request->Size;
            $data['size'] = $size;
            array_push($option, $size);
        }else{
            $data['size'] = null;
        }
        if($request->Fabric){
            $fabric = $request->Fabric;
            $data['fabric'] = $fabric;
            array_push($option, $fabric);
        }else{
            $data['fabric'] = null;
        }
        if($request->Poud){
            $poud = $request->Poud;
            $data['poud'] = $poud;
            array_push($option, $poud);
        }else{
            $data['poud'] = null;
        }

        $variant = implode('-', $option);

        if($variant != null){
            $product_info = Product_stock::where(['product_id' => $product_id, 'variant'=>$variant])->first();
            $price = $product_info->price;
            $qty = $product_info->qty;

            if($qty < $request['qty']){
                $notification = array(
                    'message' => 'This item is out of stock!.',
                    'alert-type' => 'warning'
                );
                return back()->with($notification);
            }
        }else{
            $product_info = Product_stock::where('product_id', $product_id)->first();
            $price = $product->unit_price;
        }
        
        $data['id'] = $product_info->id;
        $data['owner_id'] = $product->user_id;
        $data['name'] = $product->name;
        $data['variant'] = $product_info->variant;
        $data['qty'] = $request['qty'];
        $data['price'] = $price - $product->discount;

        if($product_info->image){
            $data['image'] = $product_info->image;
        }else{
            $data['image'] = $product->photos;
        }

        if($request->session()->has('cart')){
            $foundInCart = false;
            $cart = collect();

            foreach ($request->session()->get('cart') as $key => $cartItem){
                if($cartItem['id'] == $product_info->id){
                    if($cartItem['variant'] == $variant && $variant != null){
                        $product_stock = Product_stock::where(['product_id' => $product_id, 'variant'=>$variant])->first();
                        $quantity = $product_stock->qty;

                        if($quantity < $cartItem['qty'] + $request['qty']){
                            // return array('status' => 0, 'view' => view('frontend.partials.outOfStockCart')->render());
                            $notification = array(
                                        'message' => 'This item is out of stock!.',
                                        'alert-type' => 'warning'
                                    );
                            return back()->with($notification);
                        }
                        else{
                            $foundInCart = true;
                            $cartItem['qty'] += $request['qty'];
                        }
                    }else{
                        $product_stock = Product_stock::where(['product_id' => $product_id])->first();
                        $quantity = $product_stock->qty;
                        if($quantity < $cartItem['qty'] + $request['qty']){
                            $notification = array(
                                        'message' => 'This item is out of stock!.',
                                        'alert-type' => 'warning'
                                    );
                            return back()->with($notification);
                        }else{
                            $foundInCart = true;
                            $cartItem['qty'] += $request['qty'];
                        } 
                    }
                }
                $cart->push($cartItem);
            }
            if (!$foundInCart) {
                $cart->push($data);
            }
            $request->session()->put('cart', $cart);
        }else{
            $cart = collect([$data]);
            $request->session()->put('cart', $cart);
        }
        $notification = array(
                    'message' => 'Item has been added to cart.',
                    'alert-type' => 'success'
                );
        //$request->session()->forget('cart');
        return back()->with($notification);
        
    }

    public function cart_destroy()
    {
        session()->forget('cart');
        $notification = array(
            'message' => 'Clear cart successfully.',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function cart_remove($key)
    {
        if(session()->has('cart')){
            $cart = session()->get('cart', collect([]));
            $cart->forget($key);
            session()->put('cart', $cart);
        }
        $notification = array(
            'message' => 'Item has been removed from cart.',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    public function update_cart(Request $request)
    {
        
        $cart = $request->session()->get('cart', collect([]));

        $cart = $cart->map(function ($object, $key) use ($request) {
            if($key == $request->key){
                $product = Product_stock::find($object['id']);
                if($object['variant'] != null){
                    $quantity = $product->qty;
                    if($quantity >= $request->qty){
                        if($request->qty >= $product->min_qty){
                            $object['qty'] = $request->qty;
                        }
                    }
                }
                elseif ($product->qty >= $request->qty) {
                    if($request->qty >= $product->min_qty){
                        $object['qty'] = $request->qty;
                    }
                }
            }
            return $object;
        });
        $request->session()->put('cart', $cart);
        
       // echo json_encode(["total" => $total, "subtotal" => 250000]);

    }

    public function select_product(Request $request)
    {
        $option = array();
        if($request->colors){
            $colors = $request->colors;
            $data['color'] = $colors;
            array_push($option, $colors);
        }else{
            $data['color'] = null;
        }
        if($request->Size){
            $size = $request->Size;
            $data['size'] = $size;
            array_push($option, $size);
        }else{
            $data['size'] = null;
        }
        if($request->Fabric){
            $fabric = $request->Fabric;
            $data['fabric'] = $fabric;
            array_push($option, $fabric);
        }else{
            $data['fabric'] = null;
        }
        if($request->Poud){
            $poud = $request->Poud;
            $data['poud'] = $poud;
            array_push($option, $poud);
        }else{
            $data['poud'] = null;
        }

        $variant = implode('-', $option);
        if($variant != null){
            $product_info = Product_stock::where(['product_id' => $request->product_id, 'variant'=>$variant])->first();
            $price = number_format($product_info->price, 2);
            $qty = $product_info->qty;
        }else{
            $product_info = Product_stock::where('product_id', $request->product_id)->first();
            $price = number_format($product->unit_price, 2);
            $qty = $product_info->qty;
        }
        $data = array("price" => $price, "qty" => $qty);
        echo json_encode($data);
    }

    public function buy(Request $request)
    {
    
        $product_id = $request->product_id;
        $product = Product::find($product_id);

        $option = array();
        $data = array();
        $data['pro_id'] = $product_id;
        if($request->color){
            $color = $request->color;
            $data['color'] = $color;
            array_push($option, $color);
        }else{
            $data['color'] = null;
        }
        if($request->Size){
            $size = $request->Size;
            $data['size'] = $size;
            array_push($option, $size);
        }else{
            $data['size'] = null;
        }
        if($request->Fabric){
            $fabric = $request->Fabric;
            $data['fabric'] = $fabric;
            array_push($option, $fabric);
        }else{
            $data['fabric'] = null;
        }
        if($request->Poud){
            $poud = $request->Poud;
            $data['poud'] = $poud;
            array_push($option, $poud);
        }else{
            $data['poud'] = null;
        }

        $variant = implode('-', $option);

        if($variant != null){
            $product_info = Product_stock::where(['product_id' => $product_id, 'variant'=>$variant])->first();
            $price = $product_info->price;
            $qty = $product_info->qty;

            if($qty < $request['qty']){
                $notification = array(
                    'message' => 'This item is out of stock!.',
                    'alert-type' => 'warning'
                );
                return back()->with($notification);
            }
        }else{
            $product_info = Product_stock::where('product_id', $product_id)->first();
            $price = $product->unit_price;
        }
        
        $data['id'] = $product_info->id;
        $data['owner_id'] = $product->user_id;
        $data['name'] = $product->name;
        $data['variant'] = $product_info->variant;
        $data['qty'] = $request['qty'];
        $data['price'] = $price - $product->discount;

        if($product_info->image){
            $data['image'] = $product_info->image;
        }else{
            $data['image'] = $product->photos;
        }

        if($request->session()->has('cart')){
            $foundInCart = false;
            $cart = collect();

            foreach ($request->session()->get('cart') as $key => $cartItem){
                if($cartItem['id'] == $product_info->id){
                    if($cartItem['variant'] == $variant && $variant != null){
                        $product_stock = Product_stock::where(['product_id' => $product_id, 'variant'=>$variant])->first();
                        $quantity = $product_stock->qty;

                        if($quantity < $cartItem['qty'] + $request['qty']){
                            $notification = array(
                                        'message' => 'This item is out of stock!.',
                                        'alert-type' => 'warning'
                                    );
                            return back()->with($notification);
                        }
                        else{
                            $foundInCart = true;
                            $cartItem['qty'] += $request['qty'];
                        }
                    }else{
                        $product_stock = Product_stock::where(['product_id' => $product_id])->first();
                        $quantity = $product_stock->qty;

                        if($quantity < $cartItem['qty'] + $request['qty']){
                            $notification = array(
                                        'message' => 'This item is out of stock!.',
                                        'alert-type' => 'warning'
                                    );
                            return back()->with($notification);
                        }
                        else{
                            $foundInCart = true;
                            $cartItem['qty'] += $request['qty'];
                        }
                    }
                }
                $cart->push($cartItem);
            }
            if (!$foundInCart) {
                $cart->push($data);
            }
            $request->session()->put('cart', $cart);
            return $product->user_id;
        }else{
            $cart = collect([$data]);
            $request->session()->put('cart', $cart);
            return $product->user_id;
        }
    }

}
