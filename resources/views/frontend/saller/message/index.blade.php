@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <div class="col-sm-9">
        <div class="centent_card_1r clearfix">
            <div class="centent_card_1r3 tab-content">
                <p>All Message</p>
                <div class="addproduct">
                    <a href="{{ route('sent.message') }}" class="btn btn-primary"> Sent Message</a>
                </div>
                <hr>
                <div class="card-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>#</th>
                                <th>Write by</th>
                                <th>Message</th>
                                <th>Dated</th>
                            </tr>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($message as $row)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $row->user->name }}</td>
                                <td scope="row">{{ $row->message }}</td>
                                <td>{{ $row->created_at }}</td>
                            </tr>
                            @php
                                $i++;
                            @endphp
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
