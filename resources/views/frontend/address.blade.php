
@foreach ($address as $item)
<div class="col-sm-6 address" id="address">
    <div class="flex-grow-1 pl-3 text-left address_l">
        <span>
            {{ $item->user->name . ' ' . $item->user->surname }}
        </span>
        <span>
            {{ $item->address }}
        </span>
        <span>
            {{ $item->city . ', ' . $item->province . ', ' . $item->postal_code }}
        </span>
        <span>
            {{ $item->country }}
        </span>
        <span>
            {{ $item->phone }}
        </span>
        <div class="text-right">
            @if ($item->status == 'unselect')
            <a href="{{ route('select.address', $item->id) }}" class="text-right">Select</a> |
           
            @endif
            <a href="" id="{{ $item->id }}" data="{{ $item->id }}" class="text-right editAddress">Edit</a>
        </div>
    </div>
</div>
@endforeach
<div class="col-sm-4 address">
    <div class="add_address">
        <i class="fa fa-plus la-2x mb-3"></i>
        <div class="alpha-7">Add New Address</div>
    </div>
</div>

{{-- modal edit address --}}
<div class="modal fade" id="myModalEditAddress" tabindex="-1" role="dialog" style="z-index: 10000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">New Address</h4>
            </div>
            <form class="form-horizontal" action="{{ route('address.address_update') }}" id="form_address"
                method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-10">
                            <input type="hidden" name="address_id" id="address_id">
                            <input type="hidden" name="user_id" id="user_id">
                            <textarea class="form-control" name="address" id="address"
                                placeholder="Your address" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Country</label>
                        <div class="col-sm-10">
                            <select name="country" class="form-control" id="country" required>
                                <option value="">Select Country</option>
                                @foreach (\App\Models\Country::all() as $country)
                                    <option value="{{ $country->name }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">State</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="state" id="state" placeholder="State"
                                required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">City</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="city" id="city" placeholder="City"
                                required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Postal Code</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="postal_code" id="postal_code"
                                placeholder="Postal Code" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Phone</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="phone" id="phone"
                                placeholder="Phone" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
     $('.add_address').click(function() {
                $('#myModal').modal('show');
            });

    $('.editAddress').click(function (e) { 
        e.preventDefault();
        var id = $(this).attr('id');
        $.ajax({
            type: "get",
            url: '/address/edit/'+id,
            success: function (data) {
                $("#myModalEditAddress").modal('show');
                $('#myModalEditAddress #address_id').val(data.id);
                $('#myModalEditAddress #user_id').val(data.user_id);
                $('#myModalEditAddress #address').val(data.address);
                $('#myModalEditAddress #country').val(data.country);
                $('#myModalEditAddress #state').val(data.province);
                $('#myModalEditAddress #city').val(data.city);
                $('#myModalEditAddress #postal_code').val(data.postal_code);
                $('#myModalEditAddress #phone').val(data.phone);
            }
        });
     });
</script>
