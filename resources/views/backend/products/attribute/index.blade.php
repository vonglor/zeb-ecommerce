@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto text-xl">
                Attribute
            </h2>
        </div>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
                <!-- BEGIN: Vertical Form -->
                <form action="{{route('attribute.store')}}" method="POST">
                    @csrf
                <div class="intro-y box">
                    <div id="vertical-form" class="p-5">
                        <div class="preview">
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Name</label>
                                <input id="vertical-form-1" type="text" name="name" class="form-control border-2 border-gray-500 text-base" placeholder="Name" required>
                                @error('name')
                                <span style="color: red;">{{ $message }}</span>
                                @enderror
                            </div>
                                <div class="mt-3">
                                    <button type="submit" class="btn btn-primary mt-5">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>

                <!-- BEGIN: Shipping Configuration -->

            <div class="intro-y col-span-12 lg:col-span-6">
                <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
                <table class="table table-report -mt-2" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base">#</th>
                            <th class="whitespace-nowrap text-base">NAME</th>
                            <th class="text-center whitespace-nowrap text-base">ACTIONS</th>
                        </tr>
                    </thead>
                        <tbody>
                            @php
                                $i = 0;
                            @endphp
                        @foreach ($attribute as $item)
                        <tr class="intro-x">
                            <td class="text-base">{{++$i}}</td>
                            <td class="text-base">{{$item->name}}</td>
                            <td class="table-report__action w-56 p-5">
                                <div class="flex justify-center items-center">
                                    <a class="btn btn-primary" href="{{route('attribute.edit', $item->id)}}"> <i data-feather="check-square" class="w-4 h-3 mr-1"></i></a>
                                    <form action="{{route('attribute.destroy', $item->id)}}" method="POST" class="px-2">
                                            @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
<!-- END: Content -->
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();    
    });
</script>
    
@endsection