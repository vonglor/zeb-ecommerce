@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <form action="{{ route('product.update', $product->id) }}" method="POST" enctype="multipart/form-data"
        id="choice_form">
        @csrf
        <div class="col-sm-6">
            <p class="title_header">Edit Your Products</p>
            <div class="centent_card_1r clearfix">
                <div class="centent_card_1r3 tab-content">
                    <p>Product Information</p>
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="inputPassword6" class="col-form-label">Product name</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" value="{{ $product->name }}"
                                        required>
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                </div>
                                <div id="demo">

                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="Category" class="col-form-label">Category</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="category_id" class="form-control"
                                        data-selected="{{ $product->category_id }}" data-live-search="true" required>
                                        <option value="">Select Brand</option>
                                        @foreach (App\Models\Category::where('id', $product->category_id)->get() as $item)
                                            <option value="{{ $item->id }}" @if ($product->category_id == $item->id) selected @endif>{{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="Brands" class="col-form-label">Brands</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="brand_id" class="form-control">
                                        <option value="">Select Brand</option>
                                        @foreach (App\Models\Brand::where('id', $product->brand_id)->get() as $item)
                                            <option value="{{ $item->id }}" @if ($product->brand_id == $item->id) selected @endif>{{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="unit" class="col-form-label">Unit</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="unit" name="unit" class="form-control"
                                        value="{{ $product->unit }}" required>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="min_qty" class="col-form-label">Minimum Qty</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="min_qty" name="min_qty" class="form-control"
                                        value="{{ $product->min_qty }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="centent_card_1r clearfix">
                <div class="centent_card_1r3 tab-content">
                    <p>Product Images, Videos and PDF</p>
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="image" class="col-form-label">Images</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="image" name="image" accept="image/*" class="form-control">
                                    <div class="image-preview" id="image-preview">
                                        @if ($product->photos)
                                            <img src="{{ url($product->Upload->file_name) }}" alt=""
                                                style="max-height: 80px; max-width: 80px;">
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="Thumbnail" class="col-form-label">Thumbnail</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="thumbnail" name="thumbnail[]" accept="image/*"
                                        class="form-control" multiple>
                                    <div class="image-preview" id="thumbnail-preview">
                                        @if ($product->thumbnail_img)
                                            @foreach (explode(',', $product->thumbnail_img) as $key => $item)
                                                <img src="{{ url(App\Models\Upload::find($item)->file_name) }}" alt=""
                                                    style="max-height: 80px; max-width: 80px;">
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="video_link" class="col-form-label">Video Link</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="video_link" name="video_link" class="form-control"
                                        value="{{ $product->videos }}">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="PDF" class="col-form-label">PDF Specification</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="pdf" name="pdf" class="form-control"
                                        placeholder="PDF Specification">
                                    <div class="image-preview" id="pdf-preview">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="centent_card_1r clearfix">
                <div class="centent_card_1r3 tab-content">
                    <p>Product Variation</p>
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="Colors" class="col-form-label">Colors</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="colors[]" id="colors" class="form-control my-select selectpicker"
                                        data-live-search="true" multiple data-selected-text-format="count">
                                        @foreach (\App\Models\Color::orderBy('name', 'asc')->get() as $key => $color)
                                            <option value="{{ $color->name }}"
                                                data-content="<span><span class='size-15px d-inline-block mr-2 rounded border' style='background:{{ $color->code }}'></span><span>{{ $color->name }}</span></span>"
                                                <?php if (in_array($color->name, json_decode($product->colors))) {
                                                    echo 'selected';
                                                } ?>></option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="Attributes" class="col-form-label">Attributes</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="attributes[]" id="attributes" class="form-control my-select selectpicker"
                                        multiple data-selected-text-format="count">
                                        @foreach (\App\Models\Attributes::all() as $key => $attribute)
                                            <option value="{{ $attribute->id }}" @if ($product->attributes != null && in_array($attribute->id, json_decode($product->attributes, true))) selected @endif>
                                                {{ $attribute->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <p>Choose the attributes of this product and then input values of each attribute</p><br>
                            </div>
                            <div id="choice_options">
                                @foreach (json_decode($product->choice_options) as $key => $choice_option)
                                    <div class="row col-sm-12 field">
                                        <div class="col-sm-3"><label for=""
                                                class="col-form-label">{{ \App\Models\Attributes::find($choice_option->attribute_id)->name }}</label>
                                            <input type="hidden" name="choice_no[]"
                                                value="{{ $choice_option->attribute_id }}">
                                            <input type="hidden" name="choice[]"
                                                value="{{ \App\Models\Attributes::find($choice_option->attribute_id)->name }}">
                                        </div>
                                        <div class="col-sm-8">
                                            <input name="choice_options_{{ $choice_option->attribute_id }}[]" id="idInput{{ $choice_option->attribute_id }}" class="tags"
                                                placeholder="Enter values" autofocus value="{{ implode(',', $choice_option->values) }}" data-on-change="update_sku">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="centent_card_1r clearfix">
                <div class="centent_card_1r3 tab-content">
                    <p>Product price + stock</p>
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="unit_price" class="col-form-label">Unit price</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" id="unit_price" name="unit_price" class="form-control"
                                        value="{{ $product->unit_price }}" required>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="purchase_price" class="col-form-label">Purchase price</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" id="purchase_price" name="purchase_price" class="form-control"
                                        value="{{ $product->purchase_price }}" required>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="Discount" class="col-form-label">Discount</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" id="discount" name="discount" class="form-control"
                                        value="{{ $product->discount }}">
                                </div>
                            </div>
                            <div class="row col-sm-12 field" id="quantity">
                                <div class="col-sm-3">
                                    <label for="Quantity" class="col-form-label">Quantity</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" name="quantity" class="form-control" placeholder="0">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row col-sm-12 field" id="sku_combination">

                        </div>
                    </div>
                </div>
            </div>
            <div class="centent_card_1r clearfix">
                <div class="centent_card_1r3 tab-content">
                    <p>Product Description</p>
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="description" class="col-form-label">Description</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="description" id="description" cols="30" rows="10"
                                        class="form-control">{{ $product->description }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="centent_card_1r clearfix">
                <div class="centent_card_1r3 tab-content">
                    <p>Meta Tags</p>
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="inputPassword6" class="col-form-label">Meta Title</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="meta_title" class="form-control"
                                        value="{{ $product->meta_title }}">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="inputPassword6" class="col-form-label">Meta Description</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="meta_description" id="" cols="30" rows="10"
                                        class="form-control">{{ $product->meta_description }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="centent_card_1r clearfix" style="margin-top: 60px">
                <div class="centent_card_1r3 tab-content">
                    <p>Estimate Shipping Time</p>
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="field">
                                <label for="inputPassword6" class="col-form-label">Shipping Days</label>
                                <input type="text" name="shipping_day" value="{{ $product->shipping_day }}"
                                    class="form-control">
                            </div>
                            <div class="field">
                                <label for="inputPassword6" class="col-form-label">Tax</label>
                                <input type="text" name="tax" class="form-control" value="{{ $product->tax }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 text-center">
            <button class="button btn-save" id="btn_save">Update</button>
        </div>
    </form>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            update_sku();
            $('.my-select').selectpicker();
            $('[class=tags]').tagify();
            $('#colors').change(function(e) {
                update_sku();
            });
            $('.tags').on('change', function(e) {
                    update_sku();
                });
            function add_more_choice_option(i, name) {
                $('#choice_options').append(
                    '<div class="row col-sm-12 field"><div class="col-sm-3"><label for="" class="col-form-label">' +
                    name +
                    '</label><input type="hidden" name="choice_no[]" value="' + i +
                    '"><input type="hidden" name="choice[]" value="' + name +
                    '"></div><div class="col-sm-8"><input name="choice_options_' + i +
                    '[]" id="idInput' + i + '" class="tags" placeholder="Enter ' + name +
                    ' values" autofocus></div></div>');

                $('[class=tags]').tagify();
                var input = document.querySelector('input[class=tags]'),
                    tagify = new Tagify(input);
                $('#idInput' + i).on('change', function(e) {
                    update_sku();
                });
            }

            $(document).on('change', '#image', function() {
                var property = document.getElementById('image').files[0];
                var img_name = property.name;
                var img_extension = img_name.split('.').pop().toLowerCase();
                var img_preview = $('#image-preview');
                if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg', 'gif']) == -1) {
                    img_preview.html('<span style="color: red;">This not support file reader</span>');
                    return false;
                } else {
                    var img_size = property.size;
                    if (img_size > 2000000) {
                        img_preview.html('<span style="color: red;">This file is to big</span>');
                        return false;
                    } else {
                        if (typeof(FileReader) != 'undefined') {
                            img_preview.empty();
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('<img/>', {
                                    'src': e.target.result,
                                    'class': 'img-fluid',
                                    'style': 'max-height: 80px; max-width: 80px;'
                                }).appendTo(img_preview);
                            }
                            img_preview.show();
                            reader.readAsDataURL($(this)[0].files[0]);
                        } else {
                            img_preview.html(
                                '<span style="color:red;">This not support file reader</span>');
                        }
                    }
                }

            });

            $(document).on('change', '#thumbnail', function() {
                var totalfiles = document.getElementById('thumbnail').files.length;
                for (var index = 0; index < totalfiles; index++) {
                    var property = document.getElementById('thumbnail').files[index];
                    var img_name = property.name;
                    var img_extension = img_name.split('.').pop().toLowerCase();
                    var img_preview = $('#thumbnail-preview');
                    if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg']) == -1) {
                        img_preview.html('<span style="color: red;">This not support file reader</span>');
                        return false;
                    } else {
                        var img_size = property.size;
                        if (img_size > 2000000) {
                            img_preview.html('<span style="color: red;">This file is to big</span>');
                            return false;
                        } else {
                            if (typeof(FileReader) != 'undefined') {
                                img_preview.empty();
                                var reader = new FileReader();
                                reader.onload = function(e) {
                                    $('<img/>', {
                                        'src': e.target.result,
                                        'style': 'max-height: 80px; max-width: 80px;'
                                    }).appendTo(img_preview);
                                }
                                img_preview.show();
                                reader.readAsDataURL($(this)[0].files[index]);
                            } else {
                                img_preview.html(
                                    '<span style="color:red;">This not support file reader</span>');
                            }
                        }
                    }
                }

            });

            function update_sku() {
                $.ajax({
                    type: "POST",
                    url: '{{ route('products.sku_combination_edit') }}',
                    data: $('#choice_form').serialize(),
                    success: function(data) {

                        $('#sku_combination').html(data);
                        if (data.length > 1) {
                            $('#quantity').hide();
                        } else {
                            $('#quantity').show();
                        }
                    }
                });
            }

            $('input[name="unit_price"]').on('input', function() {
                update_sku();
            });

            $('#attributes').change(function(e) {
                $.each($("#attributes option:selected"), function(j, attribute){
                    flag = false;
                    $('input[name="choice_no[]"]').each(function(i, choice_no) {
                        if($(attribute).val() == $(choice_no).val()){
                            flag = true;
                        }
                    });
                    if(!flag){
                        add_more_choice_option($(attribute).val(), $(attribute).text());
                    }
                });

                var str = @php echo $product->attributes @endphp;

                $.each(str, function(index, value){
                    flag = false;
                    $.each($("#attributes option:selected"), function(j, attribute){
                        if(value == $(attribute).val()){
                            flag = true;
                        }
                    });
                    if(!flag){
                        $('input[name="choice_no[]"][value="'+value+'"]').parent().parent().remove();
                    }
                });

                update_sku();
            });

            $('#btn_delete').click(function(e) {
                e.preventDefault();
            });
        });
    </script>
@endsection
