@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <div class="col-sm-9">
        <div class="centent_card_1r clearfix">
            <div class="centent_card_1r3 tab-content">
                <p>Wallet Recharge History</p>
                <hr>
                <div class="card-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Date </th>
                                <th>Your balance</th>
                                <th>Action</th>
                            </tr>
                            <tr>
                                <td scope="row">{{ $wallet->user->name }}</td>
                                <td>{{ date('d-m-Y') }}</td>
                                <td>{{ convert_balance($wallet->amount) }}</td>
                                <td><a href="" class="btn btn-success btn-xs"> Request</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
