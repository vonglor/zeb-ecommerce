 <!-- BEGIN: JS Assets-->
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=["AIzaSyA_Re- AIzaSyD8uRuD_YczWa67ZQMH-UeUdu3LA3gEkfU]&libraries=places></script>
        <script src="{{URL::asset('backend/dist/js/app.js')}}"></script>

        <script src="{{ asset('js/jQuery.tagify.min.js') }}"></script>
        <script src="{{ asset('js/tagify.min.js') }}"></script>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

        {{-- datatable --}}
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

        <script>
            $(document).ready(function () {

            $(".btn_delete").click(function (e) { 
            var form = $(this).closest('form');
            e.preventDefault();
            swal("Do you really want to delete these records?", {
                buttons: ["Cancel", true],
            }).then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
            });

            $(".clear_cart_items").click(function (e) { 
            var form = $(this).closest('form');
            e.preventDefault();
            swal("Do you really want to clear cart?", {
                buttons: ["Cancel", true],
            }).then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
            });
        });
        
        @if (Session::has('message'))
         var type = "{{ Session::get('alert-type') }}";
         toastr.options = {
         "closeButton": true,
         "debug": false,
         "newestOnTop": false,
         "progressBar": true,
         //"positionClass": "toast-bottom-right",
         "preventDuplicates": false,
         "onclick": null,
         "showDuration": "300",
         "hideDuration": "1000",
         "timeOut": "2000",
         "extendedTimeOut": "1000",
         "showEasing": "swing",
         "hideEasing": "linear",
         "showMethod": "fadeIn",
         "hideMethod": "fadeOut"
         }
     
         switch (type) {
         case 'info':
         toastr.info("{{ Session::get('message') }}");
         break;
     
         case 'success':
         toastr.success("{{ Session::get('message') }}");
         break;
     
         case 'warning':
         toastr.warning("{{ Session::get('message') }}");
         break;
     
         case 'error':
         toastr.error("{{ Session::get('message') }}");
         break;
         }

         
             
     @endif
        </script>

    @toastr_js
    @toastr_render
       
        <!-- END: JS Assets-->
    </body>
</html>