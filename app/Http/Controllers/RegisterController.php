<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Customer;
use App\Models\Seller;
use App\Models\Address;
use Session;
class RegisterController extends Controller
{
    //
    public function register(Request $request)
    {
        $request->validate([
                'card_img' => 'mimes:jpg,jpeg,png',
                'email' => 'required|email|unique:users,email',
                'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:7',
                'password' => 'required|min:6',
                'c_password' => 'required|same:password|min:6'
            ],
            [
                'c_password.same' => 'The password confirmation does not match'
            ]
        );

        $refer = User::where('refer_code_others', $request->refer_id)->first();
        if($refer){
            $img = $request->file('card_img');
            if($img){
                $name_img = hexdec(uniqid());
                $img_ext = strtolower($img->extension());
                $img_new_name = $name_img.'.'.$img_ext;
                $upload_path = 'uploads/card/';
                $card_img = $upload_path.$img_new_name;
        
                $img->move($upload_path, $img_new_name);
            }else{
                $card_img = '';
            }
            
            $data = [
                'currency' => $request->currency,
                'user_type' => $request->user_type,
                'card_id' => $request->card_id,
                'card_img' => $card_img,
                'refer_id' => $request->refer_id,
                'ss_id' => $request->ss_id,
                'name' => $request->name,
                'surname' => $request->surname,
                'dob' => $request->dob,
                'gender' => $request->gender,
                'phone' => $request->phone,
                'address' => $request->address,
                'country' => $request->country,
                'city' => $request->city,
                'postal_code' => $request->postal_code,
                'email' => $request->email,
                'password' => $request->password
            ];
              
            return view('frontend.register', \compact('data'));
        }else{
            return back()->with('err', 'ReferID not found in user...');
        }

        
    }

    public function store(Request $request)
    {
        $userQty = User::all();
        $refer_code_others = count($userQty);

        $user = new User();
        $user->user_type = $request->user_type;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone = $request->phone;
        $user->gender = $request->gender;
        $user->date_of_birth = $request->dob;
        $user->refer_id = $request->refer_id;
        $user->refer_code_others = $refer_code_others + 1;
        $user->currency = $request->currency;
        $user->level1 = $request->refer_id;

        $level1 = User::where('refer_code_others', $request->refer_id)->first();
        if($level1){
            $user->level2 = $level1->refer_id;
            $level2 = User::where('refer_code_others', $level1->refer_id)->first();
            if($level2){
                $user->level3 = $level2->refer_id;
                $level3 = User::where('refer_code_others', $level2->refer_id)->first();
                if($level3){
                    $user->level4 = $level3->refer_id;
                }
            }
        }
        $user->save();
    
        if($request->user_type == "buy"){
            Customer::create([
                'user_id' => $user->id,
                'card_id' => $request->card_id,
                'card_img' => $request->card_img
            ]);
        }else{
            Seller::create([
                'user_id' => $user->id,
                'card_id' => $request->card_id,
                'card_img' => $request->card_img
            ]);
        }
        Address::create([
            'user_id' => $user->id,
            'address' => $request->address,
            'country' => $request->country,
            'province' => $request->state,
            'city' => $request->city,
            'postal_code' => $request->postal_code,
            'phone' => $request->phone,
            'status' => 'selected'
        ]);
        $notification = array(
            'message' => 'Register successfully.',
            'alert-type' => 'success'
        );
        return redirect()->route('sign_in')->with('success', 'You register successfully please login...');
    }
}
