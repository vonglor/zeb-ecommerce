<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $brands = Brand::latest()->paginate(10);

        return view('backend.products.brand.index', compact('brands'))
        ->with('i', (request()->input('page', 1) - 1) * 10);
    }
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.products.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'name' => 'required|unique:brands,name',
            'logo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048' 
        ],
        [
          'name.unique' => 'The Brand Name is Already taken'
        ]
        );

        $input = $request->all();

        if ($logo = $request->file('logo')) {
            $destinationPath = 'uploads/images/';
            $profileImage = date('YmdHis') . "." . $logo->getClientOriginalExtension();
            $logo->move($destinationPath, $profileImage);
            $input['logo'] = "$profileImage";
        }
    
        Brand::create($input);

        if ($input) {
            $notification = array(
                'message' => 'Insert successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('brand.index')->with($notification);
        } else {
            $notification = array(
                'message' => 'Insert warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('brand.index')->with($notification);
        }

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return view('backend.products.brand.show', compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::find($id);
        return view('backend.products.brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $input = $request->all();
  
        if ($logo = $request->file('logo')) {
            $destinationPath = 'uploads/images/';
            $profileImage = date('YmdHis') . "." . $logo->getClientOriginalExtension();
            $logo->move($destinationPath, $profileImage);
            $input['logo'] = "$profileImage";
        }else{
            unset($input['logo']);
        }
          
        $brand->update($input);

        if ($brand) {
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('brand.index')->with($notification);
        } else {
            $notification = array(
                'message' => 'Update warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('brand.index')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);
        $brand->delete();

        if ($brand) {
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('brand.index')->with($notification);
        } else {
            $notification = array(
                'message' => 'Delete warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('brand.index')->with($notification);
        }
    }
}
