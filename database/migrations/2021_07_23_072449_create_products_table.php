<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_id', 20);
            $table->string('name', 200);
            $table->string('added_by', 6);
            $table->integer('user_id');
            $table->integer('category_id');
            $table->integer('brand_id')->nullable();
            $table->string('photos', 200)->nullable();
            $table->string('thumbnail_img', 200)->nullable();
            $table->string('videos')->nullable();
            $table->string('pdf',200)->nullable();
            $table->text('description')->nullable();
            $table->double('unit_price', 20, 2);
            $table->double('purchase_price', 20, 2);
            $table->string('currency_code', 20);
            $table->mediumText('attributes')->nullable();
            $table->mediumText('choice_options')->nullable();
            $table->mediumText('colors')->nullable();
            $table->string('unit', 20)->nullable();
            $table->integer('min_qty')->default('1');
            $table->double('discount', 20, 2)->nullable();
            $table->integer('num_of_sale')->default('0');
            $table->string('slug')->nullable();
            $table->string('meta_title', 100)->nullable();
            $table->text('meta_description')->nullable();
            $table->string('condition', 20)->default('New');
            $table->text('delivery')->nullable();
            $table->integer('shipping_day')->nullbable();
            $table->double('shipping', 20, 0)->nullable();
            $table->double('tax', 10, 2)->nullable();
            $table->integer('views')->default('0');
            $table->string('product_location', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
