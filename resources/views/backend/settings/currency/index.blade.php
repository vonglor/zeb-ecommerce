@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h1 class="text-xl font-medium mr-auto">
                System Default Currency
            </h1>
        </div>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 overflow-auto">
                <div class="float-right">
                    <a type="submit" class="btn btn-primary text-base" data-toggle="modal" data-target="#modal-add">Add New Currency</a>
                </div>
            <!-- start insert -->
            <div class="intro-y box mt-5">
                    <div id="header-footer-modal">
                        <div class="preview">
                            <div id="modal-add" class="modal" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h2 class="font-medium text-lg mr-auto">
                                                Add New Currency
                                            </h2>
                                            <button type="button" data-dismiss="modal" class="col-span-6 sm:col-span-3 lg:col-span-2 xl:col-span-1"><i data-feather="x" class="block mx-auto"></i> </button>
                                            
                                        </div>
                                        <form action="{{route('add.currency')}}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                                                <div class="col-span-12 sm:col-span-6">
                                                    <label for="modal-form-1" class="form-label text-base">Code</label>
                                                    <input id="modal-form-1" type="text" name="code" class="form-control border-gray-500 text-base" placeholder="Enter Currency Code" required>
                                                </div>
                                                <div class="col-span-12 sm:col-span-6">
                                                    <label for="modal-form-2" class="form-label text-base">Name</label>
                                                    <input id="modal-form-2" type="text" name="name" class="form-control border-gray-500 text-base" placeholder="Enter Currency Name" required>
                                                </div>
                                                <div class="col-span-12 sm:col-span-6">
                                                    <label for="modal-form-4" class="form-label text-base">Symbol</label>
                                                    <input id="modal-form-4" type="text" name="symbol" class="form-control border-gray-500 text-base" placeholder="Enter Currency Symbol" required>
                                                </div>
                                            </div>
                                            <div class="modal-footer text-right">
                                                <button type="button" data-dismiss="modal" class="btn btn-danger w-20 mr-1 text-base">Cancel</button>
                                                <button type="submit" class="btn btn-primary w-20 text-base">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            <!-- end insert -->
            
                <table class="table table-report -mt-2" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base">#</th>
                            <th class="whitespace-nowrap text-base">CURRENCY CODE</th>
                            <th class="text-base">CURRENCY NAME</th>
                            <th class="text-base">SYMBOL</th>
                            <th class="text-base">STATUS</th>
                            <th class="text-center whitespace-nowrap text-base">ACTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 0;
                        @endphp
                        @foreach ($currency as $item)
                        <tr class="intro-x">
                            <td class="w-10 text-base">{{++$i}}</td>
                            <td class="w-40 text-base">{{$item->code}}</td>
                            <td class="w-40 text-base">{{$item->name}}</td>
                            <td class="w-40 text-base">{{$item->symbol}}</td>
                            <td class="w-40 text-base">
                                <input class="form-check-switch" type="checkbox">
                            </td>
                            <td class="table-report__action w-56 p-5">
                                <div class="flex justify-center items-center">
                                    <a class="btn btn-primary" href="{{route('edit.currency', $item->id)}}"> <i data-feather="check-square" class="w-4 h-3 mr-1"></i></a>
                                    <form action="{{route('delete.currency', $item->id)}}" method="POST" class="px-2">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
<!-- END: Content -->

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();    
    });
</script>

    
@endsection

@push('script')
    <!-- Page level custom scripts -->
    <script src="{{ asset('public/backend/dist/js/select2.min.js')}}"></script>
    <script>
        $(".js-example-theme-single").select2({
            theme: "classic"
        });

        $(".js-example-responsive").select2({
            width: 'resolve'
        });
    </script>

    <script>
        $(document).on('change', '.status', function () {
            var id = $(this).attr("id");
            if ($(this).prop("checked") == true) {
                var status = 1;
            } else if ($(this).prop("checked") == false) {
                var status = 0;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
         
        });
    </script>
@endpush