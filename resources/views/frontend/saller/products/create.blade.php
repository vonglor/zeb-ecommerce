@extends('frontend.saller.content_left')
@section('css')
<link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data" id="choice_form">
        @csrf
        <div class="col-sm-9">
            <p class="title_header">Add Your Products</p>
            <div class="centent_card_1r clearfix">
                <div class="centent_card_1r3 tab-content">
                    <p>Product Information</p>
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Category" class="col-form-label">Category</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="category_id" class="form-control" required>
                                        <option value="">Select Category</option>
                                        @foreach ($category as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Brands" class="col-form-label">Brands</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="brand_id" class="form-control">
                                        <option value="">Select Brand</option>
                                        @foreach ($brand as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Product name</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="name" rows="5" class="form-control" placeholder="Product name" required></textarea>
                                    {{-- <input type="text" class="form-control" name="name" placeholder="Product Name" required> --}}
                                    @error('name')
                                        <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div id="demo">

                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="unit" class="col-form-label">Unit</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="unit" name="unit" class="form-control"
                                        placeholder="Unit (e.g.KG, Pc etc)" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- start --}}
                <div class="centent_card_1r3 tab-content">
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Colors" class="col-form-label">Colors</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="colors[]" id="colors" class="form-control my-select selectpicker"
                                        data-live-search="true" multiple data-selected-text-format="count">
                                        @foreach (\App\Models\Color::all() as $key => $color)
                                            <option value="{{ $color->name }}" data-content="<span><span style='background:{{ $color->code }}; border: 1px solid #000; padding: 0 8px;'> </span><span> {{ $color->name }}</span></span>">
                                                
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <a href="" id="add_color">Add color</a>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Attributes" class="col-form-label">Size: </label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="attributes[]" id="attributes" class="form-control my-select selectpicker"
                                        multiple data-selected-text-format="count">
                                        @foreach (\App\Models\Attributes::all() as $key => $attribute)
                                            <option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div id="choice_options">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="centent_card_1r3 tab-content">
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            
                            <div class="card-content">
                                <div id="sku_combination">
                                    
                                </div>
                            </div>
                            <div class="row col-sm-12 field" id="quantity">
                                <div class="col-sm-2">
                                    <label for="Quantity" class="col-form-label">Total</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" name="quantity" class="form-control" placeholder="0">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="min_qty" class="col-form-label">Minimum Qty</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="min_qty" name="min_qty" class="form-control" value="1"
                                        required>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="unit_price" class="col-form-label">Price</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" id="unit_price" name="unit_price" class="form-control"
                                        placeholder="0" required>
                                </div>
                                <div class="col-sm-2">
                                    <select name="currency_code" class="form-control" id="" required>
                                        @foreach (App\Models\Currency::where('status', 1)->get() as $item)
                                            <option value="{{ $item->code }}">{{ $item->code }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="purchase_price" class="col-form-label">Purchase price</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" id="purchase_price" name="purchase_price" class="form-control"
                                        placeholder="0" required>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Discount" class="col-form-label">Discount</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" id="discount" name="discount" class="form-control" placeholder="0">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="centent_card_1r3 tab-content">
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="image" class="col-form-label">Images</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="image" name="image" accept="image/*" class="form-control">
                                    <div class="image-preview" id="image-preview">
                                    </div>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Thumbnail" class="col-form-label">Thumbnail</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="thumbnail" name="thumbnail[]" accept="image/*" class="form-control" multiple>
                                    <div class="image-preview" id="thumbnail-preview">
                                    </div>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="video_link" class="col-form-label">Video Link</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="video_link" name="video_link" class="form-control"
                                        placeholder="Video Link">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="PDF" class="col-form-label">PDF Specification</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="pdf" name="pdf" class="form-control"
                                        placeholder="PDF Specification">
                                        <div class="image-preview" id="pdf-preview">
                                        </div>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Shipping</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" name="shipping" placeholder="Shipping" class="form-control">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Product location</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="product_location" placeholder="Product location" class="form-control">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Delivery</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="delivery" id="" cols="30" rows="10"
                                        class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="description" class="col-form-label">Description</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="description" id="description" cols="30" rows="10"
                                                class="form-control" placeholder="Product description"></textarea>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Meta Title</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="meta_title" class="form-control" placeholder="Meta Title">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Meta Description</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="meta_description" id="" cols="30" rows="10"
                                        class="form-control" placeholder="Meta Description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="centent_card_1r3 tab-content">
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            
                        <div class="col-sm-12 text-center">
                            <button class="button" id="btn_save"> Save</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    {{-- modal add color --}}
    <div class="modal fade" id="Add_color" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add color</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{ route('add.color') }}" method="POST">
                        @csrf
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Color name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="name" id="inputEmail3" placeholder="Color name" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Color</label>
                          <div class="col-sm-9">
                            <input type="color" class="form-control" name="code" id="inputPassword3" placeholder="" required>
                          </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary text-right">Save</button>
                        </div>
                      </form>
                </div>
                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
    @section('script')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">
        // var loadFile = function(event){
        //     var output = document.getElementById('output');
        //     output.src = URL.createObjectURL(event.target.files[0]);
        // }
        $(document).ready(function() {
            $('.my-select').selectpicker();
            $('#colors').change(function(e) {
                update_sku();
            });

            function add_more_choice_option(i, name) {
                $('#choice_options').append(
                    '<div class="row col-sm-12 field"><div class="col-sm-2"><label for="" class="col-form-label">' +
                    name +
                    '</label><input type="hidden" name="choice_no[]" value="' + i +
                    '"><input type="hidden" name="choice[]" value="' + name +
                    '"></div><div class="col-sm-8"><input name="choice_options_' + i +
                    '[]" id="idInput' + i + '" class="tags" placeholder="Enter ' + name +
                    ' values" autofocus></div></div>');

                $('[class=tags]').tagify();
                var input = document.querySelector('input[class=tags]'),
                    tagify = new Tagify(input);
                $('#idInput' + i).on('change', function(e) {
                    update_sku();
                });
            }

            $(document).on('change', '#image', function() {
                var property = document.getElementById('image').files[0];
                var img_name = property.name;
                var img_extension = img_name.split('.').pop().toLowerCase();
                var img_preview = $('#image-preview');
                if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg', 'gif']) == -1) {
                    img_preview.html('<span style="color: red;">This not support file reader</span>');
                    return false;
                } else {
                    var img_size = property.size;
                    if (img_size > 2000000) {
                        img_preview.html('<span style="color: red;">This file is to big</span>');
                        return false;
                    } else {
                        if (typeof(FileReader) != 'undefined') {
                            img_preview.empty();
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('<img/>', {
                                    'src': e.target.result,
                                    'class': 'img-fluid',
                                    'style': 'max-height: 80px; max-width: 80px;'
                                }).appendTo(img_preview);
                            }
                            img_preview.show();
                            reader.readAsDataURL($(this)[0].files[0]);
                        } else {
                            img_preview.html(
                            '<span style="color:red;">This not support file reader</span>');
                        }
                    }
                }

            });

            $(document).on('change', '#pdf', function() {
                var property = document.getElementById('pdf').files[0];
                var img_name = property.name;
                var img_extension = img_name.split('.').pop().toLowerCase();
                var img_preview = $('#pdf-preview');
                if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg', 'gif']) == -1) {
                    img_preview.html('<span style="color: red;">This not support file reader</span>');
                    return false;
                } else {
                        if (typeof(FileReader) != 'undefined') {
                            img_preview.empty();
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('<img/>', {
                                    'src': e.target.result,
                                    'class': 'img-fluid',
                                    'style': 'max-height: 80px; max-width: 80px;'
                                }).appendTo(img_preview);
                            }
                            img_preview.show();
                            reader.readAsDataURL($(this)[0].files[0]);
                        } else {
                            img_preview.html(
                            '<span style="color:red;">This not support file reader</span>');
                        }
                    }

            });

            $(document).on('change', '#thumbnail', function() {
                var totalfiles = document.getElementById('thumbnail').files.length;
                for (var index = 0; index < totalfiles; index++) {
                    var property = document.getElementById('thumbnail').files[index];
                    var img_name = property.name;
                    var img_extension = img_name.split('.').pop().toLowerCase();
                    var img_preview = $('#thumbnail-preview');
                    if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg']) == -1) {
                        img_preview.html('<span style="color: red;">This not support file reader</span>');
                        return false;
                    } else {
                        var img_size = property.size;
                        if (img_size > 2000000) {
                            img_preview.html('<span style="color: red;">This file is to big</span>');
                            return false;
                        } else {
                            if (typeof(FileReader) != 'undefined') {
                                img_preview.empty();
                                var reader = new FileReader();
                                reader.onload = function(e) {
                                    $('<img/>', {
                                        'src': e.target.result,
                                        'style': 'max-height: 80px; max-width: 80px;'
                                    }).appendTo(img_preview);
                                }
                                img_preview.show();
                                reader.readAsDataURL($(this)[0].files[index]);
                            } else {
                                img_preview.html(
                                    '<span style="color:red;">This not support file reader</span>');
                            }
                        }
                    }
                }

            });

            function update_sku() {
                $.ajax({
                    type: "POST",
                    url: '{{ route('products.sku_combination') }}',
                    data: $('#choice_form').serialize(),
                    success: function(data) {

                        $('#sku_combination').html(data);

                        if (data.length > 1) {
                            $('#quantity').hide();
                        } else {
                            $('#quantity').show();
                        }
                    }
                });
            }


            $('input[name="unit_price"]').on('input', function() {
                update_sku();
            });

            $('#attributes').change(function(e) {
                $('#choice_options').html(null);
                $.each($("#attributes option:selected"), function() {
                    add_more_choice_option($(this).val(), $(this).text());

                });
                update_sku();
            });

            $('#btn_delete').click(function(e) {
                e.preventDefault();
                alert('vong');
            });

            $('#add_color').click(function(e) {
                e.preventDefault();
                $('#Add_color').modal('show');
            })
        });
    </script>
@endsection
