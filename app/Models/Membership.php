<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    use HasFactory;
    protected $table    = 'memberships';
    protected $fillable = [
        'name',
    ];

    public function membership_withdraw_request()
    {
        return $this->hasMany("App\Models\WithdrawRequest", "membership_id", "id");
    }
}
