<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Seller;
use App\Models\Upload;
use App\Models\Customer;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function update(Request $request){
        $user = User::find($request->user_id);
        $data = ([
            'name' => $request->name,
            'surname' => $request->surname,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'date_of_birth' => $request->date_of_birth
        ]);
        $user->update($data);

        $seller = Seller::where('user_id', $request->user_id)->first();
        $img = $request->file('card_img');
        if($img != null){
            $name_img = hexdec(uniqid());
            $img_ext = strtolower($img->extension());
            $img_new_name = $name_img.'.'.$img_ext;
            $upload_path = 'uploads/card/';
            $card_img = $upload_path.$img_new_name;

            $img->move($upload_path, $img_new_name);
            $seller->card_img = $card_img;
        }
        $seller->card_id = $request->card_id;
        $seller->ss_id = $request->ss_id;
        $seller->bank_name = $request->bank_name;
        $seller->bank_acc_name = $request->bank_acc_name;
        $seller->bank_acc_no = $request->bank_acc_number;
        if($seller->update()){
            $notification = array(
                'message' => 'Update your accout successfully.',
                'alert-type' => 'success'
            );
            
        }else{
            $notification = array(
                'message' => 'Update info samething warning.',
                'alert-type' => 'warning'
            );
        }
        return redirect()->back()->with($notification);
    }

    public function update_profile(Request $request)
    {
        $user = User::find($request->user_id);

        $upload_path = 'uploads/user_img/';
        $img = $request->file('photo');
        $name_img = hexdec(uniqid());
        $img_ext = strtolower($img->extension());
        $img_new_name = $name_img.'.'.$img_ext;
        $photo = $upload_path.$img_new_name;
        $img->move($upload_path, $img_new_name);

        if($user->photo){
            $upload = Upload::find($user->photo);
            
            $upload->file_name = $photo;
            $upload->type = 'image';
            $upload->extension = $img_ext;
            $upload->save();
            $user->photo = $upload->id;
        }else{
            $upload = new Upload;
            $upload->file_name = $photo;
            $upload->type = 'image';
            $upload->extension = $img_ext;
            $upload->save();
            $user->photo = $upload->id;
        }

        if($user->update()){
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            
        }else{
            $notification = array(
                'message' => 'Update info samething warning.',
                'alert-type' => 'warning'
            );
        }
        return redirect()->back()->with($notification);
    }

    public function update_name(Request $request)
    {
        $user = User::find($request->user_id);
        $user->name = $request->name;
        $user->surname = $request->surname;
        if($user->update()){
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            
        }else{
            $notification = array(
                'message' => 'Update info samething warning.',
                'alert-type' => 'warning'
            );
        }
        return redirect()->back()->with($notification);

    }

    public function update_email(Request $request)
    {
        $user = User::find($request->user_id);
        $user->email = $request->email;
        if($user->update()){
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
        }else{
            $notification = array(
                'message' => 'Update info samething warning.',
                'alert-type' => 'warning'
            );
        }
        return redirect()->back()->with($notification);
    }

    public function update_phone(Request $request)
    {
        $user = User::find($request->user_id);
        $user->phone = $request->phone;
        if($user->update()){
            $notification = array(
                'message' => 'Updata successfully',
                'alert-type' => 'success'
            );
        }else{
            $notification = array(
                'message' => 'Update info samething warning.',
                'alert-type' => 'warning'
            );
        }
        return back()->with($notification);
    }

    public function update_payment(Request $request)
    {
        if($request->user_type == 'sale'){
            $data = Seller::find($request->seller_id);
            $data->bank_name = $request->bank_name;
            $data->bank_acc_name = $request->bank_acc_name;
            $data->bank_acc_no = $request->bank_acc_no;
            if($data->update()){
                $notification = array(
                    'message' => 'Update successfully.',
                    'alert-type' => 'success'
                );
            }else{
                $notification = array(
                    'message' => 'Update info samething warning.',
                    'alert-type' => 'warning'
                );
            }
            return back()->with($notification);
        }elseif($request->user_type == 'buy'){
            $data = Customer::find($request->seller_id);
            $data->bank_name = $request->bank_name;
            $data->bank_acc_name = $request->bank_acc_name;
            $data->bank_acc_no = $request->bank_acc_no;
            if($data->update()){
                $notification = array(
                    'message' => 'Update successfully.',
                    'alert-type' => 'success'
                );
            }else{
                $notification = array(
                    'message' => 'Update info samething warning.',
                    'alert-type' => 'warning'
                );
            }
            return back()->with($notification);
        }
        
        
    }
}
