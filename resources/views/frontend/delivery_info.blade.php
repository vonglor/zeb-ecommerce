@extends('frontend.layouts.app')

@section('css')
    <link href="{{ asset('frontend/css/cart.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Cart</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cart" class="clearfix">
        <div class="container">
            <div class="row">
                @php
                    $admin_products = array();
                    $seller_products = array();
                    $cart = Session::get('cart');
                    foreach ($cart as $key => $cartItem) {
                        if(App\Models\Product::find(App\Models\Product_stock::find($cartItem['id'])->product_id)->added_by == 'admin'){
                                array_push($admin_products, $cartItem['id']);
                            }
                            else{
                                $product_ids = array();
                                if(array_key_exists(\App\Models\Product::find(App\Models\Product_stock::find($cartItem['id'])->product_id)->user_id, $seller_products)){
                                    $product_ids = $seller_products[\App\Models\Product::find(App\Models\Product_stock::find($cartItem['id'])->product_id)->user_id];
                                }
                                array_push($product_ids, $cartItem['id']);
                                $seller_products[\App\Models\Product::find(App\Models\Product_stock::find($cartItem['id'])->product_id)->user_id] = $product_ids;
                            }
                    }
                @endphp
    
                @foreach ($seller_products as $key => $seller_product)
                <div class="cart_2 clearfix delivery" style="margin-bottom: 15px;">
                        <div class="col-sm-12">
                            <h5 class="fs-16 fw-600 mb-0">{{ \App\Models\Shop::where('user_id', $key)->first()->name }} Products</h5><hr>
                            @foreach ($seller_product as $cartItem)
                            @php
                                $product = \App\Models\Product_stock::find($cartItem);
                            @endphp
                            <div class="clearfix" style="margin-top: 15px;">
                                <span class="mr-2">
                                @if ($product->image)
                                <img
                                    src="{{ url(App\Models\Upload::find($product->image)->file_name) }}"
                                    class="img-fit size-60px rounded"
                                    alt=""
                                    style="width:60px; height:80px; float:left; margin-right:10px;"
                                >
                                @else
                                <img
                                    src="{{ url(App\Models\Upload::find(App\Models\Product::find($product->product_id)->photos)->file_name) }}"
                                    class="img-fit size-60px rounded"
                                    alt=""
                                    style="width:60px; height:80px; float:left; margin-right:10px;"
                                >
                                @endif
                                    
                                </span>
                                <span class="fs-14 opacity-60">{{ $product->Product->name.'   '.$product->variant }}</span>
                            </div>
                            @endforeach
                        <hr>
                        </div>
                        <div class="cart_4 text-right   clearfix">
                            <h5><a class="button" href="{{ route('checkout.store_delivery_info', $key) }}">Continue to Payment</a></h5>
                        </div>
                </div>
                @endforeach
                
            </div>
        </div>
    </section>
@endsection
<script src=""></script>
