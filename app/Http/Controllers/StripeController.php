<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Models\address;
use App\Http\Controllers\OrderController;

class StripeController extends Controller
{

    public function stripePost(Request $request)
    {
        $stripe = Stripe::setApiKey(env('STRIPE_SECRET'));
        try {

            $token = $stripe->tokens()->create([
                'card' => [
                    'number' => $request->card_number,
                    'exp_mounth' => $request->expire,
                    'cvc' => $request->cvc
                ]
            ]);

            $customer = $stripe->customer()->create([
                'name' => 'vonglor',
                'email' => 'vonglor@gmail.com',
                'phone' => '77892881'
            ]);

            $charge = $stripe->charge()->create([
                "amount" => $request->get('amount'),
                "currency" => "usd",
                "description" => "Test payment from itsolutionstuff."
            ]);

        } catch (\Throwable $th) {
            echo 'errors';
        }
          
        return back();
    }

    public function stripe(Request $request)
    {   
        // Enter Your Stripe Secret
        \Stripe\Stripe::setApiKey('sk_test_51Jd4FUC9jbftbmhrPKaDWifQHxhb2twMVz11D0KHzedjIyzVpojaKk1FHkxDXbLcPWygtvPfhWOnvOA8BICyMZme00WZ4ZfUXC');
        $user = Session::get('user');
        $address_id = $request->address_id;
        $total = $request->total;
		$amount = $request->total;
		$amount *= 100;
        $amount = (int) $amount;
        $customer = \Stripe\Customer::create([
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
        ]);

        $payment_intent = \Stripe\PaymentIntent::create([
			'description' => 'Stripe Test Payment',
			'amount' => $amount,
			'currency' => 'USD',
			'description' => 'Payment From Codehunger',
			'payment_method_types' => ['card'],
		]);
		$intent = $payment_intent->client_secret;

		return view('frontend.payments.stripe',compact('intent', 'total', 'address_id'));
    }

    public function afterPayment(Request $request)
    {
        $orderController = new OrderController;
        $orderController->store($request);
        $request->session()->put('cart', Session::get('cart')->where('owner_id', '!=', Session::get('owner_id')));
        $request->session()->forget('owner_id');
        return redirect()->route('order_confirmed');
    }

    
}
