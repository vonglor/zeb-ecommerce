@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h1 class="text-lg font-medium mr-auto">
                Edit message to customer
            </h1>
        </div>
        <div class="grid grid-cols-6 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
            <form action="{{route('update.message', $message->id)}}" method="POST" id="tnc-form">
                @csrf
                 @method('PUT')
                <div class="form-group">
                    <label for="min_qty" class="form-label text-base">Title</label>
                    <input type="text" id="title" name="title" class="form-control border-gray-500 text-base" value="{{$message->title}}" placeholder="Title" required>
                </div>
                <div class="form-group mt-4">
                    <label for="min_qty" class="form-label text-base">Message</label>
                    <textarea class=" form-control text-base" name="message" id="message">{{old('message',$message->message)}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary text-base">Edit Send customer</button>
            </form>
            </div>
        </div>
    </div>
<!-- END: Content -->
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });
    </script>
    
@endsection

