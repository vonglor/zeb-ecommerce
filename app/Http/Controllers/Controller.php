<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Exchangerate;
use App\Models\BusinessSetting;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    public function convert_prices($price, $cur_code)
    {
        $business_settings = BusinessSetting::where('type', 'system_default_currency')->first();
        // $currency_from = Currency::where('code', $cur_code)->first();
        // if($business_settings != null){
        //     $currency_to = Currency::find($business_settings->value);
        //     $exchange = Exchangerate::where('from', $cur_code)
        //                 ->where('to', $currency_to->code)
        //                 ->first();
        //     dd($exchange->exchange);
        //         $price = floatval($price) / floatval($exchange->exchange);    
        // }

        $code = Currency::findOrFail(BusinessSetting::where('type', 'system_default_currency')->first()->value)->code;
        if(Session::has('currency_code')){
            $currency_to = Currency::where('code', Session::get('currency_code', $code))->first();
        }
        else{
            $currency_to = Currency::where('code', $code)->first();
        }
        $exchange = Exchangerate::where('to', $currency_to->code)
                    ->first();
        $prices = ($price).($exchange->operator).($exchange->exchange);
           // $cur_code = $exc->exchange;
        return $prices;
    }
}
