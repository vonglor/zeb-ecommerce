<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'card_id',
        'card_img',
        'bank_name',
        'bank_acc_no',
    ];
    /**
     * Get the user that owns the Customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function User()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function withdraw_request()
    {
        return $this->hasMany("App\Models\WithdrawRequest", "user_id", "user_id");
    }

    public function order()
    {
        return $this->hasMany("App\Models\Order", "user_id", "user_id");
    }
}
