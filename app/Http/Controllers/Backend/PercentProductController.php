<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\PercentProduct;
use Illuminate\Http\Request;

class PercentProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        // $percentproduct = PercentProduct::paginate(10);
        $percentproduct = PercentProduct::where('name','percent_buy')->get();
        return view('backend.settings.productpercent.percentbuy',compact('percentproduct'));
    }
    public function percentsale()
    {
       
        // $percentsale = PercentProduct::paginate(10);
        $percentsale = PercentProduct::where('name','percent_sale')->get();
        return view('backend.settings.productpercent.percentsale',compact('percentsale'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $percentproduct = $request->all();

        // dd( $percentproduct);

        PercentProduct::create($percentproduct);

        if ($percentproduct) {
            $notification = array(
                'message' => 'Insert successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('percentproduct')->with($notification);
        } else {
            $notification = array(
                'message' => 'Insert warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('percentproduct')->with($notification);
        }
    }

    public function addpercentsale(Request $request)
    {
        $percentsale = $request->all();

        PercentProduct::create($percentsale);

        if ($percentsale) {
            $notification = array(
                'message' => 'Insert successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('percentsale')->with($notification);
        } else {
            $notification = array(
                'message' => 'Insert warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('percentsale')->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PercentProduct  $percentProduct
     * @return \Illuminate\Http\Response
     */
    public function show(PercentProduct $percentProduct)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PercentProduct  $percentProduct
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $editpercentbuy = PercentProduct::find($id);
        return view('backend.settings.productpercent.editbuy',compact('editpercentbuy'));
    }
    public function editsale($id)
    {
       
        $editpercentsale = PercentProduct::find($id);
        return view('backend.settings.productpercent.editsale',compact('editpercentsale'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PercentProduct  $percentProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $percentproduct = PercentProduct::find($id);
        $percentproduct->start_amount = $request->input('start_amount');
        $percentproduct->end_amount = $request->input('end_amount');
        $percentproduct->name = $request->input('name');
        $percentproduct->percent = $request->input('percent');
        $percentproduct->update();

        if ($percentproduct) {
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('percentproduct')->with($notification);
        } else {
            $notification = array(
                'message' => 'Update warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('percentproduct')->with($notification);
        }
    }

    public function updatepercent(Request $request, $id)
    {

        $percentsale = PercentProduct::find($id);
        $percentsale->start_amount = $request->input('start_amount');
        $percentsale->end_amount = $request->input('end_amount');
        $percentsale->name = $request->input('name');
        $percentsale->percent = $request->input('percent');
        $percentsale->update();

        if ($percentsale) {
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('percentsale')->with($notification);
        } else {
            $notification = array(
                'message' => 'Update warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('percentsale')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PercentProduct  $percentProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // dd('delete');
        $percentproduct = PercentProduct::find($id);
        $percentproduct->delete();

        if ($percentproduct) {
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('percentproduct')->with($notification);
        } else {
            $notification = array(
                'message' => 'Delete warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('percentproduct')->with($notification);
        }
    }
    public function deletePercentSale($id)
    {

        // dd('delete');
        $percentproduct = PercentProduct::find($id);
        $percentproduct->delete();

        if ($percentproduct) {
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('percentproduct')->with($notification);
        } else {
            $notification = array(
                'message' => 'Delete warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('percentproduct')->with($notification);
        }
    }
}
