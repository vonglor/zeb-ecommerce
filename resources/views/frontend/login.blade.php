@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/login.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Login</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="login" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="login_1 clearfix">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4 login_1c">

                        @if (Session::get('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        
                        <form action="{{ route('user.login') }}" method="POST">
                        @csrf
                            <div class="login_1l clearfix">
                                <h4 class="mgt">Login to your account</h4>
                            </div><br>
                            <div class="login_1l1 clearfix">
                                <div class="space_left">
                                    <h5>Your Id <span class="col_3">*</span></h5>
                                    <input class="form-control" name="user_id" type="text" required>
                                </div>
                                <div class="space_left">
                                    <h5>Email <span class="col_3">*</span></h5>
                                    <input class="form-control" name="email" type="email" required>
                                </div>
                                <div class="space_left">
                                    <h5>Password <span class="col_3">*</span></h5>
                                    <input class="form-control" name="password" type="password" required>
                                </div>
                            </div><br>
                            <div class="login_1l1 clearfix">
                                <button type="submit" class="btn-login"> LOGIN</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

<script src=""></script>