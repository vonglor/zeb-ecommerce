<div class="grid grid-cols-12 gap-5 mt-5">
  @if($products->count() > 0)
  @foreach ($products as $row)
  <div class="col-span-12 sm:col-span-4 xxl:col-span-3 box p-5 cursor-pointer zoom-in">
      @if ($row->image !== null)
      <img src="{{ url(App\Models\Upload::find($row->image)->file_name) }}" class="img-responsive" alt="Responsive image" width="20%">
      @else
      <img src="{{ url($row->Upload->file_name) }}" class="img-responsive" alt="Responsive image" width="20%">
  @endif
  <div class="font-medium text-base">{{ Str::limit($row->name, 12) }}</div>
      <div class="text-gray-600">${{ number_format(($row->price - $row->discount), 2) }} </div>
      <a href="{{ route('pos-sale.add_to_cart', $row->id) }}" class="btn btn-primary btn-sm" style="width: 100%"> {{__('Add to cart')}}</a>
  </div>
  @endforeach
  @else
  <div class="col-span-12 sm:col-span-12 xxl:col-span-3 box p-5 cursor-pointer zoom-in">
      <div class="font-medium text-base" style="color: red">{{__('No products found')}}</div>
  </div>
  @endif
</div>
</div>
<br>
<div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
  <ul class="pagination">
          {!! $products->links() !!}
  </ul>
</div>