<?php

namespace App\Http\Controllers\frontend;

use App\Models\Message;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        $message = Message::all();
        return view('frontend.saller.message.index', \compact('message'));
    }

    public function message()
    {
        return view('frontend.saller.message.create');
    
    }

    public function store(Request $request)
    {
        $message = new Message;
        $user_id = session()->get('user')->id;
        $message->user_id = $user_id;
        $message->message = $request->message;
        
        if($message->save()){
            $notification = array(
                'message' => 'Sent message successfully.',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        }else{
            $notification = array(
                'message' => 'Someting is waring...',
                'alert-type' => 'waring'
            );
            return redirect()->back()->with($notification);
        }
    }
}
