@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <div class="col-sm-9">
        <div class="centent_card_1r clearfix">
            <div class="centent_card_1r3 tab-content">
                <p>Payment history</p>
                <hr>
                <div class="card-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>#</th>
                                <th>Seller name</th>
                             
                                <th>Amount</th>
                                <th>Payment method</th>
                                <th>Detial</th>
                                <th class="text-right">Options</th>
                            </tr>
                           @php
                             $i=1;

                           @endphp
                           @foreach($pay as $payment)
                 
                        <tr>

                        <td scope="row">{{ $i }}</td>

                        <td>{{App\Models\User::find(App\Models\Seller::find($payment->seller_id)->user_id)->name}}</td>

                                 <td>{{$payment->amount}}</td>
                                 <td>{{$payment->payment_method}}</td>
                                 <td>{{$payment->payment_details}}</td>
                               
                                
                                 <td>
                                    <button type="submit" id="btn_delete" class="btn btn-danger btn-xs btn_delete float-left"><i class="fa fa-trash-o"></i></button>
                             
                            </td>
                        </tr>
                        @php
                        $i++;
                        @endphp
                      @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="center_product_1r4 clearfix">
                <div class="col-sm-6">
                    <div class="center_product_1r4l clearfix">
                     
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="center_product_1r4r text-left clearfix">
                    <ul class="pagination mgt">
                        <li class="disabled"><a href="#">«</a></li>
                        <li class="active"><a href="#l">1 <span
                                    class="sr-only">(current)</span></a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    {{-- moal detail --}}
    <div class="modal fade" id="order_detail_modal" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div id="order-details-modal-body">

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.btn-veiw-detail').click(function(e) {
                e.preventDefault();
                var order_id = $(this).attr('id');
                $.ajax({
                    method: "get",
                    url: "/order/order_detail/"+order_id,
                    success: function (data) {
                        $('#order-details-modal-body').html(data);
                        $('#order_detail_modal').modal('show');
                    }
                });
            });
        });
    </script>
@endsection
