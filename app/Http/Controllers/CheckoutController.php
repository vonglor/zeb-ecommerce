<?php

namespace App\Http\Controllers;

use Session;
use App\Models\Order;
use App\Models\Wallet;
use App\Models\Order_detail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\OrderController;

class CheckoutController extends Controller
{
    public function shipping()
    {
        return view('frontend.shipping');
    }
    public function delivery_info()
    {
        return view('frontend.delivery_info');
    }

    public function store_delivery_info($owner_id)
    {
        $owner_id = session()->put('owner_id', $owner_id);
        return view('frontend.payment_select', \compact('owner_id'));
    }
    public function checkout(Request $request)
    {
        $request->validate([
            'address_id' => 'required',
            'payment_type' => 'required'
        ]);
        if ($request->payment_type != null) {
            // $orderController = new OrderController;
            // $orderController->store($request);
            $request->session()->put('payment_type', 'cart_payment');
            // if ($request->session()->get('order_id') != null) {

                if ($request->payment_type == 'paypal') {
                    // store to table orders
                    $orderController = new OrderController;
                    $orderController->store($request);
                    
                    $paypal = new PaypalController;
                    return $paypal->index($request);

                } elseif ($request->payment_type == 'stripe') {
                    $stripe = new StripeController;
                    return $stripe->stripe($request);

                } elseif ($request->payment_type == 'cash') {
                    $orderController = new OrderController;
                    $orderController->store($request);
                    $request->session()->put('cart', Session::get('cart')->where('owner_id', '!=', Session::get('owner_id')));
                    $request->session()->forget('owner_id');
                    echo '<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">';

                    echo '<script>
                                setTimeout(function() {
                                swal({
                                    title: "Order successfully",
                                    text: "Please wait for verification from the store",
                                    type: "success",
                                    button: "Aww yiss!"
                                }, function() {
                                    window.location = "/order/confirmed";
                                });
                            }, 1000);
                        </script>';
                    // return redirect()->route('order_confirmed');
                } elseif ($request->payment_type == 'wallet') {
                    $user = Session::get('user');
                    $wallet = Wallet::where('user_id', $user->id)->first();
                    // dd($request->all());
                    $orderController = new OrderController;
                    $orderController->store($request);
                    $request->session()->put('cart', Session::get('cart')->where('owner_id', '!=', Session::get('owner_id')));
                    $request->session()->forget('owner_id');

                    $order = Order::findOrFail($request->session()->get('order_id'));
                    if ($wallet->amount >= $order->grand_total) {
                        $wallet->amount -= $order->grand_total;
                        $wallet->save();
                        echo '<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">';

                    echo '<script>
                                setTimeout(function() {
                                swal({
                                    title: "Order successfully",
                                    text: "Please wait for verification from the store",
                                    type: "success",
                                    button: "Aww yiss!"
                                }, function() {
                                    window.location = "/order/confirmed";
                                });
                            }, 1000);
                        </script>';
                    }
                    
                } else {
                    $order = Order::findOrFail($request->session()->get('order_id'));
                    $order->manual_payment = 1;
                    $order->save();

                    $request->session()->put('cart', Session::get('cart')->where('owner_id', '!=', Session::get('owner_id')));
                    $request->session()->forget('owner_id');

                    // flash(translate('Your order has been placed successfully. Please submit payment information from purchase history'))->success();
                    return redirect()->route('order_confirmed');
                }
        } else {
            // flash(translate('Select Payment Option.'))->warning();
            return back();
        }
    }

    
    public function order_confirmed()
    {
        $order = Order::findOrFail(Session::get('order_id'));
        
        return view('frontend.order_confirmed', \compact('order'));

    }
}
