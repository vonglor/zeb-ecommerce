
@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-xl font-medium mr-auto text-xl">
                Update Membership Fee
            </h2>
        </div>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
                <!-- BEGIN: Vertical Form -->
                <form action="{{route('update/customer/memberfee', $membership->id)}}" method="POST">
                    @csrf
                <div class="intro-y box">
                    <div id="vertical-form" class="p-5">
                        <div class="preview">
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Membership Fee / month</label>
                                <input id="vertical-form-1" type="number" name="name" value="{{$membership->name}}" class="form-control border-2 border-gray-500 text-base" placeholder="0" required>
                            </div>
                            <div class="mt-3">
                                <button type="submit" class="btn btn-primary mt-5 text-base">Edit</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            </div>
        </div>
    </div>
<!-- END: Content -->
    
@endsection