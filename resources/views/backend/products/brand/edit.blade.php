@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
    <div class="content">
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto text-xl">
            Edit Brand
        </h2>
    </div>
    <div class="grid grid-cols-6 gap-12 mt-5">
        <div class="intro-y col-span-12 lg:col-span-6">
            <form action="{{route('brand.update', $brand->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="intro-y box">
                    <div id="vertical-form" class="p-5">
                        <div class="preview">
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Name</label>
                                <input id="vertical-form-1" type="text" class="form-control border-gray-500 text-base" name="name" value="{{$brand->name}}" placeholder="Brand Name" required>
                                @error('name')
                                <span style="color: red;">{{ $message }}</span>
                                @enderror
                            </div>
                            <div>
                                <strong>Logo</strong>
                                <input type="file" name="logo" class="form-control" placeholder="Logo">
                                <img src="/uploads/images/{{ $brand->logo }}" width="150px">
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Slug</label>
                                <input id="vertical-form-1" type="text" class="form-control border-gray-500 text-base" name="slug" value="{{$brand->slug}}" placeholder="Slug">
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Meta Title</label>
                                <input id="vertical-form-1" type="text" class="form-control border-gray-500 text-base" name="meta_title" value="{{$brand->meta_title}}" placeholder="Meta Title">
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Meta Description</label>
                                <textarea id="vertical-form-1" type="text" name="meta_description" class="form-control border-gray-500 text-base" placeholder="meta_description" rows="4" cols="50">{{$brand->meta_description}}</textarea>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary mt-5 text-base">Edit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END: Content -->
    
@endsection