@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <form action="{{ route('product.update_product') }}" method="POST" enctype="multipart/form-data" id="choice_form">
        @csrf
        <div class="col-sm-9">
            <p class="title_header">Edit Your Products</p>
            <div class="centent_card_1r clearfix">
                <div class="centent_card_1r3 tab-content">
                    <p>Product Information</p>
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Category" class="col-form-label">Category</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="category_id" class="form-control" required>
                                        @foreach (App\Models\Category::all() as $item)
                                            <option value="{{ $item->id }}" @if ($product->category_id == $item->id) selected @endif>{{ $item->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Brands" class="col-form-label">Brands</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="brand_id" class="form-control">
                                        @if ($product->brand_id)
                                            <option value="{{ $product->brand_id }}">{{ $product->Brand->name }}
                                            </option>
                                        @else
                                            <option value="">Select Brand</option>
                                        @endif
                                        @foreach (App\Models\Brand::all() as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Product name</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="name" rows="5" class="form-control" placeholder="Product name" required>{{ $product->name }}</textarea>
                                    {{-- <input type="text" class="form-control" name="name" placeholder="Product Name"
                                        value="{{ $product->name }}" required> --}}
                                    @error('name')
                                        <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                </div>
                                <div id="demo">

                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="unit" class="col-form-label">Unit</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="unit" name="unit" class="form-control"
                                        placeholder="Unit (e.g.KG, Pc etc)" value="{{ $product->unit }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="centent_card_1r3 tab-content">
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Colors" class="col-form-label">Colors</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="colors[]" id="colors" class="form-control my-select selectpicker"
                                        data-live-search="true" multiple data-selected-text-format="count">
                                        @foreach (\App\Models\Color::orderBy('name', 'asc')->get() as $key => $color)
                                            <option value="{{ $color->name }}"  data-content="<span><span style='background:{{ $color->code }}; border: 1px solid #000; padding: 0 8px;'> </span><span> {{ $color->name }}</span></span>"
                                                <?php if (in_array($color->name, json_decode($product->colors))) {
                                                    echo 'selected';
                                                } ?>></option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="col-sm-2">
                                    <a href="" id="add_color">Add color</a>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Attributes" class="col-form-label">Attributes</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="attributes[]" id="attributes" class="form-control my-select selectpicker"
                                        multiple data-selected-text-format="count">
                                        @foreach (\App\Models\Attributes::all() as $key => $attribute)
                                            <option value="{{ $attribute->id }}" @if ($product->attributes != null && in_array($attribute->id, json_decode($product->attributes, true))) selected @endif>
                                                {{ $attribute->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div id="choice_options">
                                @foreach (json_decode($product->choice_options) as $key => $choice_option)
                                    <div class="row col-sm-12 field">
                                        <div class="col-sm-2"><label for=""
                                                class="col-form-label">{{ \App\Models\Attributes::find($choice_option->attribute_id)->name }}</label>
                                            <input type="hidden" name="choice_no[]"
                                                value="{{ $choice_option->attribute_id }}">
                                            <input type="hidden" name="choice[]"
                                                value="{{ \App\Models\Attributes::find($choice_option->attribute_id)->name }}">
                                        </div>
                                        <div class="col-sm-8">
                                            <input name="choice_options_{{ $choice_option->attribute_id }}[]"
                                                id="idInput{{ $choice_option->attribute_id }}" class="tags"
                                                placeholder="Enter values" autofocus
                                                value="{{ implode(',', $choice_option->values) }}"
                                                data-on-change="update_sku">
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
                <div class="centent_card_1r3 tab-content">
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="card-content">
                                <div id="sku_combination">
                                    @php
                                        $productStock = App\Models\Product_stock::where('product_id', $product->id)->get();
                                    @endphp
                                    @if ($productStock)
                                        <table class="table table-bordered" id="example">
                                            <input type="hidden" name="product_stock" value="old">
                                            <thead>
                                                <tr>
                                                    <th style="width: 30%">Variant</th>
                                                    <th>Variant Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($productStock as $pro_stock)
                                                    <tr class="variant_{{ $pro_stock->variant }}">
                                                        <td><label for="" class="control-label">
                                                                <span class="{{ $pro_stock->variant }}">
                                                                    <i class="fa fa-plus-circle text-primary"
                                                                        id="icon_{{ $pro_stock->variant }}"
                                                                        aria-hidden="true"></i>
                                                                </span> {{ $pro_stock->variant }}</label>
                                                        </td>
                                                        <td>
                                                            <input type="number" name="price_{{ $pro_stock->variant }}"
                                                                value="{{ $pro_stock->price }}" class="form-control"
                                                                placeholder="0">
                                                            <input type="hidden" name="prs_id" value="{{ $pro_stock->id }}">
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-sm btn-danger"
                                                                id="{{ $pro_stock->variant }}"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                            <tbody class="table_middle_{{ $pro_stock->variant }} child_row" id="table_middle">
                                                <tr>
                                                    <td style="text-align: right"><label for=""
                                                            class="control-label">Quantity</label>
                                                    </td>
                                                    <td colspan="2"><input type="number" name="qty_{{ $pro_stock->variant }}"
                                                            value="{{ $pro_stock->qty }}" class="form-control"
                                                            placeholder="0"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right"><label for=""
                                                            class="control-label">Photo</label>
                                                    </td>
                                                    <td colspan="2">
                                                        <input type="file" name="img_{{ $pro_stock->variant }}" accept="image/*"
                                                            id="img_{{ $pro_stock->variant }}" class="form-control" value="">
                                                        <div class="image-preview" id="show_img_{{ $pro_stock->variant }}">
                                                            @if ($pro_stock->image != null)
                                                                <img src="{{ url(\App\Models\Upload::find($pro_stock->image)->file_name) }}"
                                                                    alt="" style="max-height: 80px; max-width: 80px; margin-top: 10px;">
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                    @endforeach
        
                                    </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                            @php
                                $pro = App\Models\Product_stock::where('product_id', $product->id)->first();
                            @endphp
                            @if ($pro->variant == null)
                                <div class="row col-sm-12 field" id="quantity">
                                    <div class="col-sm-2">
                                        <label for="Quantity" class="col-form-label">Total</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="number" name="quantity" class="form-control"
                                            value="{{ $pro->qty }}">
                                    </div>
                                </div>
                            @endif
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="min_qty" class="col-form-label">Minimum Qty</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="min_qty" name="min_qty" class="form-control"
                                        value="{{ $product->min_qty }}" required>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="unit_price" class="col-form-label">Price</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" id="unit_price" name="unit_price" class="form-control"
                                        placeholder="0" value="{{ $product->unit_price }}" required>
                                </div>
                                <div class="col-sm-2">
                                    <select name="currency_code" class="form-control" id="" required>
                                        @foreach (App\Models\Currency::all() as $item)
                                            <option value="{{ $item->code }}" @if ($product->currency_code == $item->code) selected @endif>{{ $item->code }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="purchase_price" class="col-form-label">Purchase price</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" id="purchase_price" name="purchase_price" class="form-control"
                                        placeholder="0" value="{{ $product->purchase_price }}" required>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Discount" class="col-form-label">Discount</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" id="discount" name="discount" value="{{ $product->discount }}"
                                        class="form-control" placeholder="0">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="centent_card_1r3 tab-content">
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="image" class="col-form-label">Images</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="image" name="image" accept="image/*" class="form-control">
                                    <div class="image-preview" id="image-preview">
                                        @if ($product->photos)
                                            <img src="{{ url($product->Upload->file_name) }}" alt=""
                                                style="max-height: 80px; max-width: 80px;">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="Thumbnail" class="col-form-label">Thumbnail</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="thumbnail" name="thumbnail[]" accept="image/*"
                                        class="form-control" multiple>
                                    <div class="image-preview" id="thumbnail-preview">
                                        @if ($product->thumbnail_img)
                                            @foreach (explode(',', $product->thumbnail_img) as $key => $item)
                                                <img src="{{ url(App\Models\Upload::find($item)->file_name) }}" alt=""
                                                    style="max-height: 80px; max-width: 80px;">
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="video_link" class="col-form-label">Video Link</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="video_link" name="video_link" class="form-control"
                                        placeholder="Video Link" value="{{ $product->videos }}">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="PDF" class="col-form-label">PDF</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="pdf" name="pdf" class="form-control"
                                        placeholder="PDF Specification">
                                    <div class="image-preview" id="pdf-preview">
                                        @if ($product->pdf)
                                            <img src="{{ url(App\Models\Upload::find($product->pdf)->file_name) }}" alt=""
                                                style="max-height: 80px; max-width: 80px;">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="centent_card_1r3 tab-content">
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Shipping</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" name="shipping" value="{{ $product->shipping }}" class="form-control">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Product location</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="product_location" value="{{ $product->product_location }}" placeholder="Product location" class="form-control">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Condition</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="condition" class="form-control">
                                        <option value="{{ $product->condition }}">{{ $product->condition }}</option>
                                        @if ($product->condition == 'New')
                                            <option value="Old">Old</option>
                                        @else
                                            <option value="New">New</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Delivery</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="delivery" id="" cols="30" rows="10"
                                        class="form-control">{{ $product->delivery }}</textarea>
                                </div>
                            </div>
                            
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="description" class="col-form-label">Description</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="description" id="description" cols="30" rows="10"
                                        class="form-control">{{ $product->description }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="centent_card_1r3 tab-content">
                    <p>Meta Tags</p>
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Meta Title</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="meta_title" class="form-control"
                                        value="{{ $product->meta_title }}" placeholder="Meta Title">
                                </div>
                            </div>
                            <div class="row col-sm-12 field">
                                <div class="col-sm-2">
                                    <label for="inputPassword6" class="col-form-label">Meta Description</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="meta_description" id="" cols="30" rows="10"
                                        class="form-control">{{ $product->meta_description }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="centent_card_1r3 tab-content">
                    <hr>
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            
                        <div class="col-sm-12 text-center">
                            <button class="button" id="btn_save"> Update</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    {{-- modal add color --}}
    <div class="modal fade" id="Add_color" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add color</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="{{ route('add.color') }}" method="POST">
                        @csrf
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Color name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="name" id="inputEmail3" placeholder="Color name" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Color</label>
                          <div class="col-sm-9">
                            <input type="color" class="form-control" name="code" id="inputPassword3" placeholder="" required>
                          </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary text-right">Save</button>
                        </div>
                      </form>
                </div>
                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            update_sku();
            $('.my-select').selectpicker();
            $('[class=tags]').tagify();
            $('#colors').change(function(e) {
                update_sku();
            });
            $('.tags').on('change', function(e) {
                update_sku();
            });

            function add_more_choice_option(i, name) {
                $('#choice_options').append(
                    '<div class="row col-sm-12 field"><div class="col-sm-2"><label for="" class="col-form-label">' +
                    name +
                    '</label><input type="hidden" name="choice_no[]" value="' + i +
                    '"><input type="hidden" name="choice[]" value="' + name +
                    '"></div><div class="col-sm-8"><input name="choice_options_' + i +
                    '[]" id="idInput' + i + '" class="tags" placeholder="Enter ' + name +
                    ' values" autofocus></div></div>');

                $('[class=tags]').tagify();
                var input = document.querySelector('input[class=tags]'),
                    tagify = new Tagify(input);
                $('#idInput' + i).on('change', function(e) {
                    update_sku();
                });
            }

            $(document).on('change', '#image', function() {
                var property = document.getElementById('image').files[0];
                var img_name = property.name;
                var img_extension = img_name.split('.').pop().toLowerCase();
                var img_preview = $('#image-preview');
                if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg', 'gif']) == -1) {
                    img_preview.html('<span style="color: red;">This not support file reader</span>');
                    return false;
                } else {
                    var img_size = property.size;
                    if (img_size > 2000000) {
                        img_preview.html('<span style="color: red;">This file is to big</span>');
                        return false;
                    } else {
                        if (typeof(FileReader) != 'undefined') {
                            img_preview.empty();
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('<img/>', {
                                    'src': e.target.result,
                                    'class': 'img-fluid',
                                    'style': 'max-height: 80px; max-width: 80px;'
                                }).appendTo(img_preview);
                            }
                            img_preview.show();
                            reader.readAsDataURL($(this)[0].files[0]);
                        } else {
                            img_preview.html(
                                '<span style="color:red;">This not support file reader</span>');
                        }
                    }
                }

            });

            $(document).on('change', '#thumbnail', function() {
                var totalfiles = document.getElementById('thumbnail').files.length;
                for (var index = 0; index < totalfiles; index++) {
                    var property = document.getElementById('thumbnail').files[index];
                    var img_name = property.name;
                    var img_extension = img_name.split('.').pop().toLowerCase();
                    var img_preview = $('#thumbnail-preview');
                    if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg']) == -1) {
                        img_preview.html('<span style="color: red;">This not support file reader</span>');
                        return false;
                    } else {
                        var img_size = property.size;
                        if (img_size > 2000000) {
                            img_preview.html('<span style="color: red;">This file is to big</span>');
                            return false;
                        } else {
                            if (typeof(FileReader) != 'undefined') {
                                img_preview.empty();
                                var reader = new FileReader();
                                reader.onload = function(e) {
                                    $('<img/>', {
                                        'src': e.target.result,
                                        'style': 'max-height: 80px; max-width: 80px;'
                                    }).appendTo(img_preview);
                                }
                                img_preview.show();
                                reader.readAsDataURL($(this)[0].files[index]);
                            } else {
                                img_preview.html(
                                    '<span style="color:red;">This not support file reader</span>');
                            }
                        }
                    }
                }

            });

            $(document).on('change', '#pdf', function() {
                var property = document.getElementById('pdf').files[0];
                var img_name = property.name;
                var img_extension = img_name.split('.').pop().toLowerCase();
                var img_preview = $('#pdf-preview');
                if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg', 'gif']) == -1) {
                    img_preview.html('<span style="color: red;">This not support file reader</span>');
                    return false;
                } else {
                        if (typeof(FileReader) != 'undefined') {
                            img_preview.empty();
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('<img/>', {
                                    'src': e.target.result,
                                    'class': 'img-fluid',
                                    'style': 'max-height: 80px; max-width: 80px;'
                                }).appendTo(img_preview);
                            }
                            img_preview.show();
                            reader.readAsDataURL($(this)[0].files[0]);
                        } else {
                            img_preview.html(
                            '<span style="color:red;">This not support file reader</span>');
                        }
                    }

            });

            function update_sku() {
                $.ajax({
                    type: "POST",
                    url: '{{ route('products.sku_combination_edit') }}',
                    data: $('#choice_form').serialize(),
                    success: function(data) {

                        $('#sku_combination').html(data);
                        if (data.length > 1) {
                            $('#quantity').hide();
                        } else {
                            $('#quantity').show();
                        }
                    }
                });
            }

            $('input[name="unit_price"]').on('input', function() {
                update_sku();
            });

            $('#attributes').change(function(e) {
                $.each($("#attributes option:selected"), function(j, attribute) {
                    flag = false;
                    $('input[name="choice_no[]"]').each(function(i, choice_no) {
                        if ($(attribute).val() == $(choice_no).val()) {
                            flag = true;
                        }
                    });
                    if (!flag) {
                        add_more_choice_option($(attribute).val(), $(attribute).text());
                    }
                });

                var str = @php echo $product->attributes @endphp;

                $.each(str, function(index, value) {
                    flag = false;
                    $.each($("#attributes option:selected"), function(j, attribute) {
                        if (value == $(attribute).val()) {
                            flag = true;
                        }
                    });
                    if (!flag) {
                        $('input[name="choice_no[]"][value="' + value + '"]').parent().parent()
                            .remove();
                    }
                });

                update_sku();
            });

            $('#btn_delete').click(function(e) {
                e.preventDefault();
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#example #table_middle').hide();

            $('#example tbody').on('click', 'tr td span', function() {
                var attr = $(this).attr('class');
                var oldClass = $('#icon_' + attr).attr('class');
                if (oldClass == 'fa fa-plus-circle text-primary') {
                    $('.table_middle_' + attr).show();
                    $('#icon_' + attr).removeClass("fa fa-plus-circle text-primary").addClass(
                        "fa fa-minus-circle text-danger");
                } else {
                    $('.table_middle_' + attr).hide();
                    $('#icon_' + attr).removeClass("fa fa-minus-circle text-danger").addClass(
                        "fa fa-plus-circle text-primary");
                }
            });

            $('#example tbody').on('click', 'tr td button', function() {
                var attr = $(this).attr('id');
                $('.variant_' + attr).remove();
                $('.table_middle_' + attr).hide();
            });



            $('#example tbody').on('change', 'tr td input[type="file"]', function() {
                var attr = $(this).attr('id');
                var property = document.getElementById(attr).files[0];
                var img_name = property.name;
                var img_extension = img_name.split('.').pop().toLowerCase();
                var img_preview = $('#show_' + attr);
                if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg', 'gif']) == -1) {
                    img_preview.html('<span style="color: red;">This not support file reader</span>');
                    return false;
                } else {
                    var img_size = property.size;
                    if (img_size > 2000000) {
                        img_preview.html('<span style="color: red;">This file is to big</span>');
                        return false;
                    } else {
                        if (typeof(FileReader) != 'undefined') {
                            img_preview.empty();
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('<img/>', {
                                    'src': e.target.result,
                                    'class': 'img-fluid',
                                    'style': 'max-height: 80px; max-width: 80px;'
                                }).appendTo(img_preview);
                            }
                            img_preview.show();
                            reader.readAsDataURL($(this)[0].files[0]);
                        } else {
                            img_preview.html(
                                '<span style="color:red;">This not support file reader</span>');
                        }
                    }
                }
            })

            $('#add_color').click(function(e) {
                e.preventDefault();
                $('#Add_color').modal('show');
            })

        });
    </script>
@endsection
