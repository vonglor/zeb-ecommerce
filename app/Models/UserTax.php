<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTax extends Model
{
    use HasFactory;

    public function withdraw_request()
    {
        return $this->hasMany("App\Models\WithdrawRequest", "user_id", "user_id");
    }

}