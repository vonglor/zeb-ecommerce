@php
    $user_id = Session::get('user')->id;
    $user = App\Models\User::find($user_id);
    $seller = App\Models\Seller::where('user_id', $user->id)->first();
@endphp
@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <div class="col-sm-7">
        <p class="title_header">Manage Profile</p>
        <div class="centent_card_1r clearfix">
            <div class="centent_card_1r3 tab-content">
                    <div class="card-content">
                        <h4>Shop Info</h4>
                        <hr>
                        @if($shop != null)
                        {{-- form update shop --}}
                        <form action="{{ route('shop.update') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Shop Name</label>
                                <div class="col-sm-8">
                                    <input type="hidden" name="shop_id" value="{{ $shop->id }}">
                                    <input type="text" class="form-control" value="{{ $shop->name }}" name="name" id="name"
                                        placeholder="Shop Name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Logo</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" accept="image/*" name="logo" id="logo"
                                        placeholder="Logo" >
                                        <div class="logo-preview" id="logo-preview">
                                            @if ($shop->logo)
                                                <img src="{{ url($shop->logo) }}" alt=""
                                                    style="max-height: 80px; max-width: 80px; margin-top: 10px;">
                                            @endif
                                        </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Sliders</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" accept="image/*" name="slider[]" id="slider" multiple>
                                    <div class="slider-preview" id="slider-preview">
                                        @if ($shop->sliders)
                                            @foreach (explode(',', $shop->sliders) as $key => $item)
                                                <img src="{{ url($item) }}" alt=""
                                                    style="max-height: 80px; max-width: 80px; margin-top: 10px;">
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Address</label></label>
                                <div class="col-sm-8">
                                    <textarea name="address" class="form-control">{{ $shop->address }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Facebook</label></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $shop->facebook }}" name="facebook" id="facebook"
                                        placeholder="Facebook" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Youtube</label></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ $shop->youtube }}" name="youtube" id="youtube"
                                        placeholder="Youtube" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Meta title</label></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="meta_title" value="{{ $shop->meta_title }}" id="meta_title"
                                        placeholder="Meta title" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Meta description</label></label>
                                <div class="col-sm-8">
                                    <textarea name="meta_description" class="form-control">{{ $shop->meta_description }}</textarea>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>

                        @else
                        {{-- form add shop --}}
                        <form action="{{ route('shop.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Shop Name</label>
                                <div class="col-sm-8">
                                    <input type="hidden" name="shop_id" value="">
                                    <input type="text" class="form-control" name="name" id="name"
                                        placeholder="Shop Name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Logo</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" accept="image/*" name="logo" id="logo"
                                        placeholder="Logo" >
                                        <div class="logo-preview" id="logo-preview">
                                            
                                        </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Sliders</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" accept="image/*" name="slider[]" id="slider" multiple>
                                    <div class="slider-preview" id="slider-preview">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Address</label></label>
                                <div class="col-sm-8">
                                    <textarea name="address" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Facebook</label></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="facebook" id="facebook"
                                        placeholder="Facebook" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Youtube</label></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="youtube" id="youtube"
                                        placeholder="Youtube" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Meta title</label></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="meta_title" id="meta_title"
                                        placeholder="Meta title" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-3 control-label">Meta description</label></label>
                                <div class="col-sm-8">
                                    <textarea name="meta_description" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                        @endif
                    </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $(document).on('change', '#logo', function() {
                var property = document.getElementById('logo').files[0];
                var img_name = property.name;
                var img_extension = img_name.split('.').pop().toLowerCase();
                var img_preview = $('#logo-preview');
                if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg', 'gif']) == -1) {
                    img_preview.html('<span style="color: red;">This not support file reader</span>');
                    return false;
                } else {
                    var img_size = property.size;
                    if (img_size > 2000000) {
                        img_preview.html('<span style="color: red;">This file is to big</span>');
                        return false;
                    } else {
                        if (typeof(FileReader) != 'undefined') {
                            img_preview.empty();
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('<img/>', {
                                    'src': e.target.result,
                                    'class': 'img-fluid',
                                    'style': 'max-height: 80px; max-width: 80px; margin-top: 10px;'
                                }).appendTo(img_preview);
                            }
                            img_preview.show();
                            reader.readAsDataURL($(this)[0].files[0]);
                        } else {
                            img_preview.html(
                                '<span style="color:red;">This not support file reader</span>');
                        }
                    }
                }

            });

            $(document).on('change', '#slider', function() {
                var totalfiles = document.getElementById('slider').files.length;
                for (var index = 0; index < totalfiles; index++) {
                    var property = document.getElementById('slider').files[index];
                    var img_name = property.name;
                    var img_extension = img_name.split('.').pop().toLowerCase();
                    var img_preview = $('#slider-preview');
                    if (jQuery.inArray(img_extension, ['png', 'jpeg', 'jpg']) == -1) {
                        img_preview.html('<span style="color: red;">This not support file reader</span>');
                        return false;
                    } else {
                        var img_size = property.size;
                        if (img_size > 2000000) {
                            img_preview.html('<span style="color: red;">This file is to big</span>');
                            return false;
                        } else {
                            if (typeof(FileReader) != 'undefined') {
                                img_preview.empty();
                                var reader = new FileReader();
                                reader.onload = function(e) {
                                    $('<img/>', {
                                        'src': e.target.result,
                                        'style': 'max-height: 80px; max-width: 80px; margin: 10px;'
                                    }).appendTo(img_preview);
                                }
                                img_preview.show();
                                reader.readAsDataURL($(this)[0].files[index]);
                            } else {
                                img_preview.html(
                                    '<span style="color:red;">This not support file reader</span>');
                            }
                        }
                    }
                }

            });
        });
    </script>
@endsection