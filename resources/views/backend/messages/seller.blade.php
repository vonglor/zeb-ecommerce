@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h1 class="text-lg font-medium mr-auto">
                Send message to sellers
            </h1>
        </div>
        <div class="grid grid-cols-6 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
            <form action="{{route('add.message')}}" method="POST" id="tnc-form">
                @csrf
                <div class="form-group">
                    <label for="min_qty" class="form-label text-base">Title</label>
                    <input type="text" id="title" name="title" class="form-control border-gray-500 text-base" placeholder="Title" required>
                    {{-- @error('title')
                    <span style="color: red;">{{ $message }}</span>
                    @enderror --}}
                </div>
                <div class="form-group mt-4">
                    <label for="min_qty" class="form-label text-base">Message</label>
                    <textarea class="ckeditor form-control text-base" name="message" id="message" ></textarea>
                    {{-- @error('message')
                    <span style="color: red;">{{ $message }}</span>
                    @enderror --}}
                </div>

                <button type="submit" class="btn btn-primary text-base">Send only customer</button>
            </form>
            </div>
        </div>

        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
                <tr>
                    <th class="whitespace-nowrap text-base">#</th>
                    <th class="whitespace-nowrap text-base">Title</th>
                    <th class="whitespace-nowrap text-base">Message</th>
                    <th class="whitespace-nowrap text-base">Status</th>
                    <th class="text-center whitespace-nowrap text-base">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 0;
                @endphp
                @foreach ($message as $item)
                    <tr class="intro-x">
                    <td>
                        <h1 class="text-base">{{ ++$i }}</h1> 
                    </td>
                    <td>
                        <h1 class="font-medium whitespace-nowrap text-base">{{$item->title}}</h1> 
                    </td>
                    <td class="text-base">{{ substr($item->message,0,100) }} ....</td>
                    <td id="resp{{ $item->id }}">
                    <br>
                        @if($item->status == 1)
                        <button type="button" class="btn btn-sm btn-success">Activa</button>
                            @else
                        <button type="button" class="btn btn-sm btn-danger">Inactiva</button>
                        @endif
                    </td>
                    <td >
                        <input data-id="{{ $item->id }}" class="form-check-switch text-base mi_checkbox" type="checkbox">
                    </td>
                   
                    <td class="table-report__action w-56 p-5">
                        <div class="flex justify-center items-center">
                            <a class="btn btn-primary" href="{{route('edit.message', $item->id)}}"> <i data-feather="check-square" class="w-4 h-3 mr-1"></i></a>
                            <form action="{{route('delete.message', $item->id)}}" method="POST" class="px-2">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- END: Data List -->
    </div>
<!-- END: Content -->
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });
    </script>

    {{-- change status --}}

<script src="{{ asset('js/jquery.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $(window).load(function() {
            $(".cargando").fadeOut(1000);
        });


$('.mi_checkbox').change(function() {
    //Verifico el estado del checkbox, si esta seleccionado sera igual a 1 de lo contrario sera igual a 0
    var status = $(this).prop('checked') == true ? 1 : 0; 
    var id = $(this).data('id'); 
        console.log(status);

    $.ajax({
        type: "GET",
        dataType: "json",
        //url: '/StatusNoticia',
        url: '{{ route('UpdateStatusNoti') }}',
        data: {'status': status, 'id': id},
        success: function(data){
            $('#resp' + id).html(data.var); 
            console.log(data.var)
         
        }
    });
})
      
});
</script>
    
@endsection

