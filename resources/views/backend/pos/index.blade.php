@extends('backend.layouts.sidebar')
@section('content')


@php
$user_id = Session::get('user')->id;
$user_type = Session::get('user')->user_type;
$user = App\Models\User::find($user_id);
@endphp
 
  <!-- BEGIN: Content -->
  <div class="content">
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">
            {{__('Pos Management')}}
        </h2>
    </div>
    <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">
        <!-- BEGIN: Item List -->
        <div class="intro-y col-span-12 lg:col-span-12">
            <div class="lg:flex intro-y">
                <form class="" role="search" action="{{ route('pos-sale.search_product') }}" method="get">
                    @csrf
                <div class="relative text-gray-700 dark:text-gray-300">
                    <input type="text" name="search" id="search" required 
                    class="form-control py-3 px-4 w-full lg:w-64 box pr-10 placeholder-theme-8" 
                    placeholder="Search products...">
                    <button type="submit" name="btnSeacth">
                        <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
                    </button> 
                </div>
                </form>
                @php
                $categories = App\Models\Category::all();
                @endphp
                <select class="form-select py-3 px-8 box w-full lg:w-auto mt-3 lg:mt-0 ml-auto" id="category" name="category" onchange="filterCategory(this.value)">
                    <option value="">{{__('Filter Category')}}</option>
                    <option value="all">All Products</option>
                    @if($categories->count() > 0)
                    @foreach($categories as $row)
                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                    @endforeach
                    @endif
                </select>
                @csrf
            </div>

            <div id="showProducts">
                <div class="grid grid-cols-12 gap-5 mt-5">
                    @if($products->count() > 0)
                    @foreach ($products as $row)
                    <div class="col-span-12 sm:col-span-4 xxl:col-span-3 box p-5 cursor-pointer zoom-in">
                        @if ($row->image !== null)
                        <img src="{{ url(App\Models\Upload::find($row->image)->file_name) }}" class="img-responsive" alt="Responsive image" width="20%">
                        @else
                        <img src="{{ url($row->Upload->file_name) }}" class="img-responsive" alt="Responsive image" width="20%">
                    @endif
                    <div class="font-medium text-base">{{ Str::limit($row->name, 12) }}</div>
                        <div class="text-gray-600">${{ number_format(($row->price - $row->discount), 2) }} </div>
                        <a href="{{ route('pos-sale.add_to_cart', $row->id) }}" class="btn btn-primary btn-sm" style="width: 100%"> {{__('Add to cart')}}</a>
                    </div>
                    @endforeach
                    @else
                    <div class="col-span-12 sm:col-span-4 xxl:col-span-3 box p-5 cursor-pointer zoom-in">
                        <div class="font-medium text-base" style="color: red">{{__('No products found')}}</div>
                    </div>
                    @endif
                </div>
                <br>
                <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
                    <ul class="pagination">
                            {!! $products->links() !!}
                    </ul>
                </div>
            </div>
        </div>
        {{-- {{ url('uploads/images/no-product.png') }} --}}
        <!-- END: Item List -->
        <!-- BEGIN: Ticket -->
        <div class="col-span-12 lg:col-span-12">
            <div class="tab-content">
                <div id="ticket" class="tab-pane active" role="tabpanel" aria-labelledby="ticket-tab">
                    <div class="pos__ticket box p-2 mt-5">
                        <table  class="table table-report -mt-2">
                            <thead>
                                <tr>
                                    <th>{{__('No.')}}</th>
                                    <th>{{__('Description')}}</th>
                                    <th>{{__('Price')}}</th>
                                    <th class="text-center">{{__('Qty')}}</th>
                                    <th>{{__('SubTotal')}}</th>
                                    <th class="text-center">{{__('Delete')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(Session::get('cart') !== null && count(Session::get('cart')) > 0)
                                @php
                                $i = 1;
                                $total = 0;
                                $totalShipping = 0;
                                @endphp
                                @foreach (Session::get('cart') as $key => $row)
                                @php
                                $total += $row['price'] * $row['qty'];
                                $product = App\Models\Product::find($row['pro_id']);
                                $totalShipping += $product->shipping * $row['qty'];
                                @endphp
                                <tr>
                                    <td>
                                        {{ $i++ }}.
                                    </td>
                                    <td>
                                        <div class="flex items-center">
                                            <div class="w-10 h-10 bg-theme-9 flex items-center justify-center mr-3">
                                                <img src="{{ url(App\Models\Upload::find($row['image'])->file_name) }}" class="w-full h-full" alt="">
                                            </div>
                                            <div class="flex-1">
                                                <div class="font-medium">{{ $row['name'] }}</div>
                                                <div class="text-gray-600">{{ $row['variant'] }}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>${{ number_format($row['price'], 2) }}</td>
                                    <td class="text-center">
                                        <div class="input-group number-spinner">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" id="{{ $key }}"
                                                    data-dir="dwn">-</button>
                                            </span>
                                            <input type="hidden" name="key" value="{{ $key }}">
                                            <input type="text" style="width: 80px" name="qty" class="form-control text-center"
                                                value="{{ $row['qty'] }}">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" id="{{ $key }}"
                                                    data-dir="up">+</button>
                                            </span>
                                        </div>
                                    </td>
                                    <td>${{ number_format($row['price'] * $row['qty'], 2) }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('pos-sale.remove_cart', $key) }}" class="btn btn-danger btn-sm">
                                            <i data-feather="trash" class="w-4 h-4 mr-1"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif

                                @if(Session::get('cart') !== null && count(Session::get('cart')) > 0)
                                <tr>
                                    <td colspan="4" class="text-right">
                                        <div class="font-medium">{{__('Shipping')}}</div>
                                    </td>
                                    <td><div class="font-medium">${{ number_format($totalShipping, 2) }}</div></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right">
                                        <div class="font-medium">{{__('Total')}}</div>
                                    </td>
                                    <td><div class="font-medium">${{ number_format(($total + $totalShipping), 2) }}</div></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-right">
                                        <div class="font-medium">{{__('Customers:')}}</div>
                                    </td>
                                    <td>
                                        <div class="form-check mr-2">
                                            <input  class="form-check-input member_type" type="radio" name="member_type" 
                                            value="member" {{ old('member_type') === "member" ? "checked": "" }}>
                                            <label class="form-check-label" for="member">{{__('Member')}}</label>
                                        </div>
                                    </td>
                                    <td colspan="3">
                                        <div class="form-check mr-2">
                                            <input  class="form-check-input member_type" type="radio" name="member_type" 
                                            value="general" {{ old('member_type') === "general" ? "checked": "" }}>
                                            <label class="form-check-label" for="general">{{__('General')}}</label>
                                        </div>
                                    </td>
                                </tr>

                                    <tr>
                                        <td colspan="2" class="text-right">
                                            <div class="font-medium" id="show_label_member" style="display: none">{{__('Select:')}}</div>
                                        </td>
                                        <td colspan="4">
                                                <div id="showMember">
    
                                                </div>
                                        </td>
                                    </tr>
                                
                                <tr>
                                    <td colspan="2" class="text-right">
                                        <div class="font-medium">{{__('Payment Options:')}}</div>
                                    </td>
                                    <td>
                                        <div class="form-check mr-2">
                                            <input id="radio-switch-4" class="form-check-input" type="radio" name="payment_type" id="payment_type"
                                            value="paypal" {{ old('payment_type') === "paypal" ? "checked": "" }}>
                                            <label class="form-check-label" for="radio-switch-4">{{__('Paypal')}}</label>
                                        </div>
                                    </td>
                                    <td colspan="3">
                                        <div class="form-check mr-2">
                                            <input id="radio-switch-4" class="form-check-input" type="radio" name="payment_type" id="payment_type"
                                            value="cash" {{ old('payment_type') === "cash" ? "checked": "" }}>
                                            <label class="form-check-label" for="radio-switch-4">{{__('Cash')}} </label>
                                        </div>
                                    </td>
                                </tr>
                                @endif 
                            </tbody>  
                        </table>

                        
                    </div>
                    
                    <div class="flex mt-5">
                        <a href="{{ route('pos-sale.clear_cart_items', 0) }}" onclick="return confirm('Do you really want to clear cart?');" class="btn btn-danger btn-sm">{{__('Clear cart')}}</a>
                        <button disabled class="btn btn-primary w-32 shadow-md ml-auto">{{__('Save')}}</button>
                    </div>
                </div>
            </div>

        </div>
        <!-- END: Ticket -->
    </div>
</div>
<!-- END: Content -->

<script src="https://www.paypal.com/sdk/js?client-id=test&currency=USD"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>

<script>
    $(document).ready(function() {
        $('.number-spinner .btn').on('click', function() {
            var btn = $(this);
            var input = btn.closest('.number-spinner').find('input[name="qty"]');
            var oldValue = input.val().trim();
            var newVal = 0;
            var key = btn.attr('id');
            if (btn.attr('data-dir') === 'up') {
                newVal = parseInt(oldValue) + 1;
            } else {
                if (oldValue > 1) {
                    newVal = parseInt(oldValue) - 1;
                } else {
                    newVal = 1;
                }
            }
            input.val(newVal);

            $.ajax({
                url: '{{ route('pos-sale.update_cart') }}',
                type: 'POST',
                data: {
                    key: key,
                    qty: newVal,
                    _token: '{{ csrf_token() }}'
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });
        
        $('.member_type').on('change', function() {
            var member_type = $(this).val();
            var lb = document.getElementById("show_label_member");
            if(member_type === 'member') {
                lb.style.display = "block";
                $.ajax({
                        url: '{{ route('pos-sale.get_users') }}',
                        type: 'get',
                        data: {
                            _token: '{{ csrf_token() }}'
                        },
                        success: function(data) {
                            $('#showMember').html(data);
                            $('.list_users').selectpicker();
                            //$('.list_users').selectpicker('refresh');
                            //$('.list_users').selectpicker('render');
                        }
                });
            } else {
                $('#showMember').html('');
                lb.style.display = "none";
            }
        });

    });

    function filterCategory(filter_by){
        $.ajax({
            url: '{{ route('pos-sale.filter_category') }}',
            type: 'POST',
            data: {
                filter_by: filter_by,
                _token: '{{ csrf_token() }}'
            },
            success: function(data) {
                $('#showProducts').html(data);
            }
        });
    }
</script>
    
@endsection