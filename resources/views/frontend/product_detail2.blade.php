@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/detail.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/product-list.css') }}" rel="stylesheet">
    <style>
        .small-img-row {
            display: flex;
            justify-content: space-around;
            margin-bottom: 10px;
        }

        .small-img-row .active{
            border: 1px solid #FF5C00;
        }

        .col-2 img {
            max-width: 100%;
            padding: 50px 0;
        }

        .small-img-col {
            flex-basis: 24%;
            cursor: pointer;
            margin-left: 5px;
            border: 1px solid #ddd;
        }

        .small-img-col img{
            height: 100px;
            width: auto;
        }

        .col-2 img {
            padding: 0;
        }

        .col-2 {
            padding: 20px;
        }

    </style>
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Product Detail</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="product" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="product_1 clearfix">
                    <div class="col-sm-12">
                        <div class="center_detail_2 clearfix">
                            <div class="col-sm-6">
                                <div class="center_detail_2_left clearfix">
									<div class="carousel slide article-slide" id="article-photo-carousel">

										<!-- Wrapper for slides -->
										<div class="carousel-inner cont-slider">
											<div class="item active">
												<div class="mag">
													<div class="magnify">
														<div class="magnify"><img data-toggle="magnify" src="{{ url('frontend/img/31.jpg') }}"
																alt="">
														</div>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="mag">
													<div class="magnify">
														<div class="magnify"><img data-toggle="magnify" src="{{ url('frontend/img/32.jpg') }}"
																alt="">
															<div class="magnify-large"
																style="background: url(&quot;img/54.jpg&quot;) no-repeat;">
															</div>
														</div>
														<div class="magnify-large"></div>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="mag">
													<div class="magnify">
														<div class="magnify"><img data-toggle="magnify" src="{{ url('frontend/img/33.jpg') }}"
																alt="">
															<div class="magnify-large"
																style="background: url(&quot;img/55.jpg&quot;) no-repeat;">
															</div>
														</div>
														<div class="magnify-large"></div>
													</div>
												</div>
											</div>
											<div class="item ">
												<div class="mag">
													<div class="magnify">
														<div class="magnify"><img data-toggle="magnify" src="{{ url('frontend/img/34.jpg') }}"
																alt="">
															<div class="magnify-large"
																style="background: url(&quot;img/56.jpg&quot;) -101px -21px no-repeat; display: block; left: 17px; top: -27.5px; opacity: 0;">
															</div>
														</div>
														<div class="magnify-large"></div>
													</div>
												</div>
											</div>
										</div>
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li class="active" data-slide-to="0" data-target="#article-photo-carousel">
                                                <img alt="" src="{{ url('frontend/img/31.jpg') }}">
                                            </li>
                                            <li class="" data-slide-to="1" data-target="#article-photo-carousel">
                                                <img alt="" src="{{ url('frontend/img/32.jpg') }}">
                                            </li>
                                            <li class="" data-slide-to="2" data-target="#article-photo-carousel">
                                                <img alt="" src="{{ url('frontend/img/33.jpg') }}">
                                            </li>
                                            <li class="" data-slide-to="3" data-target="#article-photo-carousel">
                                                <img alt="" src="{{ url('frontend/img/34.jpg') }}">
                                            </li>
                                        </ol>
									</div>
								</div>
                            </div>
                            <div class="col-sm-6">
                                <div class="center_detail_2_right clearfix">
                                    <form action="{{ route('add_to_cart') }}" id="form-add-cart" method="POST">
                                        @csrf
                                        <div class="center_detail_2_right_inner clearfix">
                                            <h4>
                                            <?php
                                            $array = explode( "\n", wordwrap( $detail->name, 50));
                                            echo implode("<br>",$array);
                                            ?>
                                            </h4>
                                            <h4 class="reviewed">
                                                <a href="#">3 reviewed</a>
                                            </h4>
                                            <h5><span>Condition:</span> new</h5>
                                            <h6 style="color: #000"><a class="print-size">Print size</a></h6>
                                            <h4>
                                                @if($detail->discount != null)
                                                <span class="col_2">${{ number_format($detail->unit_price, 2) }}</span> 
                                                $<span id="show_price">{{ number_format(($detail->unit_price)-($detail->discount), 2) }}</span></h4>
                                                @else
                                                $<span id="show_price">{{ number_format($detail->unit_price, 2) }}</span></h4>
                                                @endif
                                            @if (count(json_decode($detail->colors)) > 0)
                                                <div class="pd_n clearfix color-choose">
                                                    <h5 class="bold">Color <span class="col_2">*</span>
                                                    </h5>
                                                    <ul>
                                                        @php
                                                            $i = 1;
                                                        @endphp
                                                        @foreach (json_decode($detail->colors) as $key => $color)
                                                            @php
                                                                $colors = App\Models\Color::where('name', $color)->first();
                                                            @endphp
                                                            <input type="radio" name="color" id="{{ $color }}"
                                                                class="radio_{{ $i }}"
                                                                value="{{ $color }}" style="display: none">
                                                            <label for="{{ $color }}">
                                                                <li style="background: {{ $colors->code }}"
                                                                    id="color_{{ $i }}" name="{{ $color }}"></li>
                                                            </label>
                                                            @php
                                                                $i++;
                                                            @endphp
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            @if ($detail->choice_options != null && count(json_decode($detail->choice_options)) > 0)
                                                @foreach (json_decode($detail->choice_options) as $key => $choice)
                                                    <div class="pd_n_1 clearfix choice_options">
                                                        @php
                                                            $option = App\Models\Attributes::find($choice->attribute_id);
                                                        @endphp
                                                        <h5 class="bold">{{ $option->name }} <span
                                                                class="col_2">*</span>
                                                        </h5>
                                                        <ul>
                                                            @php
                                                                $i = 1;
                                                            @endphp
                                                            @foreach ($choice->values as $key => $value)
                                                                <label>
                                                                    <input type="radio" name="{{ $option->name }}"
                                                                        id="{{ $option->name }}"
                                                                        class="radio_{{ $i }}"
                                                                        value="{{ $value }}" style="display: none">
                                                                        <li id="{{ $option->name . $value }}" name="{{ $value }}"
                                                                            class="{{ $option->name }}_{{ $i }}"
                                                                            data="{{ $option->name }}">{{ $value }}
                                                                        </li>
                                                                </label>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endforeach
                                            @else
                                             <input type="hidden" id="Size" name="Size" value="">
                                             <input type="hidden" id="Fabric" name="Fabric" value="">
                                             <input type="hidden" id="Poud" name="Poud" value="">
                                            @endif
                                            
                                            <h6></h6>
                                            <div class="input-group number-spinner">
                                                <span class="input-group-btn">
                                                    <a class="btn btn-default" data-dir="dwn"><span
                                                            class="glyphicon glyphicon-minus"></span></a>
                                                </span>
                                                <input type="text" name="qty" class="form-control text-center" value="1">
                                                <span class="input-group-btn">
                                                    <a class="btn btn-default" data-dir="up"><span
                                                            class="glyphicon glyphicon-plus"></span></a>
                                                </span>
                                            </div>
                                            <input type="hidden" name="product_id" id="product_id" value="{{ $detail->id }}">
                                        </div>
                                        <div class="center_detail_2_right_inner_1 clearfix">
                                            <ul>
                                                <li><button type="submit" id="add_to_cart" class="btn-addcart"><i
                                                            class="fa fa-shopping-bag"></i> ADD TO CART</button>
                                                </li>
                                                <li><a href="#" id="btn-buy" class="btn-buy"> BUY NOW</a></li>
                                            </ul>
                                        </div>
                                    </form>
                                    <div class="center_detail_2_right_inner_2 clearfix">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#home">Delivery</a></li>
                                        </ul>
                        
                                        <div class="tab-content clearfix">
                                          <div id="home" class="tab-pane fade in active clearfix">
                                             <div class="click clearfix">
                                                <p class="mgt">
                                                    {{ $detail->delivery }}
                                                </p>
                                            </div>
                                          </div>
                                        </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            
            <div class="center_detail_2 clearfix">
                <div class="center_detail_2_right_inner_2 clearfix">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home_description">Seller Description</a>
                        </li>
                        <li class=""><a data-toggle="tab" href="#menu_information">Policy return</a></li>
                    </ul>

                    <div class="tab-content clearfix">
                        <div id="home_description" class="tab-pane fade clearfix active in">
                            <div class="click clearfix">
                                <p>{{ $detail->description}}</p>
                            </div>
                        </div>
                        <div id="menu_information" class="tab-pane fade clearfix ">
                            <div class="click clearfix">
                                <p>Praesent libero Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at
                                    nibh elementum imperdiet. Duis sagittis ipsum.Praesent mauris. Fusce nec
                                    tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu
                                    eget nulla.Class aptent taciti sociosqu ad litora torquent per conubia
                                    nostra, per inceptos himenaeos. Curabitursodales ligula in libero.Sed
                                    dignissim lacinia nunc.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="product_list">
		<div class="container" style="background: #fff; padding: 15px 0 30px 0; border: 1px solid #f3f3f3;">
			<div class="row">
				<div class="product_list text-center clearfix">
					<div class="col-sm-12">
						<h3 class="mgt">Product Items</h3>
						<hr>
					</div>
				</div>
				<div class="gallery_1 clearfix">
					<div class="col-sm-12">
						<div class="workout_page_1_left clearfix">

							<div class="tab-content clearfix">
								<div id="home" class="tab-pane fade  clearfix active in">
									<div class="click clearfix">
										<div class="home_inner clearfix">
                                            @php
                                                $product = App\Models\Product::all()->paginate(8);
                                            @endphp
											@foreach ($product as $products)
											<div class="col-sm-3 offset-md-0 offset-sm-1 pt-md-0 pt-4">
												<div class="card"> <a href="{{ route('product_detail', $products->id) }}"><img class="card-img-top" src="{{ url($products->Upload->file_name) }}"></a>
													<div class="card-body">
														<h5 class="font-weight-bold pt-1"><a href="{{ route('product_detail', $products->id) }}">{{ Str::limit($products->name, 27) }}</a></h5>
														<div class="d-flex align-items-center justify-content-between pt-3">
															<div class="d-flex flex-column">
																<div class="h5 font-weight-bold">
                                                                    @if ($products->discount)
																		<span class="col_2">${{ number_format($products->unit_price, 2) }}</span>
																	@endif
																	${{ number_format($products->unit_price-$products->discount, 2) }} 
                                                                </div>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endforeach 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="product_1_last text-center clearfix">
					<div class="col-sm-12">
						<ul>
							{!! $product->links() !!}
						</ul>
					</div>
				</div>
			</div>
		</div>

        {{-- moal detail --}}
        <div class="modal fade" id="print_modal" tabindex="-1" role="dialog" style="z-index: 10000;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Print size</h4>
                    </div>
                    <div class="modal-body">
                        @if($detail->pdf != null)
                        <img src="{{ url(App\Models\Upload::find($detail->pdf)->file_name) }}" alt="" style="max-width: 600px; max-height: 600px;">
                        @else
                        NO Print size...
                        @endif
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
	</section>
@endsection

@section('script')
    <script type="text/javascript">
        ! function($) {

            "use strict"; // jshint ;_;


            /* MAGNIFY PUBLIC CLASS DEFINITION
             * =============================== */

            var Magnify = function(element, options) {
                this.init('magnify', element, options);
            }

            Magnify.prototype = {

                constructor: Magnify

                    ,
                init: function(type, element, options) {
                    var event = 'mousemove',
                        eventOut = 'mouseleave';

                    this.type = type;
                    this.$element = $(element);
                    this.options = this.getOptions(options);
                    this.nativeWidth = 0;
                    this.nativeHeight = 0;

                    this.$element.wrap('<div class="magnify" \>');
                    this.$element.parent('.magnify').append('<div class="magnify-large" \>');
                    this.$element.siblings(".magnify-large").css("background", "url('" + this.$element.attr("src") +
                        "') no-repeat");

                    this.$element.parent('.magnify').on(event + '.' + this.type, $.proxy(this.check, this));
                    this.$element.parent('.magnify').on(eventOut + '.' + this.type, $.proxy(this.check, this));
                },
                getOptions: function(options) {
                    options = $.extend({}, $.fn[this.type].defaults, options, this.$element.data())

                    if (options.delay && typeof options.delay == 'number') {
                        options.delay = {
                            show: options.delay,
                            hide: options.delay
                        }
                    }

                    return options;
                },
                check: function(e) {
                    var container = $(e.currentTarget);
                    var self = container.children('img');
                    var mag = container.children(".magnify-large");

                    // Get the native dimensions of the image
                    if (!this.nativeWidth && !this.nativeHeight) {
                        var image = new Image();
                        image.src = self.attr("src");

                        this.nativeWidth = image.width;
                        this.nativeHeight = image.height;

                    } else {

                        var magnifyOffset = container.offset();
                        var mx = e.pageX - magnifyOffset.left;
                        var my = e.pageY - magnifyOffset.top;

                        if (mx < container.width() && my < container.height() && mx > 0 && my > 0) {
                            mag.fadeIn(100);
                        } else {
                            mag.fadeOut(100);
                        }

                        if (mag.is(":visible")) {
                            var rx = Math.round(mx / container.width() * this.nativeWidth - mag.width() / 2) * -1;
                            var ry = Math.round(my / container.height() * this.nativeHeight - mag.height() / 2) * -
                                1;
                            var bgp = rx + "px " + ry + "px";

                            var px = mx - mag.width() / 2;
                            var py = my - mag.height() / 2;

                            mag.css({
                                left: px,
                                top: py,
                                backgroundPosition: bgp
                            });
                        }
                    }

                }
            }

            /* MAGNIFY PLUGIN DEFINITION
             * ========================= */

            $.fn.magnify = function(option) {
                return this.each(function() {
                    var $this = $(this),
                        data = $this.data('magnify'),
                        options = typeof option == 'object' && option
                    if (!data) $this.data('tooltip', (data = new Magnify(this, options)))
                    if (typeof option == 'string') data[option]()
                })
            }

            $.fn.magnify.Constructor = Magnify

            $.fn.magnify.defaults = {
                delay: 0
            }


            /* MAGNIFY DATA-API
             * ================ */

            $(window).on('load', function() {
                $('[data-toggle="magnify"]').each(function() {
                    var $mag = $(this);
                    $mag.magnify()
                })
            })

        }(window.jQuery);


        $(document).ready(function() {
            $('.color-choose label #color_1').addClass('act');
            $('.color-choose .radio_1').attr('checked', true);
            $('.color-choose label li').on('click', function() {
                $('.act').removeClass('act');
                $(this).addClass('act');
                var colors = $(this).attr('name');
                var Size = $('#Size').val();
                var Fabric = $('#Fabric').val();
                var Poud = $('#Poud').val();
                var product_id = $('#product_id').val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "POST",
                    url: "{{ route("select") }}",
                    data: {
                        Size: Size,
                        Fabric: Fabric,
                        Poud: Poud,
                        colors: colors,
                        product_id: product_id,
                        _token: _token,
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $('#show_price').html(data.price);
                    }
                });
            });
            
            $('.choice_options label .Size_1').addClass('act_1');
            $('.choice_options label .Fabric_1').addClass('act_1');
            $('.choice_options label .Poud_1').addClass('act_1');
            $('.choice_options .radio_1').attr('checked', true);
            $('.choice_options label li').on('click', function() {
                var att = $(this).attr('id');
                var data = $(this).attr('data');
                $('.choice_options .act_1').removeClass('act_1');
                $('.choice_options #' + att).addClass('act_1');

                var colors = $('#color').val();
                var Size = $('#Size').val();
                var Poud = $(this).attr('name');
                var Fabric = $('#Fabric').val();
                var product_id = $('#product_id').val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "POST",
                    url: "{{ route("select") }}",
                    data: {
                        Size: Size,
                        Fabric: Fabric,
                        Poud: Poud,
                        colors: colors,
                        product_id: product_id,
                        _token: _token,
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        $('#show_price').html(data.price);
                    }
                });
            });
            
            $('#btn-buy').click(function (e) { 
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: '{{ route('buy') }}',
                    data: $('#form-add-cart').serialize(),
                    success: function(data) {
                        window.location.href="/cart";
                    }
                });
            });

            $('.print-size').click(function (e) { 
                e.preventDefault();
                $('#print_modal').modal('show');
            });
        });
    </script>

    <script type="text/javascript">
        var price = $('#show_price').text();
        $(document).on('click', '.number-spinner .btn', function() {
            var btn = $(this),
                oldValue = btn.closest('.number-spinner').find('input').val().trim();
            newVal = 0;
            if (btn.attr('data-dir') == 'up') {
                newVal = parseInt(oldValue) + 1;
            } else {
                if (oldValue > 1) {
                    newVal = parseInt(oldValue) - 1;
                } else {
                    newVal = 1;
                }
            }
            var subtotal = price * newVal;
             subtotal = subtotal.toFixed(2);
            btn.closest('.number-spinner').find('input').val(newVal);
            $('#subtotal').html(subtotal);
        });

        $(document).on('click', '.small-img-row .small-img-col', function () { 
            $('.active').removeClass('active');
            $(this).addClass('active');
         })
    </script>
@endsection
