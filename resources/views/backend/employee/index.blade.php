@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
                <div class="content">
                    <h2 class="intro-y text-lg font-medium mt-10 text-xl">
                        All Employee
                    </h2>
                    <div class="grid grid-cols-12 gap-6 mt-5">
                        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                            <a href="{{route('employee')}}" class="btn btn-primary shadow-md mr-2 text-lg">Add New Employee</a>
                            <div class="hidden md:block mx-auto text-gray-600"></div>
                            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                                <div class="w-56 relative text-gray-700 dark:text-gray-300">
                                    <input type="text" class="form-control w-56 box pr-10 placeholder-theme-8" placeholder="Search...">
                                    <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i> 
                                </div>
                            </div>
                        </div>
                        <!-- BEGIN: Data List -->
                        <div class="intro-y col-span-12 overflow-auto lg:overflow">
                            <table class="table table-report -mt-2 ">
                                <thead>
                                    <tr>
                                        <th class="whitespace-nowrap text-base">#</th>
                                        <th class="whitespace-nowrap text-base">EMPLOYEE ID</th>
                                        <th class="whitespace-nowrap text-base">FIRST NAME</th>
                                        <th class="whitespace-nowrap">LAST NAME</th>
                                        <th class="text-center whitespace-nowrap text-base">EMAIL</th>
                                        <th class="text-center whitespace-nowrap text-base">PHONE</th>
                                        <th class="text-center whitespace-nowrap text-base">ADDRESS</th>
                                        <th class="text-center whitespace-nowrap text-base">ACTIONS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 0;
                                    @endphp
                                    @foreach ($employees as $employee)
                                    <tr class="intro-x">
                                        <td>
                                            <a href="" class="font-medium whitespace-nowrap text-base">{{ ++$i }}</a> 
                                        </td>
                                         <td>
                                            <a href="" class="font-medium whitespace-nowrap text-base">{{$employee->id}}</a> 
                                        </td>
                                        {{-- <td class="w-40">
                                            <div class="flex">
                                                <div class="w-10 h-10 image-fit zoom-in">
                                                    <img alt="Icewall Tailwind HTML Admin Template" class="tooltip rounded-full" src="{{asset('backend/dist/images/preview-10.jpg')}}" title="Uploaded at 5 September 2022">
                                                </div>
                                            </div>
                                        </td> --}}
                                        
                                        <td>
                                            <a href="" class="font-medium whitespace-nowrap text-base">{{$employee->first_name}}</a> 
                                        </td>
                                        <td class="text-center text-base">{{$employee->last_name}}</td>
                                        <td class="w-40">
                                            <div class="flex items-center justify-center text-base">{{$employee->email}}</div>
                                        </td>
                                        <td class="w-40">
                                            <div class="flex items-center justify-center text-base"> {{$employee->phone}} </div>
                                        </td>
                                        <td class="w-40">
                                            <div class="flex items-center justify-center text-base"> {{$employee->address}} </div>
                                        </td>
                                        <td class="table-report__action w-56 p-5">
                                            <div class="flex justify-center items-center">
                                                <a class="btn btn-primary" href="{{route('employee.edit', $employee->id)}}"> <i data-feather="check-square" class="w-4 h-3 mr-1"></i></a>
                                                <form action="{{route('employee.destroy', $employee->id)}}" method="POST" class="px-2">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                     @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END: Data List -->
                        <!-- BEGIN: Pagination -->
                        <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
                            <ul class="pagination">
                                <li>
                                    <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-left"></i> </a>
                                </li>
                                <li>
                                    <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-left"></i> </a>
                                </li>
                                <li> <a class="pagination__link" href="">...</a> </li>
                                <li> <a class="pagination__link" href="">1</a> </li>
                                <li> <a class="pagination__link pagination__link--active" href="">2</a> </li>
                                <li> <a class="pagination__link" href="">3</a> </li>
                                <li> <a class="pagination__link" href="">...</a> </li>
                                <li>
                                    <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-right"></i> </a>
                                </li>
                                <li>
                                    <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-right"></i> </a>
                                </li>
                            </ul>
                        </div>
                        <!-- END: Pagination -->
                    </div>
                </div>
                <!-- END: Content -->
    
@endsection