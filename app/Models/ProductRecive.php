<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductRecive extends Model
{
    use HasFactory;

    protected $fillable = [
        'qty', 'product_id', 'pro_stock_id', 'price', 'currency_code', 'date'
    ];

    /**
     * Get the product that owns the ProductRecive
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(product::class, 'product_id', 'id');
    }

    /**
     * Get the user that owns the ProductRecive
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stock()
    {
        return $this->belongsTo(Product_stock::class, 'pro_stock_id', 'id');
    }
}
