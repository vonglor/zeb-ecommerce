<?php
use App\Http\Middleware\CheckLogin;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\POSController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\MywalletController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\Frontend\ShopController;
use App\Http\Controllers\Backend\AddressController;
use App\Http\Controllers\backend\CurrencyController;
use App\Http\Controllers\frontend\MessageController;
use App\Http\Controllers\frontend\ProductController;
use App\Http\Controllers\Frontend\ProductReciveController;
use App\Http\Controllers\Backend\WithdrawRequestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('product_detail/{id}', [HomeController::class, 'product_detail'])->name('product_detail');
Route::get('product/seller/location/{id}', [ShopController::class, 'location'])->name('seller.location');

Route::get('products', [HomeController::class, 'product'])->name('shopping');
Route::get('cart', [HomeController::class, 'cart'])->name('cart');
Route::get('contact', [HomeController::class, 'contact'])->name('contact');
Route::get('product/category/{cat_id}', [ProductController::class, 'category'])->name('product.category');
Route::get('product/brand/{brand_id}', [ProductController::class, 'brand'])->name('product.brand');
Route::get('product/search', [ProductController::class, 'product_search'])->name('product_search');

Route::middleware([CheckLogin::class])->group(function () {
    // product route
    Route::get('account', [HomeController::class, 'account'])->name('account');
    Route::get('saller/dashboard', [HomeController::class, 'dashboard'])->name('saller.dashboard');
    // Route::resource('sale/product', \App\Http\Controllers\frontend\ProductController::class, ['as' => 'sale']);
    Route::get('sale/product', [ProductController::class, 'index'])->name('sale.product.index');
    Route::get('sale/product/create', [ProductController::class, 'create'])->name('sale.product.create');
    Route::get('sale/product/edit/{id}', [ProductController::class, 'edit'])->name('sale.product.edit');
    Route::get('sale/product/destroy/{id}', [\App\Http\Controllers\Backend\ProductController::class, 'destroy'])->name('sale.product.destroy');

    Route::post('seller/product/update', [ProductController::class, 'update_product'])->name('seller.update_product');
    Route::post('upload', [UploadController::class, 'upload'])->name('upload');
    Route::get('saller/product/search', [ProductController::class, 'search'])->name('saller.product.search');
    Route::get('saller/bestsell', [ProductController::class, 'bestsell'])->name('sale.bestsell');
    Route::get('saller/bestsell/search', [ProductController::class, 'bastSaleProduct'])->name('bast.search');

    // product recive route
    Route::get('/product/recive', [ProductReciveController::class, 'index'])->name('sale.product_recive');
    Route::get('/product/recive/create', [ProductReciveController::class, 'create'])->name('product_recive.create');
    Route::get('/product/recive/edit/{id}', [ProductReciveController::class, 'edit'])->name('product_recive.edit');
    Route::put('/product/recive/update/{id}', [ProductReciveController::class, 'update'])->name('product_recive.update');
    Route::get('/product/product_id', [ProductReciveController::class, 'variantion'])->name('variantion');
    Route::post('/product/recive/store', [ProductReciveController::class, 'store'])->name('producRecive.store');

    // order route
    Route::post('order/store', [OrderController::class, 'store'])->name('order.store');
    Route::get('order/confirmed', [CheckoutController::class, 'order_confirmed'])->name('order_confirmed');
    Route::get('order/all_order', [OrderController::class, 'all_order'])->name('order.all_order');
    Route::get('order/order_detail/{order_id}', [OrderController::class, 'order_detail'])->name('order.order_detail');
    Route::get('order/update_payment_status/{id}', [OrderController::class, 'update_payment_status'])->name('orders.update_payment_status');
    Route::post('order/update_delivery_status', [OrderController::class, 'update_delivery_status'])->name('orders.update_delivery_status');
    Route::get('customer/order', [OrderController::class, 'customer_order'])->name('customer.order');
    Route::get('customer/order_detail/{order_id}', [OrderController::class, 'customer_order_detail']);
    Route::get('order/search', [OrderController::class, 'search'])->name('order.search');
    Route::get('customer/order/search', [OrderController::class, 'customerSearch'])->name('purchase.search.order');
    Route::get('order/search/date', [OrderController::class, 'date'])->name('search.date');

    //sale route
    Route::get('/user/sale', [OrderController::class, 'saleAll'])->name('sale.all');
    Route::get('sale/sale_detail/{order_id}', [OrderController::class, 'sale_detail']);
    Route::get('/user/sale/date', [OrderController::class, 'saleDate'])->name('sale.search.date');

    // manage account route
    Route::post('account/update', [AccountController::class, 'update'])->name('account.update');
    Route::post('account/update_profile', [AccountController::class, 'update_profile'])->name('user.update_profile');
    Route::post('account/update_name', [AccountController::class, 'update_name'])->name('user.update_name');
    Route::post('account/update_email', [AccountController::class, 'update_email'])->name('user.update_email');
    Route::post('account/update_phone', [AccountController::class, 'update_phone'])->name('user.update_phone');
    Route::post('account/update_payment', [Accountcontroller::class, 'update_payment'])->name('user.update_payment');

    // shop setting
    Route::get('shop/setting', [ShopController::class, 'setting'])->name('shop.setting');
    Route::post('shop/store', [ShopController::class, 'store'])->name('shop.store');
    Route::post('shop/update', [ShopController::class, 'update'])->name('shop.update');

    // message routing
    Route::get('message/index', [MessageController::class, 'index'])->name('message');
    Route::get('sent/message', [MessageController::class, 'message'])->name('sent.message');
    Route::post('message/store', [MessageController::class, 'store'])->name('saller.message.store');

    Route::get('payment/history', [PaymentController::class, 'history'])->name('payment.history');
    Route::get('my/wallet', [MywalletController::class, 'wallet'])->name('my.wallet');

    // pos route
    Route::get('pos/index', [POSController::class, 'index'])->name('pos.index');
    Route::get('pos/add_to_cart/{id}', [POSController::class, 'add_to_cart'])->name('pos.add_to_cart');
    Route::post('pos/update_cart', [POSController::class, 'update_cart'])->name('pos.update_cart');
    Route::get('pos/remove_cart/{key}', [POSController::class, 'remove_cart'])->name('pos.remove_cart');
    Route::get('/pos/search_id/{value}', [POSController::class, 'search_id']);
    Route::get('pos/search_name', [POSController::class, 'search'])->name('pos.search');
    Route::get('pos/category/{id}', [POSController::class, 'category']);
    Route::get('pos/select/user_id', [POSController::class, 'select_user_id'])->name('select.user_id');
    Route::post('pos/checkout', [POSController::class, 'checkout'])->name('pos.checkout');
    Route::post('pos/credit-card', [POSController::class, 'credit_card'])->name('pos.credit-card');
    Route::get('pos/comfirm', [POSController::class, 'pos_order_confirmed'])->name('pos_order_confirmed');

    // members
    Route::get('member/level1', [MemberController::class, 'index'])->name('member.index');
    Route::get('member/all', [MemberController::class, 'all'])->name('member.all');
    Route::get('member/view/{id}', [MemberController::class, 'view'])->name('member.view');
    Route::get('member/view2/{id}', [MemberController::class, 'level2'])->name('member.view2');
    Route::get('member/view3/{id}', [MemberController::class, 'level3'])->name('member.view3');
    Route::get('member/shop', [MemberController::class, 'shop'])->name('member.shop');

    // address
    Route::get('change/address', [AddressController::class, 'change'])->name('change');
    Route::get('select/address/{id}', [AddressController::class, 'select'])->name('select.address');
    Route::get('address/edit', [AddressController::class, 'edit'])->name('edit.address');
    Route::post('address/update', [AddressController::class, 'address_update'])->name('update.address');

    Route::get('moneywithdraw', [WithdrawRequestController::class, 'moneywithdraw'])->name('moneywithdraw');
    
});

// route register and login and logout
Route::get('registration/{user_type}', [HomeController::class, 'registration'])->name('registration');
Route::post('register', [RegisterController::class, 'register'])->name('user.register');
Route::post('register/store', [RegisterController::class, 'store'])->name('user.register.store');
Route::get('sign-in', [HomeController::class, 'sign_in'])->name('sign_in');
Route::post('login', [HomeController::class, 'login'])->name('user.login');
Route::post('login/buy', [HomeController::class, 'login_buy'])->name('user.buy.login');
Route::get('/user/logout', [HomeController::class, 'logout'])->name('user.logout');

// cart route
Route::post('select-color', [CartController::class, 'select_color'])->name('choose');
Route::post('select/product', [CartController::class, 'select_product'])->name('select');
Route::post('add_to_cart', [CartController::class, 'add_to_cart'])->name('add_to_cart');
Route::get('cart_destroy', [CartController::class, 'cart_destroy'])->name('cart_destroy');
Route::get('cart_remove/{key}', [CartController::class, 'cart_remove'])->name('remove');
Route::post('update_cart', [CartController::class, 'update_cart'])->name('update_cart');



Route::post('/currency/change', [CurrencyController::class, 'changeCurrency'])->name('currency.change');


// Payment route/
// Route::get('stripe', [StripeController::class, 'stripe']);
// Route::post('stripe/payment', [StripeController::class, 'stripePost'])->name('stripe.post');
// Route::post('checkout', [StripeController::class, 'afterpayment'])->name('checkout.credit-card');

// paypal payment
Route::get('paypal/payment', [PaypalController::class, 'index'])->name('paypal.index');

// paypal route
Route::get('paypal', [PaypalController::class, 'index'])->name('paypal.index');
Route::get('success-paypal', [PayPalController::class, 'successPaypal'])->name('success.paypal');
Route::get('cancel-paypal', [PayPalController::class, 'cancelPaypal'])->name('cancel.paypal');

// checkout route
Route::get('checkout', [CheckoutController::class, 'checkout'])->name('checkout');
Route::get('shipping', [CheckoutController::class, 'shipping'])->name('shipping');
Route::get('checkout/delivery_info', [CheckoutController::class, 'delivery_info'])->name('delivery_info');
Route::get('checkout/payment_select/{owner_id}', [CheckoutController::class, 'store_delivery_info'])->name('checkout.store_delivery_info');
Route::post('checkout/payment', [CheckoutController::class, 'checkout'])->name('payment.checkout');

Route::post('buy', [CartController::class, 'buy'])->name('buy');