<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawRequest extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function user_tax()
    {
        return $this->belongsTo('App\Models\UserTax', 'user_id', 'user_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'user_id', 'user_id');
    }

    public function membership()
    {
        return $this->belongsTo('App\Models\Membership', 'membership_id', 'id');
    }

    public function seller()
    {
        return $this->belongsTo('App\Models\Seller', 'user_id', 'user_id');
    }
}
