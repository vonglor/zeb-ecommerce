@php
$user = Session::get('user');
@endphp
@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/checkout.css') }}" rel="stylesheet">
    <style>
        table{
            font-size: 14px;
        }
    </style>
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Checkout</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="checkout" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="checkout_1 clearfix">

                    <div class="col-sm-8 checkout_1l">
                        <div class="check_lm clearfix">
                            <div class="row">
                                <h4 style="margin-left: 15px">Select dilivery info</h4>

                                @php
                                    $item = App\Models\Address::where('user_id', $user->id)->where('status', 'selected')->first();
                                @endphp
                                <div id="address">
                                    <div class="col-sm-6 address" id="address">
                                        <div class="flex-grow-1 pl-3 text-left address_l">
                                            <span>
                                                {{ $user->name . ' ' . $user->surname }}
                                            </span>
                                            <span>
                                                {{ $item->address }}
                                            </span>
                                            <span>
                                                {{ $item->city . ', ' . $item->province . ', ' . $item->postal_code }}
                                            </span>
                                            <span>
                                                {{ $item->country }}
                                            </span>
                                            <span>
                                                {{ $item->phone }}
                                            </span>
                                            <div class="text-right">
                                                <a href="" id="change" data="{{ $user->id }}" class="text-right">Change</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <hr>
                        {{-- product list --}}
                        <div class="row">
                            <h4 style="margin-left: 15px">Review items</h4>
                            <div class="table-responsive col-sm-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr class="">
                                            <th width="15%">Image</th>
                                            <th>Name</th>
                                            <th width="20%" class="text-center">qty</th>
                                            <th width="15%">price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $subtotal = 0;
                                        $total_shipping = 0;
                                        $product_list = Session::get('cart')->where('owner_id', Session::get('owner_id'));
                                    @endphp
                                    @foreach ($product_list as $key => $cartItem)
                                        @php
                                            $subtotal += $cartItem['price'] * $cartItem['qty'];
                                        @endphp
                                        <tr>
                                            <td>

                                                <img src="{{ url(App\Models\Upload::find($cartItem['image'])->file_name) }}"
                                                    class="iw" alt="abc"
                                                    style="max-width:80px; max-height:80px; float:left; margin-right:10px;">
                                            </td>
                                            <td>
                                                <span>
                                                {{-- @php
                                                    $array = explode("\n", wordwrap($cartItem['name'], 80));
                                                                        echo implode('<br>', $array);
                                                @endphp --}}
                                               {{{ $cartItem['name']}}}
                                                </span><br>
                                                @if ($cartItem['color'])
                                                    <span>{{ $cartItem['color'] }}</span><br>
                                                @endif
                                                @if ($cartItem['size'])
                                                    <span>{{ $cartItem['size'] }}</span><br>
                                                @endif
                                                @if ($cartItem['fabric'])
                                                    <span>{{ $cartItem['fabric'] }}</span><br>
                                                @endif
                                                @if ($cartItem['poud'])
                                                    <span>{{ $cartItem['poud'] }}</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="input-group number-spinner">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" id="{{ $key }}"
                                                            data-dir="dwn"><span
                                                                class="glyphicon glyphicon-minus"></span></button>
                                                    </span>
                                                    <input type="hidden" name="key" value="{{ $key }}">
                                                    <input type="text" name="qty" class="form-control text-center"
                                                        value="{{ $cartItem['qty'] }}">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" id="{{ $key }}"
                                                            data-dir="up"><span
                                                                class="glyphicon glyphicon-plus"></span></button>
                                                    </span>
                                                </div>
                                            </td>
                                            <td class="text-right">
                                                <span style="font-size: 16px;">${{ number_format($cartItem['price'] * $cartItem['qty'], 2) }}</span><br>
                                                @php
                                                    $product = App\Models\Product::find($cartItem['pro_id']);
                                                    $shipping = $product->shipping * $cartItem['qty'];
                                                    $total_shipping += $product->shipping * $cartItem['qty'];
                                                @endphp
                                                <span>+ ${{ number_format($shipping, 2) }}</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('payment.checkout') }}" method="POST" id="checkout-form2">
                        @csrf
                        @php
                            $subtotal = 0;
                        @endphp
                        @foreach (Session::get('cart')->where('owner_id', Session::get('owner_id')) as $key => $cartItem)
                            @php
                                $subtotal += $cartItem['qty'] * $cartItem['price'];
                            @endphp

                        @endforeach
                        <div class="col-sm-4">
                            <div class="checkout_1r clearfix">
                                <h4 class="mgt">CART TOTALS</h4>
                                <hr class="hr_1">
                                <h5>Sub Total <span class="pull-right">${{ number_format($subtotal, 2) }}</span></h5>
                                @php
                                    $total = $subtotal + $total_shipping;
                                @endphp
                                <h5>(+) Shipping <span class="pull-right">${{ number_format($total_shipping, 2) }}</span>
                                </h5>
                                <hr>
                                <input type="hidden" name="address_id" value="{{ $item->id }}" >
                                <h5>Total <span class="pull-right">${{ number_format($total, 2) }}</span></h5><br>
                                <input type="hidden" name="total" value="{{ $total }}">
                                <h4>PAYMENTS</h4>
                                <hr class="hr_1">

                                {{-- paypal button --}}
                                <label>
                                    <input type="radio" name="payment_option" value="paypal" checked> Pay with PayPal
                                    <img src="{{ url('uploads/cards/paypal-mark.jpg') }}" alt="Pay with Paypal"
                                        style="height: 40px;">
                                </label>
                                <br>
                                <label>
                                    <input type="radio" name="payment_option" value="cash"> Pay with Cash
                                    <img src="{{ url('uploads/cards/cash.png') }}" alt="Pay with cash"
                                        style="height: 40px;">
                                </label>
                                <br>
                                <div id="paypal-button-container"></div>
                                <div id="card-button-container"><button type="submit" id="btn_order" style="
                                            height: 35px; 
                                            background: blue; 
                                            border: none; 
                                            font-weight: bold;
                                            border-radius: 4px; 
                                            font-size: 16px; 
                                            color: #fff; 
                                            width: 100%;">
                                        Order now</button>
                                </div>
                                {{-- end paypal button --}}
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            {{-- modal add new address --}}
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" style="z-index: 10000;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">New Address</h4>
                        </div>
                        <form class="form-horizontal" action="{{ route('address.store') }}" id="form_address"
                            method="POST">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="address" id="address"
                                            placeholder="Your address" required></textarea>
                                        <span id="msg_address" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Country</label>
                                    <div class="col-sm-10">
                                        <select name="country" class="form-control" id="" required>
                                            <option value="">Select Country</option>
                                            @foreach (\App\Models\Country::all() as $country)
                                                <option value="{{ $country->name }}">{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                        <span id="msg_country" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Province</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="province" id="province" placeholder="Province"
                                            required>
                                        <span id="msg_province" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">City</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="city" id="city" placeholder="City"
                                            required>
                                        <span id="msg_city" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Postal Code</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="postal_code" id="postal_code"
                                            placeholder="Postal Code" required>
                                        <span id="msg_postal_code" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="phone" id="phone"
                                            placeholder="Phone" required>
                                        <span id="msg_phone" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                        <input type="hidden" class="form-control" name="user_id" id="user_id"
                                            value="{{ $user->id }}">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </section>
@endsection
<script src=""></script>
@section('script')
    <script src="https://www.paypal.com/sdk/js?client-id=test&currency=USD"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function() {
            $(document).on('click', '.number-spinner .btn', function() {
                var btn = $(this),
                    oldValue = btn.closest('.number-spinner').find('input[name="qty"]').val().trim();
                newVal = 0;
                var key = $(this).attr('id');
                if (btn.attr('data-dir') == 'up') {
                    newVal = parseInt(oldValue) + 1;
                } else {
                    if (oldValue > 1) {
                        newVal = parseInt(oldValue) - 1;
                    } else {
                        newVal = 1;
                    }
                }
                btn.closest('.number-spinner').find('input[name="qty"]').val(newVal);
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "post",
                    url: '{{ route('update_cart') }}',
                    data: {
                        qty: newVal,
                        key: key,
                        _token: _token
                    },
                    success: function(data) {
                        window.location.reload();
                    }
                });

            });

            $('.address label').click(function() {
                $('.address .active').removeClass('active');
                $(this).addClass('active');
            });

            $('.add_address').click(function() {
                $('#myModal').modal('show');
            });

            $('#change').click(function(e) {
                e.preventDefault();
                var ad_id = $(this).attr('data');
                $.ajax({
                    type: "get",
                    url: "{{ route("change") }}",
                    data: {
                        ad_id: ad_id
                    },
                    success: function (response) {
                        $('#address').html(response);
                    }
                });
            });

            //btn_order js
            $('#btn_order').click(function(e){
                e.preventDefault();
                var form = $(this).closest('form');
                swal("Are you sure you want to order this?", {
                    buttons: ["Cancel", true],
                }).then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            })

        });
    </script>

    <script>
        // Listen for changes to the radio fields
        document.querySelectorAll('input[name=payment_option]').forEach(function(el) {
            el.addEventListener('change', function(event) {

                // If PayPal is selected, show the PayPal button
                if (event.target.value === 'paypal') {
                    document.querySelector('#card-button-container').style.display = 'none';
                    document.querySelector('#paypal-button-container').style.display = 'inline-block';
                    document.querySelector('#paypal-button-container').style.width = '100%';
                }

                // If Card is selected, show the standard continue button
                if (event.target.value === 'cash') {
                    document.querySelector('#card-button-container').style.display = 'inline-block';
                    document.querySelector('#card-button-container').style.width = '100%';
                    document.querySelector('#paypal-button-container').style.display = 'none';
                }
            });
        });

        // Hide Non-PayPal button by default
        document.querySelector('#card-button-container').style.display = 'none';

        // Render the PayPal button into #paypal-button-container
        paypal.Buttons({
            style: {
                label: 'pay',
                color: 'blue',
                height: 35,
                layout: 'horizontal'
            }
        }).render('#paypal-button-container');
    </script>
@endsection
