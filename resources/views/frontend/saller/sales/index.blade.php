@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">

@endsection
@section('css')
    <link href="{{ asset('frontend/css/checkout.css') }}" rel="stylesheet">
    <style>
    .card {
        margin-bottom: 30px;
    }
    .detail span {
        font-size: 14px;
    }
    .card-body {
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        min-height: 1px;
        padding: 1.25rem;
    }
    
    *, ::after, ::before {
        box-sizing: border-box;
    }

    div {
        display: block;
    }
    .card {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0,0,0,.125);
        border-radius: .25rem;
    }

    .text-center {
        text-align: center!important;
    }
    .pb-4, .py-4 {
        padding-bottom: 1.5rem!important;
    }
    
    .pt-4, .py-4 {
        padding-top: 1.5rem!important;
    }
    .mb-4, .my-4 {
        margin-bottom: 1.5rem!important;
    }
    .opacity-70 {
        opacity: 0.7 !important;
    }
    .font-italic {
        font-style: italic!important;
        font-size: 13px;
    }
    
    p {
        margin-top: 0;
        margin-bottom: 1rem;
    }
    .text-primary {
        color: #e62e04 !important;
    }
    .h5 {
        font-size: 20px;
    }
    .mb-3{
        font-size: 27px;
    }
    i.fa.fa-check-circle-o.la-3x.text-success.mb-3::before{
        font-size: 39px;
    }
    .produt_qty{
            display: flex;
            flex-direction: row;
        }
        .color{
            margin-left: 10px;
            width:25px;
            height:25px;
            border:1px solid #000;
            display:inline-block;
            margin-right:10px;
            cursor:pointer;
            text-align:center;
            line-height:35px;
            font-size:12px;
        }
        .address{
            border: 1px solid #ddd;
            margin-left: 25px;
            padding-left: 15px;
            width: 50%;
        }
    </style>
@endsection
@section('content_right')
    <div class="col-sm-9">
        <div class="centent_card_1r clearfix" id="order_list">
            <div class="centent_card_1r3 tab-content">
                <p>Sale history</p>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input class="form-control" type="text" data-field="date" name="date" onchange="date(this.value)" data-format="yyyy-MM-dd" style="margin-right: 10px;" placeholder="Search date">
                        </div>
                        <div id="dtBox"></div>
                    </div>
                </div>

                <div class="card-content" >
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>#</th>
                                <th>Order Number</th>
                                <th>Date</th>
                                <th>Num of Products</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Delivery Status</th>
                                <th>Payment Status</th>
                                <th class="text-right">Options</th>
                            </tr>
                            <tbody id="show_order">
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($sales as $sale)
                                    <tr>
                                        <td scope="row">{{ $i }}</td>
                                        <td>{{ $sale->code }}</td>
                                        <td>{{ $sale->date }}</td>
                                        <td>{{ count(App\Models\Order_detail::where('order_id', $sale->id)->get()) }}</td>
                                        <td>
                                            @if ($sale->user_id)
                                            {{ $sale->user->name }}
                                            @else
                                            Genneral
                                            @endif
                                        </td>
                                        <td>{{ single_price($sale->grand_total) }}</td>
                                        <td>
                                            @if ($sale->delivery_status == 'pending')
                                                <span class="label label-danger">{{ $sale->delivery_status }}</span>
                                            @elseif ($sale->delivery_status == 'confirmed')
                                                <span class="label label-primary">{{ $sale->delivery_status }}</span>
                                            @elseif ($sale->delivery_status == 'on_delivery')
                                                <span class="label label-info">{{ $sale->delivery_status }}</span>
                                            @elseif($sale->delivery_status == 'delivered')
                                                <span class="label label-success">{{ $sale->delivery_status }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($sale->payment_status == 'paid')
                                                <span class="label label-success">Paid</span>
                                            @else
                                                <span class="label label-danger">Unpaid</span>
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if ($sale->payment_status == 'unpaid')
                                            <a href="{{ route('orders.update_payment_status', $sale->id) }}" class="btn btn-xs btn-primary">paid</a>
                                            @endif
                                            <a href="" id="{{ $sale->id }}" class="btn btn-info btn-veiw-detail btn-xs"><i
                                                    class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="center_product_1r4 clearfix">
                <div class="col-sm-6">
                    <div class="center_product_1r4l clearfix">
                        {{ $sales->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- moal detail --}}
    <div class="modal fade" id="order_detail_modal" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div id="order-details-modal-body">

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.btn-veiw-detail').click(function(e) {
                e.preventDefault();
                var order_id = $(this).attr('id');
                $.ajax({
                    method: "get",
                    url: "/sale/sale_detail/"+order_id,
                    success: function (data) {
                        $('#order-details-modal-body').html(data);
                        $('#order_detail_modal').modal('show');
                    }
                });
            });

            $('#search').change(function (e) { 
                e.preventDefault();
                var val = $(this).val();
                $.ajax({
                    type: "get",
                    url: "{{ route("order.search") }}",
                    data: {val: val},
                    success: function (response) {
                        $('#order_list').html(response);
                    }
                });
            });
        });

        function date(val){
            $.ajax({
                type: "get",
                url: "{{ route("sale.search.date") }}",
                data: {date:val},
                success: function (response) {
                    $('#show_order').html(response);
                }
            });
        }

        $(document).ready(function()
        {
        
            $("#dtBox").DateTimePicker();
        
        });
    </script>
@endsection
