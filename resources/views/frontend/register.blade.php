@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/login.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Login</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="login" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="login_1 clearfix">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6 login_1c">
                            <div class="login_1l clearfix">
                                <h4 class="mgt">Privacy and Terms</h4>
                            </div><br>
                            <div class="row">
                                <div class="col-sm-10 login_1l1 clearfix">
                                    @php
                                        $policy = App\Models\Policy::where('policy_type', 'signup')->first();
                                    @endphp
                                    <div class="space_left">
                                        <p>{{ $policy->content }}</p>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="space_left">
                                <label for=""><input type="checkbox" id="agree"> I agree abaut your policy.</label>
                            </div>
                            <br>
                            <div class="login_1l1 clearfix">
                                <form action="{{ route('user.register.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="currency" value="{{ $data['currency'] }}">
                                    <input type="hidden" name="user_type" value="{{ $data['user_type'] }}">
                                    <input type="hidden" name="card_id" value="{{ $data['card_id'] }}">
                                    <input type="hidden" name="card_img" value="{{ $data['card_img'] }}">
                                    <input type="hidden" name="refer_id" value="{{ $data['refer_id'] }}">
                                    <input type="hidden" name="ss_id" value="{{ $data['ss_id'] }}">
                                    <input type="hidden" name="name" value="{{ $data['name'] }}">
                                    <input type="hidden" name="surname" value="{{ $data['surname'] }}">
                                    <input type="hidden" name="dob" value="{{ $data['dob'] }}">
                                    <input type="hidden" name="gender" value="{{ $data['gender'] }}">
                                    <input type="hidden" name="phone" value="{{ $data['phone'] }}">
                                    <input type="hidden" name="address" value="{{ $data['address'] }}">
                                    <input type="hidden" name="country" value="{{ $data['country'] }}">
                                    <input type="hidden" name="city" value="{{ $data['city'] }}">
                                    <input type="hidden" name="postal_code" value="{{ $data['postal_code'] }}">
                                    <input type="hidden" name="email" value="{{ $data['email'] }}">
                                    <input type="hidden" name="password" value="{{ $data['password'] }}">
                                    <button type="submit" class="btn btn-primary" id="sign_up" disabled> Sign up now</button>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
<script src=""></script>
@section('script')
    <script>
        $(document).ready(function () {
            $('#agree').change(function (e) { 
                e.preventDefault();
                if(!$('#agree').is(':checked')){
                    $('#sign_up').prop('disabled', true);
                }else{
                    $('#sign_up').prop('disabled', false);
                }
            });
        });
    </script>
@endsection