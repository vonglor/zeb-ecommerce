@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <div class="col-sm-9">
        <div class="centent_card_1r clearfix">
            <div class="centent_card_1r3 tab-content">
                <p>Wallet Recharge History</p>
                <hr>
                <div class="card-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Amount</th>
                            </tr>
                            @foreach ($money as $row)
                            <tr>
                                <td scope="row">{{ $row->user->name }}</td>
                                <td>{{ date('d-m-Y') }}</td>
                                <td>{{ number_format($row->amount, 2) }}</td>
                            </tr>
                            @endforeach
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection