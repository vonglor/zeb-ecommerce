@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <h2 class="intro-y text-xl font-medium mt-10">
            Request withdraw
        </h2>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                
                <h1 class="text-xl shadow-md mr-2 text-lg">Customer withdraw</h1>
                <div class="float-right">
                    <a href="" class="btn btn-primary text-lg">Pay to Customer</a>
                </div>
                <div class="hidden md:block mx-auto text-gray-600">
                    
                </div>
               
                <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                    
                    <div class="w-56 relative text-gray-700 dark:text-gray-300">
                        <input type="text" class="form-control w-56 box pr-10 placeholder-theme-8" placeholder="Search...">
                        <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i> 
                    </div>
                   
                </div>
            </div>
            
            <div class="intro-y col-span-12 overflow-auto">
                <table class="table table-report -mt-2" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base">#</th>
                            <th class="whitespace-nowrap text-base">ID</th>
                            <th class="whitespace-nowrap text-base">Date Time</th>
                            <th class="whitespace-nowrap text-base">Seller Name</th>
                            <th class="text-center whitespace-nowrap text-base">Balance</th>
                            <th class="text-center whitespace-nowrap text-base">Membership/M</th>
                            <th class="text-center whitespace-nowrap text-base">Taxes 12%</th>
                            <th class="text-center whitespace-nowrap text-base">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 0;
                        @endphp
                        @foreach ($customerwithdraw as $item)
                        <tr class="">
                            <td class="w-10">
                                <h1 class="whitespace-nowrap text-base">{{ ++$i }}</h1> 
                            </td>
                            <td class="w-10">
                                <h1 class="whitespace-nowrap text-base">{{$item->user_id}}</h1> 
                            </td>
                            <td>
                                <h1 class="font-medium whitespace-nowrap text-base">{{$item->created_at}}</h1> 
                            </td>
                            <td class="w-40">
                                <h1 class="font-medium whitespace-nowrap text-base">{{$item->user_id}}</h1> 
                            </td>
                            <td class="text-center text-base">${{$item->amount}}</td>
                            <td class="text-center text-base"><input  type="checkbox" class="form-check-input border-gray-600 mr-2">$10</td>
                            <td class="text-center"><p class="text-red-500 text-base"><input  type="checkbox" class="form-check-input border-gray-600 mr-2">$18</p></td>
                            <td class="table-report__action w-56">
                                <div class="flex justify-center items-center">
                                    <a class="btn btn-primary" href="#" > <i data-feather="check-square" class="w-5 h-4 mr-1"></i></a>
                                    <form action="#" method="POST" class="px-2">
                                        <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-5 h-4 mr-1"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr> 
                        @endforeach 
                        
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
        </div>
    </div>
    <!-- END: Content -->
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();    
        });
    </script>
    
@endsection