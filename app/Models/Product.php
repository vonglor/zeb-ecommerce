<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'added_by',
        'user_id',
        'category_id',
        'brand_id',
        'photos',
        'thumbnail_img',
        'videos',
        'pdf',
        'description',
        'unit_price',
        'purchase_price',
        'attributes',
        'choice_options',
        'colors',
        'unit',
        'min_qty',
        'discount',
        'slug',
        'meta_title',
        'meta_description',
        'shipping_day',
        'tax',
    ];

    /**
     * Get the Category that owns the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * Get the Brand that owns the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * Get the Upload that owns the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Upload()
    {
        return $this->belongsTo(Upload::class, 'photos', 'id');
    }

    public function stocks()
    {
        return $this->hasMany(Product_stock::class);
    }

    public function order_detail()
    {
        return $this->hasMany("App\Models\Order_detail", "product_id", "id");
    }

}