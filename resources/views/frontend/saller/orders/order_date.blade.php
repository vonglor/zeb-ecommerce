
@if (count($orders) > 0)
    @php
    $i = 1;
    @endphp
    @foreach ($orders as $order)
    <tr>
        <td scope="row">{{ $i }}</td>
        <td>{{ $order->code }}</td>
        <td>{{ $order->date }}</td>
        <td>{{ count(App\Models\Order_detail::where('order_id', $order->id)->get()) }}</td>
        <td>
            @if ($order->user_id)
            {{ $order->user->name }}
            @else
            Genneral
            @endif
            
        </td>
        <td>${{ number_format($order->grand_total, 2) }}</td>
        <td>
            @if ($order->delivery_status == 'pending')
                                                <span class="label label-danger">{{ $order->delivery_status }}</span>
                                            @elseif ($order->delivery_status == 'confirmed')
                                                <span class="label label-primary">{{ $order->delivery_status }}</span>
                                            @elseif ($order->delivery_status == 'on_delivery')
                                                <span class="label label-info">{{ $order->delivery_status }}</span>
                                            @elseif($order->delivery_status == 'delivered')
                                                <span class="label label-success">{{ $order->delivery_status }}</span>
                                            @endif
        </td>
        <td>
            @if ($order->payment_status == 'paid')
                <span class="label label-success">Paid</span>
            @else
                <span class="label label-danger">Unpaid</span>
            @endif
        </td>
        <td class="text-right">
            <a href="" id="{{ $order->id }}" class="btn btn-info btn-veiw-detail btn-xs"><i class="fa fa-eye"></i></a>
        </td>
    </tr>
    @php
        $i++;
    @endphp
    @endforeach
@else
    <tr>
        <td colspan="9" class="text-center">Data not found...</td>
    </tr>
@endif
