@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
                   <div class="content">
                    <div class="intro-y flex items-center mt-8">
                        <h2 class="text-lg font-medium mr-auto text-xl">
                            Add New Permmission
                        </h2>
                    </div>
                    <div class="grid grid-cols-12 gap-6 mt-5">
                        <div class="intro-y col-span-12 lg:col-span-6 ">
                            <!-- BEGIN: Vertical Form -->
                            <form action="">
                            <div class="intro-y box">
                                <div id="vertical-form" class="p-5">
                                    <div class="preview">
                                        <div>
                                            <label for="vertical-form-1" class="form-label text-base">Role Name</label>
                                            <input id="vertical-form-1" type="text" name="name" class="form-control border-gray-500 text-base" placeholder="Name" required>
                                        </div>
                                        <!-- BEGIN: Social Media Button -->
                                        <div class="intro-y box mt-5">
                                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                                <h2 class="font-medium text-base mr-auto text-base">
                                                    Products
                                                </h2>
                                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                    <input id="show-example-5" data-target="#social-media-button" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                                </div>
                                            </div>
                                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                                <h2 class="font-medium text-base mr-auto text-base">
                                                    All Orders
                                                </h2>
                                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                    <input id="show-example-5" data-target="#social-media-button" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                                </div>
                                            </div>
                                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                                <h2 class="font-medium text-base mr-auto text-base">
                                                    Inhouse orders
                                                </h2>
                                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                    <input id="show-example-5" data-target="#social-media-button" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                                </div>
                                            </div>
                                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                                <h2 class="font-medium text-base mr-auto text-base">
                                                    Seller Orders
                                                </h2>
                                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                    <input id="show-example-5" data-target="#social-media-button" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                                </div>
                                            </div>
                                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                                <h2 class="font-medium text-base mr-auto text-base">
                                                    Sellers
                                                </h2>
                                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                    <input id="show-example-5" data-target="#social-media-button" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                                </div>
                                            </div>
                                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                                <h2 class="font-medium text-base mr-auto text-base">
                                                    Customers
                                                </h2>
                                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                    <input id="show-example-5" data-target="#social-media-button" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                                </div>
                                            </div>
                                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                                <h2 class="font-medium text-base mr-auto text-base">
                                                    Marketings
                                                </h2>
                                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                    <input id="show-example-5" data-target="#social-media-button" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                                </div>
                                            </div>
                                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                                <h2 class="font-medium text-base mr-auto text-base">
                                                    Support
                                                </h2>
                                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                    <input id="show-example-5" data-target="#social-media-button" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                                </div>
                                            </div>
                                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                                <h2 class="font-medium text-base mr-auto text-base">
                                                    Website Setup
                                                </h2>
                                                <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                    <input id="show-example-5" data-target="#social-media-button" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                                </div>
                                            </div>
                                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                                <button type="submit" class="btn btn-primary w-32 mr-2 mb-2"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Save </button> 
                                            </div>
                                            
                                        </div>
                                        <!-- END: Social Media Button -->
                                        
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
<!-- END: Content -->
    
@endsection