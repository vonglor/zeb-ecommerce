<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_type',
        'name',
        'surname',
        'email',
        'password',
        'country',
        'state',
        'city',
        'postal_code',
        'phone',
        'gender',
        'date_of_birth',
        'refer_id',
        'refer_code_others',
        'currency',
        'level1',
        'level2',
        'level3',
        'level4',

    ];
    public function Customer()
    {
        return $this->hasmany(Customer::class);
    }
    public function Seller()
    {
        return $this->hasmany(Seller::class);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_withdraw_request()
    {
        return $this->hasMany("App\Models\WithdrawRequest", "user_id", "id");
    }

    public function user_order()
    {
        return $this->hasMany("App\Models\Order", "user_id", "id");
    }

    public function staff()
    {
        return $this->hasOne(Staff::class);
    }

    public function user_seller(Type $var = null)
    {
        return $this->hasMany("App\Models\Seller", "user_id", "id");
    }

}