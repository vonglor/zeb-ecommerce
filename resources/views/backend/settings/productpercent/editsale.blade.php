@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">
                Edit Percent sale
            </h2>
        </div>
        <div class="grid grid-cols-6 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
                <!-- BEGIN: Vertical Form -->
                <form action="{{route('update/percentsale', $editpercentsale->id)}}" method="POST">
                    @csrf
                <div class="intro-y box">
                    <div id="vertical-form" class="p-5">
                        <div class="preview">
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Start Amount</label>
                                <input id="vertical-form-1" type="text" name="start_amount" class="form-control border-gray-500 text-base" value="{{$editpercentsale->start_amount}}" placeholder="Start Amount" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">End Amount</label>
                                <input id="vertical-form-1" type="text" name="end_amount" class="form-control border-gray-500 text-base" value="{{$editpercentsale->end_amount}}"  placeholder="End Amount" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Type Payment</label>
                                <select class="form-select mt-2 sm:mr-2 border-gray-500" name="name" aria-label="Default select example" required>
                                    <option value="">Select Type Payment</option>
                                    @foreach (\App\Models\Percent::all() as $key => $percentsale)
                                    <option value="{{ $percentsale->name }}">{{ $percentsale->name }}</option>
                                    @endforeach
                                </select>
                                {{-- <input id="vertical-form-1" type="text" name="name" class="form-control border-gray-500 text-base"  placeholder="Type Payment" value="{{$editpercentsale->name}}" required> --}}
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Percent</label>
                                <input id="vertical-form-1" type="number" step="any" name="percent" class="form-control border-gray-500 text-base"  placeholder="Percent" value="{{$editpercentsale->percent}}" required>
                            </div>
                            <div>
                            <div class="mt-10">
                                <button type="submit" class="btn btn-primary w-32 mr-2 mb-2 text-base"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Edit</button> 
                            </div>

                        </div>
                    </div>
                </div>
                </form>
            </div> 
        </div>
    </div>
<!-- END: Content -->
    
@endsection