@extends('frontend.saller.content_left')
@section('css')
<link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <form action="{{ route('saller.message.store') }}" method="POST">
        @csrf
        <div class="col-sm-9">
            <p class="title_header">Wrete Your Message</p>
            <div class="centent_card_1r clearfix">
                <div class="centent_card_1r3 tab-content">
                    <div class="card-content">
                        <div class="row g-3 align-items-center">
                            <div class="row col-sm-12 field">
                                <div class="col-sm-3">
                                    <label for="description" class="col-form-label">Message</label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="message" id="description" cols="30" rows="10"
                                        class="form-control" placeholder="Write your message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center" style="margin-bottom: 25px;">
                    <button class="btn btn-primary"> Send</button>
                </div>
            </div>
        </div>
    </form>

@endsection
