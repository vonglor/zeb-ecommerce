<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::latest()->paginate(10);
        return view('backend.products.category.index',compact('category'))
        ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function category()
    {
        $category = Category::paginate(10);
        return view('backend.products.product.create', compact('category'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.products.category.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:categories,name',
            'banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',  
        ],
        [
          'name.unique' => 'The Category Name is Already taken'
        ]
    );

        $input = $request->all();

        if ($banner = $request->file('banner')) {
            $destinationPath = 'uploads/images/';
            $profileImage = date('YmdHis') . "." . $banner->getClientOriginalExtension();
            $banner->move($destinationPath, $profileImage);
            $input['banner'] = "$profileImage";
        }
    
        Category::create($input);

        if($input){
            $notification = array(
                'message' => 'Insert successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('category.index')->with($notification);
        }else{
            $notification = array(
                'message' => 'Insert warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('category.index')->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return view('backend.products.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $category = Category::find($id);
       return view('backend.products.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required'
        ]);

        
        $input = $request->all();
  
        if ($banner = $request->file('banner')) {
            $destinationPath = 'uploads/images/';
            $profileImage = date('YmdHis') . "." . $banner->getClientOriginalExtension();
            $banner->move($destinationPath, $profileImage);
            $input['banner'] = "$profileImage";
        }else{
            unset($input['banner']);
        }
          
        $category->update($input);

        

        if ($category) {
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('category.index')->with($notification);
        } else {
            $notification = array(
                'message' => 'Update warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('category.index')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        if ($category) {
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('category.index')->with($notification);
        } else {
            $notification = array(
                'message' => 'Delete warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('category.index')->with($notification);
        }
        
        
    }
}
