<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 use App\Models\About;
 use Brian2694\Toastr\Facades\Toastr;
class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $about_us = About::all();
        return view('backend.settings.about.index', compact('about_us'));
    }

    public function update(Request $request, About $about_us)
    {
        $request->validate([
            "about_us" => ['required'],
        ]);

        $about_us->about_us = $request->about_us;
        $about_us->save();

        toastr()->success('Save', 'Successfully', ['timeOut' => 2000]);
        return redirect(route('about'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // $about_us = $request->all();

        // About::create($about_us);

        // if ($about_us) {
        //     $notification = array(
        //         'message' => 'Insert successfully.',
        //         'alert-type' => 'success'
        //     );
        //     return redirect()->route('about-us')->with($notification);
        // } else {
        //     $notification = array(
        //         'message' => 'Insert warning!!.',
        //         'alert-type' => 'warning'
        //     );
        //     return redirect()->route('about-us')->with($notification);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
