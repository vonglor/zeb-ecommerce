@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <h2 class="intro-y text-xl font-medium mt-10">
            All Orders
        </h2>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                <h1 class="text-xl shadow-md mr-2 text-lg">Order Information</h1>
                <div class="hidden md:block mx-auto text-gray-600"></div>
                <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                    <div class="w-56 relative text-gray-700 dark:text-gray-300">
                        <input type="text" class="form-control w-56 box pr-10 placeholder-theme-8 text-base" placeholder="Search...">
                        <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i> 
                    </div>
                </div>
            </div>
            <!-- BEGIN: Data List -->
            <div class="intro-y col-span-12 overflow-auto lg:overflow">
                <table class="table table-report -mt-2" id="dataTable">
                    <thead>
                        
                        <tr>
                            <th class="whitespace-nowrap text-base">#</th>
                            <th class="whitespace-nowrap text-base">Order Code</th>
                            <th class="whitespace-nowrap text-base">Num</th>
                            <th class="text-center whitespace-nowrap text-base">Customer</th>
                            <th class="text-center whitespace-nowrap text-base">Order Date</th>
                            <th class="text-center whitespace-nowrap text-base">Amount</th>
                            <th class="text-center whitespace-nowrap text-base">Delivery Status</th>
                            <th class="text-center whitespace-nowrap text-base">Payment Status</th>
                            <th class="text-center whitespace-nowrap text-base">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                            @php
                            $i = 0;
                        @endphp
                        @foreach ($orders as $order)
                        <tr class="intro-x">
                            <td class="w-40">
                                <h1 class="font-medium whitespace-nowrap text-base">{{++$i}}</h1> 
                            </td>
                            <td>
                                <h1 class="font-medium whitespace-nowrap text-base">{{$order->id}}</h1> 
                            </td>
                            <td class="text-center text-base">{{$order->seller_id}}</td>
                            <td class="text-center text-base">{{$order->user_id}}</td>
                            <td class="text-center text-base">{{$order->date}}</td>
                            <td class="text-center text-base">{{$order->grand_total}}</td>
                            <td class="text-center text-base">{{$order->delivery_status}}</td>
                            <td class="w-40">
                                <div class="flex items-center justify-center text-theme-10 text-base"> <i data-feather="check-square" class="w-4 h-4 mr-2"></i> {{$order->payment_status}} </div>
                            </td>
                            <td class="table-report__action w-56 p-5">
                                <div class="flex justify-center items-center">
                                    <a class="btn btn-primary" href="{{route('detail.order', $order->id)}}">  <i class="w-4 h-3 mr-2" data-feather="eye"></i></a>
                                    <form action="#" method="POST" class="px-2">
                                        <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
        </div>
    </div>
    <!-- END: Content -->

    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();    
        });
    </script>
    
@endsection