@extends('frontend.saller.content_left')
@section('css')
<link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
<div class="col-sm-9">
    <div class="centent_card_1r clearfix">
        <div class="centent_card_1r3 tab-content">
            <p>Import products</p>
            <hr>
            <div class="card-content">
                <form action="{{ route('producRecive.store') }}" method="post">
                    @csrf
                    <div class="row col-sm-12 field">
                        <div class="col-sm-2">
                            <label for="Brands" class="col-form-label">Product name</label>
                        </div>
                        <div class="col-sm-8">
                            <select name="product_id" id="product_id" class="form-control selectpicker" data-live-search="true" required>
                                <option value="">Select Product</option>
                                @foreach (App\Models\Product::where('user_id', user()->id)->get() as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row col-sm-12 field" id="variantion">
                        <div class="col-sm-2">
                            <label for="Brands" class="col-form-label">Variantion</label>
                        </div>
                        <div class="col-sm-8">
                                <select name="variantion" class="form-control selectpicker" data-live-search="true">
                                    <option value="">Select variantion</option>
                                </select>                            
                        </div>
                    </div>
                    
                    <div class="row col-sm-12 field">
                        <div class="col-sm-2">
                            <label for="Brands" class="col-form-label">Quantity</label>
                        </div>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" name="qty" value="1">
                        </div>
                    </div>

                    <div class="row col-sm-12 field">
                        <div class="col-sm-2">
                            <label for="Brands" class="col-form-label">Price</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" name="price">
                        </div>
                        <div class="col-sm-2">
                            <select name="currency" class="form-control" id="">
                                @foreach (App\Models\Currency::where('status', 1)->get() as $item)
                                <option value="{{ $item->code }}" @if ($item->code == currency_code_account())
                                    selected
                                @endif>{{ $item->code }} ({{ $item->symbol }})</option>
                                @endforeach
                                
                            </select>
                        </div>
                    </div>
                    
                    <div class="row col-sm-12 field">
                        <div class="col-sm-12 text-center mb-5" style="margin-bottom: 10px;">
                            <button class="button" id="btn_save"> Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    $(document).ready(function () {
    
        $('#product_id').change(function (){
            var product_id = $(this).val();
            $.ajax({
                type: "get",
                url: "{{ route("variantion") }}",
                data: {product_id: product_id},
                success: function (response) {
                    $('#variantion').html(response);
                    $('.my-select').selectpicker();
                }
            });
        });



    });

    

</script>
@endsection