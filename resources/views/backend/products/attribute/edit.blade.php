@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto text-xl">
                Edit Attribute
            </h2>
        </div>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
                <!-- BEGIN: Vertical Form -->
                <form action="{{route('attribute.update', $attribute->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                <div class="intro-y box">
                    <div id="vertical-form" class="p-5">
                        <div class="preview">
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Name</label>
                                <input id="vertical-form-1" type="text" name="name" value="{{$attribute->name}}" class="form-control border-2 border-gray-500 text-base" placeholder="Name" required>
                                @error('name')
                                <span style="color: red;">{{ $message }}</span>
                                @enderror
                            </div>
                                <div class="mt-3">
                                    <button type="submit" class="btn btn-primary mt-5">Edit</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            </div>
        </div>
    </div>
<!-- END: Content -->
    
@endsection