
@php
$user_id = Session::get('user')->id;
$user_type = Session::get('user')->user_type;
$user = App\Models\User::find($user_id);
@endphp
@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/product-pos.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <style>
        label {
            font-size: 14px;
        }
        table {
            font-size: 14px;
        }
    </style>
@endsection
@section('content')
<section id="center" class="center_shop clearfix">
   <div class="container">
       <div class="row">
           <div class="center_shop_1 clearfix">
               <div class="col-sm-12">
                   <h5 class="mgt">
                       POS Manager
                   </h5>
               </div>
           </div>
       </div>
   </div>
</section>
<section id="center" class="clearfix center_product" style="background: #f4f4f4;" >
   <div class="container_body">
       <div class="row">
           <div class="centent_card clearfix">
               <div class="col-sm-6">
                   <div class="card">
                       <div class="card-header">
                           <h5 class="card-title">
                               <div class="row">
                                   <div class="col-sm-6">
                                    POS manager
                                   </div>
                                   <div class="col-sm-6">
                                    <form class="" role="search" action="{{ route('pos.search') }}" method="GET">
                                        <div class="input-group">
                                            <input type="text" name="search" class="form-control" placeholder="Search product name">
                                            <span class="input-group-btn">
                                                <button class="btn btn-search" type="submit"> Search</button>
                                            </span>
                                        </div>
                                    </form>
                                   </div>
                               </div>
                           </h5>
                       </div>
                       <div class="card-body">
                        <div class="row">
                            <div class="product_list text-left clearfix row">
                                <div class="col-sm-5 category">
                                    <input type="text" name="search" id="search" oninput="search(this.value)" class="form-control" placeholder="Search Item ID">
                                    @csrf
                                </div>
                                <div class="col-sm-5 category">
                                    <select name="category" id="category" class="form-control" onchange="category(this.value)">
                                        <option value="all">All products</option>
                                        @foreach (App\Models\Category::all() as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    @csrf
                                </div>
                            </div>
                            <div id="showProduct">
                                <div class="gallery_1 clearfix">
                                    <div class="col-sm-12">
                                        <div class="workout_page_1_left clearfix">
                                            <div class="tab-content clearfix">
                                                <div id="home" class="tab-pane fade  clearfix active in">
                                                    <div class="click clearfix">
                                                        <div class="home_inner clearfix">
                                                            @foreach ($products as $product)
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 offset-md-0 offset-sm-1 pt-md-0 pt-4">
                                                                <div class="card_product card-product">
                                                                    @if ($product->image)
                                                                    <img src="{{ url(App\Models\Upload::find($product->image)->file_name) }}" class="img-responsive" alt="Responsive image">
                                                                    @else
                                                                    <img src="{{ url($product->Upload->file_name) }}" class="img-responsive" alt="Responsive image">
                                                                    @endif
                                                                    <div class="card_product-body">
                                                                        <h5 class="font-weight-bold pt-1">{{ Str::limit($product->name, 12) }}</h5>
                                                                        <h5 class="font-weight-bold pt-1">{{ $product->variant }}</h5>
                                                                        <div class="d-flex align-items-center justify-content-between pt-3">
                                                                            <div class="d-flex flex-column">
                                                                                <div class="h5 font-weight-bold">
                                                                                    {{ format_product_price($product->price - $product->discount, $product->currency_code) }} 
                                                                                </div>
                                                                                <a href="{{ route('pos.add_to_cart', $product->id) }}" class="add_cart btn btn-primary btn-xs"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_1_last text-center clearfix">
                                    <div class="col-sm-12">
                                        <ul>
                                            {!! $products->links() !!}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                       </div>
                   </div>
               </div>
               <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">
                                POS Cart
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                        <tr>
                                            <th>ITEM</th>
                                            <th>NAME</th>
                                            <th width="23%" class="text-center">QTY</th>
                                            <th>PRICE</th>
                                            <th>DELETE</th>
                                        </tr>
                                        @if (Session::get('pos_cart') != null && count(Session::get('pos_cart')) > 0)
                                        @php 
                                        $sub = 0; 
                                        @endphp
                                        @foreach (Session::get('pos_cart') as $key => $row)
                                            @php
                                                $product = App\Models\Product::find($row['pro_id']);
                                                $sub += $row['price'] * $row['qty'];
                                            @endphp
                                            <tr>
                                                <td>
                                                    <img src="{{ url(App\Models\Upload::find($row['image'])->file_name) }}"
                                                    class="iw" alt="abc"
                                                    style="width:80px; height:80px;">
                                                </td>
                                                <td>
                                                    {{ $row['name'] }}
                                                    <p>{{ $row['variant'] }}</p></td>
                                                <td>
                                                    <div class="input-group number-spinner">
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-default" id="{{ $key }}"
                                                                data-dir="dwn"><span
                                                                    class="glyphicon glyphicon-minus"></span></button>
                                                        </span>
                                                        <input type="hidden" name="key" value="{{ $key }}">
                                                        <input type="text" name="qty" class="form-control text-center"
                                                            value="{{ $row['qty'] }}">
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-default" id="{{ $key }}"
                                                                data-dir="up"><span
                                                                    class="glyphicon glyphicon-plus"></span></button>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td>{{ currency_symbol_account().pos_price($row['price'], $product->currency_code) }}</td>
                                                <td class="text-center">
                                                    <a href="{{ route('pos.remove_cart', $key) }}" class="btn btn-danger btn-xs"><i
                                                    class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                </table>
                            </div>
                            @if (Session::get('pos_cart') != null && count(Session::get('pos_cart')) > 0)
                                <form action="{{ route('pos.checkout') }}" method="POST">
                                    @csrf

                                    <div class="checkout_1r clearfix">
                                        <h4 class="mgt">Cart totals</h4>
                                        <hr>
                                        <h5>Sub Total <span class="pull-right">{{currency_symbol_account().format_number($sub) }}</span></h5>
                                        @php
                                            $total = $sub;
                                        @endphp
                                        <h5>Shipping <span class="pull-right"> free</span>
                                        </h5>
                                        <h5>Total <span class="pull-right">{{ currency_symbol_account().number_format($total, 2) }}</span></h5><br>
                                        <input type="hidden" name="total" value="{{ $total }}">
                                        <input type="hidden" name="currency_code" value="{{ currency_code_account() }}">

                                        <h4>Customers</h4>
                                        <hr> 
                                        <div class="form-group">
                                            <label>
                                                <input type="radio" name="member_type" class="member_type" value="member" required> Member 
                                            </label>
                                            &nbsp;&nbsp;
                                            <label>
                                                <input type="radio" name="member_type" class="member_type" value="general" required> General
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <div id="show_member">
                                                
                                            </div>
                                        </div>

                                        <h4>Payment options</h4>
                                        <hr>
                                        <div class="form-group">
                                            <label>
                                                <input type="radio" name="payment_type" value="paypal" required> Pay with PayPal
                                                <img src="{{ url('uploads/cards/paypal-mark.jpg') }}" alt="Pay with Paypal"
                                                    style="height: 40px;">
                                            </label>
                                            <br>
                                            <label>
                                                <input type="radio" name="payment_type" value="cash" required> Pay with Cash
                                                <img src="{{ url('uploads/cards/cash.png') }}" alt="Pay with cash"
                                                    style="height: 40px;">
                                            </label>
                                        </div>
                                        <button class="btn btn-primary">Order now</button>
                                    </div>
                                </form>                                        
                            @endif        
                        </div>
                    </div>
               </div>
            </div>
       </div>
   </div>
</section>
    
@endsection

@section('script')
<script src="https://www.paypal.com/sdk/js?client-id=test&currency=USD"></script>
    <script>
        $(document).ready(function () {

            $(document).on('click', '.number-spinner .btn', function() {
                var btn = $(this),
                    oldValue = btn.closest('.number-spinner').find('input[name="qty"]').val().trim();
                newVal = 0;
                var key = $(this).attr('id');
                if (btn.attr('data-dir') == 'up') {
                    newVal = parseInt(oldValue) + 1;
                } else {
                    if (oldValue > 1) {
                        newVal = parseInt(oldValue) - 1;
                    } else {
                        newVal = 1;
                    }
                }
                btn.closest('.number-spinner').find('input[name="qty"]').val(newVal);
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "post",
                    url: '{{ route('pos.update_cart') }}',
                    data: {
                        qty: newVal,
                        key: key,
                        _token: _token
                    },
                    success: function(data) {
                        window.location.reload();
                    }
                });

            });

            $('.member_type').change(function(){
                var type = $(this).val();
                if(type == 'member'){
                    $.ajax({
                        type: "get",
                        url: "{{ route("select.user_id") }}",
                        success: function (response) {
                            $('#show_member').html(response);
                            $('.my-select').selectpicker();
                        }
                    });
                }else{
                    $('#show_member').html('');
                }
            })

            $('.btn-order').click(function(e){
                e.preventDefault();
                $('#pos_order').modal('show');
            })
        });

        function category(val){
            $.ajax({
                type: "get",
                url: "/pos/category/"+val,
                success: function (response) {
                    $('#showProduct').html(response);
                }
            });
        }

       function search(val){
           $.ajax({
               type: "get",
               url: "/pos/search_id/"+val,
               success: function (data) {
                   $('#showProduct').html(data);
               }
           });
       }
        // function pos_detail(val){
        //     var id = val;
        //     $.ajax({
        //         method: "get",
        //         url: "/pos/product_detail/"+id,
        //         success: function (data) {
        //             $('#pos_detail-modal-body').html(data);
        //             $('#pos_detail').modal('show');
        //        }
        //     });     
        // }
    </script>
    <script>
        // Listen for changes to the radio fields
        document.querySelectorAll('input[name=payment_option]').forEach(function(el) {
            el.addEventListener('change', function(event) {

                // If PayPal is selected, show the PayPal button
                if (event.target.value === 'paypal') {
                    document.querySelector('#card-button-container').style.display = 'none';
                    document.querySelector('#paypal-button-container').style.display = 'inline-block';
                    document.querySelector('#paypal-button-container').style.width = '100%';
                }

                // If Card is selected, show the standard continue button
                if (event.target.value === 'cash') {
                    document.querySelector('#card-button-container').style.display = 'inline-block';
                    document.querySelector('#card-button-container').style.width = '100%';
                    document.querySelector('#paypal-button-container').style.display = 'none';
                }
            });
        });

        // Hide Non-PayPal button by default
        document.querySelector('#card-button-container').style.display = 'none';

        // Render the PayPal button into #paypal-button-container
        paypal.Buttons({
            style: {
                label: 'pay',
                color: 'blue',
                height: 35,
                layout: 'horizontal'
            }
        }).render('#paypal-button-container');
    </script>
@endsection

