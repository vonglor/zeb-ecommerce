<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Hash;
use Illuminate\Http\Request;
use Session;

class HomeController extends Controller
{

    public function index()
    {
        $products = Product::paginate(16);
        return view('frontend.index', \compact('products'));
    }

    public function product_detail($id)
    {
        $detail = Product::find($id);
        $detail->increment('views');
        return view('frontend.product_detail', \compact('detail'));
    }

    public function checkout()
    {
        return view('frontend.checkout');
    }

    public function registration($user_type)
    {
        return view('frontend.registration', compact('user_type'));
    }

    public function contact()
    {
        return view('frontend.contact');
    }

    public function sign_in()
    {
        return view('frontend.login');
    }

    public function product()
    {
        $product = Product::paginate(20);
        return view('frontend.product', \compact('product'));
    }

    public function cart()
    {
        return view('frontend.cart');
    }

    public function login(Request $request)
    {
        $user_id  = $request->user_id;
        $password = $request->password;
        $email    = $request->email;
        $user     = User::find($user_id);
        if ($user) {
            // $user = User::where('email', $email)->first();
            if ($user->email == $request->email) {
                if (Hash::check($password, $user->password)) {
                    $request->session()->put('user', $user);
                    if ($user->user_type == "buy") {
                        $notification = array(
                            'message'    => 'Login successfully.',
                            'alert-type' => 'success',
                        );
                        return redirect()->route('account')->with($notification);
                    }elseif($user->user_type=="sale"){
                        $notification = array(
                            'message'    => 'Login successfully.',
                            'alert-type' => 'success',
                        );
                        return redirect()->route('account')->with($notification);
                    }else{
                        $notification = array(
                            'message'    => 'Login successfully.',
                            'alert-type' => 'success',
                        );
                        return redirect()->route('sale.product.index')->with($notification);
                    }

                } else {
                    $notification = array(
                        'message'    => 'Your password invalied.',
                        'alert-type' => 'warning',
                    );
                    return back()->with($notification);
                }
            } else {
                $notification = array(
                    'message'    => 'Your email invalied.',
                    'alert-type' => 'warning',
                );
                return back()->with($notification);
            }
        } else {
            $notification = array(
                'message'    => 'Your ID invalied.',
                'alert-type' => 'warning',
            );
            return back()->with($notification);
        }

    }

    public function login_buy(Request $request)
    {
        $password = $request->password;
        $email    = $request->email;
        $user     = User::where('email', $email)->first();
        if ($user) {
            if (Hash::check($password, $user->password)) {
                $request->session()->put('user', $user);
                if ($user->user_type == "buy") {
                    $notification = array(
                        'message'    => 'Login successfully.',
                        'alert-type' => 'success',
                    );
                    return back()->with($notification);
                } elseif ($user->user_type == "sale") {
                    $notification = array(
                        'message'    => 'Login successfully.',
                        'alert-type' => 'success',
                    );
                    return back()->with($notification);
                } else {
                    $notification = array(
                        'message'    => 'Login successfully.',
                        'alert-type' => 'success',
                    );
                    return back()->with($notification);
                }

            } else {
                $notification = array(
                    'message'    => 'Your password invalied.',
                    'alert-type' => 'warning',
                );
                return back()->with($notification);
            }
        } else {
            $notification = array(
                'message'    => 'Your email invalied.',
                'alert-type' => 'warning',
            );
            return back()->with($notification);
        }
    }

    public function logout(Request $request)
    {
        $request->session()->forget('user');
        return redirect()->route('index');
    }

    public function dashboard()
    {
        $order = Order::all();
        $pd    = Product::selectRaw('category_id, COUNT(category_id) as amount')
            ->groupByRaw('category_id')
            ->paginate(10);
        return view('frontend.saller.dashboard', compact('pd', 'order'));
    }

    public function account()
    {
        return view('frontend.saller.account');
    }

    public function test()
    {
        return view('backend.settings.productpercent.test');
    }
}
