@extends('frontend.layouts.app')
@section('css')
<link href="{{ asset('frontend/css/index.css') }}" rel="stylesheet">
<link href="{{ asset('frontend/css/product-list.css') }}" rel="stylesheet">
@endsection
	@section('content')
	<section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Product List</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="product" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="product_1 clearfix">
                    
                    <div class="col-sm-12">
                        <div class="home_inner clearfix">
                            @if ($product != null && count($product) > 0)
                                @foreach ($product as $row)
                                <div class="col-sm-3 offset-md-0 offset-sm-1 pt-md-0 pt-4">
                                    <div class="card"> <a href="{{ route('product_detail', $row->id) }}"><img class="card-img-top img-responsive" src="{{ url($row->Upload->file_name) }}"></a>
                                        <div class="card-body">
                                            <h5 class="font-weight-bold pt-1"><a href="{{ route('product_detail', $row->id) }}">{{ Str::limit($row->name, 27) }}</a></h5>
                                            <div class="d-flex align-items-center justify-content-between pt-3">
                                                <div class="d-flex flex-column">
                                                    <div class="h5 font-weight-bold">
                                                        @if ($row->discount)
															<span class="col_2">{{ convert_price($row->unit_price, $row->currency_code) }}</span>
														@endif
														{{ convert_price($row->unit_price-$row->discount, $row->currency_code) }} 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @else
                               <p>Not fount product this values....</p>
                            @endif
                            
                        </div>
                        <div class="product_1_last1 text-center clearfix" style="margin-top: 25px;">
                            {!! $product->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	@endsection
<script src=""></script>
	