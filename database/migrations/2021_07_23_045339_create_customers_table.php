<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('card_id', 20);
            $table->string('card_img');
            $table->string('ss_id', 20)->nullable();
            $table->string('bank_name', 200);
            $table->string('bank_acc_name', 200);
            $table->string('bank_acc_no', 50);
            $table->tinyInteger('bank_payment_status')->default('0');
            $table->tinyInteger('verification_status')->default('0');
            $table->text('verification_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
