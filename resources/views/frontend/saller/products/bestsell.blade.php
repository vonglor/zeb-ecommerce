@extends('frontend.saller.content_left')
@section('css')
<link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
<div class="col-sm-9">
    <div class="centent_card_1r clearfix">
        <div class="centent_card_1r3 tab-content">
            <p>Products best sale</p>
            <div class="row">
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input class="form-control" type="text" data-field="date" name="date" onchange="date(this.value)" data-format="yyyy-MM-dd" style="margin-right: 10px;" placeholder="Search date">
                        </div>
                        <div id="dtBox"></div>
                    </div>

                    <div class="col-sm-3">
                        <p>All products: {{ countProductBestsale() }}</p>
                    </div>
                    {!! Form::open(['method'=>'get', 'route'=>'saller.product.search']) !!}
                        <div class="col-sm-5">
                            <div class="input-group">
                                {!! Form::text('search', '', ['class'=>'form-control', 'placeholder'=>'Search product name...']) !!}
                                <span class="input-group-btn">
                                    {!! Form::submit('Search', ['class'=>'btn btn-search']) !!}
                                </span>
                            </div><!-- /input-group -->
                        </div>
                    {!! Form::close() !!}
            </div>
            <div class="card-content">
                <div class="table-responsive">
                    <table class="table table-hover">
                      <tr>
                          <th>#</th>
                          <th>Image</th>
                          <th width="">Name</th>
                          <th>Onhand</th>
                          <th>Sold out</th>
                          <th>Price sale</th>
                          <th class="text-right">Total sale</th>
                      </tr>
                      <tbody id="show_product">
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($product as $row)
                        <tr>
                            <td scope="row">{{ $i }}</td>
                            <td>
                                <a data-lightbox="{{$row->Upload->file_name}}" data-title="{{$row->Upload->file_name}}" href="{{URL::to($row->Upload->file_name)}}" target="_blank">
                                <img src="{{ url($row->Upload->file_name) }}" alt="Snow" style="max-width: 50px; max-height: 50px">
                                </a>
                            </td>
                            <td>
                                <?php
                                $array = explode( "\n", wordwrap( $row->name, 30));
                                echo implode("<br>",$array);
                                ?>
                            </td>
                            <td>
                                @php
                                    $pro_stock = App\Models\Product_stock::where('product_id', $row->id)->get();
                                    $qty = 0;
                                    foreach($pro_stock as $pt){
                                        $qty += $pt->qty;
                                    }
                                    echo $qty;
                                @endphp
                            </td>
                            <td>{{ $row->num_of_sale }}</td>
                            <td>{{ format_product_price($row->unit_price, $row->currency_code) }}</td>
                            <td class="text-right">
                                {{ format_product_price($row->num_of_sale * $row->unit_price, $row->currency_code) }}
                            </td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                        @endforeach
                        </tbody>
                    </table>
                  </div>
            </div>
        </div>
        <div class="center_product_1r4 clearfix">
            <div class="col-sm-6">
                <div class="center_product_1r4l clearfix">
                    {{ $product->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script>
         $(document).ready(function () {
            $(".btn_delete").click(function (e) { 
            var form = $(this).closest('form');
            e.preventDefault();
            swal("Are you sure you want to do this?", {
                buttons: ["Cancel", true],
            }).then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
            });
        });

        $(document).ready(function()
        {
        
            $("#dtBox").DateTimePicker();
        
        });

        function date(val){
            $.ajax({
                type: "get",
                url: "{{ route("bast.search") }}",
                data: { data: val },
                success: function (response) {
                    $('#show_product').html(response);
                }
            });
        }
    </script>
    
@endsection