@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto text-xl">
                Add New Brand
            </h2>
        </div>
        <div class="grid grid-cols-6 gap-12 mt-5">
                <form action="{{route('brand.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="intro-y box">
                        <div id="vertical-form" class="p-5">
                            <div class="preview">
                                <div>
                                    <label for="vertical-form-1" class="form-label text-base">Name</label>
                                    <input id="vertical-form-1" type="text" class="form-control border-gray-500 text-base" name="name" placeholder="Name" required>
                                    @error('name')
                                    <span style="color: red;">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div>
                                    <label for="vertical-form-1" class="form-label">Logo</label>
                                    <input id="vertical-form-1" type="file" class="form-control cursor-pointer border-gray-500" name="logo" >
                                </div>
                                <div>
                                    <label for="vertical-form-1" class="form-label text-base">Slug</label>
                                    <input id="vertical-form-1" type="text" class="form-control border-gray-500" name="slug" placeholder="Slug">
                                </div>
                                <div>
                                    <label for="vertical-form-1" class="form-label text-base">Meta Title</label>
                                    <input id="vertical-form-1" type="text" class="form-control border-gray-500" name="meta_title" placeholder="Meta Title">
                                </div>
                                <div>
                                    <label for="vertical-form-1" class="form-label text-base">Meta Description</label>
                                    <textarea type="text" class="form-control border-gray-500" name="meta_descriiption" placeholder="Meta Description" rows="4" cols="50"></textarea>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary mt-5 text-base">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- END: Content -->
    
@endsection