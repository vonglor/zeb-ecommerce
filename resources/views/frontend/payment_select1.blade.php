@php
$user = Session::get('user');
@endphp

@extends('frontend.layouts.app')

@section('css')
    <link href="{{ asset('frontend/css/checkout.css') }}" rel="stylesheet">
 
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Checkout</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="checkout" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="checkout_1 clearfix">
                    <form action="{{ route('payment.checkout') }}" method="POST" id="checkout-form2">
                        @csrf
                    <div class="col-sm-8">
                        <div class="check_lm clearfix">
                            <div class="row">
                                <h4 style="margin-left: 15px">Select dilivery info</h4>
                                @error('address_id')
                                    <br><span style="color: red; margin-left: 15px; font-size: 12px;"
                                        class="alert alert-danger">{{ $message }}</span>
                                @enderror
                                <hr>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach (App\Models\Address::where('user_id', $user->id)->get() as $item)
                                    <div class="col-sm-6 address">
                                        <label id="address">
                                            <input type="radio" name="address_id" value="{{ $item->id }}"
                                                style="display: none">
                                            <span class="d-flex p-3 aiz-megabox-elem">
                                                <span class="flex-grow-1 pl-3 text-left">
                                                    {{-- <span>Address {{ $i }}</span> --}}
                                                    <div>
                                                        <span class="opacity-60">Address:</span>
                                                        <span class="fw-600 ml-2"> {{ $item->address }}</span>
                                                    </div>
                                                    <div>
                                                        <span class="opacity-60">Postal Code:</span>
                                                        <span class="fw-600 ml-2"> {{ $item->postal_code }}</span>
                                                    </div>
                                                    <div>
                                                        <span class="opacity-60">City:</span>
                                                        <span class="fw-600 ml-2"> {{ $item->city }}</span>
                                                    </div>
                                                    <div>
                                                        <span class="opacity-60">Country:</span>
                                                        <span class="fw-600 ml-2"> {{ $item->country }}</span>
                                                    </div>
                                                    <div>
                                                        <span class="opacity-60">Phone:</span>
                                                        <span class="fw-600 ml-2"> {{ $item->phone }}</span>
                                                    </div>
                                                </span>
                                            </span>

                                        </label>
                                    </div>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                                <div class="col-sm-6 address">
                                    <div class="add_address">
                                        <i class="fa fa-plus la-2x mb-3"></i>
                                        <div class="alpha-7">Add New Address</div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>


                        
                    </div>
                    @php
                        $subtotal = 0;
                    @endphp
                    @foreach (Session::get('cart')->where('owner_id', Session::get('owner_id')) as $key => $cartItem)
                        @php
                            $subtotal += $cartItem['qty'] * $cartItem['price'];
                        @endphp

                    @endforeach
                        <div class="col-sm-4">
                            <div class="checkout_1r clearfix">
                                <h4 class="mgt">CART TOTALS</h4>
                                <hr class="hr_1">
                                <h5>Sub Total <span class="pull-right">${{ number_format($subtotal, 2) }}</span></h5>
                                @php
                                    $shipping = 0;
                                    $total = $subtotal+$shipping;
                                @endphp
                                <h5>(+) Shipping <span class="pull-right">${{ number_format($shipping, 2) }}</span></h5>
                                <hr>
                                <h5>Total <span class="pull-right">${{ number_format($total, 2) }}</span></h5><br>
                                <input type="hidden" name="total" value="{{ $total }}">
                                <h4>PAYMENTS</h4>
                                <hr class="hr_1">
                                <p>
                                    <label><input type="radio" name="payment_option" class="payment" id="stripe" value="stripe"> <span>Card</span></label>
                                </p>
                                <p>
                                    <label><input type="radio" name="payment_option" class="payment" id="cash" value="cash"> <span>Cash</span></label>
                                </p>
                                
                                <h6><button class="button" type="submit">Order now</button></h6>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <h4 style="margin-left: 15px">Your Products Items</h4>
                        <hr>
                        <div class="table-responsive col-sm-12">
                            <table class="table table-bordered">
                                    <tr>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th width="15%">qty</th>
                                        <th width="20%">price</th>
                                    </tr>
                                    @php
                                        $subtotal = 0;
                                    @endphp
                                    @foreach (Session::get('cart')->where('owner_id', Session::get('owner_id')) as $key => $cartItem)
                                    @php
                                        $subtotal += $cartItem['price']*$cartItem['qty'];
                                    @endphp
                                    <tr>
                                        <td>
                                            
                                        <img src="{{ url(App\Models\Upload::find($cartItem['image'])->file_name) }}"
                                        class="iw" alt="abc"
                                        style="max-width:100px; max-height:100px; float:left; margin-right:10px;">
                                        </td>
                                        <td>
                                            <span>{{ $cartItem['name'] }}</span><br>
                                            @if ($cartItem['color'])
                                            <span>{{ $cartItem['color'] }}</span><br>
                                            @endif
                                            @if ($cartItem['size'])
                                            <span>{{ $cartItem['size'] }}</span><br>
                                            @endif
                                            @if ($cartItem['fabric'])
                                            <span>{{ $cartItem['fabric'] }}</span><br>
                                            @endif
                                            @if ($cartItem['poud'])
                                            <span>{{ $cartItem['poud'] }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="input-group number-spinner">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" id="{{ $key }}" data-dir="dwn"><span
                                                            class="glyphicon glyphicon-minus"></span></button>
                                                </span>
                                                <input type="hidden" name="key" value="{{ $key }}">
                                                <input type="text" name="qty" class="form-control text-center"
                                                    value="{{ $cartItem['qty'] }}">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" id="{{ $key }}" data-dir="up"><span
                                                            class="glyphicon glyphicon-plus"></span></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td>${{ number_format($cartItem['price'], 2) }}</td>
                                    </tr>
                                    @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {{-- modal add new address --}}
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" style="z-index: 10000;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">New Address</h4>
                        </div>
                        <form class="form-horizontal" action="{{ route('address.store') }}" id="form_address"
                            method="POST">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="address" id="address"
                                            placeholder="Your address" required></textarea>
                                        <span id="msg_address" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Country</label>
                                    <div class="col-sm-10">
                                        <select name="country" class="form-control" id="" required>
                                            <option value="">Select Country</option>
                                            @foreach (\App\Models\Country::all() as $country)
                                                <option value="{{ $country->name }}">{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                        <span id="msg_country" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">City</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="city" id="city" placeholder="City"
                                            required>
                                        <span id="msg_city" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Postal Code</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="postal_code" id="postal_code"
                                            placeholder="Postal Code" required>
                                        <span id="msg_postal_code" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="phone" id="phone"
                                            placeholder="Phone" required>
                                        <span id="msg_phone" class="alert-message"
                                            style="color: red; font-size: 12px;"></span>
                                        <input type="hidden" class="form-control" name="user_id" id="user_id"
                                            value="{{ $user->id }}">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="btn_save">Save</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </section>
@endsection
<script src=""></script>
@section('script')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        $('.credit-card').hide();
        $(document).ready(function() {
            $('.address label').click(function() {
                $('.address .active').removeClass('active');
                $(this).addClass('active');
            });

            $('.add_address').click(function() {
                $('#myModal').modal('show');
            });

            $('.payment').click(function() {
                // $('#payment .active').removeClass('active');
                // $(this).addClass('active');
                var payment_option = $(this).val();
                if (payment_option == 'stripe') {
                    $('.credit-card').show();
                } else {
                    $('.credit-card').hide();
                    $('.credit-card .col2 input').removeAttr("required");
                }
            });

        });

        $(function() {

            var cards = [{
                nome: "mastercard",
                colore: "#0061A8",
                src: "https://upload.wikimedia.org/wikipedia/commons/0/04/Mastercard-logo.png"
            }, {
                nome: "visa",
                colore: "#E2CB38",
                src: "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Visa_Inc._logo.svg/2000px-Visa_Inc._logo.svg.png"
            }, {
                nome: "dinersclub",
                colore: "#888",
                src: "http://www.worldsultimatetravels.com/wp-content/uploads/2016/07/Diners-Club-Logo-1920x512.png"
            }, {
                nome: "americanExpress",
                colore: "#108168",
                src: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/American_Express_logo.svg/600px-American_Express_logo.svg.png"
            }, {
                nome: "discover",
                colore: "#86B8CF",
                src: "https://lendedu.com/wp-content/uploads/2016/03/discover-it-for-students-credit-card.jpg"
            }, {
                nome: "dankort",
                colore: "#0061A8",
                src: "https://upload.wikimedia.org/wikipedia/commons/5/51/Dankort_logo.png"
            }];

            var month = 0;
            var html = document.getElementsByTagName('html')[0];
            var number = "";

            var selected_card = -1;

            $(document).click(function(e) {
                if (!$(e.target).is(".cvc") || !$(e.target).closest(".cvc").length) {
                    $(".card").css("transform", "rotatey(0deg)");
                    $(".seccode").css("color", "var(--text-color)");
                }
                if (!$(e.target).is(".expire") || !$(e.target).closest(".expire").length) {
                    $(".date_value").css("color", "var(--text-color)");
                }
                if (!$(e.target).is(".number") || !$(e.target).closest(".number").length) {
                    $(".card_number").css("color", "var(--text-color)");
                }
                if (!$(e.target).is(".inputname") || !$(e.target).closest(".inputname").length) {
                    $(".fullname").css("color", "var(--text-color)");
                }
            });


            //Card number input
            $(".number").keyup(function(event) {
                $(".card_number").text($(this).val());
                number = $(this).val();

                if (parseInt(number.substring(0, 2)) > 50 && parseInt(number.substring(0, 2)) < 56) {
                    selected_card = 0;
                } else if (parseInt(number.substring(0, 1)) == 4) {
                    selected_card = 1;
                } else if (parseInt(number.substring(0, 2)) == 36 || parseInt(number.substring(0, 2)) ==
                    38 || parseInt(number.substring(0, 2)) == 39) {
                    selected_card = 2;
                } else if (parseInt(number.substring(0, 2)) == 34 || parseInt(number.substring(0, 2)) ==
                    37) {
                    selected_card = 3;
                } else if (parseInt(number.substring(0, 2)) == 65) {
                    selected_card = 4;
                } else if (parseInt(number.substring(0, 4)) == 5019) {
                    selected_card = 5;
                } else {
                    selected_card = -1;
                }

                if (selected_card != -1) {
                    html.setAttribute("style", "--card-color: " + cards[selected_card].colore);
                    $(".bankid").attr("src", cards[selected_card].src).show();
                } else {
                    html.setAttribute("style", "--card-color: #cecece");
                    $(".bankid").attr("src", "").hide();
                }

                if ($(".card_number").text().length === 0) {
                    $(".card_number").html(
                        "&#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF;"
                    );
                }

            }).focus(function() {
                $(".card_number").css("color", "white");
            }).on("keydown input", function() {

                $(".card_number").text($(this).val());

                if (event.key >= 0 && event.key <= 9) {
                    if ($(this).val().length === 4 || $(this).val().length === 9 || $(this).val().length ===
                        14) {
                        $(this).val($(this).val() + " ");
                    }
                }
            })

            //Name Input
            $(".inputname").keyup(function() {
                $(".fullname").text($(this).val());
                if ($(".inputname").val().length === 0) {
                    $(".fullname").text("FULL NAME");
                }
                return event.charCode;
            }).focus(function() {
                $(".fullname").css("color", "white");
            });

            //Security code Input
            $(".cvc").focus(function() {
                $(".card").css("transform", "rotatey(180deg)");
                $(".seccode").css("color", "white");
            }).keyup(function() {
                $(".seccode").text($(this).val());
                if ($(this).val().length === 0) {
                    $(".seccode").html("&#x25CF;&#x25CF;&#x25CF;");
                }
            }).focusout(function() {
                $(".card").css("transform", "rotatey(0deg)");
                $(".seccode").css("color", "var(--text-color)");
            });


            //Date expire input
            $(".expire").keypress(function(event) {
                if (event.charCode >= 48 && event.charCode <= 57) {
                    if ($(this).val().length === 1) {
                        $(this).val($(this).val() + event.key + " / ");
                    } else if ($(this).val().length === 0) {
                        if (event.key == 1 || event.key == 0) {
                            month = event.key;
                            return event.charCode;
                        } else {
                            $(this).val(0 + event.key + " / ");
                        }
                    } else if ($(this).val().length > 2 && $(this).val().length < 9) {
                        return event.charCode;
                    }
                }
                return false;
            }).keyup(function(event) {
                $(".date_value").html($(this).val());
                if (event.keyCode == 8 && $(".expire").val().length == 4) {
                    $(this).val(month);
                }

                if ($(this).val().length === 0) {
                    $(".date_value").text("MM / YYYY");
                }
            }).keydown(function() {
                $(".date_value").html($(this).val());
            }).focus(function() {
                $(".date_value").css("color", "white");
            });
        });

        Stripe.setPublishableKey(
            'pk_test_51Jd4FUC9jbftbmhrmjMzD1Cmc3RGXXAk980uO5m9LP896wWe23b1LP8mDtZIPrbnn9ChNbStLzGWRuKRujaMVoKb00TYRbRQLQ'
            );

        var $form = $('#checkout-form2');

        $form.submit(function(event) {
            $('#checkout-error').addClass('hidden');
            $form.find('button').prop('disabled', true);
            Stripe.card.createToken({
                number: $('#card-number').val(),
                cvc: $('#card-cvc').val(),
                exp: $('#card-exp').val(),
                name: $('#card-name').val()
            }, stripeResponseHandler);
            return false;
        });

        function stripeResponseHandler(status, response) {
            if (response.error) {
                $('#checkout-error').removeClass('hidden');
                $('#checkout-error').text(response.error.message);
                $form.find('button').prop('disabled', false);
            } else {
                var token = response.id;
                $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                $form.get(0).submit();
            }
        }
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {
            $(document).on('click', '.number-spinner .btn', function() {
                var btn = $(this),
                    oldValue = btn.closest('.number-spinner').find('input[name="qty"]').val().trim();
                newVal = 0;
                var key = $(this).attr('id');
                if (btn.attr('data-dir') == 'up') {
                    newVal = parseInt(oldValue) + 1;
                } else {
                    if (oldValue > 1) {
                        newVal = parseInt(oldValue) - 1;
                    } else {
                        newVal = 1;
                    }
                }
                btn.closest('.number-spinner').find('input[name="qty"]').val(newVal);
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "post",
                    url: '{{ route('update_cart') }}',
                    data: {
                        qty: newVal,
                        key: key,
                        _token: _token
                    },
                    success: function(data) {
                        window.location.reload();
                    }
                });

            });
        });
    </script>
@endsection
