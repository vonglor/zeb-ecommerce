<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Address;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->all());
        $request->validate([
            'address' => 'required',
            'country' => 'required',
            'province' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
            'phone' => 'required',
            ]);

        $address = new Address;
        $address->user_id = $request->user_id;
        $address->address = $request->address;
        $address->country = $request->country;
        $address->province = $request->state;
        $address->city = $request->city;
        $address->postal_code = $request->postal_code;
        $address->phone = $request->phone;
        $address->status = "selected";
        $address->save();

        Address::where('user_id', $request->user_id)
        ->where('id', '!=', $address->id)
        ->update([
            'status' => 'unselect'
        ]);
        
        $notification = array(
            'message' => 'Added address successfully.',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $address = Address::find($id);
        
        if($address->delete()){
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
        }
        return back()->with($notification);
    }

    public function address_edit($id)
    {
        $address = Address::find($id);
        return response()->json($address);
    }

    public function address_update(Request $request){

        $address = Address::find($request->address_id);
        $address->address = $request->address;
        $address->country = $request->country;
        $address->province = $request->state;
        $address->city = $request->city;
        $address->postal_code = $request->postal_code;
        $address->phone = $request->phone;

        if($address->update()){
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
        }
        return back()->with($notification);
    }

    public function delete($id)
    {
        $address = Address::find($id);
        
        if($address->delete()){
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
        }
        return back()->with($notification);
    }

    public function change(Request $request) {
        $address = Address::where('user_id', $request->ad_id)->get();
        return view('frontend.address', \compact('address'));
    }

    public function select($id) {
        $addr = Address::find($id);
        $addr->status = 'selected';
        $addr->save();
        Address::where('user_id', $addr->user_id)
                ->where('id', '!=', $id)
                ->update([
                    'status' => 'unselect'
                ]);
        return back();
    }
}
