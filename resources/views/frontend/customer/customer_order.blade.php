@extends('frontend.saller.content_left')
@section('css')
    <link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
    <div class="col-sm-9">
        <div class="centent_card_1r clearfix">
            <div class="centent_card_1r3 tab-content">
                <p>Purchase History</p>
                <div class="row">
                    {!! Form::open(['method'=>'get', 'route'=>'purchase.search.order']) !!}
                    <div class="col-sm-6">
                            <div class="input-group">
                                {!! Form::text('search', '', ['class'=>'form-control', 'placeholder'=>'Search Order Number...']) !!}
                                {{-- <input type="text" name="search" class="form-control" placeholder="Search Order Number..."> --}}
                                <span class="input-group-btn">
                                    {!! Form::submit('Search', ['class'=>'btn btn-search']) !!}
                                </span>
                            </div><!-- /input-group -->
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="card-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>#</th>
                                <th>Order Number</th>
                                <th>Date</th>
                                <th>Num of Products</th>
                                <th>Amount</th>
                                <th>Delivery Status</th>
                                <th>Payment Status</th>
                                <th class="text-right">Options</th>
                            </tr>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($orders as $order)
                                <tr>
                                    <td scope="row">{{ $i }}</td>
                                    <td>{{ $order->code }}</td>
                                    <td>{{ $order->date }}</td>
                                    <td>{{ count(App\Models\Order_detail::where('order_id', $order->id)->get()) }}</td>
                                    <td>{{ single_price($order->grand_total) }}</td>
                                    <td>{{ $order->delivery_status }}</td>
                                    <td>
                                        @if ($order->payment_status == 'paid')
                                            <span class="label label-success">Paid</span>
                                        @else
                                            <span class="label label-danger">Unpaid</span>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <a href="" id="{{ $order->id }}" class="btn btn-info btn-veiw-detail btn-xs"><i
                                                class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                @php
                                    $i++;
                                @endphp
                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
            <div class="center_product_1r4 clearfix">
                <div class="col-sm-6">
                    <div class="center_product_1r4l clearfix">
                        {{ $orders->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- moal detail --}}
    <div class="modal fade" id="order_detail_modal" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div id="order-details-modal-body">

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.btn-veiw-detail').click(function(e) {
                e.preventDefault();
                var order_id = $(this).attr('id');
                $.ajax({
                    method: "get",
                    url: "/customer/order_detail/"+order_id,
                    success: function (data) {
                        $('#order-details-modal-body').html(data);
                        $('#order_detail_modal').modal('show');
                    }
                });
            });
        });
    </script>
@endsection
