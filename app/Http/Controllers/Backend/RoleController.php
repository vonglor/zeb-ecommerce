<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index(Type $var = null)
    {
        $roles = Role::orderBy('id', 'desc')->get();

        return view('backend.roles.index', compact('roles'));
    }

    public function create()
    {
        return view('backend.roles.create');
    }

    public function store(request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255', 'unique:roles'],
        ]);

        if ($request->has('permissions')) {
            $role              = new Role();
            $role->name        = $request->name;
            $role->permissions = json_encode($request->permissions);

            if ($role->save()) {
                toastr()->success('Save', 'Role successfully.', ['timeOut' => 2000]);
                return redirect(route('roles'));
            } else {
                toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
                return redirect()->back();
            }
        }

        toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
        return redirect()->back();
    }

    public function edit(Role $role)
    {
        if (!$role) {
            toastr()->error(__("Role Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        return view('backend.roles.edit', compact('role'));
    }

    public function update(Request $request, Role $role)
    {
        if (!$role) {
            toastr()->error(__("Role Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255', 'unique:roles,name,' . $role->id],
        ]);

        if ($request->has('permissions')) {
            $role->name        = $request->name;
            $role->permissions = json_encode($request->permissions);
            if ($role->save()) {
                toastr()->success('Update', 'Role successfully.', ['timeOut' => 2000]);
                return redirect()->back();
            } else {
                toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
                return redirect()->back();
            }
        }

        toastr()->error('Error', 'Something went wrong', ['timeOut' => 2000]);
        return redirect()->back();
    }

    public function destroy(Role $role)
    {
        if (!$role) {
            toastr()->error(__("Role Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if ($role->delete()) {
            toastr()->success(__("Role Deleted"), __("Success"), ['timeOut' => 2000]);
            return redirect(route('roles'));
        } else {
            toastr()->error(__("Role Not Deleted"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

}