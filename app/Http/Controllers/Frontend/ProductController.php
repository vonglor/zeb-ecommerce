<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Product_stock;
use App\Http\controllers\frontend\ProductController;
use Session;

use Illuminate\Support\Collection;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Session::get('user');
        $product = Product::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(15);
        // dd($user);
        return view('frontend.saller.products.index', \compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        $brand = Brand::all();
        return view('frontend.saller.products.create', compact('category', 'brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $product_stock = Product_stock::where('product_id', $id)->get();
        return view('frontend.saller.products.edit', \compact('product', 'product_stock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sku_combination(Request $request)
    {
        $options = array();
        if($request->has('colors') && count($request->colors) > 0){
            array_push($options, $request->colors);
        }

        $unit_price = $request->unit_price;
        $product_name = $request->name;
        if($request->has('choice_no')){
            foreach ($request->choice_no as $key => $no) {
                $name = 'choice_options_'.$no;             
                $data = array();
                foreach (json_decode($request[$name][0]) as $key => $item) {

                    array_push($data, $item->value);
                }
                array_push($options, $data);
            }
        }
        
        $combinations = array(array());

        foreach ($options as $key => $values) {
            $append = array();
            foreach($combinations as $product) {
                foreach($values as $item) {
                    $product[$key] = $item;
                    $append[] = $product;
                }
            }

            $combinations = $append;
        }
        return view('product_all_inhouse', compact('combinations', 'unit_price', 'product_name'));
    }

    public function product_search(Request $request)
    {
        if($request->category_id){
            $product = Product::where('name', 'LIKE', '%'.$request->value_search.'%')->where('category_id', $request->category_id)->get()->paginate(15);
        }else{
            $product = Product::where('name', 'LIKE', '%'.$request->value_search.'%')->paginate(15);
        }
        return view('frontend.product', \compact('product'));
    }

    public function category($cat_id)
    {
        $product = Product::where('category_id', $cat_id)->get()->paginate(15);
        return view('frontend.product', \compact('product'));
    }

    public function brand($brand_id)
    {
        $product = Product::where('brand_id', $brand_id)->get()->paginate(15);
        return view('frontend.product', \compact('product'));
    }

    public function search(Request $request)
    {
        $user_id = Session::get('user')->id;
        $product = Product::where('name', 'like', '%'.$request->search.'%')
                    ->where('user_id', $user_id)
                    ->paginate(15);
        return view('frontend.saller.products.index', \compact('product'));
    }

    public function bestsell()
    {
        $user = Session::get('user');
        $product = Product::where('user_id', $user->id)
                ->where('num_of_sale', '>', 0)
                ->orderBy('num_of_sale', 'desc')
                ->paginate(15);
        return view('frontend.saller.products.bestsell', \compact('product'));
    }

    public function bastSaleProduct(Request $request){
        $user = Session::get('user');
        $product = Product::where('user_id', $user->id)
                ->where('num_of_sale', '>', 0)
                ->where('num_of_sale', '>', 0)
                ->orderBy('num_of_sale', 'desc')
                ->paginate(15);
        return view('frontend.saller.products.best_sale_date', \compact('product'));
    }

    public function getQytProduct(){
        $user = Session::get('user');
        $product = Product::join('product_stocks', 'product_stocks.product_id', 'products.id')
        ->select('product_stocks.qty')
        ->where('products.user_id', $user->id)
        ->get();

        $total = 0;
        foreach ($product as $value) {
            $total += $value->qty; 
        }
        return $total;
    }

  
}
