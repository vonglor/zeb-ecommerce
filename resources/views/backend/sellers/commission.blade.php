@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
    <div class="content">
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-xl font-medium mr-auto">
            Seller Commission
        </h2>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Vertical Form -->
            <form action="">
            <div class="intro-y box">
                <div id="vertical-form" class="p-5">
                    <div class="preview">
                        <div>
                                <label for="vertical-form-1" class="form-label text-base">Seller Commission</label>
                                <input id="vertical-form-1" type="text" name="percent" class="form-control border-gray-500 text-base" placeholder="%" required>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary mt-5 text-base">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Vertical Form -->
            <form action="{{route('product.store')}}">
            <div class="intro-y box">
                <div id="vertical-form" class="p-5">
                    <div class="preview">
                        <div>
                            <label for="vertical-form-1" class="form-label text-lg">Note</label>
                                <p class="text-base">1. 10% ລາຄາສິນຄ້າຈະຖືກຫັກອອກຈາກລາຍໄດ້ຂອງຮ້ານຄ້າ.</p>
                                <p class="text-base">2. ຄ່ານາຍຫນ້າດຳເນີນການໄດ້ເມື່ອນາຍຫນ້າອີງຕາມປະເພດສິນຄ້າຖືກປິດຈາກການຕັ້ງຄ່າທຸລະກິດ.</p>
                                <p class="text-base">3. ຄ່ານາຍຫນ້າບໍ່ເຮັດວຽກຖ້າລະບົບແພັກເກດຂອງຮ້ານຄ້າເພີ່ມເຂົ້າສູ່ລະບົບ.</p>
                        </div>
                            <!-- BEGIN:  -->
                        
                        <!-- END:  -->
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- END: Content -->
    
@endsection