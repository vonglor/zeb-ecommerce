@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->

    <div class="content">
        <h2 class="intro-y text-xl font-medium mt-10">
            Payout History
        </h2>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                <h1 class="text-xl shadow-md mr-2 text-lg">Customer Payout History</h1>
                <div class="hidden md:block mx-auto text-gray-600"></div>
                <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                    <div class="w-56 relative text-gray-700 dark:text-gray-300">
                        <input type="text" class="form-control w-56 box pr-10 placeholder-theme-8" placeholder="Search...">
                        <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i> 
                    </div>
                </div>
            </div>

            <!-- BEGIN: Data List -->
            
            <div class="intro-y col-span-12 overflow-auto">
                <table class="table table-report -mt-2" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base">#</th>
                            <th class="whitespace-nowrap text-base">Date</th>
                            <th class="whitespace-nowrap text-base">Paid by</th>
                            <th class="whitespace-nowrap text-base">Customer Name</th>
                            <th class="whitespace-nowrap text-base">Bank Name</th>
                            <th class="whitespace-nowrap text-base">Bank Account</th>
                            <th class="text-center whitespace-nowrap text-base">Amount</th>
                            <th class="text-center whitespace-nowrap text-base">Detail payment</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="">
                            <td class="w-10">
                                <h1 class="whitespace-nowrap text-base">1</h1> 
                            </td>
                            
                            <td>
                                <h1 class="font-medium whitespace-nowrap text-base">2021-06-17</h1> 
                            </td>
                            <td class="w-40">
                                <h1 class="font-medium whitespace-nowrap text-base">xuemoua</h1> 
                            </td>
                            <td class="w-40">
                                <h1 class="font-medium whitespace-nowrap text-base">David</h1> 
                            </td>
                            <td class="w-40">
                                <h1 class="font-medium whitespace-nowrap text-base">BCEL</h1> 
                            </td>
                            <td class="w-40">
                                <h1 class="font-medium whitespace-nowrap text-base">3425364576457</h1> 
                            </td>
                            <td class="text-center text-base">193,995₭</td>
                            <td class="text-center"><p class="text-red-500 text-base">Bank payment</p></td>
                        </tr>
                            <tr class="intro-x">
                            <td class="w-40">
                                <h1 class="whitespace-nowrap text-base">2</h1> 
                            </td>
                            <td>
                                <h1 class="font-medium whitespace-nowrap text-base">2021-06-18</h1> 
                            </td>
                            <td class="w-40">
                                <h1 class="font-medium whitespace-nowrap text-base">Xuemoua</h1> 
                            </td>
                            <td class="w-40">
                                <h1 class="font-medium whitespace-nowrap text-base">Xaimoua</h1> 
                            </td>
                            <td class="w-40">
                                <h1 class="font-medium whitespace-nowrap text-base">BCEL</h1> 
                            </td>
                            <td class="w-40">
                                <h1 class="font-medium whitespace-nowrap text-base">3425364576457</h1> 
                            </td>
                            <td class="text-center text-base">393,995₭</td>
                            <td class="text-center"><p class="text-red-500 text-base">Bank payment</p></td>
                        </tr>  
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->

        </div>
    </div>

    <!-- END: Content -->
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();    
        });
    </script>
    
@endsection