<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Order_detail;
use DB;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index($status = null)
    {
        if ($status === 'all') {
            $orders = Order::orderBy('id', 'desc')->get();
        } else {
            $orders = Order::orderBy("id", "desc")
                ->where("delivery_status", $status)->get();
        }

        switch ($status) {
            case "pending":
                return view('backend.order.pending', compact('orders'));
                break;
            case "confirmed":
                return view('backend.order.confirmed', compact('orders'));
                break;
            case "completed":
                return view('backend.order.completed', compact('orders'));
                break;
            case "all":
                return view('backend.order.index', compact('orders'));
                break;
            default:
                toastr()->warning(__("Order page not found!"), __("Error"), ['timeOut' => 2000]);
                return redirect()->back();
        }
    }

    public function show(Order $order, $param = null)
    {
        if ($param !== 'admin' && $param !== 'seller') {
            toastr()->warning(__("Order page not found!"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if (!$order) {
            toastr()->error(__("Order Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $orders = Order_detail::where('order_id', $order->id)->get();

        $data = array(
            'order'  => $order,
            'orders' => $orders,
        );

        if ($param === 'admin') {
            return view('backend.order.show')->with($data);
        } else {
            return view('backend.order.order_seller.show')->with($data);
        }
    }

    public function change_status(Request $request, Order $order)
    {
        if (!$order) {
            toastr()->error(__("Order Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $request->validate([
            "delivery_status" => ['required', 'string', 'in:pending,confirmed,completed'],
        ]);

        if ($order->delivery_status === 'completed') {
            toastr()->warning(__("This order is already completed!."), __("Warning"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $order->delivery_status = $request->delivery_status;
        if ($order->save()) {
            toastr()->success(__("Order status changed successfully"), __("Success"), ['timeOut' => 2000]);
            return redirect()->back();
        } else {
            toastr()->error(__("Order status not changed"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

    public function destroy(Order $order)
    {
        if (!$order) {
            toastr()->error(__("Order Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if ($order->delivery_status === 'completed' || $order->payment_status === 'paid') {
            toastr()->warning(__("This order is already completed cannot deleted!."), __("Warning"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if (Order::destroy($order->id)) {
            toastr()->success(__("Order deleted successfully"), __("Success"), ['timeOut' => 2000]);
            return redirect()->back();
        } else {
            toastr()->error(__("Order not deleted"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

    public function fetchOrderBySeller($status = null)
    {
        if ($status !== 'pending' && $status !== 'confirmed' && $status !== 'completed' && $status !== 'all') {
            toastr()->error(__("Page not found!"), __("Errors"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        if ($status === 'all') {
            if ($this->getAllOrders()) {
                $orders = $this->getAllOrders();
            } else {
                toastr()->error(__("No orders found!"), __("Error"), ['timeOut' => 2000]);
                return redirect()->back();
            }
        } else {
            if ($this->getOrdersByStatus($status)) {
                $orders = $this->getOrdersByStatus($status);
            } else {
                toastr()->error(__("No orders found!"), __("Error"), ['timeOut' => 2000]);
                return redirect()->back();
            }
        }

        switch ($status) {
            case "pending":
                return view('backend.order.order_seller.pending', compact('orders'));
                break;
            case "confirmed":
                return view('backend.order.order_seller.confirmed', compact('orders'));
                break;
            case "completed":
                return view('backend.order.order_seller.completed', compact('orders'));
                break;
            case "all":
                return view('backend.order.order_seller.index', compact('orders'));
                break;
            default:
                toastr()->warning(__("Order page not found!"), __("Error"), ['timeOut' => 2000]);
                return redirect()->back();
        }
    }

    public function getOrdersByStatus($status = null)
    {
        $orders = DB::table('orders as o')
            ->select('o.id',
                'o.shipping_address',
                'o.delivery_status',
                'o.payment_type',
                'o.payment_details',
                'o.grand_total',
                'o.payment_status',
                'o.code',
                'o.date',
                'o.viewed',
                'o.delivery_viewed',
                'o.payment_status_viewed',
                'o.commision_calculated',
                'o.created_at',
                'o.updated_at',

                's.id',
                's.ss_id',
                's.bank_name',
                's.bank_acc_name',
                's.bank_acc_no',
                's.bank_payment_status',
                's.verification_status',
                's.verification_info',
                's.admin_to_pay',

                'u.id as user_id',
                'u.name',
                'u.surname',
                'u.user_type',
            )
            ->join('sellers as s', 's.id', '=', 'o.seller_id')
            ->join('users as u', 'u.id', '=', 's.user_id')
            ->where('o.delivery_status', $status)
            ->where('u.user_type', 'sale')
            ->orderBy('o.id', 'desc')
            ->get();

        if (!$orders) {
            return false;
        }

        return $orders;
    }

    public function getAllOrders()
    {
        $orders = DB::table('orders as o')
            ->select('o.id',
                'o.shipping_address',
                'o.delivery_status',
                'o.payment_type',
                'o.payment_details',
                'o.grand_total',
                'o.code',
                'o.date',
                'o.payment_status',
                'o.viewed',
                'o.delivery_viewed',
                'o.payment_status_viewed',
                'o.commision_calculated',
                'o.created_at',
                'o.updated_at',

                's.id as seller_id',
                's.ss_id',
                's.bank_name',
                's.bank_acc_name',
                's.bank_acc_no',
                's.bank_payment_status',
                's.verification_status',
                's.verification_info',
                's.admin_to_pay',

                'u.id as user_id',
                'u.name',
                'u.surname',
                'u.user_type',
            )
            ->join('sellers as s', 's.id', '=', 'o.seller_id')
            ->join('users as u', 'u.id', '=', 's.user_id')
            ->where('u.user_type', 'sale')
            ->orderBy('o.id', 'desc')
            ->get();

        if (!$orders) {
            return false;
        }

        return $orders;
    }

}