@extends('frontend.layouts.app')
@section('css')
    <link href="{{ asset('frontend/css/login.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section id="center" class="center_shop clearfix">
        <div class="container">
            <div class="row">
                <div class="center_shop_1 clearfix">
                    <div class="col-sm-12">
                        <h5 class="mgt">
                            <a href="#">Home <i class="fa fa-long-arrow-right"></i> </a>
                            <a href="#">Login</a>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="login" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="login_1 clearfix">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8 login_1c">
                        <form action="{{ route('user.register') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                            <div class="login_1l clearfix">
                                <h4 class="mgt">Sign up for {{ $user_type }}</h4>
                            </div><br>
                            @if (Session::get('err'))
                                <span class="alert alert-danger">{{ Session::get('err') }}</span>
                            @endif
                            <div class="row">
                                <div class="col-sm-6 login_1l1 clearfix">
                                    <div class="space_left">
                                        <select name="currency" class="form-control" id="">
                                            @foreach (App\Models\Currency::where('status', '1')->get() as $item)
                                            <option value="{{ $item->code }}"> {{ $item->code }} ({{ $item->symbol }})</option>
                                            @endforeach
                                           
                                        </select>
                                    </div>
                                    <div class="space_left">
                                        <input type="hidden" class="form-control" value="{{ $user_type }}" name="user_type">
                                        <input class="form-control" placeholder="ID NO" type="text" name="card_id" required>
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="image" type="file" name="card_img" >
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="Refer ID" type="text" name="refer_id" required>
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="SS#" type="text" name="ss_id" required>
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="First name" type="text" name="name" required>
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="Last name" type="text" name="surname" required>
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="Date of birth" type="date" name="dob" required>
                                    </div>
                                    <div class="space_left">
                                        <select class="form-control" id="gender" name="gender" required>
                                            <option value="">Gender</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 login_1l1 clearfix">
                                    <div class="space_left">
                                        <input class="form-control" placeholder="Phone" type="text" name="phone" required>
                                        @error('address')
                                            <span style="color: red;">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="Address" type="text" name="address" required>
                                        @error('address')
                                            <span style="color: red;">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="space_left">
                                        <select class="form-control" id="country" name="country" required>
                                            <option value="">Country</option>
                                            @foreach (App\Models\Country::all() as $item)
                                            <option value="{{ $item->name }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="State" type="text" name="state" required>
                                        @error('state')
                                            <span style="color: red;">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="City" type="text" name="city" required>
                                        @error('city')
                                            <span style="color: red;">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="Zip code" type="text" name="postal_code" required>
                                        @error('postal_code')
                                            <span style="color: red;">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="Email" type="email" name="email" required>
                                        @error('email')
                                            <span style="color: red;">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="Password" type="password" name="password" required>
                                        @error('password')
                                            <span style="color: red;">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="space_left">
                                        <input class="form-control" placeholder="Confirm password" type="password" name="c_password" required>
                                        @error('c_password')
                                            <span style="color: red;">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            
                            <br>
                            <div class="col-sm-6 login_1l1 clearfix text-center">
                                <button type="submit" class="btn-login"> Sign Up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
<script src=""></script>