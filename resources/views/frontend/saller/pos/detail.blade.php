<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Product detail</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-5 col-md-5 col-lg-5">
            <img src="{{ url($product->Upload->file_name) }}" class="img-responsive" alt="">
            <span class="war-video"></span> <span class="war-content">
                @if ($product->videos != null)
                    <a href="{{ $product->videos }}" target="_blank">
                        <?php
                        $array = explode("\n", wordwrap($product->videos, 5));
                        echo implode("<br>",$array);
                        ?>
                    </a>
                @else
                    No video link
                @endif
                
            </span>
        </div>
        <form action="{{ route('pos.add_to_cart') }}" method="POST">
            @csrf
            <div class="col-sm-7">
                <h4>
                    <?php
                    $array = explode("\n", wordwrap($product->name, 50));
                    echo implode('<br>', $array);
                    ?>
                </h4>
                @if (count(json_decode($product->colors)) > 0)
                    <div class="pd_n clearfix color-choose">
                        <h5 class="bold">Color</h5>
                        <ul class="">
                            @php
                                $i = 1;
                            @endphp
                            @foreach (json_decode($product->colors) as $key => $color)
                                @php
                                    $colors = App\Models\Color::where('name', $color)->first();
                                @endphp
                                <input type="radio" name="color" id="{{ $color }}" class="radio_{{ $i }}"
                                    value="{{ $color }}" style="display: none">
                                <label for="{{ $color }}">
                                    <li style="background: {{ $colors->code }}" id="color_{{ $i }}"
                                        name="{{ $color }}"></li>
                                </label>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if ($product->choice_options != null && count(json_decode($product->choice_options)) > 0)
                    @foreach (json_decode($product->choice_options) as $key => $choice)
                        <div class="pd_n_1 clearfix choice_options">
                            @php
                                $option = App\Models\Attributes::find($choice->attribute_id);
                            @endphp
                            <h5 class="bold">{{ $option->name }}</h5>
                            <ul>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($choice->values as $key => $value)
                                    <label>
                                        <input type="radio" name="{{ $option->name }}" id="{{ $option->name }}"
                                            class="radio_{{ $i }}" value="{{ $value }}"
                                            style="display: none">
                                        <li id="{{ $option->name . $value }}" name="{{ $value }}"
                                            class="{{ $option->name }}_{{ $i }}" data="{{ $option->name }}">
                                            {{ $value }}
                                        </li>
                                    </label>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                @else
                    <input type="hidden" id="Size" name="Size" value="">
                    <input type="hidden" id="Fabric" name="Fabric" value="">
                    <input type="hidden" id="Poud" name="Poud" value="">
                @endif
                <h5 class="bold price">
                    <span>Price:</span>
                        $<span id="show_price">
                        {{ number_format($product->unit_price - $product->discount, 2) }}
                        </span>
                </h5>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-sm-5 col-md-2 col-lg-2">
                        <h5 class="bold price qty">Quantity:</h5>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="input-group number-spinner">
                            <span class="input-group-btn">
                                <a class="btn btn-default" data-dir="dwn"><span
                                        class="glyphicon glyphicon-minus"></span></a>
                            </span>
                            <input type="text" name="qty" class="form-control text-center" value="1">
                            <span class="input-group-btn">
                                <a class="btn btn-default" data-dir="up"><span
                                        class="glyphicon glyphicon-plus"></span></a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="span">
                    <span>ItemId: {{ $product->id }} </span> <span> Total: @php
                        $pro_stock = App\Models\Product_stock::where('product_id', $product->id)->get();
                        $qty = 0;
                        foreach ($pro_stock as $pt) {
                            $qty += $pt->qty;
                        }
                        echo $qty;
                    @endphp</span>
                </div>
                
                <input type="hidden" name="product_id" id="product_id" value="{{ $product->id }}">
                
                {{-- <h5 class="bold price">Total:
                    @php
                        $pro_stock = App\Models\Product_stock::where('product_id', $product->id)->get();
                        $qty = 0;
                        foreach ($pro_stock as $pt) {
                            $qty += $pt->qty;
                        }
                        echo $qty;
                    @endphp
                </h5> --}}
                <h5 class="bold price">
                    <span>Shipping:</span>
                        <span id="show_price">
                            @php
                                if($product->shipping){
                                    echo $product->shipping;
                                }else{
                                    echo 'free';
                                }
                            @endphp
                        </span>
                </h5>
                <div class="">
                    <button class="button"><i class="fa fa-shopping-cart"> </i>  Add To Cart</button>
                </div>
                
                <div class="row">
                    @if ($product->delivery)
                        <p class="mgt">
                            <span class="col-sm-3 delivery-title">Delivery: </span> <span
                            class="col-sm-9 delivery">{{ $product->delivery }}</span>
                        </p>
                    @endif
                </div>
            </div>
        </form>
    </div>
    <div class="row ">
            <h5 class="bold">Description</h5>
            <p>{{ $product->description }}</p>
            <h5 class="bold">Policy return</h5></h5>
            <p>{{ $product->description }}</p>
    </div>
</div>

<script type="text/javascript">
    var price = $('#show_price').text();
    $(document).on('click', '.number-spinner .btn', function() {
        var btn = $(this),
            oldValue = btn.closest('.number-spinner').find('input').val().trim();
        newVal = 0;
        if (btn.attr('data-dir') == 'up') {
            newVal = parseInt(oldValue) + 1;
        } else {
            if (oldValue > 1) {
                newVal = parseInt(oldValue) - 1;
            } else {
                newVal = 1;
            }
        }
        var subtotal = price * newVal;
        subtotal = subtotal.toFixed(2);
        btn.closest('.number-spinner').find('input').val(newVal);
        $('#subtotal').html(subtotal);
    });

    $(document).ready(function() {
            $('.color-choose label #color_1').addClass('act');
            $('.color-choose .radio_1').attr('checked', true);
            $('.color-choose label li').on('click', function() {
                $('.act').removeClass('act');
                $(this).addClass('act');
                var colors = $(this).attr('name');
                var Size = $('#Size').val();
                var Fabric = $('#Fabric').val();
                var Poud = $('#Poud').val();
                var product_id = $('#product_id').val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "POST",
                    url: "{{ route('select') }}",
                    data: {
                        Size: Size,
                        Fabric: Fabric,
                        Poud: Poud,
                        colors: colors,
                        product_id: product_id,
                        _token: _token,
                    },
                    dataType: 'JSON',
                    success: function(data) {
                        $('#show_price').html(data.price);
                    }
                });
            });

            $('.choice_options label .Size_1').addClass('act_1');
            $('.choice_options label .Fabric_1').addClass('act_1');
            $('.choice_options label .Poud_1').addClass('act_1');
            $('.choice_options .radio_1').attr('checked', true);
            $('.choice_options label li').on('click', function() {
                var att = $(this).attr('id');
                var data = $(this).attr('data');
                $('.choice_options .act_1').removeClass('act_1');
                $('.choice_options #' + att).addClass('act_1');

                var colors = $('#color').val();
                var Size = $('#Size').val();
                var Poud = $(this).attr('name');
                var Fabric = $('#Fabric').val();
                var product_id = $('#product_id').val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    type: "POST",
                    url: "{{ route('select') }}",
                    data: {
                        Size: Size,
                        Fabric: Fabric,
                        Poud: Poud,
                        colors: colors,
                        product_id: product_id,
                        _token: _token,
                    },
                    dataType: 'JSON',
                    success: function(data) {
                        $('#show_price').html(data.price);
                    }
                });
            });

            $('#btn-buy').click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: '{{ route('buy') }}',
                    data: $('#form-add-cart').serialize(),
                    success: function(data) {
                        window.location.href = "/cart";
                    }
                });
            });
        });
</script>
