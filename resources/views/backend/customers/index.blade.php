@extends('backend.layouts.sidebar')
@section('content')
 
  <!-- BEGIN: Content -->
    <div class="content">
        <h2 class="intro-y text-xl font-medium mt-10">
            All Buy Information
        </h2>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                <div class="dropdown">
                </div>
                <div class="hidden md:block mx-auto text-gray-600"></div>
                <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                    <div class="w-56 relative text-gray-700 dark:text-gray-300">
                        <input type="text" class="form-control w-56 box pr-10 placeholder-theme-8" placeholder="Search...">
                        <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i> 
                    </div>
                </div>
            </div>
            <!-- BEGIN: Customers Layout -->
            @php
                $i = 0;
            @endphp
            @foreach ($customer as $item)

            <div class="intro-y md:col-span-6">
                <div class="box">
                    <div class="flex items-start px-5 pt-5">
                        <div class="w-full flex flex-col lg:flex-row items-center">
                            <div class="w-20 h-20 image-fit">
                                <img src="{{url(App\Models\Customer::where('user_id', $item->id)->first()->card_img)}}" alt="">
                            </div>

                            <div class="flex">
                                <div class="lg:ml-4 text-center lg:text-left mt-3 lg:mt-0">
                                    <div class="font-medium">ID: {{$item->id}}</div> 
                                    <div class="font-medium">Full Name: {{$item->name}} {{$item->surname}}</div> 
                                    <div class="font-medium">Gender: {{$item->refer_code_others}}</div> 
                                    <div class="font-medium">Birthofdate: {{$item->date_of_birth}}</div> 
                                    <div class="font-medium">Address: {{$item->city}}, {{$item->country}}</div> 
                                </div>
                                <div class="lg:ml-4 text-center lg:text-left mt-3 lg:mt-0">
                                    <div class="font-medium">Reffer ID: {{$item->refer_id}}</div> 
                                    <div class="font-medium">Reffer From: {{$item->gender}}</div> 
                                    <div class="font-medium">Card ID: {{App\Models\Customer::where('user_id', $item->id)->first()->card_id;}}</div> 
                                    <div class="font-medium">Email: {{$item->email}}</div> 
                                    <div class="font-medium">Phone: {{$item->phone}}</div> 
                                </div>
                            </div>
                          
                        </div>
                        <div class="absolute right-0 top-0 mr-5 mt-3 dropdown">
                            <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false"> <i data-feather="more-horizontal" class="w-5 h-5 text-gray-600 dark:text-gray-300"></i> </a>
                            <div class="dropdown-menu w-40">
                                <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                                    <a href="{{route('edit/customer', $item->id)}}" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="edit-2" class="w-4 h-4 mr-2"></i> Edit </a>
                                    
                                    <form action="{{route('delete/customer', $item->id)}}" method="POST" class="px-2">
                                        @csrf
                                        @method('DELETE')
                                        <a type="submit" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete </a>
                                        {{-- <button type="submit" class="btn btn-danger btn_delete text-base"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button> --}}
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div style="margin-top: 100px">
                            @if($item->status_active === 'inactive')
                            <input id="show-example-5" data-target="#social-media-button" onclick="changeStatus('{{ $item->id }}', '{{ $item->status_active }}')" 
                            id="status_active" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                @else
                                <input id="show-example-5" data-target="#social-media-button" onclick="changeStatus('{{ $item->id }}', '{{ $item->status_active }}')"  
                                id="status_active" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox" checked>
                            @endif
                        </div>
                      
                    </div>
                </div>
            </div>
            @endforeach

            <!-- END: Customers Layout -->
        </div>
    </div>
    <!-- END: Content -->
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();    
        });

        function changeStatus(id, status) {
            if(status === 'inactive') {
                status = 'active';
            }else{
                status = 'inactive';
            }
            $.ajax({
                url: "{{ route('users.changeStatus') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id,
                    "status": status,
                },
                success: function(data) {
                    if(data.status === 'success'){
                        toastr.success(data.message);
                        setTimeout(function(){
                            window.location.reload();
                        }, 1000);
                    }else{
                        toastr.error(data.message);
                    }
                }
            });
    }
    </script>
    
@endsection