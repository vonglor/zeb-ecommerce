@if (count($product) > 0)
    @php
    $i = 1;
    @endphp
    @foreach ($product as $row)
    <tr>
        <td scope="row">{{ $i }}</td>
        <td>
            <a data-lightbox="{{$row->Upload->file_name}}" data-title="{{$row->Upload->file_name}}" href="{{URL::to($row->Upload->file_name)}}" target="_blank">
            <img src="{{ url($row->Upload->file_name) }}" alt="Snow" style="max-width: 50px; max-height: 50px">
            </a>
        </td>
        <td>
            <?php
            $array = explode( "\n", wordwrap( $row->name, 30));
            echo implode("<br>",$array);
            ?>
        </td>
        <td>
            @php
                $pro_stock = App\Models\Product_stock::where('product_id', $row->id)->get();
                $qty = 0;
                foreach($pro_stock as $pt){
                    $qty += $pt->qty;
                }
                echo $qty;
            @endphp
        </td>
        <td>{{ $row->num_of_sale }}</td>
        <td>{{ format_product_price($row->unit_price, $row->currency_code) }}</td>
        <td class="text-right">
            {{ format_product_price($row->num_of_sale * $row->unit_price, $row->currency_code) }}
        </td>
    </tr>
    @php
        $i++;
    @endphp
    @endforeach
@else
    <tr>
        <td colspan="7" class="text-center">Data not found...</td>
    </tr>
@endif
                        