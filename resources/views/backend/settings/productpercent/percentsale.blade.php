@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
    <div class="content">
    <div class="intro-y flex items-center mt-8">
        <h1 class="text-xl font-medium mr-auto ">
            set up percet to product for sale
        </h1>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 overflow-auto">
            <div class="float-right">
                <a type="submit" class="btn btn-primary text-lg" data-toggle="modal" data-target="#modal-add">Add New</a>
            </div>

        <!-- start insert -->
        <div class="intro-y box mt-5">
            <div id="header-footer-modal">
                <div class="preview">
                    <div id="modal-add" class="modal" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h2 class="font-medium text-lg mr-auto text-xl">
                                    Add percet to product for buy
                                    </h2>
                                    <button type="button" data-dismiss="modal" class="col-span-6 sm:col-span-3 lg:col-span-2 xl:col-span-1"><i data-feather="x" class="block mx-auto"></i> </button>
                                    
                                </div>
                                <form action="{{route('add/percentsale')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                                        <div class="col-span-12 sm:col-span-6">
                                            <label for="modal-form-1" class="form-label text-base">Start Amount</label>
                                            <input id="modal-form-1" type="text" name="start_amount" class="form-control border-gray-500 text-base" placeholder="amount" required>
                                        </div>
                                        <div class="col-span-12 sm:col-span-6">
                                            <label for="modal-form-2" class="form-label text-base">End Amount</label>
                                            <input id="modal-form-2" type="text" name="end_amount" class="form-control border-gray-500 text-base" placeholder="amount" required>
                                        </div>
                                        <div class="col-span-12 sm:col-span-6">
                                            <label for="modal-form-3" class="form-label text-base">Type Payment</label>
                                            <select class="form-select mt-2 sm:mr-2 border-gray-500 text-base" name="name">
                                                <option value="balance_sale">balance sale</option>
                                                <option value="percent_sale">percent sale</option>
                                            </select>
                                        </div>
                                        <div class="col-span-12 sm:col-span-6">
                                            <label for="modal-form-4" class="form-label text-base">Percent</label>
                                            <input id="modal-form-4" type="text" name="percent" class="form-control border-gray-500 text-base" placeholder="%" required>
                                        </div>
                                    </div>
                                    <div class="modal-footer text-right">
                                        <button type="button" data-dismiss="modal" class="btn btn-danger w-20 mr-1 text-base">Cancel</button>
                                        <button type="submit" class="btn btn-primary w-20 text-base">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end insert -->
        
            <table class="table table-report -mt-2" id="dataTable">
                <thead>
                    <tr>
                        <th class="whitespace-nowrap text-base">#</th>
                        <th class="whitespace-nowrap text-base">Start Amount</th>
                        <th class="text-base">End Amount</th>
                        <th class="whitespace-nowrap text-base">Type Payment</th>
                        <th class="text-base">Percent</th>
                        <th class="text-center whitespace-nowrap text-base">ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 0;
                    @endphp
                    @foreach ($percentsale as $item)
                    <tr class="intro-x">
                        <td class="w-10 text-base">{{++$i}}</td>
                        <td class="w-40 text-base">{{$item->start_amount}}</td>
                        <td class="w-40 text-base">{{$item->end_amount}}</td>
                        <td class="w-40 text-base">{{$item->name}}</td>
                        <td class="w-40 text-base">{{$item->percent}} %</td>
                        <td class="table-report__action w-56 p-5 text-base">
                            <div class="flex justify-center items-center">
                                <a href="{{route('edit/percentsale', $item->id)}}" class="btn btn-primary"><i data-feather="check-square" class="w-4 h-3 mr-1"></i></a>
                                {{-- <a class="btn btn-primary" href="" data-toggle="modal" data-target="#modal-edit"> <i data-feather="check-square" class="w-4 h-3 mr-1"></i></a> --}}
                                <form action="{{route('delete/percentproduct', $item->id)}}" method="POST" class="px-2">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>
<!-- END: Content -->
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();    
    });
</script>
    
    
@endsection