<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Staff;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd('ggggg');
        return view('backend.employee.create');
    }
    public function allEmployee()
    {
        // dd('ggggg');
        $employees = Staff::paginate(10);
        return view('backend.employee.index',compact('employees'));
    }
    public function permission()
    {
        return view('backend.employee.permission');
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('backend.employee.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate(
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'phone' => 'required',
                'address'=>'required',
                'email' => 'required|unique:staff',
                'password' => 'required'

            ],
            [
                'email.unique' => 'This email alread exist',
            ]
        );

        
        $employee = Staff::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'address' => $request->address,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
       
        if ($employee) {
            $notification = array(
                'message' => 'Add employee success!',
                'alert-type' => 'success'
            );
            return redirect()->route('employee')->with($notification);
        } else {
            $notification = array(
                'message' => 'Add employee success!',
                'alert-type' => 'warning'
            );
            return redirect()->route('employee')->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Staff::find($id);
        return view('backend.employee.edit', compact('employee'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $phone = $request->input('phone');
        $address = $request->input('address');
        $email = $request->input('email');
        $password = $request->input('password');

        DB::update('update staff set first_name = ?,last_name = ?,phone = ?, address = ?, email = ?,password = ? where id = ?', [$first_name, $last_name, $phone, $address, $email, $password, $id]);

        $notification = array(
            'message' => 'Updated employee success!',
            'alert-type' => 'success'
        );
        // return redirect()->back()->with($notification);
        return redirect()->route('employee')->with($notification);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Staff::find($id);
        $employee->delete();

        if ($employee) {
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('employee')->with($notification);
        } else {
            $notification = array(
                'message' => 'Delete warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('employee')->with($notification);
        }
    }
}
