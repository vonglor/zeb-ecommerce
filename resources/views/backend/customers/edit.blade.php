@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">
                Edit Buy Information
            </h2>
        </div>
        <div class="grid grid-cols-6 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
                <!-- BEGIN: Vertical Form -->
                <form action="{{route('update/customer', $customer->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                <div class="intro-y box">
                    <div id="vertical-form" class="p-5">
                        <div class="preview">
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">First Name</label>
                                <input id="vertical-form-1" type="text" name="name" class="form-control border-gray-500 text-base" value="{{$customer->name}}" placeholder=" First Name" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Last Lastname</label>
                                <input id="vertical-form-1" type="text" name="surname" class="form-control border-gray-500 text-base" value="{{$customer->surname}}" placeholder=" Last Name" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Phone</label>
                                <input id="vertical-form-1" type="number" name="phone" class="form-control border-gray-500 text-base" value="{{$customer->phone}}" placeholder="+85620" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Email</label>
                                <input id="vertical-form-1" type="email" name="email" class="form-control border-gray-500 text-base" value="{{$customer->email}}" placeholder="Email" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Country</label>
                                <input id="vertical-form-1" type="text" name="country" class="form-control border-gray-500 text-base" value="{{$customer->country}}" placeholder="Country" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">City</label>
                                <input id="vertical-form-1" type="text" name="city" class="form-control border-gray-500 text-base" value="{{$customer->city}}" placeholder="City" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Postal Code</label>
                                <input id="vertical-form-1" type="text" name="postal_code" class="form-control border-gray-500 text-base" value="{{$customer->postal_code}}" placeholder="Postal Code" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Bank Name</label>
                                <input id="vertical-form-1" type="text" name="bank_name" class="form-control border-gray-500 text-base" value="{{App\Models\Customer::where('user_id', $customer->id)->first()->bank_name;}}" placeholder="Bank Name" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Bank Account</label>
                                <input id="vertical-form-1" type="text" name="bank_acc_no" class="form-control border-gray-500 text-base" value="{{App\Models\Customer::where('user_id', $customer->id)->first()->bank_acc_no;}}" placeholder="bank account" required>
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">ID Card</label>
                                <input id="vertical-form-1" type="text" name="card_id" class="form-control border-gray-500 text-base" value="{{App\Models\Customer::where('user_id', $customer->id)->first()->card_id;}}" placeholder="ID Card" required>
                            </div>
                            <div>
                                <input type="hidden" name="customerId" value="{{App\Models\Customer::where('user_id', $customer->id)->first()->id;}}">
                            </div>
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">Image ID Card</label>
                                <img src="{{url(App\Models\Customer::where('user_id', $customer->id)->first()->card_img)}}" class="w-40 h-30" alt="">
                            </div>
                            <div class="mt-10">
                                <button type="submit" class="btn btn-primary w-32 mr-2 mb-2 text-base"> <i data-feather="activity" class="w-4 h-4 mr-2"></i> Edit</button> 
                            </div>

                        </div>
                    </div>
                </div>
                </form>
            </div> 
        </div>
    </div>
<!-- END: Content -->
    
@endsection