@extends('frontend.layouts.app')
@section('css')
<link href="{{ asset('frontend/css/index.css') }}" rel="stylesheet">
<link href="{{ asset('frontend/css/product-list.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style type="text/css">

    * {
      box-sizing: border-box;
    }

    .slider {
		margin-top: 40px;
        width: 100%;
		height: 40%;
        background-color: #00ffea;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
		margin: 30px 0;
      	width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }


    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }
    
    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }
    
  </style>
@endsection
	@section('content')
	<section id="center" class="center_home clearfix">
		<div class="container">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-top: 20px;">
				<!-- Indicators -->
				<ol class="carousel-indicators">
				  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				  <li data-target="#carousel-example-generic" data-slide-to="1"></li>
				  <li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>
			  
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
				  <div class="item active">
					<img src="{{ url('frontend/img/slide/4.jpg') }}" alt="...">
					<div class="carousel-caption">
						<a href="">name</a>
					</div>
				  </div>
				  <div class="item">
					<img src="{{ url('frontend/img/slide/4.jpg') }}" alt="...">
					<div class="carousel-caption">
					  
					</div>
				  </div>
				  <div class="item">
					<img src="{{ url('frontend/img/slide/4.jpg') }}" alt="...">
					<div class="carousel-caption">
					  
					</div>
				  </div>
				  
				</div>
			  
				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				  <span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				  <span class="sr-only">Next</span>
				</a>
			  </div>
		</div>
	</section>

	<section id="product_list">
		<div class="container" style="background: #fff; padding: 15px 15px 30px 15px; border: 1px solid #f3f3f3;">
			<div class="row">
				<div class="product_list text-center clearfix">
					<div class="col-sm-12">
						<h3 class="mgt">Product Items</h3>
						<hr>
					</div>
				</div>
				<div class="gallery_1 clearfix">
					<div class="col-sm-12">
						<div class="workout_page_1_left clearfix">

							<div class="tab-content clearfix">
								<div id="home" class="tab-pane fade  clearfix active in">
									<div class="click clearfix">
										<div class="home_inner clearfix">
											@foreach ($products as $product)
											<div class="col-sm-3 offset-md-0 offset-sm-1 pt-md-0 pt-4">
												<div class="card"> <a href="{{ route('product_detail', $product->id) }}"><img class="card-img-top img-responsive" src="{{ url($product->Upload->file_name) }}"></a>
													<div class="card-body">
														<h5 class="font-weight-bold pt-1"><a href="{{ route('product_detail', $product->id) }}">{{ Str::limit($product->name, 27) }}</a></h5>
														<div class="d-flex align-items-center justify-content-between pt-3">
															<div class="d-flex flex-column">
																<div class="h5 font-weight-bold">

																	@if ($product->discount)
																		<span class="col_2">{{ convert_price($product->unit_price, $product->currency_code) }}</span>
																	@endif
																	{{ convert_price($product->unit_price - $product->discount, $product->currency_code) }}

																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endforeach 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="product_1_last text-center clearfix">
					<div class="col-sm-12">
						<ul>
							{!! $products->links() !!}
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	{{-- <section id="shipping">
		<div class="container">
			<div class="row">
				<div class="shipping_1 clearfix">
					<div class="col-sm-3">
						<div class="shipping_1i clearfix">
							<span><i class="fa fa-rocket"></i></span>
							<h5 class="mgt">FREE SHIPING <br> <span class="col_2">Orders over $100</span></h5>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="shipping_1i clearfix">
							<span><i class="fa fa-recycle"></i></span>
							<h5 class="mgt">FREE RETURN <br> <span class="col_2">Within 30 days returns</span></h5>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="shipping_1i clearfix">
							<span><i class="fa fa-lock"></i></span>
							<h5 class="mgt">SUCURE PAYMENT <br> <span class="col_2">100% secure payment</span></h5>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="shipping_1i clearfix">
							<span><i class="fa fa-tags"></i></span>
							<h5 class="mgt">BEST PEICE <br> <span class="col_2">Guaranteed price</span></h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> --}}
	@endsection

	@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script>
		 $(document).on('ready', function() {
			$('.slider').slick({
				dots: true,
				slidesToShow: 4,
				slidesToScroll: 1,
				autoplay: true,
  				autoplaySpeed: 2000,
				responsive: [
					{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
						infinite: true,
						dots: true
					}
					},
					{
					breakpoint: 600,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
					},
					{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
					}
				]
			});
		});
	</script>
	@endsection
<script>
	$(document).ready(function () {
		$('#add').click(function (){
			alert('gon');
		});

		$('.carousel').carousel({
			interval: 2000
		});
	});
</script>
	