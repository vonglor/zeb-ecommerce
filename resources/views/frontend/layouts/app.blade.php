@php
$user = Session::get('user');
@endphp

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Shop On</title>
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/global.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/DateTimePicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/font-awesome.min.css') }}" />
    @yield('css')
    <link href="{{ asset('css/tagify.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amaranth&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alata&display=swap" rel="stylesheet">
    {{-- select css --}}
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <!-- style css -->
    <script src="{{ asset('frontend/js/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    {{-- toastr css --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" type="text/javascript"></script>
</head>

<body>
    <section id="top">
        <div class="container">
            <div class="row">
                <div class="top_1 clearfix">
                    <div class="col-sm-2">
                        <div class="top_1l text-left clearfix">
                            <ul class="nav navbar-nav">
                                <li class="dropdown" id="currency-change">
                                    @php
                                        if(Session::has('currency_code')){
                                            $currency_code = Session::get('currency_code');
                                        }
                                        else{
                                            $currency_code = \App\Models\Currency::findOrFail(\App\Models\BusinessSetting::where('type', 'system_default_currency')->first()->value)->code;
                                        }
                                    @endphp
                                    <a class="m_tag" href="#" data-toggle="dropdown" role="button"
                                        aria-expanded="false">{{ \App\Models\Currency::where('code', $currency_code)->first()->name }} {{ (\App\Models\Currency::where('code', $currency_code)->first()->symbol) }}<span class="caret"></span></a>
                                    <ul class="dropdown-menu drop_3" role="menu">
                                        @foreach (App\Models\Currency::all() as $cur)
                                        <li class=" @if($currency_code == $cur->code) active1 @endif" ><a href="javascript:void(0)" data-currency="{{ $cur->code }}">{{ $cur->name }} ({{ $cur->symbol }})</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="top_1r text-right clearfix">
                            <ul class="mgt">
                                @if ($user)
                                <li>
                                    <a href="{{ route('account') }}"><i class="fa fa-map-marker"></i>Seller location</a>
                                </li>
                                <li>
                                    <a href="{{ route('account') }}"><i class="fa fa-user"></i>My panel</a>
                                </li>
                                <li class="border_none">
                                    <a href="{{ route('user.logout') }}"><i class="fa fa-power-off"></i>Logout</a>
                                </li>
                                @else
                                <li><a href=""><i class="fa fa-user"></i>Sign Up</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ route('registration', 'sale') }}">Sale</a></li>
                                        <li><a href="{{ route('registration', 'buy') }}">Buy</a></li>
                                    </ul>
                                </li>
                                <li class="border_none">
                                    <a href="{{ route('sign_in') }}"><i class="fa fa-sign-in"></i>Login</a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="cd-secondary-nav">
        <section id="header">
            <div class="container">
                <div class="row">
                    <div class="header_1 clearfix">
                        <div class="col-sm-2">
                            <div class="header_1l clearfix">
                                {{-- <h3><a href="{{ route('index') }}">Hmoobonline</a></h3> --}}
                                <h4><a href="{{ route('index') }}"><img src="{{ url('uploads/logo/logo1.jpeg') }}" alt="" class="img-responsive" style="height: 20px;"></a></h4>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <form action="{{ route('product_search') }}" method="GET">
                                @csrf
                                <div class="header_1m text-center clearfix">
                                    <select class="form-control form_1" name="category_id">
                                        <option value="">All Category</option>
                                        @foreach (App\Models\Category::all() as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group">
                                        <input type="text" name="value_search" class="form-control form_2"
                                            placeholder="Search Products Here..." required>
                                        <span class="input-group-btn">
                                            <button class="btn_search" type="submit" >
                                                <i class="fa fa-search"></i> Search</button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-3">
                            <div class="header_1r clearfix">
                                <ul class="nav navbar-nav mgt navbar-right">
                                    @if ($user)
                                    <li><a class="tag_m1" href="#" id="print"><i class="fa fa-file-image-o" aria-hidden="true"></i> Print Size</a></li>
                                        @if ($user->user_type == 'sale')
                                            <li><a class="tag_m1" href="#" id="percent"><i class="fa fa-percent" aria-hidden="true"></i></a></li>
                                        @endif
                                    @endif
                                    
                                    @if (Session::has('cart'))
                                        <li class="dropdown">
                                            <a class="tag_m1" href="{{ route('cart') }}"
                                                aria-expanded="false"><i class="fa fa-shopping-bag"></i><span class="shopping-cart">{{ count(Session::get('cart')) }}</span> Cart</a>
                                        </li>
                                    @else
                                        <li class="dropdown">
                                            <a class="tag_m1" href="{{ route('cart') }}"
                                                aria-expanded="false"><i class="fa fa-shopping-bag"></i><span class="shopping-cart">0</span> Cart</a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="menu" class="clearfix">
            <nav class="navbar nav_t">
                <div class="container">
                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.html"> Shop On </a>
                    </div>
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a class="m_tag" href="#" data-toggle="dropdown" role="button"
                                    aria-expanded="false"><i class="fa fa-bars"></i> CATEGORIES</a>
                                <ul class="dropdown-menu drop_2" role="menu">
                                    @foreach (App\Models\Category::all() as $item)
                                    <li><a href="{{ route('product.category', $item->id) }}">{{ $item->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a class="m_tag active_tab" href="{{ route('index') }}">Home</a></li>

                            <li><a class="m_tag" href="{{ route('shopping') }}">Shop</a></li>
                            <li class="dropdown">
                                <a class="m_tag" href="#" data-toggle="dropdown" role="button"
                                    aria-expanded="false">Brand<span class="caret"></span></a>
                                <ul class="dropdown-menu drop_3" role="menu">
                                    @foreach (App\Models\Brand::all() as $item)
                                    <li><a href="{{ route('product.brand', $item->id) }}">{{ $item->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a class="m_tag" href="">Contact</a></li>
                        </ul>

                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>

        </section>
    </div>
    {{-- modal --}}
    <div class="modal fade" id="printModal" tabindex="-1" role="dialog" style="z-index: 10000;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Print size</h4>
                </div>
                    <div class="modal-body">
                        <img src="{{ url('frontend/img/print_size/2.png') }}" alt="" style="max-width: 600px; max-height: 600px;">
                    </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  
  <!-- Modal -->
  <div class="modal fade" id="percentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Percents</h4>
        </div>
        <div class="modal-body">
          <table class="table table-bordered">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>Price</th>
                      <th>Percent</th>
                      <th>Type</th>
                  </tr>
              </thead>
              <tbody>
                  @php
                      $i = 1;
                  @endphp
                  @foreach (App\Models\Percent::where('status', '1')->get() as $item)
                  <tr>
                      <td>{{ $i }}</td>
                      <td>{{ $item->start_amount. ' - '. $item->end_amount }}</td>
                      <td>{{ $item->percent }}</td>
                      <td>
                          @if ($item->name == 'percent_sale')
                              Percent sale
                          @elseif ($item->name == 'balance_buy')
                              Balance buy
                          @endif
                      </td>
                  </tr>
                  @php
                      $i++;
                  @endphp
                  @endforeach
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

    @yield('content') 

    @include('frontend.layouts.footer')
