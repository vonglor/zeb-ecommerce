<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Currency;
use App\Models\Exchangerate;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currency = Currency::latest()->paginate(10);
        return view('backend.settings.currency.index',compact('currency'))
        ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function exchangerate()
    {
        $exchangerate = Exchangerate::latest()->paginate(10);
        return view('backend.settings.exchangerate.index',compact('exchangerate'))
        ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function insertexchangerate(Request $request)
    {

       
        $request->validate([
            // 'from' => 'required|unique:exchangerate,from',
            // 'to' => 'required|unique:exchangerate,to'
        ],
        // [
        //     'from.unique' => 'The currency is Already taken'
        // ],
        // [
        //     'to.unique' => 'The currency is Already taken'
        // ]
        );
        
        $exchangerate = $request->all();

        // dd('ggggg',$exchangerate);

        Exchangerate::create($exchangerate);

        if ($exchangerate) {
            $notification = array(
                'message' => 'Insert successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('exchangerate')->with($notification);
        } else {
            $notification = array(
                'message' => 'Insert warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('exchangerate')->with($notification);
        }
    }
    public function editexchangerate($id)
    {
        // dd('edittttt');
        $exchangerate = Exchangerate::find($id);
        return view('backend.settings.exchangerate.edit',compact('exchangerate'));
    }
    public function updateExchange(Request $request, Exchangerate $exchangerate)
    {
        dd('update exchange');
        $request->validate([
            'from' => 'required',
            'to' => 'required'

        ]);

        $input = $request->all();

        $exchangerate->update($input);

        if ($exchangerate) {
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('exchangerate')->with($notification);
        } else {
            $notification = array(
                'message' => 'Update warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('exchangerate')->with($notification);
        }
    }
    public function delete($id)
    {
       
        $exchangerate = Exchangerate::find($id);
        $exchangerate->delete();

        if ($exchangerate) {
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('exchangerate')->with($notification);
        } else {
            $notification = array(
                'message' => 'Delete warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('exchangerate')->with($notification);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('backend.settings.currency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       
        $request->validate([
            'name' => 'required|unique:currencies,name',
            'code' => 'required|unique:currencies,code'
        ],
        [
            'name.unique' => 'The Name is Already taken'
        ],
        [
            'code.unique' => 'The Code is Already taken'
        ]
        );
        
        $currency = $request->all();

        // dd('ggggg',$currency);

        Currency::create($currency);

        if ($currency) {
            $notification = array(
                'message' => 'Insert successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('get.currency')->with($notification);
        } else {
            $notification = array(
                'message' => 'Insert warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('get.currency')->with($notification);
        }
    }

    public function submitDefault(Request $request)
    {
        // We are collecting all data submitting via Ajax
        $input = $request->all();
      
        /*
          $post = new Post;
          $post->name = $input['name'];
          $post->description = $input['description'];
          $post->status = $input['status'];
          $post->save();
        */
     	
        // Sending json response to client
        return response()->json([
            "status" => true,
            "data" => $input
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd('edittttt');
        $currency = Currency::find($id);
        return view('backend.settings.currency.edit',compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Currency $currency)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required'

        ]);

        $input = $request->all();

        $currency->update($input);

        if ($currency) {
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('get.currency')->with($notification);
        } else {
            $notification = array(
                'message' => 'Update warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('get.currency')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency = Currency::find($id);
        $currency->delete();

        if ($currency) {
            $notification = array(
                'message' => 'Delete successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('get.currency')->with($notification);
        } else {
            $notification = array(
                'message' => 'Delete warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('get.currency')->with($notification);
        }
    }

    public function changeCurrency(Request $request)
    {
        $request->session()->put('currency_code', $request->currency_code);
        $currency = Currency::where('code', $request->currency_code)->first();
    	// flash(translate('Currency changed to ').$currency->name)->success();
    }
}
