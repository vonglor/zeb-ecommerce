<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Wallet;
use Str;

class MywalletController extends Controller
{
     public function wallet()
    {
        $user = session()->get('user');
        $wallet=Wallet::where('user_id', $user->id)->first();
        return view('frontend.saller.mywallet.index', compact('wallet'));
    }
}
