<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Order Id: </h4>
</div>
<div class="modal-body">

    <div class="row">
        <div class="col-sm-3 delivery">
            <span class="@if ($order->delivery_status == 'pending') active @else done @endif"><i class="fa fa-file"></i> Order completed</span> 
        </div>
        <div class="col-sm-3 delivery">
            <span class="@if ($order->delivery_status == 'confirmed') active @elseif($order->delivery_status == 'on_delivery' || $order->delivery_status == 'delivered') done  @endif"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Confirmed</span>
        </div>
        <div class="col-sm-3 delivery">
            <span class="@if ($order->delivery_status == 'on_delivery') active @elseif($order->delivery_status == 'delivered') done @endif"><i class="fa fa-truck" aria-hidden="true"></i> On delivery</span>
        </div>
        <div class="col-sm-3 delivery">
            <span class="@if ($order->delivery_status == 'delivered') done @endif"><i class="fa fa-check" aria-hidden="true"></i> Delivered</span>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                        @foreach ($order_details as $item)
                        <div class="col-md-6" style="margin-top: 10px;">
                            <div class="col-md-6">
                                <p>{{ App\Models\Product::find($item->productStock->product_id)->name }}</p>
                                @php
                                    $stock = App\Models\Product_stock::find($item->product_id);
                                @endphp
                                @if ($stock != null)
                                    @if ($stock->image != null)
                                        <img src="{{ url(App\Models\Upload::find($stock->image)->file_name) }}" alt=""
                                            class="img-fluid mb-2" style="max-height: 250px; max-width: 180px; margin-top: 10px;">
                                    @else
                                        <img src="{{ url(App\Models\Upload::find(App\Models\Product::find($stock->product_id)->photos)->file_name) }}"
                                            class="img-fluid mb-2" alt="" style="max-height: 250px; max-width: 180px; margin-top: 10px;">
                                    @endif
                                @else
                                    <img src="{{ url(App\Models\Upload::find(App\Models\Product::find($stock->product_id)->photos)->file_name) }}"
                                        class="img-fluid mb-2" alt="" style="max-height: 250px; max-width: 180px; margin-top: 10px;">
                                @endif
                            </div>
                            <div class="col-md-6 detail">
                                <p>{{ date('M d Y', $order->date) }} <span
                                        style="border: 1px solid #000; font-size:14px; padding: 0 5px; background: rgb(114, 175, 245)">{{ $order->payment_type }}</span>
                                </p>
                                <p>Order number: <span style="font-size: 13px;">{{ $order->code }}</span></p>
                                <p>Condition: New</p>
                                @if ($item['color'])
                                    <div class="produt_qty">
                                        <p>Color:
                                        <div class="color"
                                            style="background: {{ App\Models\Color::where('name', $item->color)->first()->code }}">
                                        </div>
                                        </p>
                                    </div>
                                    <p>Quantity: {{ $item['qty'] }} <span
                                            style="float: right; margin-right: 30px;">SK:
                                            {{ $item['product_id'] }}</span></p>
                                @endif
                                @if ($item['size'])
                                    <p>Size: {{ $item['size'] }}</p>
                                @endif
                                @if ($item['fabric'])
                                    <p>Fabric: {{ $item['fabric'] }}</p>
                                @endif
    
                                <p>price: ${{ number_format($item['price'], 2) }} </p>
                                <p>Shipping: free</p>
                                <p>Delivery: <span>{{ $item->delivery }}</span></p>
                            </div>
                        </div>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#update_delivery_status').on('change', function(){
        var order_id = {{ $order->id }};
        var status = $('#update_delivery_status').val();
        $.post('{{ route('orders.update_delivery_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status}, function(data){
            $('#order_detail_modal').modal('hide');
            //console.log(data);
            location.reload().setTimeOut(500);
        });
    });

    $('#update_payment_status').change(function(){
        var order_id = {{ $order->id }};
        var status = $('#update_payment_status').val();
        $.post('{{ route('orders.update_payment_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status}, function(data){
            $('#order_detail_modal').modal('hide');
            //console.log(data);
            location.reload().setTimeOut(500);
        });
    });
</script>