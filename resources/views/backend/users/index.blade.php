@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">
               {{__('User Lists')}}
            </h2>
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
              <div class="hidden md:block mx-auto text-gray-600"></div>
              <a class="btn btn-dark btn-sm" href="{{ route('users.create') }}"> {{__('Add User')}}</a>
            </div>
        </div>
        
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 overflow-auto lg:overflow">
                <table class="table table-report -mt-2 display" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base">{{__('No.')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Photo')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Full Name')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Gender')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Phone')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Email')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Status Active')}}</th>
                            <th class="whitespace-nowrap text-base text-center">{{__('Actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                            @php
                            $i = 1;
                        @endphp
                        @if(!empty($staff))
                          @foreach ($staff as $row)
                        <tr class="intro-x">
                            <td class="text-base">
                                {{ $i++ }}.
                            </td>
                            <td class="text-center text-base">
                                @if(!empty($row->user->photo))
                                <img src="{{ asset('uploads/user_img/'.$row->user->photo) }}" alt="photo" class="rounded-full" width="50" height="50">
                                @else
                                <img src="{{ asset('uploads/user_img/default.png') }}" alt="photo" class="rounded-full" width="50" height="50">
                                @endif
                            </td>
                            <td class="text-base">{{ $row->user->name .' '. $row->user->surname }}</td>
                            <td class="text-center text-base">{{ $row->gender }}</td>
                            <td class="text-base">{{ $row->user->phone }}</td>
                            <td class="text-base">{{ $row->user->email }}</td>
                            <td class="text-center text-base">
                              <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                  @if($row->user->status_active === 'inactive')
                                <input id="show-example-5" data-target="#social-media-button" onclick="changeStatus('{{ $row->user->id }}', '{{ $row->user->status_active }}')" 
                                id="status_active" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                                    @else
                                    <input id="show-example-5" data-target="#social-media-button" onclick="changeStatus('{{ $row->user->id }}', '{{ $row->user->status_active }}')"  
                                    id="status_active" class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox" checked>
                                @endif
                            </div>
                            </td>
                            <td class="text-center text-base">
                                <div class="dropdown"> 
                                    <a class="dropdown-toggle" aria-expanded="false"><i data-feather="more-vertical" class="w-4 h-4 mr-2"></i></a> 
                                    <div class="dropdown-menu w-48"> 
                                        <div class="dropdown-menu__content box dark:bg-dark-1 p-2"> 
                                                <a href="{{ route('users.edit', $row->id) }}" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> 
                                                    <i data-feather="edit-2" class="w-4 h-4 mr-2"></i> {{__('Edit')}} 
                                                </a>

                                                <a href="{{ route('users.editPwd', ['user' => $row->user->id, 'param' => 'editPwd']) }}" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> 
                                                    <i data-feather="edit-2" class="w-4 h-4 mr-2"></i> {{__('Reset Password')}} 
                                                </a>
                                                
                                                <form action="{{ route('users.destroy', $row->user->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE') 
                                                    <button type="submit" class="btn_delete flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> 
                                                        <i data-feather="trash" class="w-4 h-4 mr-2"></i> {{__('Delete')}} 
                                                    </button>
                                                </form>      
                                        </div> 
                                    </div> 
                                </div> 
                            </td>
                        </tr>
                            @endforeach
                            @endif
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
        </div>
    </div>
    <!-- END: Content -->

    <script>
      $(document).ready(function() {
          $('#dataTable').DataTable({
            "lengthMenu": [
                [50, 75, 100],
                [50, 75, 100]
            ],
          });    
      });

      function changeStatus(id, status) {
            if(status === 'inactive') {
                status = 'active';
            }else{
                status = 'inactive';
            }
            $.ajax({
                url: "{{ route('users.changeStatus') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id,
                    "status": status,
                },
                success: function(data) {
                    if(data.status === 'success'){
                        toastr.success(data.message);
                        setTimeout(function(){
                            window.location.reload();
                        }, 1000);
                    }else{
                        toastr.error(data.message);
                    }
                }
            });
    }
  </script>

@endsection