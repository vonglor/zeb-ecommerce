@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
 
    <div class="content">
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                <h1 class="text-xl mr-2 text-lg">{{ __('Request withdrawal')}}</h1>
                <div class="px-2">
                    <a href="{{ route('withdraw-request', ['status' => 'pending']) }}" class="btn btn-outline-warning btn-sm">Pending</a>
                </div>
                <div class="px-2">
                    <a href="{{ route('withdraw-request', ['status' => 'completed']) }}" class="btn btn-outline-success btn-sm">Completed</a>
                </div>
                <div class="hidden md:block mx-auto text-gray-600"></div>   
            </div>
            
            <div class="intro-y col-span-12 overflow-auto">
                <table class="table table-report -mt-2 display" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base">{{__('No.')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Date Time')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Customer Name')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('Bank Name')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('AC/ No')}}</th>
                            <th class="whitespace-nowrap text-base">{{__('AC/ Name')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Amount')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Status')}}</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @php 
                            $i = 1;
                        @endphp

                        @if(!empty($w))
                            @foreach ($w as $row)
                        <tr class="">
                            <td class="w-10">
                                {{ $i++}}. 
                            </td>
                            <td>
                                <h1 class="font-medium whitespace-nowrap text-base">{{ date("d-m-Y H:i:s", strtotime($row->updated_at)) }}</h1>
                            </td>
                            <td class="w-40">
                                {{ '('.$row->id.') '.$row->name.' '.$row->surname }}
                            </td>
                            <td class="w-40">
                                {{ $row->bank_name}}
                            </td>
                            <td class="w-40">
                                {{$row->bank_acc_no }}
                            </td>
                            <td class="w-40">
                                {{$row->bank_acc_name}}
                            </td>
                            <td class="text-center text-base">
                                {{ '$'. number_format($row->amount, 2)}}
                            </td>
                            <td class="text-center">
                                <span class="text-theme-10 block">{{ $row->status }}</span> </td>
                        </tr> 
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
        </div>
        
    </div>


    <!-- END: Content -->

    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                "lengthMenu": [
                [50, 75, 100, 150, 200, 1000],
                [50, 75, 100, 150, 200, 1000]
            ],
            });
        });
    </script>
    
@endsection

