@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">
               {{__('Order Seller Details')}}
            </h2> <a href="{{ route('order.fetchOrderBySeller', ['status' => 'all']) }}">{{__('Back')}}</a>
        </div>
        <div class="intro-y box px-5 pt-5 mt-5">
          <div class="flex flex-col lg:flex-row border-b border-gray-200 dark:border-dark-5 pb-5 -mx-5">
              <div class="flex flex-1 px-5 items-center justify-center lg:justify-start">
                  <div class="w-20 h-20 sm:w-24 sm:h-24 flex-none lg:w-32 lg:h-32 image-fit relative">
                    @php
                    $photo = App\Models\Upload::find($order->user->photo); 
                  @endphp
                  @if($photo)
                      <img alt="photo" class="rounded-full" src="{{ url($photo->file_name) }}">
                      @else
                      <img alt="photo" class="rounded-full" src="{{ url('uploads/user_img/default.png') }}">
                      @endif
                  </div>

                  <div class="ml-5">
                      <div class="w-24 sm:w-40 truncate sm:whitespace-normal font-medium text-lg">
                        {{ $order->user->name .' '. $order->user->surname }}
                      </div>
                      <div class="text-gray-600">{{ $order->user->gender }}</div>
                  </div>
              </div>
              <div class="mt-6 lg:mt-0 flex-1 dark:text-gray-300 px-5 border-l border-r border-gray-200 dark:border-dark-5 border-t lg:border-t-0 pt-5 lg:pt-0">
                  <div class="font-medium text-center lg:text-left lg:mt-3">{{__('Contact')}}</div>
                    <div class="flex flex-col justify-center items-center lg:items-start mt-4">
                    <div class="truncate sm:whitespace-normal flex items-center"> <i data-feather="mail" class="w-4 h-4 mr-2"></i> {{ $order->user->email }} </div>
                    <div class="truncate sm:whitespace-normal flex items-center mt-3"> <i data-feather="phone" class="w-4 h-4 mr-2"></i> {{ $order->user->phone }} </div>
                  </div>
                  <div class="font-medium text-center lg:text-left lg:mt-3">{{__('Address')}}</div>
                    <div class="flex flex-col justify-center items-center lg:items-start mt-4">
                      <div class="truncate sm:whitespace-normal flex items-center">{{__('City:')}} {{ $order->user->city }} </div>
                      <div class="truncate sm:whitespace-normal flex items-center mt-3">{{__('Country:')}} {{ $order->user->country }} </div>
                  </div>
              </div>
              <div class="mt-6 lg:mt-0 flex-1 dark:text-gray-300 px-5 border-l border-r border-gray-200 dark:border-dark-5 border-t lg:border-t-0 pt-5 lg:pt-0">
                <div class="font-medium text-center lg:text-left lg:mt-3">{{__('Delivery Status')}}</div>
                <div class="flex flex-col justify-center items-center lg:items-start mt-4">
                  <form action="{{ route('order.change_status', $order->id )}}" method="post">
                    @csrf
                    @method('PATCH')
                <div class="w-24 sm:w-60 truncate sm:whitespace-normal font-medium">
                  {{-- <select class="form-select mt-4 sm:mr-2" name="delivery_status" id="delivery_status" style="width: 100%">
                    <option class="text-base" {{ old("delivery_status", $order->delivery_status) === "pending" ? "selected": ""}} 
                      value="pending">{{__('Pending')}}</option>
                    <option class="text-base" {{ old("delivery_status", $order->delivery_status) === "confirmed" ? "selected": ""}} 
                      value="confirmed">{{__('Confirmed')}}</option>
                    <option class="text-base" {{ old("delivery_status", $order->delivery_status) === "completed" ? "selected": ""}} 
                      value="completed">{{__('Completed')}}</option>
                  </select>
                  @if ($errors->has('delivery_status'))
                    <span class="invalid-feedback" role="alert" style="display: block; color:red; font-size:12px">
                      <strong>{{ $errors->first('delivery_status') }}</strong>
                    </span>
                  @endif --}}
                </div>
                <div class="truncate sm:whitespace-normal flex items-center mt-3">
                  @if($order->delivery_status === 'pending')
                  <button type="submit" class="btn btn-warning" disabled style="width: 100%">{{__('Pending')}}</button>
                  @elseif($order->delivery_status === 'confirmed')
                  <button type="submit" class="btn btn-primary" disabled style="width: 100%">{{__('Confirmed')}}</button>
                  @elseif($order->delivery_status === 'completed')
                  <button type="submit" class="btn btn-success" disabled style="width: 100%">{{__('Completed')}}</button>
                  @endif
                </div>
              </form>
              </div>
            </div>
          </div>
          <div class="nav nav-tabs flex-col sm:flex-row justify-center lg:justify-start" role="tablist"> 
            <a id="dashboard-tab" data-toggle="tab" data-target="#dashboard" href="javascript:;" class="py-4 sm:mr-8 active" role="tab" aria-controls="dashboard" aria-selected="true">{{__('Customer Information')}}</a> 
          </div>
      </div>
        
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 overflow-auto lg:overflow">
              <table class="table table-report -mt-2">
                <thead>
                  <tr>
                    <th scope="col" style="text-align:center; width:40px; vertical-align:middle;">{{__('No')}}.</th>
                    <th scope="col" colspan="3">{{__('Description')}}</th>
                    <th scope="col">{{__('Qty')}}</th>
                    <th scope="col">{{__('Unit Price')}}</th>
                    <th scope="col">{{__('Subtotal')}}</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                    $i = 1;
                  @endphp
                  @if(!empty($orders))
                  @foreach ($orders as $row)
                  @php
                    $img = App\Models\Upload::find($row->product->photos); 
                  @endphp
                  <tr>
                    <td>{{ $i++ }}.</td>
                    <td colspan="2">
                      @if($img)
                      <a data-lightbox="{{$img->file_name}}" data-title="{{$img->file_name}}" href="{{URL::to($img->file_name)}}" target="_blank">
                        <img src="{{ url($img->file_name) }}" alt="" style="max-width: 50px; max-height: 50px">
                      </a>
                      @endif
                    </td>
                    <td>{{ $row->product->name }}</td>
                    <td>{{ $row->qty }}</td>
                    <td>{{ '$'. number_format($row->price, 2)}}</td>
                    <td>{{ '$'. number_format($row->price, 2)}}</td>
                  </tr>
                  @endforeach
                  @endif
                  <tr>
                    <td colspan="6" style="text-align:right">{{__('Total Amount')}}:</td>
                    <td>{{ '$'. number_format($order->grand_total, 2)}}</td>
                  </tr>
                  <tr>
                    <td colspan="6" style="text-align:right">{{__('Tax')}}:</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td colspan="6" style="text-align:right">{{__('Shipping')}}:</td>
                    <td>0</td>
                  </tr>
                  <tr>
                    <td colspan="6" style="text-align:right"><b>Total: </b></td>
                    <td><b>{{ '$'. number_format($order->grand_total, 2)}}</b></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- END: Data List -->
        </div>   
      
    </div>
    <!-- END: Content -->

    <script>
      $(document).ready(function() {
          $('#dataTable').DataTable();    
      });
  </script>

@endsection