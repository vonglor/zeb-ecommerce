<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\User;
// use App\Models\Whitdraw_request;
use Illuminate\Http\Request;

// use App\Models\Membership;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = User::where('user_type', 'buy')->get();
        return view('backend.customers.index', compact('customer'));
    }

    public function payout()
    {
        return view('backend.customers.payout');
    }
    public function payoutRequest()
    {
        $customerwithdraw = Whitdraw_request::all();
        return view('backend.customers.payoutrequest', compact('customerwithdraw'));
    }

    public function membership()
    {
        $membership = Membership::paginate(10);
        return view('backend.customers.memberfee', compact('membership'));
    }
    public function editMembership($id)
    {
        $membership = Membership::find($id);
        return view('backend.customers.editmember', compact('membership'));
    }
    public function updateMembership(Request $request, Membership $membership)
    {
        dd('update');
        $request->validate([
            'name' => 'required'

        ]);

        $input = $request->all();
 
        $color->update($input);

        if ($input) {
            $notification = array(
                'message' => 'Update successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('color.index')->with($notification);
        } else {
            $notification = array(
                'message' => 'Update warning!!.',
                'alert-type' => 'warning'
            );
            return redirect()->route('color.index')->with($notification);
        }
    }

    public function genkeyreffer()
    {
        return view('backend.customers.genkeyreffer');
    }

    public function UpdateStatusUser($id)
    {

        $userUpdate = User::find($id);

        if ($userUpdate->status == '0') {
            $userUpdate->status            = '1';
            $userUpdate->refer_code_others = strtotime('now');
        } else {
            $userUpdate->status = '0';
        }
        $userUpdate->save();

        return redirect()->back();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $membership = $request->all();

        Membership::create($membership);

        if ($membership) {
            $notification = array(
                'message'    => 'Insert successfully.',
                'alert-type' => 'success',
            );
            return redirect()->route('customer/memberfee')->with($notification);
        } else {
            $notification = array(
                'message'    => 'Insert warning!!.',
                'alert-type' => 'warning',
            );
            return redirect()->route('customer/memberfee')->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = User::find($id);
        return view('backend.customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->surname = $request->input('surname');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->postal_code = $request->input('postal_code');

        $user->save();
        $customer = Customer::find($request->customerId);
        $customer->bank_name = $request->input('bank_name');
        $customer->bank_acc_no = $request->input('bank_acc_no');
        $customer->save();

        if ($customer->save()) {
            $notification = array(
                'message'    => 'Update successfully.',
                'alert-type' => 'success',
            );
            return redirect()->route('customer')->with($notification);
        } else {
            $notification = array(
                'message'    => 'Update warning!!.',
                'alert-type' => 'warning',
            );
            return redirect()->route('customer')->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = User::find($id);
        $customer->delete();

        if ($customer) {
            $notification = array(
                'message'    => 'Delete successfully.',
                'alert-type' => 'success',
            );
            return redirect()->route('customer')->with($notification);
        } else {
            $notification = array(
                'message'    => 'Delete warning!!.',
                'alert-type' => 'warning',
            );
            return redirect()->route('customer')->with($notification);
        }
    }
}
