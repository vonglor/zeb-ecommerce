<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\WithdrawRequest;
use DB;
use Illuminate\Http\Request;

class WithdrawRequestController extends Controller
{

    public function index($status = null)
    {
        if ($status !== 'pending' && $status !== 'completed') {
            toastr()->error(__("Page not found!"), __("Errors"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $w = DB::table('withdraw_requests as w')
            ->select('w.id',
                'w.amount',
                'w.payment_method',
                'w.payment_details',
                'w.status',
                'w.created_at',
                'w.updated_at',

                'c.bank_name',
                'c.bank_acc_name',
                'c.bank_acc_no',

                'u.name',
                'u.surname',
                'u.user_type',

                'm.name as membership',
                't.amount as user_tax'
            )
            ->join('users as u', 'u.id', '=', 'w.user_id')
            ->join('customers as c', 'c.user_id', '=', 'u.id')
            ->join('memberships as m', 'm.id', '=', 'w.membership_id')
            ->join('user_taxes as t', 't.user_id', '=', 'u.id')
            ->where('w.status', $status)
            ->where('u.user_type', 'buy')
            ->orderBy('id', 'desc')
            ->get();

        if ($status === 'pending') {
            return view('backend.withdraw_requests.index', compact('w'));
        }
        return view('backend.withdraw_requests.completed', compact('w'));
    }

    public function pay(Request $request)
    {
        $isId = isset($request->withdraw_req_id) ? sizeof($request->withdraw_req_id) : 0;
        if ($isId == 0) {
            toastr()->error(__("Item is required"), __("Errors"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        $request->validate([
            "withdraw_req_id"   => "required|array|min:1",
            "withdraw_req_id.*" => "required|integer",
            "withdraw_buy"      => ['required', 'string', 'in:sale,buy'],
        ]);

        DB::beginTransaction();

        try {

            if (isset($request->withdraw_req_id)) {
                $id = $request->withdraw_req_id;
                for ($i = 0; $i < count($id); $i++) {
                    $w = WithdrawRequest::find($id[$i]);
                    if (!$w) {
                        toastr()->error(__("404 Not Found"), __("Errors"), ['timeOut' => 2000]);
                        return redirect()->back();
                    }

                    if ($w->status === 'completed') {
                        toastr()->error(__("Status Completed"), __("Errors"), ['timeOut' => 2000]);
                        return redirect()->back();
                    }

                    $w->amount     = $w->amount - ($w->membership->name + $w->user_tax->amount);
                    $w->updated_at = date('d-m-Y H:i:s');
                    $w->status     = 'completed';
                    $w->save();
                }
            }

            DB::commit();

            if ($request->withdraw_buy === 'buy') {
                toastr()->success('Updated', 'Successfully', ['timeOut' => 2000]);
                return redirect(route('withdraw-request', ['status' => 'completed']));
            }
            toastr()->success('Updated', 'Successfully', ['timeOut' => 2000]);
            return redirect(route('seller-withdraw-request', ['status' => 'completed']));

        } catch (\Exception $e) {
            DB::rollback();
            toastr()->error('Something went wrong', 'Errors', ['timeOut' => 2000]);
            return redirect()->back();
        }

        return $request->all();
    }

    public function edit(WithdrawRequest $w)
    {
        if (!$w) {
            toastr()->error(__("404 Not Found"), __("Errors"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        return view('backend.withdraw_requests.edit', compact('w'));
    }

    public function update(Request $request, WithdrawRequest $w)
    {
        $request->validate([
            "amount" => "required|numeric",
        ]);

        $w->amount     = $request->amount;
        $w->updated_at = date('d-m-Y H:i:s');
        if ($w->save()) {
            toastr()->success('Updated', 'Successfully', ['timeOut' => 2000]);
            return redirect(route('withdraw-request', ['status' => 'pending']));
        } else {
            toastr()->error('Something went wrong', 'Errors', ['timeOut' => 2000]);
            return redirect()->back();
        }
    }

    public function edit2(Request $request)
    {
        $w = WithdrawRequest::find($request->id);
        if (!$w) {
            return response()->json(['status' => 'errors', 'message' => 'Withdraw request not found']);
        }

        return response()->json(['status' => 'success', 'data' => $w], 200);
    }

    public function update2(Request $request)
    {

        return $request->all();

        $error = Validator::make($request->all(), [
            "id"     => "required|integer",
            "amount" => "required|numeric",
        ]);

        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $w = WithdrawRequest::find($request->id);
        if (!$w) {
            return response()->json(['status' => 'errors', 'message' => 'Withdraw request not found']);
        }

        $w->amount     = $request->amount;
        $w->updated_at = date('d-m-Y H:i:s');
        if ($w->save()) {
            return response()->json(['status' => 'success', 'message' => 'Withdraw request updated successfully'], 200);
        } else {
            return response()->json(['status' => 'errors', 'message' => 'Something went wrong'], 500);
        }
    }

    public function destroy(WithdrawRequest $w)
    {
        if (!$w) {
            toastr()->error(__("404 Not Found"), __("Error"), ['timeOut' => 2000]);
            return redirect()->back();
        }

        WithdrawRequest::destroy($w->id);

        toastr()->success(__("Deleted"), __("Success"), ['timeOut' => 2000]);
        return redirect(route('withdraw-request', ['status' => 'pending']));
    }

    public function seller($status = null)
    {
        $w = DB::table('withdraw_requests as w')
            ->select('w.id',
                'w.amount',
                'w.payment_method',
                'w.payment_details',
                'w.status',
                'w.created_at',
                'w.updated_at',

                's.bank_name',
                's.bank_acc_name',
                's.bank_acc_no',

                'u.name',
                'u.surname',
                'u.user_type',

                'm.name as membership',
                't.amount as user_tax'
            )
            ->join('users as u', 'u.id', '=', 'w.user_id')
            ->join('sellers as s', 's.user_id', '=', 'u.id')
            ->join('memberships as m', 'm.id', '=', 'w.membership_id')
            ->join('user_taxes as t', 't.user_id', '=', 'u.id')
            ->where('w.status', $status)
            ->where('u.user_type', 'sale')
            ->orderBy('id', 'desc')
            ->get();

        if ($status === 'pending') {
            return view('backend.withdraw_requests.seller', compact('w'));
        }
        return view('backend.withdraw_requests.seller_completed', compact('w'));
    }

    public function moneywithdraw()
    {
        $money = WithdrawRequest::where('user_id', user()->id)->get();
        return view('frontend.saller.moneywithdraw.index', \compact('money'));
    }
}