@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y box mt-5">
          <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
              <h2 class="text-lg font-medium text-base mr-auto">
                <i data-feather="edit"></i> {{__('Edit Role')}}
              </h2>
              <div class="form-check w-full sm:w-auto sm:ml-auto mt-3 sm:mt-0">
                <a href="{{ route('roles') }}">{{__('Back')}}</a>
              </div>
          </div>
          <div id="select-options" class="p-5">
            <form action="{{ route('roles.update', $role->id) }}" autocomplete="off" enctype="multipart/form-data" method="POST">
              @csrf
              @method('PUT')
              <div class="preview">
                  <div class="intro-y col-span-12 overflow-auto lg:overflow">
                    <div> 
                      <label for="name" class="form-label">{{__('Name')}} <span class="txt-red">*</span></label> 
                      <input name="name" value="{{ old('name', $role->name) }}" type="text" class="form-control">
                      @if ($errors->has('name'))
                          <span class="invalid-feedback" role="alert" style="display: block; color:red">
                            <strong>{{ $errors->first('name') }}</strong>
                          </span>
                          @endif 
                    </div>
                    
                    <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                      <h2 class="font-medium text-base mr-auto text-base">
                          {{__('Permissions')}}
                      </h2>
                    </div>
                    
                    @php
                      $permissions = json_decode($role->permissions);
                    @endphp

                    <table class="table table-report -mt-2">
                      <thead>
                        <tr>
                            <th rowspan="2">{{__('Module Name')}}</th>
                        </tr>
                        <tr>
                            <th class="text-center whitespace-nowrap text-base">{{__('View')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Create')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Edit')}}</th>
                            <th class="text-center whitespace-nowrap text-base">{{__('Delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="font-medium">{{__('Product')}}</td>
                            <td class="text-center">
                              <input id="permissions" name="permissions[]" value="view-products"
                              @php if(in_array('view-products', $permissions)) echo "checked"; @endphp
                              class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                            </td>
                            <td class="text-center">
                              <input id="permissions" name="permissions[]" value="create-products"
                              @php if(in_array('create-products', $permissions)) echo "checked"; @endphp
                              class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                            </td>
                            <td class="text-center">
                              <input id="permissions" name="permissions[]" value="update-products"
                              @php if(in_array('update-products', $permissions)) echo "checked"; @endphp
                              class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                            </td>
                            <td class="text-center">
                              <input id="permissions" name="permissions[]" value="delete-products"
                              @php if(in_array('delete-products', $permissions)) echo "checked"; @endphp
                              class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                            </td>
                          </tr>

                          <tr>
                            <td class="font-medium">{{__('Sale')}}</td>
                            <td class="text-center">
                              <input id="permissions" name="permissions[]" value="view-sales"
                              @php if(in_array('view-sales', $permissions)) echo "checked"; @endphp
                              class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                            </td>
                            <td class="text-center">
                              <input id="permissions" name="permissions[]" value="create-sales"
                              @php if(in_array('create-sales', $permissions)) echo "checked"; @endphp
                              class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                            </td>
                            <td class="text-center">
                              <input id="permissions" name="permissions[]" value="update-sales"
                              @php if(in_array('update-sales', $permissions)) echo "checked"; @endphp
                              class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                            </td>
                            <td class="text-center">
                              <input id="permissions" name="permissions[]" value="delete-sales"
                              @php if(in_array('delete-sales', $permissions)) echo "checked"; @endphp
                              class="show-code form-check-switch mr-0 ml-3 border-gray-500" type="checkbox">
                            </td>
                          </tr>
                        </tbody>
                    </table>

                    <button type="submit" id="btnSubmit" class="btn btn-primary mt-5" style="width:100%;">{{__('Save')}}</button>
                    <a href="{{ route('roles') }}" class="btn btn-secondary" style="width:100%">{{__('Cancel')}}</a> 
                  </div>
              </div> 
            </form>
          </div>
      </div>
    </div>
    <!-- END: Content -->

    <script>
      $(document).ready(function() {
        // $('#permissions').click(function() {
        //   if ($(this).is(':checked')) {
        //     $('#btnSubmit').show();
        //   } else {
        //     $('#btnSubmit').hide();
        //   }
        // });
      });
  </script>

@endsection