@extends('frontend.saller.content_left')
@section('css')
<link href="{{ asset('frontend/css/mycss.css') }}" rel="stylesheet">
@endsection
@section('content_right')
<div class="col-sm-9">
    <div class="centent_card_1r clearfix">
        <div class="centent_card_1r3 tab-content">
            <p></p>
            <div class="row">
                <div class="addproduct col-sm-4">
                    <a href="{{ route('product_recive.create') }}" class="btn btn-success">+ Import products</a>
                </div>
                    {!! Form::open(['method'=>'get', 'route'=>'saller.product.search']) !!}
                        <div class="col-sm-6">
                            
                            <div class="input-group">
                                {!! Form::text('search', '', ['class'=>'form-control', 'placeholder'=>'Search product name...']) !!}
                                <span class="input-group-btn">
                                    {!! Form::submit('Search', ['class'=>'btn btn-search']) !!}
                                </span>
                            </div><!-- /input-group -->
                        </div>
                    {!! Form::close() !!}
            </div>
            
            <hr>
            <div class="card-content">
                <div class="table-responsive">
                    <table class="table table-hover">
                      <tr>
                          <th>#</th>
                          <th>Image</th>
                          <th width="">Name</th>
                          <th>ID</th>
                          <th>variant</th>
                          <th>Date</th>
                          <th>Recive</th>
                          <th>Purchase</th>
                          <th>All purchase</th>
                          <th class="text-center">Action</th>
                      </tr>
                      <tbody>
                          @php
                              $i = 1;
                          @endphp
                          @foreach ($pro_recive as $row)
                          <tr>
                            <td>{{ $i }}</td>
                            <td> 
                                <a data-lightbox="{{ App\Models\Upload::find($row->product->photos)->file_name }}" data-title="{{ App\Models\Upload::find($row->product->photos)->file_name }}" href="{{URL::to(App\Models\Upload::find($row->product->photos)->file_name)}}" target="_blank">
                                <img src="{{ url(App\Models\Upload::find($row->product->photos)->file_name) }}" alt="Snow" style="max-width: 50px; max-height: 50px">
                                </a>
                            </td>
                            <td>{{ $row->product->name }}</td>
                            <td>{{ $row->product->product_id }}</td>   
                            <td>{{ $row->stock->variant }}</td>
                            <td>{{ $row->date }}</td>
                            <td>{{ $row->qty }}</td>
                            <td>{{ format_product_price($row->price, $row->currency_code) }}</td>
                            <td>{{ format_product_price($row->qty * $row->price, $row->currency_code) }}</td>
                            <td class="text-center">
                                <a href="{{ route('product_recive.edit', $row->id) }}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
                            </td>
                          </tr>
                          @php
                              $i++;
                          @endphp
                          @endforeach
                          
                      </tbody>
                    </table>
                  </div>
            </div>
        </div>
        <div class="center_product_1r4 clearfix">
            <div class="col-sm-6">
                <div class="center_product_1r4l clearfix">
                   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    
@endsection