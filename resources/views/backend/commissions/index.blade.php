
@extends('backend.layouts.sidebar')
@section('content')
 <!-- BEGIN: Content -->
        <div class="content">
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-xl font-medium mr-auto text-xl">
                {{ __('Set Up Commssion') }}
            </h2>
        </div>
        <div class="grid grid-cols-6 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-6">
                <form action="{{ route('commission.update', $c->id) }}" method="post">
                    @csrf
                    @method('PATCH')
                <div class="intro-y box">
                    <div id="vertical-form" class="p-5">
                        <div class="preview">
                            <div>
                                <label for="vertical-form-1" class="form-label text-base">{{ __('Commission')}}</label>
                                <input id="vertical-form-1" value="{{ old('amount', $c->amount) }}" type="text" name="amount" class="form-control border-2 border-gray-500 text-base">
                                @if ($errors->has('amount'))
                                <span class="invalid-feedback" role="alert" style="display: block; color:red">
                                    <strong>{{ $errors->first('amount') }}</strong>
                                </span>
                                @endif
                            </div>
                                <div class="mt-3">
                                    <button type="submit" class="btn btn-primary mt-5 text-base">{{ __('Save')}}</button>
                                </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            </div>
        </div>
    </div>
<!-- END: Content -->
    
@endsection