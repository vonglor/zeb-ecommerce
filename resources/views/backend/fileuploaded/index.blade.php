@extends('backend.layouts.sidebar')
@section('content')
<!-- BEGIN: Content -->
    <div class="content">
        <div class="grid grid-cols-6 gap-6 mt-8">
            <div class="col-span-12 lg:col-span-3 xxl:col-span-2">
                <h2 class="intro-y text-xl font-medium mr-auto mt-2">
                    Files and Videos Uploaded
                </h2>
            </div>
            <div class="col-span-12 lg:col-span-9 xxl:col-span-10">
                <div class="intro-y grid grid-cols-12 gap-3 sm:gap-6 mt-5">
                    @php
                    $i = 0;
                    @endphp
                    @foreach ($fileUpload as $item)
                    <div class="intro-y col-span-6 sm:col-span-4 md:col-span-3 xxl:col-span-2">
                       
                        <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                            <a href="" class="w-4/5 file__icon file__icon--image mx-auto">
                                <div class="file__icon--image__preview image-fit">
                                    <img alt="IMAGE" src="{{url($item->file_name)}}">
                                </div>
                                {{-- <a href="" class="block font-medium mt-4 text-center truncate">{{$item->extension}}</a>  --}}
                                <div class="text-gray-600 text-lg text-center mt-0.5">{{$item->file_size}}</div>
                                <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                                    <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false"> <i data-feather="more-vertical" class="w-5 h-5 text-gray-600"></i> </a>
                                    <div class="dropdown-menu w-40">
                                        <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                                            <form action="{{route('delete/file', $item->id)}}" method="POST" class="px-2">
                                                @csrf
                                                @method('DELETE')
                                                {{-- <button type="submit" class="btn btn-danger btn_delete"> <i data-feather="trash-2" class="w-4 h-3 mr-1"></i></button> --}}
                                                <button type="submit" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="trash" class="w-5 h-5 mr-2"></i> Delete </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-gray-600 text-xl text-center">
                                    {{$item->created_at}}
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
                {!! $fileUpload->links() !!}
                <!-- END: Directory & Files -->
                <!-- BEGIN: Pagination -->
                <div class="intro-y flex flex-wrap sm:flex-row sm:flex-nowrap items-center mt-6">
                    <ul class="pagination">
                        <li>
                            <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-left"></i> </a>
                        </li>
                        <li>
                            <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-left"></i> </a>
                        </li>
                        <li> <a class="pagination__link pagination__link--active" href="">1</a> </li>
                        <li> <a class="pagination__link" href="">2</a> </li>
                        <li> <a class="pagination__link" href="">3</a> </li>
                        <li> <a class="pagination__link" href="">...</a> </li>
                        <li> <a class="pagination__link" href="">10</a> </li>
                        <li> <a class="pagination__link" href="">11</a> </li>
                        <li>
                            <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevron-right"></i> </a>
                        </li>
                        <li>
                            <a class="pagination__link" href=""> <i class="w-4 h-4" data-feather="chevrons-right"></i> </a>
                        </li>
                    </ul>
                </div>
                <!-- END: Pagination -->
            </div>
        </div>
    </div>
    <!-- END: Content -->
    
@endsection