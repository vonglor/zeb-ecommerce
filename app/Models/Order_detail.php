<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_id',
        'seller_id',
        'product_id',
        'variantion',
        'color',
        'size',
        'fabric',
        'price',
        'tax',
        'shipping_cost',
        'qty',
        'shipping_type',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function productStock()
    {
        return $this->belongsTo(Product_stock::class, 'product_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

}