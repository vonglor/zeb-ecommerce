

<select class="form-control selectpicker my-select" name="user_id" data-live-search="true" required>
    <option data-tokens="" value="">Customer ID</option>
    @foreach (App\Models\User::all() as $item)
    <option data-tokens="" value="{{ $item->id }}">{{ $item->id }}</option>
    @endforeach
</select>
  