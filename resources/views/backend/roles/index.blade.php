@extends('backend.layouts.sidebar')
@section('content')
 
 <!-- BEGIN: Content -->
    <div class="content">
        <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">
               {{__('Role Lists')}}
            </h2>
            <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
              <div class="hidden md:block mx-auto text-gray-600"></div>
              <a class="btn btn-dark btn-sm" href="{{ route('roles.create') }}"> {{__('Add Role')}}</a>
            </div>
        </div>
        
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 overflow-auto lg:overflow">
                <table class="table table-report -mt-2 display" id="dataTable">
                    <thead>
                        <tr>
                            <th class="whitespace-nowrap text-base" width="10%">{{__('No.')}}</th>
                            <th class="whitespace-nowrap text-base" width="70%">{{__('Name')}}</th>
                            <th class="whitespace-nowrap text-base text-center" width="20%">{{__('Actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                            @php
                            $i = 1;
                        @endphp
                        @if(!empty($roles))
                          @foreach ($roles as $row)
                        <tr class="intro-x">
                            <td class="text-base">
                                {{ $i++ }}.
                            </td>
                            <td class="text-base">{{ $row->name}}</td>
                            <td class="text-center text-base">
                                <div class="dropdown"> 
                                    <a class="dropdown-toggle" aria-expanded="false"><i data-feather="more-vertical" class="w-4 h-4 mr-2"></i></a> 
                                    <div class="dropdown-menu w-48"> 
                                        <div class="dropdown-menu__content box dark:bg-dark-1 p-2"> 
                                                <a href="{{ route('roles.edit', $row->id) }}" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> 
                                                    <i data-feather="edit-2" class="w-4 h-4 mr-2"></i> {{__('Edit')}} 
                                                </a>
                                                
                                                <form action="{{ route('roles.destroy', $row->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE') 
                                                    <button type="submit" class="btn_delete flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> 
                                                        <i data-feather="trash" class="w-4 h-4 mr-2"></i> {{__('Delete')}} 
                                                    </button>
                                                </form>      
                                        </div> 
                                    </div> 
                                </div> 
                            </td>
                        </tr>
                            @endforeach
                            @endif
                    </tbody>
                </table>
            </div>
            <!-- END: Data List -->
        </div>
    </div>
    <!-- END: Content -->

    <script>
      $(document).ready(function() {
          $('#dataTable').DataTable();    
      });
  </script>

@endsection